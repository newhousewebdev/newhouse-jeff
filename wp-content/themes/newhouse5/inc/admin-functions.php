<?php
/**
 * Admin Functions to improve backend
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}//Deny direct file access.

/* Filters And Actions */

// remove comments from back end.
add_action( 'init', 'remove_comments_from_post_type' );
add_action( 'admin_menu', 'remove_comments_from_menu' );
add_action( 'admin_init', 'remove_comment_metabox' );

/* Remove Comments Funcs */

/**
 * Remove comments from backend
 */
function remove_comments_from_post_type() {
	remove_post_type_support( 'post', 'comments' );
	remove_post_type_support( 'page', 'comments' );
}

/**
 * Remove Comments from Menu
 */
function remove_comments_from_menu() {
	remove_menu_page( 'edit-comments.php' );
}

/**
 * Remove Comments from Metabox
 */
function remove_comment_metabox() {
	remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
}
