<?php
/**
 * Partial File for theme performance optimizations
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}//Deny direct file access.


/*
	Caching
	https://wp-rocket.me/blog/object-caching-use-wordpress/

*/
/* Actions and Filters */

/*
	Hooks and Actions sequence
	muplugins_loaded
	registered_taxonomy
	registered_post_type
	plugins_loaded
	sanitize_comment_cookies
	setup_theme
	load_textdomain
	after_setup_theme
	auth_cookie_malformed
	auth_cookie_valid
	set_current_user
	init
	widgets_init
	register_sidebar
	wp_register_sidebar_widget
	wp_default_scripts
	wp_default_stypes
	admin_bar_init
	add_admin_bar_menus
	wp_loaded
	parse_request
	send_headers
	parse_query
	pre_get_posts
	posts_selection
	wp
	template_redirect
	get_header
	wp_head
	wp_enqueue_scripts
	wp_print_styles
	wp_print_scripts
	... a lot more

*/

/* Remove Jquery Migrate */
add_filter( 'wp_default_scripts', 'remove_jquery_migrate' );
/* Remove wp-embed */
add_action( 'wp_footer', 'deregister_unnecessary_scripts' );
// remove emoji stuff.
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

/**
 * Remove old version of Jquery.
 *
 * @param object $scripts core scripts.
 */
function remove_jquery_migrate( &$scripts ) {
	if ( ! is_admin() ) {
		$scripts->remove( 'jquery' );
		$scripts->add( 'jquery', false, array( 'jquery-core' ), '1.10.2' );
	}
}

/**
 * Remove old version of Jquery.
 */
function deregister_unnecessary_scripts() {
	wp_deregister_script( 'wp-embed' );
}
