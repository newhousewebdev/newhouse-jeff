<?php
/**
 * Theme Utility Functions
 *
 * Functions contained here ar for development purposes only. It is safe to remove the include in the main functions file
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}//Deny direct file access.

/**
 * Function to use var_dump on the server
 *
 * @param object $thingy to be output to the server error log.
 */
function my_server_debugger( $thingy = null ) {
	ob_start();
	var_dump( $thingy ); // phpcs:ignore
	$thingy_contents = ob_get_contents();
	ob_end_clean();
	error_log( $thingy_contents );// phpcs:ignore
}

/**
 * Function to var_dump within pre tags for front-end debugging.
 *
 * @param object $thingy to be output on the page.
 */
function my_page_debugger( $thingy ) {
	?>
	<pre>
		<?php var_dump( $thingy ); ?>
	</pre>
	<?php
}

//phpcs:disable
/*
// add_action( 'admin_footer', 'show_db_inquires_in_footer' );
function show_db_inquires_in_footer() {
	if ( current_user_can( 'administrator' ) ) {
		global $wpdb;
		echo '<pre>';
		print_r( $wpdb->queries );
		echo '</pre>';
	}
}
*/
//phpcs:enable
