<?php
/**
 * Registers the theme Menus Functions
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
} //Deny direct file access.

/* Register Menus */
register_nav_menus(
	array(
		'primary'              => 'Primary menu navigation',
		'footer-nav'           => 'Footer menu navigation',
		'footer-utility'       => 'Footer utility navigation',
		'mobile'               => 'Mobile menu navigation',
		'header-utility-left'  => 'Header left utility menu',
		'header-utility-right' => 'Header right utility menu',
		'social'               => 'Social media links',
	)
);
