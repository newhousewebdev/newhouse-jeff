<?php
/**
 * Event related Functions
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}//Deny direct file access.

// Gets the most recent events.
add_action( 'wp_ajax_get_newhouse_events', 'get_recent_newhouse_events' );
// ! If we don't include a nonce, which we should, this won't run without below
add_action( 'wp_ajax_nopriv_get_newhouse_events', 'get_recent_newhouse_events' );
// Gets a single event.
add_action( 'wp_ajax_get_newhouse_event', 'get_newhouse_event' );
// ! If we don't include a nonce, which we should, this won't run without below
add_action( 'wp_ajax_nopriv_get_newhouse_event', 'get_newhouse_event' );
// Gets the events based on the url provided in the post.
add_action( 'wp_ajax_get_more_newhouse_events', 'get_more_newhouse_events' );
// ! If we don't include a nonce, which we should, this won't run without below
add_action( 'wp_ajax_nopriv_get_more_newhouse_events', 'get_more_newhouse_events' );

/*
	! need to combine security methods
	! makes keys & url global
	Error proof as well
*/

/**
 * Get Recent Events from Events Calendar
 */
function get_recent_newhouse_events() {
	$authentication_key = 'su-events-key faa8f0230f55c30bc7fa443d49b71c8a';
	$events_url         = 'https://calendar.syracuse.edu/wp-json/su-events/v1/events';

	// Check if the Nonce has been submitted.
	if ( ! isset( $_POST['events_nonce'] ) || ! wp_verify_nonce( sanitize_key( $_POST['events_nonce'] ), 'events_nonce' ) ) { // phpcs:ignore
		return;
	}

	$filtered_args = array_filter(
		$_POST,
		function( $key ) {
			return 'action' !== $key && 'events_nonce' !== $key;
		},
		ARRAY_FILTER_USE_KEY
	);

	foreach ( $filtered_args as $key => $value ) {
		$union      = strpos( $events_url, '?' ) ? '&' : '?';
		$events_url = "$events_url$union$key=$value";
	}
	/* Args go here */
	$header_args = array(
		'headers' => array(
			'Authorization' => $authentication_key,
		),
	);

	$response      = wp_remote_get( $events_url, $header_args );
	$response_body = wp_remote_retrieve_body( $response );

	echo wp_json_encode( $response_body );
	wp_die();
}

/**
 * Get Recent Events from Events Calendar
 */
function get_newhouse_event() {
	$authentication_key = 'su-events-key faa8f0230f55c30bc7fa443d49b71c8a';
	$events_url         = 'https://calendar.syracuse.edu/wp-json/su-events/v1/events';

	// Check if the Nonce has been submitted.
	if ( ! isset( $_POST['events_nonce'] ) || ! wp_verify_nonce( sanitize_key( $_POST['events_nonce'] ), 'events_nonce' ) ) { // phpcs:ignore
		return;
	}

	$slug = isset( $_POST['slug'] ) ? sanitize_text_field( $_POST['slug'] ) : null;

	if ( ! $slug ) {
		return;
	}

	$events_url = "$events_url/$slug";

	/* Args go here */
	$header_args = array(
		'headers' => array(
			'Authorization' => $authentication_key,
		),
	);

	$response      = wp_remote_get( $events_url, $header_args );
	$response_body = wp_remote_retrieve_body( $response );

	echo wp_json_encode( $response_body );
	wp_die();
}

/**
 * Get Events with a specific Url
 */
function get_more_newhouse_events() {
	$authentication_key = 'su-events-key faa8f0230f55c30bc7fa443d49b71c8a';
	// $events_url         = 'https://calendar.syracuse.edu/wp-json/su-events/v1/events';

	/*
		! Verify that the url is the su-events
	*/
	// Check if the Nonce has been submitted.
	if ( ! isset( $_POST['events_nonce'] ) || ! wp_verify_nonce( sanitize_key( $_POST['events_nonce'] ), 'events_nonce' ) ) { // phpcs:ignore
		return;
	}

	/* Args go here */
	$header_args = array(
		'headers' => array(
			'Authorization' => $authentication_key,
		),
	);

	// (TS) will add error checks
	$response      = wp_remote_get( $_POST['url'], $header_args );
	$response_body = wp_remote_retrieve_body( $response );
	echo wp_json_encode( $response_body );
	wp_die();
}
