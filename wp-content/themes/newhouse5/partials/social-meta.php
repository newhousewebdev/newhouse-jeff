<?php
/**
 * Template part for social meta data
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}//Deny direct file access.
?>

<?php
$img_src = get_stylesheet_directory_uri() . '/src/sass/img/newhouse-school-2020.svg';
if ( is_singular() ) {
	if ( has_post_thumbnail() ) {
		$img_id  = get_post_thumbnail_id( $post->ID );
		$img_src = wp_get_attachment_image_src( $img_id, 'large' );
		$img_src = $img_src[0];
	}

	$excerpt = $post->post_excerpt;

	if ( $excerpt ) {
		$excerpt = strip_tags( $post->post_excerpt );
	} else {
		$excerpt = get_bloginfo( 'description' );
	}
	?>
	<meta property="og:title" content="<?php echo get_the_title(); ?> ">
	<meta property="og:description" content="<?php echo esc_html( $excerpt ); ?> ">
	<meta property="og:image" content="<?php echo esc_url( $img_src ); ?> "> 
	<meta property="og:url" content="<?php the_permalink(); ?>">
	<meta name="twitter:card" content=" <?php echo esc_url( $img_src ); ?> "> 
	<meta property="og:site_name" content="<?php echo esc_html( get_bloginfo() ); ?>">
<?php
} elseif ( is_home() ) {
	// Use the blog page but fallback to 'home' if not set.
	// $home_id     = get_option( 'page_for_posts' ) ? get_option( 'page_for_posts' ) : get_page_id_by_slug( 'home' );
	$name        = get_bloginfo( 'name' );
	$description = get_bloginfo( 'description' );

	// if ( has_post_thumbnail( $home_id ) ) {
	// 	$img_id  = get_post_thumbnail_id( $home_id );
	// 	$img_src = wp_get_attachment_image_src( $img_id, 'large' );
	// 	$img_src = $img_src[0];
	// }
	?>
	<meta property="og:title" content="<?php echo esc_html( $name ); ?> ">
	<meta property="og:description" content="<?php echo esc_html( $description ); ?> ">
	<meta property="og:image" content="<?php echo esc_url( $img_src ); ?> "> 
	<meta property="og:url" content="<?php bloginfo( 'url' ); ?>">
	<meta name="twitter:card" content=" <?php echo esc_url( $img_src ); ?> "> 
	<meta property="og:site_name" content="<?php echo esc_html( $name ); ?>">
	<meta name="twitter:site" content="@NewhouseSU">
	<meta name="twitter:creator" content="@NewhouseSU">
<?php
}
?>
