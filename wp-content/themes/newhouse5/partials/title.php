<?php
/**
 * Template part for page title
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}//Deny direct file access.
?>
<title>
<?php
$site_title = get_bloginfo( 'name' ) . ' | ' . get_bloginfo( 'description' );
if ( is_home() || is_front_page() ) {
	echo esc_html( $site_title );
} else {
	wp_title();
}
?>
</title>
