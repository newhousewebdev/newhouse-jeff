const webpack = require('webpack');
const dotenv = require('dotenv');
const fs = require('fs'); // to check if the file exists
const path = require('path'); // to get the current path

module.exports = (env) => {
	// Get the root path (assuming your webpack config is in the root of your project!)
	const currentPath = path.join(__dirname);
	// Create the fallback path (the production .env)
	const basePath = `${currentPath}/.env`;

	// We're concatenating the environment name to our filename to specify the correct env file!
	const envPath = `${basePath}.${env.ENVIRONMENT}`;

	// Check if the file exists, otherwise fall back to the production .env
	const finalPath = fs.existsSync(envPath) ? envPath : basePath;

	// Set the path parameter in the dotenv config
	const fileEnv = dotenv.config({ path: finalPath }).parsed;

	// reduce it to a nice object, the same as before (but with the variables from the file)
	const envKeys = Object.keys(fileEnv).reduce((prev, next) => {
		prev[`process.env.${next}`] = JSON.stringify(fileEnv[next]);
		return prev;
	}, {});

	return {
		entry: './src/index.js',
		output: {
			filename: 'main.js',
			path: path.resolve(__dirname, 'dist'),
		},
		module: {
			rules: [
				{
					test: /\.(html)$/i,
					loader: 'html-loader',
					options: { minimize: true }
					//use: ['file-loader?name=[name].file.[ext]', 'extract-loader', 'html-loader'],
				},
				{
					test: /\.(js|jsx)$/,
					exclude: /\/node_modules\/core-js\//,
					use: [
						{
							loader: 'babel-loader',
							options: {
								// configFile: './babel.config.json',
								// Caching is recommended when transpiling node_modules to speed up consecutive builds
								// cacheDirectory: true
							},
						},
						
					],
				},
				{
					test: /\.s[ac]ss$/i,
					use: [
						// Creates `style` nodes from JS strings
						'style-loader',
						// Translates CSS into CommonJS
						// 'css-loader',
						{
							loader: 'css-loader',
							options: {
								importLoaders: 2,
							},
						},
						{
							loader: 'postcss-loader',
							options: {
								// eslint-disable-next-line import/no-extraneous-dependencies
								// eslint-disable-next-line global-require
								plugins: [require('autoprefixer')],
							},
						},
						// Compiles Sass to CSS
						'sass-loader',
					],
				},
				{
					test: /\.css$/,
					use: [
						// style-loader
						{ loader: 'style-loader' },
						// css-loader
						{
							loader: 'css-loader',
							options: {
								modules: true
							}
						},
						// sass-loader
						{ loader: 'sass-loader' }
					]
				},
				{
					test: /\.svg$/,
					use: ['@svgr/webpack'],
				},
				{
					test: /\.(png|jpe?g|gif)$/i,
					loader: 'file-loader',
					options: {
						name: '/wp-content/themes/newhouse5/[path][name].[ext]',
					},
				},
				{
					test: /\.(graphql|gql)$/,
					exclude: /node_modules/,
					loader: 'graphql-tag/loader',
				},
				
			],
		},
		plugins: [
			new webpack.DefinePlugin(envKeys),
		],
	};
};
