// eslint-disable-next-line import/no-extraneous-dependencies
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

// eslint-disable-next-line no-unused-vars
module.exports = (env) => ({
	// Specify how to handle files
	module: {
		rules: [
			// Javascript
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					// ?This is for react only?
					loader: 'babel-loader',
					options: {
						// declaring presets essential for pulling components from other projects
						presets: [
							'@babel/preset-env',
							'@babel/preset-react',
						],
						plugins: [
							// ! using runtime here breaks module loading across project folders
							// "@babel/plugin-transform-runtime",
							'@babel/plugin-proposal-class-properties',
							'@babel/plugin-transform-react-jsx-source',
						],
					},
				},
			},
			// Svg
			{
				// Different methods for loading svgs discussed
				// https://react.christmas/2018/18
				// https://larsgraubner.com/webpack-images/
				test: /\.svg$/,
				use: [
					{ loader: 'url-loader' },
				],

			},
			// Jeff's svg method
			// {
			// 	test: /\.svg$/,
			// 	use: ['@svgr/webpack'],
			// },

			// Merged
			// {
			// 	test: /\.svg$/,
			// 	use: ['@svgr/webpack', 'url-loader'],
			// }
			// SASS
			{
				/* Loaders are used in reverse order; ie last one first */
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							sourceMap: true,
						},
					},
					{
						// ? Add config here
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
							plugins: [
								// eslint-disable-next-line global-require, import/no-extraneous-dependencies
								require('autoprefixer'),
							],
						},
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true,
						},
					},
				],

			},
		],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].css',
		}),
	],
});
