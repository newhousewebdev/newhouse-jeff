
module.exports = (env) => ({
	externals: {
		// '@wordpress/components': 'wp.components', // Not really a package.
		'@wordpress/blocks': 'wp.blocks', // Not really a package.
		'@wordpress/data': 'wp.data', // Not really a package.
		'@wordpress/element': 'wp.element', // Not really a package.
		'@wordpress/hooks': 'wp.hooks',
		'@wordpress/editor': 'wp.editor',
		'@wordpress/compose': 'wp.compose',
		'@wordpress/block-editor': 'wp.blockEditor',
		'@wordpress/components': 'wp.components', // Not really a package.
	},
});
