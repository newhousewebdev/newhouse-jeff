
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = (env) => ({
	// Specify how to handle files
	module: {
		rules: [
			// Javascript
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					// ?This is for react only?
					loader: 'babel-loader',
					options: {
						// declaring presets essential for pulling components from other projects
						presets: [
							'@babel/preset-env',
							'@babel/preset-react',
						],
						plugins: [
							// ! using runtime here breaks module loading across project folders
							// "@babel/plugin-transform-runtime",
							'@babel/plugin-proposal-class-properties',
							'@babel/plugin-transform-react-jsx-source',
						],
					},
				},
			},
			// Svg
			{
				// Different methods for loading svgs discussed
				// https://react.christmas/2018/18
				// https://larsgraubner.com/webpack-images/
				test: /\.svg$/,
				use: [
					{ loader: 'url-loader' },
				],

			},
			// SASS
			{
				/* Loaders are used in reverse order; ie last one first */
				test: /\.scss$/,
				exclude: /node_modules/,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							sourceMap: true,
						},
					},
					{
						loader: 'postcss-loader',
						options: {
							sourceMap: true,
							plugins: [
								// eslint-disable-next-line global-require
								require('autoprefixer'),
							],
						},
					},
					{
						loader: 'sass-loader',
						options: {
							sourceMap: true,
						},
					},
				],

			},
		],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].css',
		}),
	],

	// External Libraries that are in global scope
	// the package maps to the global vars
	// ! This should be an external preset
	externals: {
		'@wordpress/blocks': 'wp.blocks', // Not really a package.
		'@wordpress/data': 'wp.data', // Not really a package.
		'@wordpress/element': 'wp.element', // Not really a package.
		'@wordpress/hooks': 'wp.hooks',
		'@wordpress/editor': 'wp.editor',
		'@wordpress/compose': 'wp.compose',
		'@wordpress/block-editor': 'wp.blockEditor',
		'@wordpress/components': 'wp.components', // Not really a package.
	},
});
