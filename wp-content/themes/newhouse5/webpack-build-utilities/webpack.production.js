const TerserPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
/*
	TODO:
	* Add a production build process
	* https://medium.com/@rajaraodv/two-quick-ways-to-reduce-react-apps-size-in-production-82226605771a
	* https://medium.com/a-young-devoloper/analyzing-and-reducing-react-bundle-size-bb2d2577b22a
	* https://reallifeprogramming.com/get-rid-of-some-fat-from-your-react-production-build-3a5bb4970305
	* 		* Minify, css, js, imgs?, svgs?
*/

module.exports = env => ({
	// devtool: 'cheap-module-source-map',
	optimization: {
		minimize: true,
		minimizer: [new TerserPlugin({}), new OptimizeCSSAssetsPlugin({})],
	},
});
