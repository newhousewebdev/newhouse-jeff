/*
 * Edit the filenames here. Presets and loaders are defined else where.
 */

/*
 *	path.resolve will search for/utilize any given absolute paths,
	if no absolute path is given/found the current directory is used.
	__dirname is a shortcut for path.dirname($current_file)
	Discussion on path - https://medium.com/@raviroshan.talk/webpack-understanding-the-publicpath-mystery-aeb96d9effb1
*/
const path = require('path');

const pluginsPath = '../../plugins/';

module.exports = () => ({
	entry: {
		// ! Hack way to output to multiple files
		// https://github.com/webpack/webpack/issues/1189#issuecomment-216053379
		// use {path}/{filename} as the name
		// Front-end App
		'../newhouse-app/dist/main': path.resolve(__dirname, '../newhouse-app/src/index'),
		// Page editor script
		'../build/main': path.resolve(__dirname, '../editor-src/index'),
		// author editor script
		'../build/author': path.resolve(__dirname, '../editor-src/author'),

		// Theme style sheet
		'../style': path.resolve(__dirname, '../index'),
		// Editor Style sheet
		'../css/editor': path.resolve(__dirname, '../css/index'),

		// ! these work
		// Backend Gutenberg Components
		'../../../plugins/newhouse-blocks/build/main': path.resolve(pluginsPath, 'newhouse-blocks/src/index'),
		// Custom Post Type block components
		'../../../plugins/newhouse-cpt/build/main': path.resolve(pluginsPath, 'newhouse-cpt/src/index'),
	},
	output: {
		filename: '[name].js',
		// Below works with the file name trick
		path: path.resolve(__dirname), // relative to webpack.config.js location
	},
});
