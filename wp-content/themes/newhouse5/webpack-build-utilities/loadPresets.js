// eslint-disable-next-line import/no-extraneous-dependencies
const webpackMerge = require('webpack-merge');

const applyPresets = (env = { presets: [] }) => {
	const presets = env.presets || [];
	/** @type {string[]} */
	const mergedPresets = [].concat(...[presets]);
	// eslint-disable-next-line global-require, import/no-dynamic-require
	const mergedConfigs = mergedPresets.map(presetName => require(`./presets/webpack.${presetName}`)(env));

	return webpackMerge({}, ...mergedConfigs);
};

module.exports = applyPresets;
