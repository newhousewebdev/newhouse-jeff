import React, { Component } from 'react';
/*
	Ref:
	https://reactjs.org/docs/error-boundaries.html
*/

class ErrorBoundary extends Component {
	constructor(props) {
		super(props);
		this.state = {
			error: null,
			errorInfo: null,
		};
	}

	componentDidCatch(error, errorInfo) {
		this.setState({
			error,
			errorInfo,
		});
		console.error(error, errorInfo);
	}

	render() {
		if (this.state.errorInfo) {
			return (
				<section>
					<h1>Something went wrong please refresh the page</h1>
					<details style={{ whiteSpace: 'pre-wrap' }}>
						{this.state.error && this.state.error.toString()}
						<br />
						{this.state.errorInfo.componentStack}
					</details>
				</section>
			);
		}

		return this.props.children;
	}
}

export default ErrorBoundary;
