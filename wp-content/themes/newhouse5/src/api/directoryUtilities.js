import React, { Fragment } from 'react';
import { fetchAcademicDepartments, fetchAcademicPrograms } from './fetches/DirectoryFetches';
import Card from '../components/Card/Card';

const firstEntry = (object) => object[Object.keys(object)[0]];
const secondEntry = (object) => object[Object.keys(object)[1]];

/**
 * Reverses a slug so the last item is first and the first is last.
 * @param {string} slug - to reverse
*/
export const reverseSlug = (slug) => slug.split('-').reverse().join('-');

const filterAndSortJobs = (jobs, positionsIncluded) => {
	const defaultPositionKeys = positionsIncluded
	|| Object.keys(jobs)
		// remove associated Programs, they are not "jobs"
		.filter((key) => key !== 'associated_programs');

	const filteredPositions = defaultPositionKeys.flatMap((key) => jobs[key])
		.filter((position) => firstEntry(position) !== '');

	const sortedJobs = filteredPositions
		.sort((job1, job2) => job1.job_display_order - job2.job_display_order);

	return sortedJobs;
};

const JobLocation = ({ location }) => (location === '' ? '' : <h5 className='job-company card--content--company'>{location}</h5>);

const buildLocationEntires = (jobLocation) => {
	if (Array.isArray(jobLocation)) {
		const jobLocations = jobLocation
			.map((entry, index) => <JobLocation key={index} location={entry} />);
		return jobLocations;
	}
	return <JobLocation location={jobLocation} />;
};

/**
 * Builds a job title based upon the job display order
 *
 * @param {object} jobs - Jobs object to run through the builder
 * @param {array}  positionsIncluded - An array of object keys to limit the build process
 *
 * @returns {string} of Job titles separated by comma
*/
export const buildJobTitle = (jobs, positionsIncluded = undefined) => {
	const sortedJobs = filterAndSortJobs(jobs, positionsIncluded);
	const jobTitle = sortedJobs.map((job) => firstEntry(job)).join(', ');

	return jobTitle;
};

/**
 * Builds a job title list based upon the job display order
 *
 * @param {object} jobs - Jobs object to run through the builder
 * @param {array}  positionsIncluded - An array of object keys to limit the build process
 *
 * @return {array} jobs
*/
export const buildJobTitleList = (jobs, positionsIncluded = undefined) => {
	const sortedJobs = filterAndSortJobs(jobs, positionsIncluded);
	const jobTitleList = sortedJobs.map((job, index) => {
		const title = firstEntry(job);
		const jobLocationRaw = secondEntry(job);
		const jobLocation = buildLocationEntires(jobLocationRaw);

		return <li key={index}>
			<h4 className='job-title card--content--position'>{title}</h4>
			{jobLocation}
		</li>;
	});

	return <Fragment>
		{jobTitleList}
	</Fragment>;
};

export const buildDirectoryCards = (people) => people.map((person) => {
	const info = person.meta.person_meta;
	const { images: { profile_image: profileImage }, name: { display_name: displayName } } = info;
	const path = `people/${reverseSlug(person.slug)}`;
	const jobTitleList = buildJobTitleList(info.job);
	return <Card key={person.id} className="grid--item card--people">
		{profileImage
			&& <Card.DrupalProfileImage featuredImage={profileImage} path={path} />
		}
		<Card.Content alignment="center">
			<Card.Title title={displayName} path={path} />
			<ul>
				{jobTitleList}
			</ul>
			{path
				&& <Card.CTA path={path} label="View bio" />
			}
		</Card.Content>
	</Card>;
});

/**
 * Matches a department slug to department id on the directory.
 * This performs a a fetch.
 *
 * @param {string} departmentSlug - Slug to look up
 * @async
*/
export const matchDepartmentSlugToID = async (departmentSlug) => {
	const departments = await fetchAcademicDepartments();
	const matchingDepartment = departments
		.filter((department) => department.slug === departmentSlug);
	return matchingDepartment[0].id;
};

/**
 * Matches a program slug to program id on the directory.
 * This performs a a fetch.
 *
 * @param {string} departmentSlug - Slug to look up
 * @async
*/
export const matchProgramSlugToID = async (programSlug) => {
	const Programs = await fetchAcademicPrograms();
	const matchingProgram = Programs
		.filter((program) => program.slug === programSlug);
	return matchingProgram[0].id;
};

/**
 * Matches a department slug to department id on the directory.
 * This performs a a fetch.
 *
 * @param {string} departmentName - Name to look up
 * @async
*/
export const matchTaxonomyNameToID = async (departmentName) => {
	const departments = await fetchAcademicDepartments();
	const matchingDepartment = await departments
		.filter((department) => department.name === departmentName);
	return matchingDepartment[0].id;
};

