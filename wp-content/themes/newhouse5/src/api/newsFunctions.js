export const filterNewsCategories = ( newsCategories = null ) => {
	if (newsCategories && newsCategories.length > 1) {
		// if there's more than one news category, exclude "Featured News"
		return newsCategories.filter((node) => (
			// exclude featured news
			node.slug !== "featured-news"
		));
	} else {
		// leave as is
		return newsCategories;
	}
}
