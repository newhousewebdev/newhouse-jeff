import React from 'react';

export const MobileMenuContext = React.createContext();
export const MobileSearchContext = React.createContext();
export const ActiveDegreeContext = React.createContext();
export const MobileContext = React.createContext();
export const TilesContext = React.createContext();
export const ArchiveCollectionContext = React.createContext();
export const EventsDataContext = React.createContext();
export const EventsLoadingContext = React.createContext();

// New Contexts
export const DirectoryContext = React.createContext({});

// Get Started
export const GetStartedPreviousPageContext = React.createContext();
