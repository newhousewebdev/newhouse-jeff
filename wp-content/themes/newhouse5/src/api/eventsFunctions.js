// import moment from 'moment';

/* Removed Moment for bundle size reduction */
export const getDateFormatted = (date, format = {
	month: 'short', day: 'numeric', hour: 'numeric', minute: '2-digit',
}) => {
	// console.log({date});
	// const dateFormat = { month: 'long', day: 'numeric', year: '2-digit' };
	const formattedDate = new Date(date).toLocaleString('en-US', format).replace('AM', 'a.m.').replace('PM', 'p.m.');

	return [formattedDate.slice(0, 3), '.', formattedDate.slice(3)].join('');
};


/* 
	2020-10-06T21:30:00+00:00

*/
// export const getDateFormatted = (date, format = {
// 	month: 'short', day: 'numeric', hour: 'numeric', minute: '2-digit',
// }) => {
// 	// console.log({date});
// 	// const dateFormat = { month: 'long', day: 'numeric', year: '2-digit' };
// 	const formattedDate = new Date(date).toLocaleString('en-US', format).replace('AM', 'a.m.').replace('PM', 'p.m.');

// 	return [formattedDate.slice(0, 3), '.', formattedDate.slice(3)].join('');
// };
/* For Reference */
// export const getDateFormatted = (unformattedDate) => {
// 	console.log({ unformattedDate });
// 	const format = { month: 'short', day: 'numeric', hour: 'numeric', minute: '2-digit' };
// 	return humanDate(unformattedDate, format);

// console.log({ readableDate });
// const parseAmPm = (ampm) => (ampm === 'am' ? 'a.m.' : 'p.m.');
// const parseTime = (hour, minutes) => (minutes === '00' ? hour : `${hour}:${minutes}`);
// const parseMonth = (month) => {
// 	switch (month) {
// 	case 'January': return 'Jan.';
// 	case 'February': return 'Feb.';
// 	case 'August': return 'Aug.';
// 	case 'September': return 'Sep.';
// 	case 'October': return 'Oct.';
// 	case 'November': return 'Nov.';
// 	case 'December': return 'Dec.';
// 	default: return month;
// 	}
// };
// const parsedDate = {
// 	month: parseMonth(moment(unformattedDate).format('MMMM')),
// 	day: moment(unformattedDate).format('D'),
// 	hour: moment(unformattedDate).format('h'),
// 	minutes: moment(unformattedDate).format('mm'),
// 	ampm: parseAmPm(moment(unformattedDate).format('a')),
// };
// const {
// 	month, day, hour, minutes, ampm,
// } = parsedDate;

// /* Jan.  */
// const formattedDate = `${month} ${day}, ${parseTime(hour, minutes)} ${ampm}`;

// return formattedDate;
// };

/* {month: 'short' } */

export const stripHtmlTags = (str) => {
	if ((str === null) || (str === '')) {
		return false;
	}
	return str.toString().replace(/<[^>]*>/g, '');
};

export const eventTypes = [
	{ value: 160, name: 'Career Fair' },
	{ value: 116, name: 'Ceremonies' },
	{ value: 164, name: 'Clinic' },
	{ value: 150, name: 'Conferences' },
	{ value: 93, name: 'Dance Parties' },
	{ value: 100, name: 'Deadlines' },
	{ value: 92, name: 'Discussions' },
	{ value: 98, name: 'Dissertation Defenses' },
	{ value: 117, name: 'Exhibitions' },
	{ value: 105, name: 'Films' },
	{ value: 109, name: 'Information Sessions' },
	{ value: 149, name: 'Lectures and Seminars' },
	{ value: 124, name: 'Meals' },
	{ value: 107, name: 'Meditation Sessions' },
	{ value: 110, name: 'Meetings' },
	{ value: 97, name: 'Open Houses' },
	{ value: 125, name: 'Performances' },
	{ value: 143, name: 'Poster Sessions' },
	{ value: 102, name: 'Public Addresses' },
	{ value: 121, name: 'Receptions' },
	{ value: 163, name: 'Religious or Spiritual Gathering' },
	{ value: 123, name: 'Sporting Contests' },
	{ value: 91, name: 'Talks' },
	{ value: 154, name: 'Thesis Defenses' },
	{ value: 183, name: 'Virtual' },
	{ value: 113, name: 'Webinars' },
	{ value: 108, name: 'Workouts' },
	{ value: 112, name: 'Workshops' },
];

export const eventCategories = [
	{ value: 151, name: '150th Anniversary' },
	{ value: 142, name: 'Administration' },
	{ value: 153, name: 'Alumni' },
	{ value: 145, name: 'Architecture' },
	{ value: 118, name: 'Arts and Performance' },
	{ value: 122, name: 'Basketball' },
	{ value: 119, name: 'Career Development' },
	{ value: 103, name: 'Chancellor' },
	{ value: 115, name: 'Commencement' },
	{ value: 128, name: 'Communications Journalism Media' },
	{ value: 95, name: 'Diversity and Inclusion' },
	{ value: 99, name: 'Education' },
	{ value: 131, name: 'Engineering and Technology' },
	{ value: 104, name: 'Entertainment' },
	{ value: 152, name: 'Entrepreneurship' },
	{ value: 96, name: 'Graduate Admissions' },
	{ value: 185, name: 'Graduate Studies' },
	{ value: 127, name: 'Health and Sport Sciences' },
	{ value: 106, name: 'Health and Wellness' },
	{ value: 90, name: 'Humanities' },
	{ value: 146, name: 'Information and Library Science' },
	{ value: 147, name: 'Law' },
	{ value: 144, name: 'Management and Business' },
	{ value: 148, name: 'Money and Finance' },
	{ value: 182, name: 'Orientation' },
	{ value: 111, name: 'Professional Development' },
	{ value: 129, name: 'Religion and Spirituality' },
	{ value: 161, name: 'Remembrance' },
	{ value: 181, name: 'Research Support' },
	{ value: 120, name: 'Science and Mathematics' },
	{ value: 94, name: 'Social Activities' },
	{ value: 126, name: 'Social Science and Public Policy' },
	{ value: 101, name: 'Undergraduate Admissions' },
];
