export function getDegreeNameBySlug(slug) {
	// console.log({url});
	let degreeName;
	switch (slug) {
	case 'bachelors':
		degreeName = 'Bachelor\'s';
		break;
	case 'masters':
		degreeName = 'Master\'s';
		break;
	case 'online':
		degreeName = 'Master\'s';
		break;
	case 'doctoral':
		degreeName = 'Doctoral';
		break;
	case 'certificates':
		degreeName = 'Certificates';
		break;
	case 'minors':
		degreeName = 'Minors';
		break;
	default:
		degreeName = slug;
	}
	return degreeName;
}

export const deepCopy = (object) => ({ ...JSON.parse(JSON.stringify(object)) });

export function extractSubUrl(url) {
	const cleanURL = url.replace(process.env.API_URL, '');
	return cleanURL;
}

/* String Methods */
export const capitalizeFirstLetter = (string) => string.charAt(0).toUpperCase() + string.slice(1);

export const capitalizeWords = (string) => string.split(' ').map((word) => capitalizeFirstLetter(word)).join(' ');

export const removeUnderscore = (string) => string.replace('_', ' ');

export const replaceSpacesWithUnderscores = (string) => string.replace(/ /g, '_');

export const removeTrailingS = (string) => (string.endsWith('s') ? string.slice(0, string.length - 1) : string);

// time and time functions
export const filterMonthAPStyle = (month) => {
	switch (month) {
	case 'Mar':
		return 'March';
	case 'Apr':
		return 'April';
	case 'Jun':
		return 'June';
	case 'Jul':
		return 'July';
	case 'Sep':
		return 'Sept.';
	default:
		return month;
	}
};

export const filterPublishedDate = (date) => {
	// "Sep 1, 2020" should be reformatted to "Sept. 1, 2020"
	// "Mar 1, 2020" should be reformatted to "March 1, 2020"
	const dateSplit = date.split(' ');
	dateSplit[0] = filterMonthAPStyle(dateSplit[0]);
	const newDate = dateSplit.join(' ');
	return newDate;
};
