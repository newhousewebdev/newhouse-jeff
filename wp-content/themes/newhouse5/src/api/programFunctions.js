const acceptedDegreeSlugsArr = ['bachelors', 'masters', 'online', 'doctoral', 'certificates'];

export const checkDegreeSlug = (slug) => acceptedDegreeSlugsArr.includes(slug);

export const buildPageSlug = (programSlug, degreeSlug, subpageSlug) => {
	console.log("buildPageSlug", { programSlug, degreeSlug, subpageSlug});
	let slug;
	// note: academics/advertising doesn't have degreeSlug
	// e.g. advertising/careers, careers is considered degreeSlug from the URL bar
	if (subpageSlug) {
		// e.g. advertising/bachelors/curriculum
		slug = `${programSlug}/${degreeSlug}/${subpageSlug}`;
	} else if (degreeSlug && acceptedDegreeSlugsArr.includes(degreeSlug)) {
		// e.g. advertising/bachelors
		// NOT advertising/curriculum
		// should only query the parent page
		slug = `${programSlug}`;
	} else if (degreeSlug && !acceptedDegreeSlugsArr.includes(degreeSlug)) {
		// for sub-pages that apply to all degrees
		// e.g. advertising/careers
		// since degreeSlug is in second location, it's serving as subpageSlug, 
		slug = `${programSlug}/${degreeSlug}`;
	} else {
		// e.g. advertising
		slug = programSlug;
	}
	// console.log({slug});
	return slug;
};

export const getActiveDegreeSlug = (degreeSlug, activeDegree, degrees) => {
	// console.log('get Active Degree Slug', { degreeSlug }, { activeDegree }, { degrees });
	let newActiveDegreeSlug;
	if (degreeSlug !== undefined && acceptedDegreeSlugsArr.includes(degreeSlug)) {
		newActiveDegreeSlug = degreeSlug;
	} else if (activeDegree) {
		newActiveDegreeSlug = activeDegree;
	} else if (degrees.edges && degrees.edges.length === 1) {
		newActiveDegreeSlug = degrees.edges[0].slug;
	} else {
		newActiveDegreeSlug = '';
	}
	return newActiveDegreeSlug;
};

export const getPageContent = (
	pageSlugToMatch,
	childAcademicPrograms = null,
	parentPageContent = null,
) => {
	// if on advertising/bachelors -
	// use main content from bachelors subpage, not the parent advertising page

	let matchingEdge = null;
	if (childAcademicPrograms && childAcademicPrograms.edges) {
		// loop through subpages and find the subpage that matches the desired pageSlug
		// e.g. return true if uri = academics/advertising/bachelors
		// (if bachelors were the activeDegree)
		matchingEdge = childAcademicPrograms.edges.filter((edge) => edge.node.uri === pageSlugToMatch);
	}
	// console.log({matchingEdge});
	if (matchingEdge && matchingEdge.length > 0) {
		// if there's subpages, and a subpage that matches the desired path, return subpage content
		// e.g. advertising/bachelors
		console.log(matchingEdge[0].node.content);
		return matchingEdge[0].node.content;
	} if (parentPageContent) {
		// if there's subpages that match, return normal parent page content
		// e.g. advertising/careers returns advertising content
		return parentPageContent;
	}
	// if there's no subpages, and no parent page content, return empty string
	return '';
};

export const getPageSections = (childAcademicPrograms = null, activeDegreeSlug) => {
	// console.log({ childAcademicPrograms});
	const pageSections = [];

	if (childAcademicPrograms && childAcademicPrograms.edges) {
		// 1. begin the flattening process
		const pagesFlat = [];

		// loop through child pages of the program
		childAcademicPrograms.edges.forEach((edge) => {
			pagesFlat.push(edge);
			// if subpage has subpages.
			// i.e. /advertising/bachelors/curriculum
			if (edge.node.childItems.edges && edge.node.childItems.edges.length > 0) {
				// each subsubpage gets pushed in, too
				edge.node.childItems.edges.forEach((childEdge) => {
					pagesFlat.push(childEdge);
				});
			}
		});
		// console.log({pagesFlat});
		// 2. eliminates duplicates, but it's not an array
		const seen = new Set();

		// loop through flat list and return items that haven't been seen (not a duplicate)
		const pagesUnique = pagesFlat.filter((page) => {
			// boolean: is the page title in the seen list?
			const duplicate = seen.has(page.node.label);
			// add it to the seen list
			seen.add(page.node.label);
			// return the page only if it's false (not see)
			return !duplicate;
		});
		// console.log({pagesUnique});

		// 4. page titles to eliminate
		// we're not displaying these subpages in the page display
		const pageFilters = ['Bachelor&#8217;s', 'Master&#8217;s', 'Online', 'Doctoral', 'Certificates'];
		// loop through unique pages and return ones that are NOT in the pageFilters array
		// if the title is NOT in the above array, return it
		const pagesFiltered = pagesUnique.filter((page) => !pageFilters.includes(page.node.label));

		// sort pages alphabetically, for now
		// pagesFiltered.sort();
		// function compare(a, b) {
		// 	// Use toUpperCase() to ignore character casing
		// 	const nodeA = a.node.label.toUpperCase();
		// 	const nodeB = b.node.label.toUpperCase();

		// 	let comparison = 0;
		// 	if (nodeA > nodeB) {
		// 		comparison = 1;
		// 	} else if (nodeA < nodeB) {
		// 		comparison = -1;
		// 	}
		// 	return comparison;
		// }

		// alpha ordering
		// pagesFiltered.sort(compare);

		// console.log({pagesFiltered});

		// pagesFiltered is just a list of titles. It's an inventory list of what's needed.
		// I'm going back to the original array and retrieving the nodes that belong to each title
		pagesFiltered.forEach((page) => {
			// loop through pages in the original data
			childAcademicPrograms.edges.forEach((subpage) => {
				if (subpage.node.label === page.node.label) {
					pageSections.push(subpage);
				}
				// if no match, keep going into subsubpages
				if (activeDegreeSlug
					&& (activeDegreeSlug === subpage.node.slug) && subpage.node.childItems.edges) {
					subpage.node.childItems.edges.forEach((subEdge) => {
						if (subEdge.node.label === page.node.label) {
							pageSections.push(subEdge);
						}
					});
				}
			});
		});
	} // endif
	// console.log({pageSections});
	return pageSections;
};

export const getLabel = (parent, activeDegreeSlug) => {
	let label;
	const degreeFullNameArr = ['Bachelor&#8217;s', 'Bachelor\'s', 'Master&#8217;s', 'Master\'s', 'Online', 'Doctoral'];
	if (parent) {
		if (!degreeFullNameArr.includes(parent.title)) {
			label = parent.title;
		} else {
			label = parent.parent.title;
		}
	} else {
		label = activeDegreeSlug;
	}
	// console.log({ activeDegree });
	return label;
};

export const getParentItems = (parent) => (parent || null);

export const getLedeParagraph = (blocks) => {
	if (blocks.length > 0) {
		const matchingBlocks = blocks.filter((block) => (block.hasOwnProperty('__typename') && block.__typename === 'NhBlocksLedeParagraphBlock'));
		return matchingBlocks.length > 0 ? matchingBlocks[0].content : null;
	}
	return null;
};

export const getCoreEmbedYoutubeBlock = (blocks) => {
	// console.log("getCoreEmbedYoutubeBlock");
	// console.log({blocks});
	// refactor: this isn't set up for multiple videos on one page.
	if (blocks.length > 0) {
		const matchingBlocks = blocks.filter((block) => (block.hasOwnProperty('name') && block.name === 'core-embed/youtube'));
		console.log({ matchingBlocks });
		return {
			url: matchingBlocks[0].attributes.url,
			align: matchingBlocks[0].attributes.align,
		};
	}
	return null;
};
