/* Not being used */

import { useEffect, useContext, useState } from 'react';
import { DirectoryContext } from './ContextAPI';

const sampleTaxObject = {
	academic_programs: {
		advertising: 22,
		'bandier-program-in-recording-and-entertainment-industries': 45,
		'broadcast-and-digital-journalism': 23,
		'{slug}': 'id',
	},
	administrative_departments: {
		advertising: 22,
		'bandier-program-in-recording-and-entertainment-industries': 45,
		'broadcast-and-digital-journalism': 23,
		'{slug}': 'id',
	},
};

/*
	DirectoryTaxonmies

	{
		taxonomy_slug:{
			term_slug: id
		},
		academic_programs:{
			broadcast-and-digital-journalism:23
		}
	}

*/

/**
 * Lookup a taxonomy in the DirectoryContext
 *
 * @param {string} taxonomy slug to match
 *
 */
const useDirectoryTaxonomy = (taxonomy) => {
	const taxonomies = useContext(DirectoryContext);

	return taxonomies[taxonomy]; // returns a taxonomy object with slugs attached to ids
};

export default useDirectoryTaxonomy;
