/* global eventsParams:true */
/*
	Events Calendar Info

	Group: Newhouse School of Public Communications : 3438

	Organizers:
	Newhouse Career Development Center: 19836
	Newhouse Career Development Center - Seminars: 19833
	Newhouse in NYC: 19842
	Newhouse Sports Media Center: 19838
	Newhouse Center for Global Engagement: 3437
	Syracuse Welcome: Newhouse School of Public Communications : 20483

	Max events is 40
*/

// const baseEventsUrl = 'https://calendar.syracuse.edu/wp-json/su-events/v1/events/';
const ajaxUrl = eventsParams.ajax;

export const getEventsViaAjax = (params) => {
	const data = `events_nonce=${eventsParams.eventsNonce}&action=get_newhouse_events&${params || ''}`;

	return fetch(ajaxUrl, {
		method: 'POST',
		body: new URLSearchParams(data),
		credentials: 'include',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
		},
	})
		.then((res) => res.json())
		.then((res) => JSON.parse(res))
		.catch((err) => { console.error(err); });
};

// ID or slug of an event. Event slugs are in the form YYYY-mon-DD/words-with-dashes.
export const getSingleEventViaAjax = (params) => {
	const data = `events_nonce=${eventsParams.eventsNonce}&action=get_newhouse_event&slug=${params}`;

	return fetch(ajaxUrl, {
		method: 'POST',
		body: new URLSearchParams(data),
		credentials: 'include',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
		},
	})
		.then((res) => res.json())
		.then((res) => JSON.parse(res))
		.catch((err) => { console.error(err); });
};

export const getMoreEventsViaAjax = (url) => {
	const data = `events_nonce=${eventsParams.eventsNonce}&action=get_more_newhouse_events&url=${encodeURIComponent(url)}`;

	return fetch(ajaxUrl, {
		method: 'POST',
		body: new URLSearchParams(data),
		credentials: 'include',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
		},
	})
		.then((res) => res.json())
		.then((res) => JSON.parse(res))
		.catch((err) => { console.error(err); });
};

// export const getEventsCategories = () => {
// 	const data = `events_nonce=${eventsParams.eventsNonce}&action=get_events_categories`;

// 	return fetch(ajaxUrl, {
// 		method: 'POST',
// 		body: new URLSearchParams(data),
// 		credentials: 'include',
// 		headers: {
// 			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
// 		},
// 	});
// };
