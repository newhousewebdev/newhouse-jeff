import React from 'react';
import { useHistory } from 'react-router-dom';
import { useQuery } from 'react-apollo';
import gql from 'graphql-tag';
import Loader from '../components/Loader';

import Form from '../components/Form/Form';
import Row from '../components/Layout/Row';
import Col from '../components/Layout/Col';
import Button from '../components/Button';

const FindYourProgram = () => {
	const history = useHistory();

	const GET_ALL_ACADEMIC_PROGRAMS = gql`
	query MyQuery {
		academicPrograms(first: 50) {
			edges {
				node {
					uri
					title
					slug
					excerpt(format: RAW)
					degrees {
						edges {
							node {
								slug
							}
						}
					}
				}
			}
		}
		degrees {
			edges {
				node {
					name
					slug
					description
				}
			}
		}
	}
	`;

	// fetch from wpgraphql
	const { loading, error, data } = useQuery(GET_ALL_ACADEMIC_PROGRAMS);

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const { academicPrograms } = data;

	const degreeConditions = ['bachelors', 'masters'];

	// alphabetize
	/*
		Note: Refactor to an alphabetize function
	*/
	academicPrograms.edges.sort((a, b) => {
		const nameA = a.node.title.toUpperCase(); // ignore upper and lowercase
		const nameB = b.node.title.toUpperCase(); // ignore upper and lowercase
		if (nameA < nameB) {
			return -1;
		}
		if (nameA > nameB) {
			return 1;
		}

		// names must be equal
		return 0;
	});

	const bachelorPrograms = academicPrograms.edges.filter((program) => {
		const degreeSlugs = (program.node.degrees && program.node.degrees.edges) ? program.node.degrees.edges.map((degree) => degree.node.slug) : [];
		return degreeSlugs.length > 0 ? degreeSlugs.includes('bachelors') : false;
	});
	const mastersPrograms = academicPrograms.edges.filter((program) => {
		const degreeSlugs = (program.node.degrees && program.node.degrees.edges) ? program.node.degrees.edges.map((degree) => degree.node.slug) : [];
		return degreeSlugs.length > 0 ? degreeSlugs.includes('masters') : false;
	});
	const otherPrograms = academicPrograms.edges.filter((program) => {
		const degreeSlugs = (program.node.degrees && program.node.degrees.edges) ? program.node.degrees.edges.map((degree) => degree.node.slug) : [];
		const degreeTest = degreeSlugs.some((degreeSlug) => degreeConditions.includes(degreeSlug));
		return !degreeTest;
	});

	const onChangeProgram = (degree) => {
		if (degree && degreeConditions.includes(degree)) {
			history.push(`academics/${event.target.value}/${degree}`);
		} else {
			history.push(`academics/${event.target.value}`);
		}
	};

	const bachelorsOptionsArr = bachelorPrograms.length > 0 ? bachelorPrograms.map((program, key) => {
		return <option 
			key={key} 
			value={program.node.slug}>
				{program.node.title}
			</option>
	}) : [];
	const mastersOptionsArr = mastersPrograms.length > 0 ? mastersPrograms.map((program, key) => {
		return <option
			key={key}
			value={program.node.slug}>
			{program.node.title}
		</option>
	}) : [];
	const otherOptionsArr = otherPrograms.length > 0 ? otherPrograms.map((program, key) => {
		return <option
			key={key}
			value={program.node.slug}>
			{program.node.title}
		</option>
	}) : [];

	return (
		<section className="get-started">
			<div className="get-started--top">
				
				<div className="container--content">
					<h2>Your communications career<br /> 
					starts here</h2>
						<p>At the Newhouse School, we’re training the next generation of communications leaders—professionals who are prepared not only to enter a rapidly-changing media industry, but also to shape its future.</p>
						<div className="btn--group justify-content-center">
							<Button.CTA
								type="primary"
								label="Get Started"
								path="get-started"
								colorMode="orange"
							/>
						</div>
				</div>
					
			</div>
			<div className="get-started--bottom">
				<div className="container">
					<h2><span>Find your program</span></h2>
					<Form>
						<Row>
							<Col>
								<Form.Group>
									<Form.Label htmlFor="bachelors" className="hide">Bachelor's</Form.Label>
									<Form.SelectControl
									id="bachelors"
									onChange={(e) => { onChangeProgram('bachelors'); }}
									options={bachelorsOptionsArr}
									defaultOptionLabel="Bachelor's degrees"
									/>
								</Form.Group>
							</Col>
							<Col>
								<Form.Group>
									<Form.Label htmlFor="masters" className="hide">Master's</Form.Label>
									<Form.SelectControl
										id="masters"
										onChange={(e) => { onChangeProgram('masters'); }}
										options={bachelorsOptionsArr}
										defaultOptionLabel="Master's degrees"
									/>
								</Form.Group>
							</Col>
							<Col>
								<Form.Group>
									<Form.Label htmlFor="other" className="hide">Other programs</Form.Label>
									<Form.SelectControl
										id="other"
										onChange={(e) => { onChangeProgram('other'); }}
										options={otherOptionsArr}
										defaultOptionLabel="Other programs"
									/>
								</Form.Group>
							</Col>
						</Row>
					</Form>
				</div>
			</div>
		</section>
	);
};
export default FindYourProgram;
