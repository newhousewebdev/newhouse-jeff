import React from 'react';
import Menus from '../components/Menu/Menus';
import Row from '../components/Layout/Row';
import Col from '../components/Layout/Col';

const Footer = () => (
	<footer className="site-footer">
		<div className="container--fluid">
			<Row className="justify-content-between align-items-center footer-brand-row">
				<Col>
				<h2 className='visually-h3'>Newhouse School at Syracuse University</h2>
				</Col>
				<Col>
				<Menus.Registered 
					location="Social"
					label="Social media links"
					settings={{
						colorMode: 'dark',
						depth: 0,
						parentLinks: {
							format: 'row',
							style: 'icons',
							override: true
						},
						childLinks : null
					}} 
				/>
				</Col>
			</Row>
			<Row className="justify-content-between">
				<Col sm={2} className="footer-utility-col">
					<p>Copyright {new Date().getFullYear()}<br />
					S.I. Newhouse School of Public Communications at Syracuse University.</p>
					<Menus.Registered 
						location="Footer utility"
						label="Footer navigation links"
						settings={{
							colorMode: 'dark',
							depth: 0,
							parentLinks: {
								format: 'list',
							},
							childLinks: null
						}}
					/>
				</Col>
				<Col sm={10}>
				<Menus.Registered 
					location="Footer nav"
					label="Site navigation"
					settings={{
						colorMode: 'dark',
						depth: 1,
						parentLinks: {
							format: 'row',
						},
						childLinks: {
							format: 'list'
						}
					}} 
				/>
				</Col>
			</Row>
		</div>
	</footer>
);
export default Footer;
