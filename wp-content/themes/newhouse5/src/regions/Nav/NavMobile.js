// libraries
import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { motion, AnimatePresence } from 'framer-motion';

// components
import Menus from '../../components/Menu/Menus';
import Button from '../../components/Button';
import Logo from '../../components/Logo';

// context
import {
	MobileMenuContext,
} from '../../api/ContextAPI';

const NavMobile = () => {
	const [mobileMenuVisibility, setMobileMenuVisibility] = useContext(MobileMenuContext);

	const variants = {
		open: {
			left: 0,
			opacity: 1,
		},
		closed: {
			left: `${-100}vw`,
			opacity: 0,
		},
		exit: {
			left: `${-100}vw`,
			opacity: 0,
		},
	};

	return (
		<AnimatePresence>
			{mobileMenuVisibility && (
				<motion.div
					key="modal"
					initial="closed"
					animate={mobileMenuVisibility ? 'open' : 'closed'}
					variants={variants}
					exit="exit"
					className="nav--mobile-pane">
					<div className="nav--mobile-pane--left">
						<Link 
							to="/" 
							onClick={() => { setMobileMenuVisibility(false) }}>
							<Logo.Primary style="inverted" />
						</Link>
						<Menus.Registered
							location="Mobile"
							label="Primary Mobile Navigation"
							settings={{
								colorMode: 'dark',
								depth: 1,
								parentLinks: {
									format: 'list',
								},
								childLinks: {
									format: 'list',
								},
								mobileMenu: true,
							}}
							clickHandler={() => {setMobileMenuVisibility(false)}}
						/>
					</div>
					<div className="nav--mobile-pane--right">
						<Button.UI
							icon="times"
							id="menuCloseBtn"
							colorMode="orange"
							onClickHandler={() => { setMobileMenuVisibility(false); }}
							screenText='Close Mobile Menu'
						/>
						<Menus.Registered
							location="Social"
							label="Social media links"
							settings={{
								colorMode: 'dark',
								depth: 0,
								parentLinks: {
									format: 'list',
									style: 'icons',
								},
								childLinks : null,
								mobileMenu: true,
							}}
						/>
					</div>
				</motion.div>
			)}
		</AnimatePresence>
	);
};
export default NavMobile;
