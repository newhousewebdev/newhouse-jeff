import React from 'react';
import Icon from '../../components/Icon';

const Header = ({
	title, description, alignment, icon,
}) => (
	<div className={`section--header ${alignment}`}>
		{icon
				&& <div className="section--header--icon">
					<div className="section--header--icon--circle">
						<Icon icon={icon} />
					</div>
				</div>
		}
		<div className="container--content">
			<h2 className="section--header--title" dangerouslySetInnerHTML={{ __html: title }}></h2>
			{description
					&& <p dangerouslySetInnerHTML={{ __html: description }}></p>
			}

		</div>
	</div>
);
export default Header;
