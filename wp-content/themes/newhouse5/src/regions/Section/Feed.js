import React from 'react';

import Masonry from 'react-masonry-component';
import Grid from '../../components/Layout/Grid';

const Feed = ({ children, type }) => {
	// eslint-disable-next-line consistent-return
	const feedType = () => {
		if (type === 'grid') {
			return <Grid>{children}</Grid>;
		}
		return <Masonry className="masonry--grid">
			{children}
		</Masonry>;
	};
	return feedType();
};
export default Feed;
