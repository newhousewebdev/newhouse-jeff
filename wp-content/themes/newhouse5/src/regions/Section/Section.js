import React from 'react';

import Header from './Header';
import Feed from './Feed';

const Section = ({className, children, container=null }) => {
	const containerClass = container ? `container--${container}` : 'container';
	return <section className={`section ${className ? `section--${className}` : ''}`}>
		<div className={containerClass}>
			{children}
		</div>
	</section>
}
Section.Header = Header;
Section.Feed = Feed;

const Title = ({title}) => {
	return <h2 className="section--title" dangerouslySetInnerHTML={{ __html: title }} />
}
Section.Title = Title;

export default Section;
