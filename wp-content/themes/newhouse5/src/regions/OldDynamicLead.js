import React from 'react';
import { Link } from 'react-router-dom';

import { useQuery } from 'react-apollo';
import gql from 'graphql-tag';
import Loader from '../components/Loader';

import Button from '../components/Button';
import Icon from '../components/Icon';

import { extractSubUrl } from '../api/Utility';

const DynamicLead = () => {
	const DYNAMIC_LEAD_QUERY = gql`
query MyQuery {
  dynamicLeads(first: 1) {
    edges {
      node {
        title
        dynamicLeadInformation {
          primaryHeadline
          subHeadline
          callToActions {
            primary {
              url
              title
              target
            }
            learnMore {
              url
              title
              target
            }
            secondary {
              target
              title
              url
            }
          }
        }
        featuredImage {
          altText
          sourceUrl(size: LARGE)
        }
      }
    }
  }
}

	`;

	const { loading, error, data } = useQuery(DYNAMIC_LEAD_QUERY);

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const { dynamicLeadInformation, featuredImage } = data.dynamicLeads.edges[0].node;

	const { primaryHeadline, subHeadline, callToActions } = dynamicLeadInformation;

	const { primary, secondary, learnMore } = callToActions;

	return (
		<section className="dynamic-lead">
			{featuredImage
				&& <img src={featuredImage.sourceUrl} alt={featuredImage.altText} className="dynamic-lead--featuredImage" />
			}
			<div className="dynamic-lead--left">
				<div className="dynamic-lead--left--content">
					{primaryHeadline
						&& <h1>{primaryHeadline}</h1>
					}
					{subHeadline
						&& <h2>{subHeadline}</h2>
					}
					<Button.Group>
						{primary
							&& <Button.CTA type="primary" label={primary.title} path={extractSubUrl(primary.url)} colorMode="orange" />
						}
						{secondary
							&& <Button.CTA type="secondary" label={secondary.title} path={extractSubUrl(secondary.url)} colorMode="orange" />
						}
					</Button.Group>
				</div>
			</div>
			{learnMore
				&& <div className="dynamic-lead--credit">
					<Link to={extractSubUrl(learnMore.url)}>{learnMore.title} <Icon.LongArrowRight /></Link>
				</div>
			}
		</section>
	);
};
export default DynamicLead;
