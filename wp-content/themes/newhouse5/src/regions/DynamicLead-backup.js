import React, {
	useState, useEffect, useRef, useCallback, useContext,
} from 'react';
import { Link, useLocation } from 'react-router-dom';
import { motion } from 'framer-motion';

import {
	MobileContext,
	GetStartedPreviousPageContext
} from '../api/ContextAPI';

import Button from '../components/Button';
import Menus from '../components/Menu/Menus';

const dlArr = [
	{
		slug: 'kelsey',
		headline: 'Kelsey Davis co-founded a creative agency during her freshman year.',
		moreInfo: {
			label: "Learn more about Kelsey Davis.",
			uri: "news/gen-z-in-action/",
			type: "internal"
		},
		image: {
			file: 'dl-kelsey-1200x800.jpg',
			alt: 'Kelsey Davis',
		},
	},
	{
		slug: 'hanz',
		headline: 'Hanz Valbuena built an international community.',
		moreInfo: {
			label: "Learn more about Hanz Valbuena.",
			uri: "news/hanz-valbuena-builds-an-international-community/",
			type: "internal"
		},
		image: {
			file: 'dl-hans-3-1200x800.jpg',
			alt: 'Hanz Valbuena',
		},
	},
	{
		slug: 'jewel',
		headline: 'Jewél Jackson learned to give a voice to underrepresented communities.',
		moreInfo: {
			label: "Learn more about Jewél Jackson.",
			uri: "news/seeking-truth-and-justice-through-the-power-of-words/",
			type: "internal"
		},
		image: {
			file: 'dl-jewel-3-1200x800.jpg',
			alt: 'Jewel Jackson',
		},
	},
	{
		slug: 'mackenzie',
		headline: 'Mackenzie Pearce produces and directs studio shows for the ACC Network.',
		moreInfo: {
			label: "Learn more about Mackenzie Pearce.",
			uri: "news/joining-a-winning-team-in-the-competitive-world-of-sports-broadcasting/",
			type: "internal"
		},
		image: {
			file: 'dl-mackenzie-3-1200x800.jpg',
			alt: 'Mackenzie Pearce',
		},
	},
];

function useInterval(callback, delay) {
	const savedCallback = useRef();
	const intervalId = useRef(null);
	const [currentDelay, setDelay] = useState(delay);
	

	const toggleRunning = useCallback(
		() => setDelay((currentDelay) => (currentDelay === null ? delay : null)),
		[delay],
	);

	const clear = useCallback(() => clearInterval(intervalId.current), []);

	// Remember the latest function.
	useEffect(() => {
		savedCallback.current = callback;
	}, [callback]);

	// Set up the interval.
	useEffect(() => {
		function tick() {
			savedCallback.current();
		}

		if (intervalId.current) clear();

		if (currentDelay !== null) {
			intervalId.current = setInterval(tick, currentDelay);
		}

		return clear;
	}, [currentDelay, clear]);

	return [toggleRunning, !!currentDelay];
}

const DynamicLead = () => {
	//console.log("dynamic lead!");
	const location = useLocation();
	const [previousPage, setPreviousPage] = useContext(GetStartedPreviousPageContext);

	const [isMobile, setIsMobile] = useContext(MobileContext);

	const totalSlides = 4;
	const [currentSlide, setCurrentSlide] = useState(0);
	const [slideshowActive, setSlideshowActive] = useState(true);
	// console.log(currentSlide);

	const [toggle, running] = useInterval(() => {
		// Your custom logic here
		setCurrentSlide((currentSlide) => (currentSlide === totalSlides - 1 ? 0 : currentSlide + 1));
	}, 7000);

	const handleNextClick = () => {
		setCurrentSlide((currentSlide) => (currentSlide === totalSlides - 1 ? 0 : currentSlide + 1));
	};
	const handlePrevClick = () => {
		setCurrentSlide((currentSlide) => (currentSlide === 0 ? totalSlides - 1 : currentSlide - 1));
	};

	const slideVariants = {
		open: {
			opacity: 1,
			x: 0,
			transition: {
				duration: 0.5,
			},
		},
		closed: {
			opacity: 0,
			x: 100,
			transition: {
				duration: 0.5,
			},
		},
	};

	return (
		<section className="dl">
			<div className="dl--frame">
				<Menus.Registered
					location="Social"
					label="Social media links"
					settings={{
						colorMode: 'dark',
						depth: 0,
						parentLinks: {
							format: 'row',
							style: 'icons',
						},
					}}
				/>
				<div className="dl--slide-container">
					<motion.ul
						className="dl--slides"
						initial="closed"
						animate="open">
						{dlArr.map((slide, key) => {
							return <motion.li
								key={key}
								initial="closed"
								animate="open"
								variants={slideVariants}
								className={`dl--slides--item ${currentSlide === key ? 'active' : ''}`}
								data-slug={dlArr[key].slug}
							>
							</motion.li>
						})}
					</motion.ul>
				</div>
				<div className="dl--side-column">
					<div className="dl--side-content">
						<h2>{dlArr[currentSlide].headline}</h2>
						<p>What will you do at Newhouse?</p>
						<div className="btn--group">
							<Button.CTA
								type="primary"
								label="Get Started"
								path="get-started"
								colorMode="orange"
								onClickHandler={() => {
									setPreviousPage(location.pathname)
								}}
							/>
						</div>
					</div>
				</div>
			</div>
			<div className="dl--controls">
				<Button.UI
					icon="angle-left"
					id="prevBtn"
					colorMode={isMobile ? 'gray' : 'orange'}
					onClickHandler={handlePrevClick}
					// Needs Aria controls
					screenText='Show Previous Slide'
				/>
				<Button.UI
					icon={running ? 'pause' : 'play'}
					id="pauseBtn"
					colorMode={isMobile ? 'gray' : 'orange'}
					onClickHandler={toggle}
					screenText={`${running ? 'Pause' : 'Play'} Slideshow`}
				/>
				<Button.UI
					icon="angle-right"
					id="nextBtn"
					colorMode={isMobile ? 'gray' : 'orange'}
					onClickHandler={handleNextClick}
					screenText='Show Next Slide'
				/>
			</div>
			{
				dlArr.map((slide, key) => {
					const { label, uri, type } = slide.moreInfo;
					if (type === "internal") {
						return <p key={key} className={`dl--slide--caption ${currentSlide === key ? 'active' : ''}`}><Link to={uri}>{label}</Link></p>
					} else {
						return <p key={key} className={`dl--slide--caption ${currentSlide === key ? 'active' : ''}`}><a href={uri} target="_blank">{label}</a></p>
					}
					
				})
			}
		</section>
	);
};
export default DynamicLead;
