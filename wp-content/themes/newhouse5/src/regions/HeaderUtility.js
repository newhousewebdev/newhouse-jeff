import React from 'react';

import Menus from '../components/Menu/Menus';

const HeaderUtility = () => (
	<div className="header--utility">
		<Menus.Registered settings={{
			location: 'Header utility left',
			colorMode: 'light',
			depth: 0,
			parentLinks: {
				format: 'row',
			},
		}} />
		<Menus.Registered settings={{
			location: 'Header utility right',
			colorMode: 'light',
			depth: 0,
			parentLinks: {
				format: 'row',
			},
		}} />
	</div>
);
export default HeaderUtility;
