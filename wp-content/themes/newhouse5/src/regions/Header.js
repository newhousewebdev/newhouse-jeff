import React, { useState, useContext, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { motion, AnimatePresence } from 'framer-motion';

import Menus from '../components/Menu/Menus';
import Logo from '../components/Logo';
import Button from '../components/Button';
import Form from '../components/Form/Form';

import {
	MobileMenuContext,
	MobileSearchContext,
	MobileContext,
} from '../api/ContextAPI';

const Header = () => {
	const [searchShow, setSearchShow] = useState(false);

	const [mobileMenuVisibility, setMobileMenuVisibility] = useContext(MobileMenuContext);
	const [mobileSearchVisibility, setMobileSearchVisibility] = useContext(MobileSearchContext);
	const [isMobile, setIsMobile] = useContext(MobileContext);

	const variants = {
		open: {
			opacity: 1,
			marginTop: 0,
		},
		closed: {
			opacity: 0,
			marginTop: -300,
		},
		exit: {
			opacity: 0,
			marginTop: -300,
		},
	};

	return (
		<header className="header--primary">
			<div className="header--top">
				<Link to="/"><Logo.Primary /></Link>
				{!isMobile
				&& <div className="header--right">
					<Menus.Registered
						location="Primary"
						label="Main Site Navigation" 
						settings={{
							colorMode: 'light',
							depth: 1,
							parentLinks: {
								format: 'row',
							},
							childLinks: {
								format: 'dropdown',
							},
						}} 
						clickHandler={null}
					/>
					<div className={`header--search ${searchShow ? 'expanded' : ''}`}>
						{!searchShow
								&& <Button.UI
									icon="search"
									id="searchBtn"
									colorMode="white"
									onClickHandler={() => { setSearchShow(true); }}
									screenText='Open Search Bar'
								/>
						}
						{searchShow
								&& <Fragment>
									<Form.PrimarySearch />
									<Button.UI
										icon="times"
										id="searchCloseBtn"
										colorMode="orange"
										onClickHandler={() => { setSearchShow(false); }}
										screenText='Close Search Bar'
									/>
								</Fragment>
						}
					</div>
				</div>
				}
				{isMobile
					&& <div className="header--right">
						<Button.MobileMenu
							id="menuBtn"
							colorMode="white"
							onClickHandler={() => { setMobileMenuVisibility(true); }}
							screenText='Mobile Menu'
						/>

						{mobileSearchVisibility ? (
							<Button.UI
								icon="times"
								id="closeBtn"
								colorMode="white"
								onClickHandler={() => {
									setMobileSearchVisibility(false);
								}}
								screenText='Close Search Bar'
							/>
						) : (
							<Button.UI
								icon="search"
								id="searchBtn"
								colorMode="white"
								onClickHandler={() => {
									setMobileSearchVisibility(true);
								}}
								screenText='Open Search Bar'
							/>
						)}
					</div>
				}
			</div>
			<AnimatePresence>
				{(isMobile && mobileSearchVisibility)

				&& <motion.div
					className="header--bottom"
					initial="closed"
					animate={mobileSearchVisibility ? 'open' : 'closed'}
					variants={variants}
					exit="exit">
					<Form.MobileSearch />
				</motion.div>
				}
			</AnimatePresence>
		</header>
	);
};
export default Header;
