import React, {useState} from 'react';

import Button from './Button';

const MyCarousel = () => {
	const [currentSlide, setCurrentSlide] = useState(0);
	return <section className="carousel">
		<div className="carousel--frame">
			<Button.UI 
			id="carouselPrev" 
			icon="angle-left"
			colorMode="orange"
			onClick={() => {
				setCurrentSlide(currentSlide + 1)
			}}
			/>
			<Button.UI 
			id="carouselNex" 
			icon="angle-right"
			colorMode="orange"
			onClick={() => {
				setCurrentSlide(currentSlide - 1)
			}}
			/>
			<ul className="carousel--slides">
				<li className="carousel--slides--item">
						Slide 1: image and text
				</li>
				<li className="carousel--slides--item">
					Slide 2: image and text
				</li>
				<li className="carousel--slides--item">
					Slide 3: image and text
				</li>
				<li className="carousel--slides--item">
					Slide 4: image and text
				</li>
			</ul>
		</div>
	</section>
}
export default MyCarousel;
