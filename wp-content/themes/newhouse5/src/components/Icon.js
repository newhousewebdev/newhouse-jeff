import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// init FontAwesome library
import { library } from '@fortawesome/fontawesome-svg-core';
// import { far } from '@fortawesome/free-regular-svg-icons';
import {
	faBars,
	faSearch,
	faTimes,
	faAngleLeft,
	faAngleRight,
	faAngleDown,
	faAngleUp,
	faPlus,
	faPause,
	faPlay,
	faMapMarkerAlt,
	faCalendarAlt,
	faNewspaper,
} from '@fortawesome/free-solid-svg-icons';
import {
	faFacebook,
	faTwitter,
	faInstagram,
	faYoutube,
} from '@fortawesome/free-brands-svg-icons';

import ScrollIcon from '../sass/img/scroll-icon.svg';
import LongArrowRightIcon from '../sass/img/long-arrow-right.svg';

// Add all icons to the library so you can use it in your page
library.add(
	faBars,
	faSearch,
	faTimes,
	faAngleLeft,
	faAngleRight,
	faAngleDown,
	faAngleUp,
	faPlus,
	faPause,
	faPlay,
	faMapMarkerAlt,
	faCalendarAlt,
	faNewspaper,
	faFacebook,
	faTwitter,
	faInstagram,
	faYoutube,
);

const iconLibrary = [
	{
		prefix: 'fas',
		icons: ['bars', 'search', 'times', 'angle-left', 'angle-right', 'angle-down', 'angle-up', 'plus', 'pause'],
	},
	{
		prefix: 'fab',
		icons: ['facebook', 'twitter', 'instagram', 'youtube'],
	},
];

const lookUpPrefix = (icon) => {
	const matchingLibrary = iconLibrary.filter((fontLibrary) => fontLibrary.icons.includes(icon));

	return matchingLibrary[0] ? matchingLibrary[0].prefix : null;
};

const Icon = ({ icon }) => {
	const prefix = lookUpPrefix(icon);
	return <FontAwesomeIcon icon={prefix ? [prefix, icon] : icon} />;
};

const LongArrowRight = () => <LongArrowRightIcon />;
Icon.LongArrowRight = LongArrowRight;

const IconScroll = () => <ScrollIcon />;
Icon.Scroll = IconScroll;

export default Icon;
