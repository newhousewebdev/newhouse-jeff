import React, { useState, useEffect, useRef } from 'react';
import * as Scroll from 'react-scroll';

import Button from './Button';

const Slider = ({id, children}) => {
	const scroller = Scroll.scroller;
	const Events = Scroll.Events;

	const [currentSlide, setCurrentSlide] = useState(0);
	const [totalSlides, setTotalSlides] = useState(null);
	

	const [dots, setDots] = useState(null);
	const [slides, setSlides] = useState(null);
	const [visibleSlides, setVisibleSlides] = useState(null);
	const [sliderParent, setSliderParent] = useState(null);

	const modernBrowser = !!HTMLElement.prototype.scrollTo;

	// console.log("--------");
	// console.log({sliderParent});
	// console.log("--------");

	const initSlider = () => {
		const slideItemsCollection = sliderParent.children;
		const slideItemsArr = Array.from(slideItemsCollection);
		setSlides(slideItemsArr);
		//console.log({ slideItemsArr });

		const visibleSlidesArr = [];
		// better cross browser support by getting subpixels then rounding
		const sliderWidth = Math.round(sliderParent.getBoundingClientRect().width);
		//console.log(sliderWidth);
		// Add a 1 pixel buffer so that subpixels are more consistent cross-browser
		const sliderPosition =
			sliderParent.scrollLeft - 1 < 0 ? 0 : sliderParent.scrollLeft - 1;

		// Only detects items in the visible viewport of the parent element
		slideItemsArr.forEach((slide) => {
			const slideOffset = slide.offsetLeft;

			if (
				slideOffset >= sliderPosition &&
				slideOffset < sliderPosition + sliderWidth
			) {
				visibleSlidesArr.push(slide);
			}
		});
		//console.log({ visibleSlidesArr });
		//console.log();
		// apply tabIndexes
		slideItemsArr.forEach((slide, index) => {
			//console.log(index);
			if (index >= currentSlide && index <= (currentSlide + (visibleSlidesArr.length-1))) {
				//slide.tabIndex = 1;
			} else {
				slide.tabIndex = -1;
				slide.ariaHidden = true;
			}
		});

		setVisibleSlides(visibleSlidesArr.length);
		setDots(slideItemsArr.slice(0, (slideItemsArr.length - (visibleSlidesArr.length - 1))));
	}
	useEffect(() => {
		setSliderParent(document.getElementById(id));
	}, []);

	useEffect(() => {
		// waits for children prop to be loaded
		// happens on the news feed
		if (sliderParent && children) {
			initSlider();
			setCurrentSlide(0);
		}
	}, [children]);

	useEffect(() => {
		// waits for the sliderParent to be established
		// needed on the events feed
		if (sliderParent && children) {
			initSlider();
			setCurrentSlide(0);
		}
	}, [sliderParent]);

	useEffect(() => {
		window.addEventListener('resize', initSlider);
		//window.addEventListener('load', handleWindowSizeChange);
	}, []);

	useEffect(() => {
		if (sliderParent) {
			if (modernBrowser) {
				sliderParent.scroll({
					left: slides[currentSlide].offsetLeft,
					behavior: 'smooth'
				});
			} else {
				sliderParent.scrollLeft = slides[currentSlide].offsetLeft;
			}
		}
	
	}, [currentSlide]);

	const handleDotClick = (index) => {
		//console.log("handling dot click");
		setCurrentSlide(index);
	}
	const handlePrevClick = () => {
		const newIndex = currentSlide <= 0 ? 0 : currentSlide - 1;
		setCurrentSlide(newIndex);
	}
	const handleNextClick = () => {
		const newIndex = currentSlide >= (slides.length - 1) ? slides.length - 1 : currentSlide + 1;
		setCurrentSlide(newIndex);
	}
	const Dot = ({index}) => {
		return <li key={index} >
			<button
				type="button"
				className={`${currentSlide === index ? 'active' : ''}`}
				onClick={() => { handleDotClick(index) }}
			>
				{`Move slider to item #${index}`}

			</button>
		</li>
	}

	const generateDotHolder = () => {
		return <ul className="slider--dots">
			{dots && 
				dots.map((slide, key) => {
					return <Dot key={key} index={key} />
				})
			}	
		</ul>
	}

	return <div className="slider--container">
		<button className="slider--sr-only" type="button" tabIndex="0" onClick={() => {
			Events.scrollEvent.register('begin', function (to, element) {
				//console.log('begin', to, element);
			});
			scroller.scrollTo(`endOfCarousel${id}`, {
				duration: 800,
				delay: 0,
				smooth: true,
				isDynamic: true
			});
			Events.scrollEvent.register('end', function (to, element) {
				//console.log('end', to, element);
				//console.log({element});
				element.focus();
			});
		}}>Click to skip slider carousel</button>
		<div className="slider--controls">
			<Button.UI
				icon="angle-left"
				id="sliderPrevious"
				colorMode="orange"
				onClickHandler={handlePrevClick}
				screenText="Previous slide"
				className={`${currentSlide > 0 ? 'visible' : 'disabled'}`}
			/>
			<Button.UI
				icon="angle-right"
				id="sliderNext"
				colorMode="orange"
				onClickHandler={handleNextClick}
				screenText="Next slide"
				className={`${slides && (currentSlide < (slides.length - visibleSlides)) ? 'visible' : 'disabled'}`}
			/>
		</div>
		<div className="slider--frame">
			<div
				className="slider--row"
				id={id}
			>
				{children}
			</div>
		</div>
		{dots && 
			generateDotHolder()
		}
		<div className="slider--sr-only" tabIndex="-1" name={`endOfCarousel${id}`}>End of slider carousel</div>
	</div>
}
export default Slider;
