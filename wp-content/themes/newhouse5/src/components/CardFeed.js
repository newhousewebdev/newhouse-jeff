import React from 'react';

import Section from '../views/Page/Section';
import Grid from './Layout/Grid';

// add a feedOptions variable that can be parsed
const CardFeed = ({
	cards,
	noResultsMessage = null,
	...props
}) => <Section {...props}>
	<Grid>
		{cards.length === 0 && noResultsMessage ? <h4>{noResultsMessage}</h4> : cards}
	</Grid>
</Section>;
export default CardFeed;
