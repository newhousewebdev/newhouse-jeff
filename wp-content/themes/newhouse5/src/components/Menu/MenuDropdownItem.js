import React from 'react';
import { Link } from 'react-router-dom';
import { motion } from 'framer-motion';
import { extractSubUrl } from '../../api/utilityFunctions';

const MenuDropdownItem = ({ menuItem, clickHandler }) => {
	//	console.log("DROPDOWN ITEM INIT");
	const { url, description, label } = menuItem.node;
	const variants = {
		open: {
			y: 0,
			opacity: 1,
			transition: {
				y: { stiffness: 1000, velocity: -100 },
			},
		},
		closed: {
			y: 50,
			opacity: 0,
			transition: {
				y: { stiffness: 1000 },
			},
		},
	};
	
	return (
		<motion.li
			initial="closed"
			variants={variants}
			className="menu--dropdown-item"
			tabIndex="0">
			<h3 className="menu--label" dangerouslySetInnerHTML={{ __html: label }}></h3>
			<p className="menu--description" dangerouslySetInnerHTML={{ __html: description }}></p>
			<Link
				className="menu--cta"
				to={`/${extractSubUrl(url)}`}
				onClick={() => clickHandler(false)}>Explore now</Link>
		</motion.li>
	);
};
export default MenuDropdownItem;
