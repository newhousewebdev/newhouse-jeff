import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { extractSubUrl } from '../../api/utilityFunctions';
import ScreenReaderText from '../ScreenReaderText';
import Icon from '../Icon';

const MenuLink = ({url , label, style, clickHandler=null}) => {
	const linkLabel = () => {
		if (style === 'icons') {
			return <Fragment>
				<Icon icon={label.toLowerCase()} />
				<ScreenReaderText text={`Link to ${url}`} />
			</Fragment>;
		}
		return label;
	};

	if (url.includes(process.env.API_URL)) {
		return <Link 
		to={`/${extractSubUrl(url)}`}
		className="menu--label"
			onClick={() => { clickHandler ? clickHandler() :''}}
		>{linkLabel()}</Link>
	} else {
		return <a href={url} rel="noreferrer" target="_blank" className="menu--label">{linkLabel()}</a>
	}
	
}
export default MenuLink;
