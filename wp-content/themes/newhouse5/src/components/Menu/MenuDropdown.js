import React from 'react';
import classNames from 'classnames';
import { motion } from 'framer-motion';
import MenuDropdownItem from './MenuDropdownItem';

const MenuDropdown = ({ variant, menuItems, clickHandler }) => {
	const variants = {
		open: {
			height: 'auto',
			opacity: 1,
			transition: {
				type: 'tween',
				staggerChildren: 0.07,
				delayChildren: 0.3,
			},
		},
		closed: {
			height: 0,
			opacity: 0,
			transition: {
				type: 'tween',
				staggerChildren: 0.05,
				staggerDirection: -1,
			},
		},
	};

	const dropdownMenuClasses = classNames('menu--dropdown', {
		[`${variant}`] : variant
	});

	return (
		<motion.ul
			initial="closed"
			variants={variants}
			className={dropdownMenuClasses}
		>
		{
		menuItems.edges.map((menuItem, key) => 
			<MenuDropdownItem
				key={key} 
				menuItem={menuItem} 
				clickHandler={clickHandler} 
			/>
		)}
		</motion.ul>
	);
};
export default MenuDropdown;
