import React from 'react';

import { useQuery } from 'react-apollo';
import MENU_QUERY from '../../queries/menu.gql';

import Menu from './Menu';
import Loader from '../Loader';
import { replaceSpacesWithUnderscores } from '../../api/utilityFunctions';

const Registered = ({ location, label, settings, clickHandler = null }) => {
	// gql needs menu location in caps and underscores
	// footer-utility becomes FOOTER_UTILITY
	const formattedLocation = replaceSpacesWithUnderscores(location).toUpperCase();

	const { loading, error, data } = useQuery(MENU_QUERY, {
		variables: {
			"location": formattedLocation,
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const menuItems = data.menus.nodes[0].menuItems.edges;

	return <Menu
		label={label}
		location={formattedLocation}
		menuItems={menuItems}
		settings={settings}
		clickHandler={clickHandler}
	/>
};

export default Registered;
