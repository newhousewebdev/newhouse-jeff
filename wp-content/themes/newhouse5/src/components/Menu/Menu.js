import React from 'react';
import classNames from 'classnames';

// sub-components
import MenuItem from './MenuItem';

const Menu = ({ menuItems, label=null, location, settings=null, clickHandler }) => {
	const ariaLabel = label ? label : `${location} Navigation`;

	const navClasses = classNames('nav',
		{
			[`nav--${location.toLowerCase()}`]: location,
		});
	
	const menuClasses = classNames('menu',
		{
			[`menu--${settings.parentLinks.format}`]: settings.parentLinks.format,
			[`menu--${settings.colorMode}`]: settings.colorMode,
			[`menu--${location.toLowerCase()}`]: location,
			[`menu--override`]: settings.parentLinks.override,
		});

	return <nav
		className={navClasses}
		aria-label={ariaLabel}
	>
		<ul 
		className={menuClasses}>
			{menuItems.map((menuItem, key) => {
				return <MenuItem
					key={key}
					menuItem={menuItem}
					settings={settings}
					clickHandler={clickHandler}
				/>
			})}
		</ul>
	</nav>;
};

export default Menu;
