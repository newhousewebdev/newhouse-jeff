import Registered from './Registered';

const Menus = ({ children }) => ({ children });

Menus.Registered = Registered;

export default Menus;
