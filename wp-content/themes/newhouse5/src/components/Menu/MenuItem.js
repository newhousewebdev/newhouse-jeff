import React, { useState } from 'react';
import { motion } from 'framer-motion';
import classNames from 'classnames';

// components
import MenuDropdown from './MenuDropdown';
import MenuLink from './MenuLink';
import Menu from './Menu';

const MenuItem = ({ menuItem, settings, clickHandler = null }) => {
	const {
		label, url, childItems,
	} = menuItem.node;

	const { childLinks, depth, parentLinks } = settings;

	const [isDropdownOpen, toggleDropdownOpen] = useState(false);

	const displayDropdown = () => {
		if (depth > 0 && childItems && childItems.edges) {
			return <MenuDropdown variant="grid" menuItems={childItems} clickHandler={toggleDropdownOpen} />
		} else {
			return '';
		}
	}
	const displaySubMenuList = () => {
		if (depth > 0 && childItems && childItems.edges) {
			return <Menu 
			menuItems={childItems.edges} 
			location="submenu_list" 
			label="Sub-navigation list menu" 
			clickHandler={clickHandler} 
			settings={{
				parentLinks : {
					format: "list"
				}
			}}
			/>
		} else {
			return '';
		}
	}

	const itemClasses = classNames('menu--item', {
		'has-children': (childItems && childItems.edges)
	});

	return (
		<motion.li
			animate={isDropdownOpen ? 'open' : 'closed'}
			className={itemClasses}
			onMouseEnter={() => { toggleDropdownOpen(true) }}
			onMouseLeave={() => { toggleDropdownOpen(false) }}
			onFocus={() => { toggleDropdownOpen(true) }}
			onBlur={() => { toggleDropdownOpen(false) }}	
		>
			<MenuLink label={label} url={url} style={parentLinks.style} clickHandler={clickHandler} />
			{isDropdownOpen && childLinks && childLinks.format === 'dropdown' && displayDropdown()}
			{childLinks && childLinks.format === 'list' && displaySubMenuList()}
		</motion.li>
	);
};
export default MenuItem;
