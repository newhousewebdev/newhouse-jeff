import React from 'react';


import ListItem from './ListItem';

const List = ({type, children}) => {
	return (
		<ul className={`list list--${type}`}>
			{children}
		</ul>
	)
}

List.Item = ListItem;

export default List;
