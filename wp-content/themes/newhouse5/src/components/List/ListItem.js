import React from 'react';
import { Link } from 'react-router-dom';

const ListItem = ({label, path}) => {
	return (
		<li>
			<Link to={path}>{label}</Link>
		</li>
	)
}
export default ListItem;
