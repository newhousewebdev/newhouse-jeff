import React from 'react';

const Loader = () => {
	return (
		<div className="loader"><img src={`${process.env.API_URL}/wp-content/uploads/2020/05/loading.gif`} alt="loading icon" /></div>
	)
}
export default Loader;
