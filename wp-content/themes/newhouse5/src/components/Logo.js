import React from 'react';

import NewhouseLogo from '../sass/img/newhouse-school-2020.svg';
import NewhouseLogoInverted from '../sass/img/newhouse-school-2020--inverted.svg';

const Logo = () => (
	<NewhouseLogo className="logo" />
);
const Primary = ({style = null, className = null}) => {
	if (style === "inverted") {
		return <NewhouseLogoInverted className={`logo logo--primary ${className || ''}`} />
	} else {
		return <NewhouseLogo className={`logo logo--primary  ${className || ''}`} />
	}
};
Logo.Primary = Primary;

export default Logo;
