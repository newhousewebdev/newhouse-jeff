import React, { useState } from 'react';
import { domToReact } from 'html-react-parser';

// components
import Icon from '../Icon';

const Faq = ({ children }) => {
	const [faqActive, setFaqActive] = useState(false);

	const question = children[0].children[0].data;
	const answers = children[1].children.length > 0 ? children[1].children : null;

	const faqHandleClick = () => {
		// boolean value is flipped with !
		setFaqActive(!faqActive);
	};
	return <div className={`faq ${faqActive ? 'active' : ''}`}>
		<button
			type="button"
			className="faq--trigger"
			onClick={faqHandleClick}><Icon icon="plus" /></button>
		<h3 className="faq--question">{question}</h3>
		{
			answers
				&& <div className="faq--answer">
					<div>{domToReact(answers)}</div>
				</div>
		}
	</div>;
};
export default Faq;
