import React, { useEffect } from 'react';
import { Helmet } from 'react-helmet';
import { useLocation } from 'react-router-dom';
import ReactGA from 'react-ga';
import { stripHtmlTags } from '../api/eventsFunctions';

const usePageTracking = () => {
	const location = useLocation();

	useEffect(() => {
		ReactGA.set({
			page: location.pathname,
			location: `${process.env.SITE_URL}${location.pathname}`,
		});
		ReactGA.pageview(location.pathname + location.search);
	}, [location]);
};

const SEO = ({
	title = null, uri = null, description = null, image = null,
}) => {
	const initTracking = usePageTracking();
	// remove markup from default Wordpress excerpt
	const sanitizedMetaDescription = stripHtmlTags(description);

	return <Helmet>
		<title>{title ? `${title} | ` : ''}Newhouse School at Syracuse University</title>
		<meta property="og:title" content={title || 'Newhouse School at Syracuse University'} />
		<meta name="twitter:title" content={title || 'Newhouse School at Syracuse University'} />

		<link rel="icon" sizes="192x192" href={`${process.env.SITE_URL}/wp-content/themes/newhouse5/src/sass/img/android-icon-192x192.png`} />
		<link rel="icon" sizes="128x128" href={`${process.env.SITE_URL}/wp-content/themes/newhouse5/src/sass/img/android-icon-128x128.png`} />
		<link rel="apple-touch-icon" href={`${process.env.SITE_URL}/wp-content/themes/newhouse5/src/sass/img/apple-touch-icon.png`} />
		<link rel="apple-touch-icon" sizes="76x76" href={`${process.env.SITE_URL}/wp-content/themes/newhouse5/src/sass/img/apple-touch-icon-76x76-precomposed.png`} />
		<link rel="apple-touch-icon" sizes="120x120" href={`${process.env.SITE_URL}/wp-content/themes/newhouse5/src/sass/img/apple-touch-icon-120x120-precomposed.png`} />
		<link rel="apple-touch-icon" sizes="152x152" href={`${process.env.SITE_URL}/wp-content/themes/newhouse5/src/sass/img/apple-touch-icon-152x152-precomposed.png`} />

		<link rel="canonical" href={`${process.env.API_URL}${uri || ''}`} />
		<meta property="og:url" content={`${process.env.API_URL}${uri || ''}`} />

		{!description === '' ? (
			<>
				<meta name="description" content={sanitizedMetaDescription} />
				<meta property="og:description" content={sanitizedMetaDescription} />
				<meta name="twitter:description" content={sanitizedMetaDescription} />
			</>
		) : ''}

		{image ? (
			<>
				<meta property="og:image" content={image} />
				<meta name="twitter:image" content={image} />
			</>
		) : ''}

		<meta property="og:type" content="website" />
		<meta property="og:site_name" content="Newhouse School at Syracuse University" />
		<meta name="twitter:site" content="@NewhouseSU" />
		<meta name="twitter:creator" content="@NewhouseSU" />
	</Helmet>;
};
export default SEO;
