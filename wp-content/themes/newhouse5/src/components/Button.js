import React from 'react';
import { Link } from 'react-router-dom';

import Icon from './Icon';
import ScreenReaderText from './ScreenReaderText';

const Button = ({ label }) => (
	<button className="btn">{label}</button>
);
const CTA = ({
	type = 'primary', label, path, colorMode = 'orange', onClickHandler,
}) => <Link to={`/${path}`} className={`btn btn--${type} btn--${colorMode}`} onClick={onClickHandler}>
	{label}
</Link>;

const External = ({
	type = 'primary', label, path, colorMode = 'orange',
}) => (
	<a href={path} rel="noreferrer" target="_blank" className={`btn btn--${type} btn--${colorMode}`}>{label}</a>
);

const UI = ({
	icon=null, id, colorMode, onClickHandler, label=null, screenText, className=null
}) => (
	<button
		id={id}
		className={`btn btn--ui btn--${colorMode} ${className || ''}`}
		onClick={onClickHandler}>
		{label || ''}
		{icon && <Icon icon={icon} />}
		{screenText && <ScreenReaderText text={screenText}/>}
	</button>
);

const Toggle = ({
	id, label, colorMode, onClickHandler,
}) => (
	<button
		id={id}
		className={`btn btn--toggle btn--${colorMode}`}
		onClick={onClickHandler}>
		{label}
	</button>
);

const Group = ({ children, justifyContent = null }) => {
	const alignClass = () => {
		if (justifyContent === 'center') {
			return 'justify-content-center';
		} if (justifyContent === 'flex-end') {
			return 'justify-content-flex-end';
		} if (justifyContent === 'flex-start') {
			return 'justify-content-flex-start';
		}
	};
	return (
		<div className={`btn--group ${justifyContent ? alignClass() : ''}`}>
			{children}
		</div>
	);
};

const ScrollIcon = ({ color, onClickHandler }) => <button
	id="scrollBtn"
	className={`btn btn--ui btn--${color}`}
	onClick={onClickHandler}
>
	<Icon.Scroll />
</button>;

const MobileMenu = ({ id, colorMode, onClickHandler }) => <button
	id={id}
	className={`btn btn--ui btn--${colorMode}`}
	onClick={onClickHandler}>
	<Icon icon="bars" />
</button>;

Button.CTA = CTA;
Button.External = External;
Button.Group = Group;
Button.UI = UI;
Button.MobileMenu = MobileMenu;
Button.Scroll = ScrollIcon;
Button.Toggle = Toggle;

export default Button;
