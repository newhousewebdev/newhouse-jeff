import React from 'react';
import { Link } from 'react-router-dom';

// utilities
import { filterNewsCategories } from '../../api/newsFunctions';

// components
import Card from './Card';
import Button from '../Button';

const News = ({ post }) => {
	const {
		title, uri, featuredImage, author, excerpt, customAuthorName, displayAuthorToggle, newsCategories
	} = post;
	const postAuthor = customAuthorName ? customAuthorName : author.name;

	const filteredNewsCategories = filterNewsCategories(newsCategories.nodes);

	return (
		<Card className="news">
			{featuredImage && 
				<Link to={`/${uri}`}>
				<Card.FeaturedImage featuredImage={featuredImage} />
				</Link>
			}
			<div className="card--content">
				{newsCategories &&
					<ul className="card--content--labels">
					{filteredNewsCategories.map((category, key) => {
						return <li key={key} className="card--content--labels--item">
							<Link to={`/news-category/${category.slug}`}>{category.name}</Link>
						</li>
					})}	
					</ul>
				}
				<h3 className="card--content--title">
					<Link to={`/${uri}`} dangerouslySetInnerHTML={{ __html: title }} />
				</h3>
				{excerpt && 
					<p className="card--content--excerpt" dangerouslySetInnerHTML={{ __html: excerpt }} />
				}
				<Button.Group justifyContent="left">
					<Button.CTA path={uri} label="Read more" type="text" colorMode="orange" />
				</Button.Group>
			</div>
		</Card>
	);
};
export default News;
