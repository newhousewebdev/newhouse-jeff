import React from 'react';
import { Link } from 'react-router-dom';

import Button from '../Button';

// extra div inside the card for padding
const Card = ({ children, className }) => (
	<article className={`card card--${className}`}>
		<div>
			{children}
		</div>
	</article>
);
const Content = ({ children, alignment = 'center' }) => <div className={`card--content text-align-${alignment}`}>{children}</div>;
Card.Content = Content;

const Title = ({
	title, path = null, undergraduateYear = '', graduateYear = '',
}) => {
	const fullTitle = `${title} ${undergraduateYear !== '' ? `&apos;${undergraduateYear.substring(2)}` : ''} ${graduateYear !== '' ? `G&apos;${graduateYear.substring(2)}` : ''}`;
	if (path) {
		return <h3 className="card--content--title"><Link to={`/${path}`} dangerouslySetInnerHTML={{ __html: fullTitle }} tabIndex="0"></Link></h3>;
	}
	return <h3 className="card--content--title" dangerouslySetInnerHTML={{ __html: fullTitle }} />;
};
Card.Title = Title;

const Position = ({ position }) => <p className="card--content--position" dangerouslySetInnerHTML={{ __html: position }} />;
Card.Position = Position;

const Company = ({ company }) => <p className="card--content--company" dangerouslySetInnerHTML={{ __html: company }} />;
Card.Company = Company;

const CTA = ({ path, label, alignment = 'center' }) => <Button.Group justifyContent={alignment}>
	<Button.CTA path={path} label={label} type="text" colorMode="orange" />
</Button.Group>;
Card.CTA = CTA;

const DrupalProfileImage = ({ featuredImage, path = null }) => (
	<div className="card--profileImage--container">
		{path ? (
			<Link to={`/${path}`}>
				<img
					src={featuredImage.src}
					alt={featuredImage.alt}
					className="card--profileImage"
				/>
			</Link>
		) : (
			<img
				src={featuredImage.src}
				alt={featuredImage.alt}
				className="card--profileImage"
			/>
		)}

	</div>
);
Card.DrupalProfileImage = DrupalProfileImage;

const FeaturedImage = ({ featuredImage }) => (
	<div className="card--featuredImage--container">
		<img
			src={featuredImage.sourceUrl}
			alt={featuredImage.altText}
			className="card--featuredImage"
		/>
	</div>
);

Card.FeaturedImage = FeaturedImage;
const ProfileImage = ({ profileImage }) => (
	<div className="card--profileImage--container">
		<img
			src={profileImage.sourceUrl}
			alt={profileImage.altText}
			className="card--profileImage"
		/>
	</div>
);
Card.ProfileImage = ProfileImage;

const TopCap = ({ title }) => <div className="card--topcap">
	<h3 className="card--title" dangerouslySetInnerHTML={{ __html: title }} />
</div>;
Card.TopCap = TopCap;

const BottomCap = ({ person, description }) => {
	const { name, image } = person;
	return <div className="card--bottomcap">
		<img
			src={image.src}
			alt={image.alt}
			className="card--profileImage"
		/>
		<h3 className="card--bottomcap--name" dangerouslySetInnerHTML={{ __html: name }} />
		<p className="card--bottomcap--description" dangerouslySetInnerHTML={{ __html: description }} />
	</div>;
};
Card.BottomCap = BottomCap;

export default Card;
