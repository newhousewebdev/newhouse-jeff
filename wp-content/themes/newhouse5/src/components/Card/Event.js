import React from 'react';
import { Link } from 'react-router-dom';

import Card from './Card';
import Button from '../Button';
import Icon from '../Icon';

import { getDateFormatted } from '../../api/eventsFunctions';

const Event = ({ event }) => {
	const {
		title, slug, excerpt, startDateTime, imageUrl, imageDescription, region, organizers,
	} = event;

	// map the calendar image key values
	const featuredImage = {
		sourceUrl: imageUrl,
		altText: imageDescription,
	};

	const formattedDate = getDateFormatted(startDateTime);

	const filterOrganizerName = (organizer) => {
		if (organizer.name === 'Newhouse Career Development Center: Seminars' || organizer.name === 'Newhouse Career Development Center') {
			return 'Career Development Center';
		}
		return organizer.name;
	};

	return <Card className="event">
		{imageUrl
				&& <Link to={`/event/${slug}`}><Card.FeaturedImage featuredImage={featuredImage} /></Link>
		}
		<div className="card--content">
			{organizers
				&& <ul className="card--content--labels">
					{organizers.map((organizer, key) => <li key={key} className="card--content--labels--item">
						{filterOrganizerName(organizer)}
					</li>)}
				</ul>
			}
			<h3 className="card--content--title">
				<Link to={`/event/${slug}`} dangerouslySetInnerHTML={{ __html: title }} />
			</h3>
			{formattedDate
					&& <p className="card--content--startDate"><Icon icon="calendar-alt" />{formattedDate}</p>
			}
			{region
					&& <p className="card--content--region"><Icon icon="map-marker-alt" />{region}</p>
			}
			{excerpt
					&& <div className="card--content--excerpt" dangerouslySetInnerHTML={{ __html: excerpt }} />
			}
			<Button.Group justifyContent="left">
				<Button.CTA path={`event/${slug}`} label="View more" type="text" colorMode="orange" />
			</Button.Group>
		</div>
	</Card>;
};

export default Event;
