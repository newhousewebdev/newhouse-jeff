import News from './News';
// import Person from './Person';
import Event from './Event';
// import Default from './Default';

const Cards = ({ children }) => ({ children });

// Does not seem to be used
// Cards.Default = Default;
// Does not seem to be used
// Cards.Person = Person;
Cards.Event = Event; // yes this is used
// Does not seem to be used
Cards.News = News;

export default Cards;
