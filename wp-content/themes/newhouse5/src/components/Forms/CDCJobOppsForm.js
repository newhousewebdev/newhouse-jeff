import React from 'react';

import Row from '../Layout/Row';
import Col from '../Layout/Col';
import Form from '../Form/Form';

const CDCJobOppsForm = () => {
	return <Form>
		<h3>CDC Job opps</h3>
		<Form.Group>
			<Form.Label htmlFor="job-title">
				Job title
			</Form.Label>
			<Form.Input 
				type="text"
				placeholder="Enter job title..."
				id="job-title"
				name="job-title"
			/>
		</Form.Group>
		<Form.Group>
			<Form.Label htmlFor="job-description">
				Job description
			</Form.Label>
			<Form.Textarea
				type="text"
				placeholder="Enter job description..."
				id="job-title"
				name="job-title"
			/>
		</Form.Group>
		<Form.Group>
			<Form.Label htmlFor="job-title">
				Company name
			</Form.Label>
			<Form.Input
				type="text"
				placeholder="Enter company name..."
				id="company-name"
				name="company-name"
			/>
		</Form.Group>
	</Form>
}
export default CDCJobOppsForm;
