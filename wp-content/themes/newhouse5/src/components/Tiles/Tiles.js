import React, { useContext } from 'react';
import { AnimatePresence, motion } from 'framer-motion';

import Tile from './Default';

import {
	TilesContext,
} from '../../api/ContextAPI';

const Tiles = ( { type, children, activeDegree } ) => {
	const [isTilesOpen, setIsTilesOpen] = useContext(TilesContext);

	const variants = {
		open: {
			transition: {
				staggerChildren: 0.07,
				delayChildren: 0.3
			}
		},
		closed: {
			transition: {
				staggerChildren: 0.07,
				staggerDirection: -1
			}
		}
	};

	if (isTilesOpen === false) {
		setIsTilesOpen(true)
	}

	return (
		<AnimatePresence>
		{isTilesOpen && 
		<motion.ul
			initial="closed"
			variants={variants}
			animate={isTilesOpen ? "open" : "closed"}
			exit="closed"
			className={`tiles tiles--${type}`}>
			{children}
		</motion.ul>
		}	
		</AnimatePresence>
	)
}

export default Tiles;
