import React from 'react';
import { motion } from 'framer-motion';

import { Link } from 'react-router-dom';

const Tile = ({ children, activeDegree }) => {

	//console.log({ activeDegree });
	const variants = {
		open: {
			y: 0,
			opacity: 1,
			transition: {
				y: { stiffness: 1000, velocity: -100 },
			},
		},
		closed: {
			y: 50,
			opacity: 0,
			transition: {
				y: { stiffness: 1000 },
			},
		},
	};
	// console.log(uri);
	return (
		<motion.li
			initial="closed"
			variants={variants}
			className={`tile tile--${activeDegree ? activeDegree.toLowerCase() : 'bachelors'}`}
			//tabIndex="0"
		>
			{children}
		</motion.li>
	);
};
export default Tile;

const Hover = ({ uri, label="Learn more", type=null }) => {
	return <div 
	className="tile--hover" 
	>
		{type === "external" ? 
			<a tabIndex="0" href={uri} target="_blank">{label}</a>
			: <Link tabIndex="0" to={`/${uri}`}>{label}</Link>
		}
	</div>
	};
Tile.Hover = Hover;

const Title = ({ title }) => <h3 className="tile--title" dangerouslySetInnerHTML={{ __html: title }}></h3>;
Tile.Title = Title;

const Excerpt = ({ excerpt }) => <p className="tile--description" dangerouslySetInnerHTML={{ __html: excerpt }}></p>;
Tile.Excerpt = Excerpt;
