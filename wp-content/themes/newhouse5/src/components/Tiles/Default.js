import React from 'react';
import { motion } from 'framer-motion';

import { Link } from 'react-router-dom';

const Tile = ({ item, activeDegree }) => {
	const degreeName = (activeDegree) => {
		if (activeDegree === 'bachelors') {
			return 'BA in';
		} if (activeDegree === 'masters') {
			return 'MA in';
		} if (activeDegree === 'online') {
			return 'MA in';
		} if (activeDegree === 'doctoral') {
			return 'PhD in';
		}
	};

	const variants = {
		open: {
			y: 0,
			opacity: 1,
			transition: {
				y: { stiffness: 1000, velocity: -100 },
			},
		},
		closed: {
			y: 50,
			opacity: 0,
			transition: {
				y: { stiffness: 1000 },
			},
		},
	};
	const {
		id, title, excerpt, uri,
	} = item.node;
	// console.log(uri);
	return (
		<motion.li
			initial="closed"
			variants={variants}
			className={`tile tile--${activeDegree.toLowerCase()}`}
		>
			<div className="tile--hover"><Link to={`/${uri}`}>Learn more</Link></div>
			<h3 className="tile--title"><span>{degreeName(activeDegree)}</span><br />
				{title}</h3>
			<p className="tile--description" dangerouslySetInnerHTML={{ __html: excerpt }}></p>
		</motion.li>
	);
};
export default Tile;
