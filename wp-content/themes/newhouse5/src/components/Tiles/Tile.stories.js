import React from 'react';
import { motion } from "framer-motion";
import { action } from '@storybook/addon-actions';
import { withKnobs, object } from '@storybook/addon-knobs/react';

import { MemoryRouter } from 'react-router-dom';

import Tile from './Tile';



export default {
	component: Tile,
	title: 'Tile',
	// Our exports that end in "Data" are not stories.
	excludeStories: /.*Data$/,
	decorators: [
		//[withKnobs],
		getStory => <MemoryRouter>{getStory()}</MemoryRouter>
	]
};

export const tileData = {
	node: {
		id: '1',
		title: 'Advertising',
		excerpt: 'This is the advertising program description',
		uri: 'about',
	}
};

export const actionsData = {
	onClickTile: action('onClickTile'),
};

export const Default = () => {
	return <motion.ul
		animate="open"
		className="tiles tiles--grid">
		<Tile item={{...tileData}} {...actionsData} activeDegree="bachelors" />
	</motion.ul>
};
