import React from 'react';

const FormLabel = ({htmlFor, children, className}) => {
	return <label htmlFor={htmlFor} className={`form--label ${className ? className : ''}`}>{children}</label>
}
export default FormLabel;
