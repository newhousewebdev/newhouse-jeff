import React, {useState} from 'react';
import DatePicker from "react-datepicker";
import 'react-datepicker/dist/react-datepicker-cssmodules.css';

import Icon from '../Icon';
import Button from '../Button';

const Input = ({
	type, placeholder, id, name, value, ...props
}) => {
/*
button, checkbox, color, date, datetime-local, email,
file, hidden, image, month, number, password, radio, range,
reset, search, submit, tel, text, time, url, week
*/
	//console.log({type});
	if (type === 'text') {
		return (
			<input
				type="text"
				className="form--input"
				name={name}
				placeholder={placeholder}
				id={id}
			/>
		);
	} else if (type === "email") {
		return (
			<input
				type="email"
				name={name}
				className="form--input"
				placeholder={placeholder}
				id={id}
				{...props}
			/>
		);
	} else if (type === "url") {
		return (
			<input
				type="url"
				name={name}
				className="form--input"
				placeholder={placeholder}
				id={id}
			/>
		);
	} else if (type === "date") {
		const [startDate, setStartDate] = useState(new Date());
		return (
			<DatePicker
				dateFormat="yyyy-MM-dd"
				name={name} 
				selected={startDate} 
				onChange={date => setStartDate(date)} 
				className="form--input"
			/>
		);
	} else if (type === "tel") {
		return <input
			type="tel"
			className="form--input"
			id={id}
			name={name}
		/>
	} else if (type === "checkbox") {
		return (
			<input 
				type="checkbox" 
				className="form--input--checkbox"
				id={id} 
				name={name}
				value={value}
			/>
		);
	} else if (type === "hidden") {
		return (
			<input 
				type="text"
				className="form--input--hidden"
				defaultValue={value}
				name={name}
				id={id}
			/>
		)
	} else if (type === "submit") {
		return (
			<div className="btn--group">
				<Button.UI 
				id={id} 
				colorMode="orange" 
				label="Submit" 
				screenText="Submit form" 
				className="form--submit"
				/>
			</div>
			
		)
	}
};
const WithLeadingIcon = ({
	placeholder, icon, id, ref,
}) => (
	<div className="form--input--group">
		<Input type="text" placeholder={placeholder} id={id} ref={ref} />
		<Icon icon={icon} />
	</div>

);
Input.WithLeadingIcon = WithLeadingIcon;
export default Input;
