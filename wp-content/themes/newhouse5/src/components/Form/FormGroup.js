import React from 'react';

const Group = ({children}) => {
	return <div className="form--group">{children}</div>
}
export default Group;
