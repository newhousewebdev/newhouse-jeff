import React from 'react';
import Icon from '../Icon';

const FormControl = ({ type, onChange, placeholder, id, inputRef, className, defaultValue }) => {
	return <input
		type={type ? type : 'text'}
		className={`form--input form--input--${type} ${className ? className : ''}`}
		placeholder={placeholder}
		id={id}
		name={id}
		ref={inputRef}
		onChange={onChange}
		defaultValue={defaultValue}
	/>
}
const WithLeadingIcon = ({
	placeholder, icon, id, ref,
}) => (
		<div className="form--input--group">
			<FormControl type="text" placeholder={placeholder} id={id} ref={ref} />
			<Icon icon={icon} />
		</div>

	);
FormControl.WithLeadingIcon = WithLeadingIcon;
export default FormControl;
