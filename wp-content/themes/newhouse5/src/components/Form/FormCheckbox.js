import React, { Fragment } from 'react';
import Form from './Form';
import {ParseChildren} from './ContactForm7Parser';

const FormCheckbox = ({ node }) => {
	//console.log("FormCheckbox", {node});
	const {children} = node;
	return <div className="form--check">
		{children && children.length > 0 &&
			<ParseChildren children={children} />
		}
	</div>
}
export default FormCheckbox;
