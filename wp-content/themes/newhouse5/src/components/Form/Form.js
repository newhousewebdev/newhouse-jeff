import React, { useRef, useEffect } from 'react';

import { useHistory } from 'react-router-dom';

// components
import FormGroup from './FormGroup';
import FormCheckbox from './FormCheckbox';
import FormControl from './FormControl';
import FormLabel from './FormLabel';
import FormInput from './FormInput';
import FormRadio from './FormRadio';
import FormSelect from './FormSelect';
import FormSelectControl from './FormSelectControl';
import FormText from './FormText';
import FormTextarea from './FormTextarea';

// contexts

const Form = ({
	children,
	className = null,
	action = null,
	onSubmitHandler = null,
	id = null,
	method = null,
	...props
}) => (
	<form
		id={id}
		method={method}
		className={`form ${className || ''}`}
		action={action}
		onSubmit={onSubmitHandler}
		role="form"
		{...props}
	>
		{children}
	</form>
);

Form.Checkbox = FormCheckbox;
Form.Group = FormGroup;
Form.Control = FormControl;
Form.Label = FormLabel;
Form.Input = FormInput;
Form.Radio = FormRadio;
Form.Select = FormSelect;
Form.SelectControl = FormSelectControl;
Form.Text = FormText;
Form.Textarea = FormTextarea;

const MobileSearch = () => {
	// ref allows to focus the input on mount
	const textInput = useRef(null);

	useEffect(() => {
		textInput.current.focus();
	}, []);

	// on submit, navigate to /search path and use the search value
	const history = useHistory();

	const handleInputChange = (e) => {
		e.preventDefault();
		const searchValue = document.getElementById('mobileSearchValue').value;
		const searchValueFormatted = searchValue.split(' ').join('+');

		history.push(`/search/?q=${searchValueFormatted}`);
	};

	return (
		<Form onSubmitHandler={(e) => { handleInputChange(e); }}>
			<input
				type="text"
				className="form--input form--input--text"
				placeholder="Search newhouse.syr.edu"
				id="mobileSearchValue"
				ref={textInput}
			/>
		</Form>
	);
};
Form.MobileSearch = MobileSearch;

const PrimarySearch = () => {
	const textInput = useRef(null);

	useEffect(() => {
		textInput.current.focus();
	}, []);

	const history = useHistory();

	const handleInputChange = (e) => {
		e.preventDefault();
		const searchValue = document.getElementById('primarySearchValue').value;
		const searchValueFormatted = searchValue.split(' ').join('+');
		history.push(`/search/?q=${searchValueFormatted}`);
	};
	return (
		<Form onSubmitHandler={(e) => { handleInputChange(e); }}>
			<input
				type="text"
				placeholder="Search newhouse.syr.edu"
				className="form--input form--input--text"
				id="primarySearchValue"
				ref={textInput}
			/>
		</Form>
	);
};
Form.PrimarySearch = PrimarySearch;

export default Form;
