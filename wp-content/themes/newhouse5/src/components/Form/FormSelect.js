import React from 'react';

const FormSelect = ({
	options,
	onChange,
	value,
	label,
	defaultOptionLabel,
	name,
	id,
	selectRef,
	className
}) => {

	return <select
		value={value}
		id={id}
		name={name}
		className={`form--select ${className ? className : ''}`}
		onChange={onChange}
		ref={selectRef}
	>
		{defaultOptionLabel &&
			<option value>{defaultOptionLabel}</option>
		}
		{options}
	</select>
};

export default FormSelect;
