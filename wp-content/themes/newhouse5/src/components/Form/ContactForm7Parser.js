import React, { useState, useContext, useEffect, Fragment } from 'react';
import axios from "axios";
import Form from './Form';

import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
//import ApolloClient from 'apollo-boost';

import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';

import { ApolloLink } from 'apollo-link';
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import introspectionQueryResultData from '../../fragmentTypes.json';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';

const fragmentMatcher = new IntrospectionFragmentMatcher({
	introspectionQueryResultData,
});


import Container from '../Layout/Container';

const FormContext = React.createContext();

const debugForm = false;

const removeArrBracketsFromEndofName = (theName) => {
	return theName.replace(/\[]$/, '');
}

// main parsers
export const ParseChildren = ({ children }) => {
	debugForm ? console.log("ParseChildren", { children }) : '';

	return children.map((child, key) => {
		const { type } = child;
		if (type === "text") {
			return <ParseText key={key} node={child} />
		} else if (type === "tag") {
			return <ParseTag key={key} node={child} />
		}
	});
}

// sub-parsers
const ParseDiv = ({ node }) => {
	debugForm ? console.log("ParseDiv", { node }) : '';

	const { children, attribs } = node;
	const { class: className, role } = attribs;

	if (children && children.length > 0) {
		return <div className={className} role={role ? role : ''}><ParseChildren children={children} /></div>
	} else {
		//return <div className={className} role={role ? role : ''}>Empty DIV</div>
		return '';
	}
}

const ParseForm = ({node}) => {
	debugForm ? console.log("ParseForm", { node }) : '';
	
	const [formId, setFormId] = useState(null);
	const [messageSent, setMessageSent] = useState(false);
	const [errorMessage, setErrorMessage] = useState(null);

	const {children, attribs} = node;
	const {action, class:className, method } = attribs;
	const CONTACT_MUTATION = gql`
	mutation CreateSubmissionMutation(
		$clientMutationId: String!, 
		$job_title: String!, 
		$job_description: String!, 
		$company_name: String!, 
		$company_website_url: String!, 
		$location: String!, 
		$industries: String!, 
		$opportunity_type: String!, 
		$compensation: String!, 
		$external_posting_url: String!, 
		$expiration_date: String!, 
		$your_name: String!, 
		$phone_number: String!, 
		$email_address: String!, 
		$additional_notes: String!
		) {
			createOpportunity(input: {
				clientMutationId: $clientMutationId, 
				job_title: $job_title, 
				job_description: $job_description, 
				company_name : $company_name,
				company_website_url : $company_website_url,
				location : $location,
				industries : $industries,
				opportunity_type : $opportunity_type,
				compensation : $compensation,
				external_posting_url : $external_posting_url,
				expiration_date : $expiration_date,
				your_name : $your_name,
				phone_number : $phone_number,
				email_address : $email_address,
				additional_notes : $additional_notes,
			}) {
				success
				data
			}
		}
	`;
	const cache = new InMemoryCache({ fragmentMatcher });
	const client = new ApolloClient({
		link: new HttpLink({
			uri: 'https://resources.newhouse.syr.edu/cdc/graphql',
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			},
		}),
		cache,
	})

	return <Container.Content>
	<FormContext.Provider value={[formId, setFormId]}>
	<ApolloProvider client={client}>
	<Mutation mutation={CONTACT_MUTATION}>
		{(createSubmission, { loading, error, data }) => (
			<Fragment>
		<Form
			//action={action}
			className="mt--3"
			method={method}
			data-form-id={formId}
			onSubmitHandler={async event => {
				event.preventDefault();
				console.log(event.target);
				// formdata
				let formData = new FormData(event.target);

				console.log({formData});
				axios
					.post(
						`${process.env.SITE_URL}/wp-json/contact-form-7/v1/contact-forms/1193/feedback`,
						formData,
						{
							headers: {
								"content-type": "multipart/form-data",
							},
						}
					)
					.then(res => {
						console.log({ res });
						// reset form after sent
						res.data.status === "mail_sent" ? (
							<Fragment>
								{setMessageSent(res.data.message)}
							</Fragment>
						) : setErrorMessage(res.data.message)
					})

				const variables = {
					clientMutationId: 'example',
				}
				for (var pair of formData.entries()) {
					console.log(pair[0] + ', ' + pair[1]);
					variables[pair[0]] = pair[1];
				}
				
				createSubmission({
					variables: variables
				});
				
            }}
		>
			<ParseChildren children={children} />
			{messageSent && (
				<p>
					{messageSent}
				</p>
			)}
			{errorMessage && (
				<p>
					{errorMessage}
				</p>
			)}
		</Form>
		  <div>
            {loading && <p>Loading...</p>}
            {error && (
              <p>An unknown error has occured, please try again later...</p>
            )}
            {data && <p>Your job opportunity has been submitted to a database successfully.</p>}
          </div>
			</Fragment>
				)}
			</Mutation>
			</ApolloProvider>
	</FormContext.Provider>
	</Container.Content>
}

const ParseHeading = ({node, level}) => {
	debugForm ? console.log("parseHeading", { node }) : '';

	const {children, attribs} = node;
	const {class:className} = attribs;

	if (level === "h2") {
		return <h2 className={className}><ParseChildren children={children} /></h2>
	} else if (level === "h3") {
		return <h3 className={className}><ParseChildren children={children} /></h3>
	} else if (level === "h4") {
		return <h4 className={className}><ParseChildren children={children} /></h4>
	}
}

const ParseInput = ({ node }) => {
	debugForm ? console.log("parseInput", {node}) : '';
	
	const [formId, setFormId] = useContext(FormContext);

	const { type, name, value, class:className } = node.attribs;
	console.log({name});
	
	const sanitizedName = name ? removeArrBracketsFromEndofName(name) : null;
	console.log({ sanitizedName });
	console.log({ name });
	console.log("----------");
	if (type === "email") {
		console.log("email", { node });
	}
	useEffect(() => {
		name === "_wpcf7" ? setFormId(value) : '';
	}, []);

	return <Form.Input 
		type={type} 
		name={sanitizedName ? sanitizedName : name} 
		value={value}
		className={className} 
		aria-required="true" 
	/>;
}

const ParseLabel = ({node}) => {
	debugForm ? console.log("parseLabel", { node }) : '';

	const {attribs} = node;

	return <Form.Label htmlFor={attribs.for ? attribs.for : ''}>
		<ParseChildren children={node.children} />
	</Form.Label>;
}

const ParseParagraph = ({ node }) => {
	debugForm ? console.log("ParseParagraph", { node }) : '';

	const { children } = node;
	if (children && children.length > 0) {
		return <ParseChildren children={children} />
	} else {
		// Empty paragraph 
		return '';
	}
}

const ParseSpan = ({ node }) => {
	// WPCF7 uses span to create elements
	// pay attention to the classnames for each span
	debugForm ? console.log("ParseSpan", { node }) : '';

	const { children, attribs } = node;
	const { class: className } = attribs;

	const classArr = className.split(' ');

	debugForm ? console.log({ classArr }) : '';

	if (classArr.includes('wpcf7-form-control-wrap')) {
		return <ParseChildren children={children} />
	} else if (classArr.includes('wpcf7-checkbox')) {
		debugForm ? console.log("wpcf7-checkbox", { children }) : '';

		return children.map((node, key) => {
			return <Form.Checkbox key={key} node={node} />
		});

	} else if (classArr.includes('wpcf7-list-item-label')) {
		return <Form.Label>
			{children && children.length > 0 &&
				<ParseChildren children={children} />
			}
		</Form.Label>
	} else {
		return <div className={className}>
			{children && children.length > 0 &&
				<ParseChildren children={children} />
			}
		</div>
	}
}

const ParseTag = ({ node }) => {
	debugForm ? console.log("ParseTag", { node }) : '';

	const { name } = node;

	if (name === "div") {
		return <ParseDiv node={node} />
	} else if (name === "form") {
		return <ParseForm node={node} />;
	} else if (name === "h2") {
		return <ParseHeading node={node} level="h2" />
	} else if (name === "h3") {
		return <ParseHeading node={node} level="h3" />
	} else if (name === "h4") {
		return <ParseHeading node={node} level="h4" />
	} else if (name === "p") {
		return <ParseParagraph node={node} />
	} else if (name === "input") {
		return <ParseInput node={node} />
	} else if (name === "span") {
		return <ParseSpan node={node} />
	} else if (name === "label") {
		return <ParseLabel node={node} />
	} else if (name === "br") {
		//return <br />
		return '';
	} else if (name === "textarea") {
		return <ParseTextarea node={node} />;
	} else {
		return <p>{name}: ParseTag else</p>
	}
}


const ParseText = ({node}) => {
	debugForm ? console.log("ParseText", {node}) : '';

	return node.data === "↵" ? '' : node.data;
}

const ParseTextarea = ({ node }) => {
	debugForm ? console.log("ParseTextarea", { node }) : '';

	const { name } = node.attribs;
	console.log({ name });

	const sanitizedName = name ? removeArrBracketsFromEndofName(name) : null;
	console.log({ sanitizedName });
	console.log({ name });
	console.log("----------");

	return <Form.Textarea name={sanitizedName ? sanitizedName : name}  />
}


const ContactForm7Parser = ({children}) => {
	debugForm ? console.log("ContactForm7Parser", { children }) : '';

	return <ParseChildren children={children} />
		
}
export default ContactForm7Parser;
