import React from 'react';

const FormSelectControl = ({
	options,
	onChange,
	value,
	label,
	defaultOptionLabel,
	name
}) => {

	const selectChange = (event) => {
		const { target } = event;
		onChange(parseInt(target.value, 10));
	};

	function getRandomArbitraryNumber(min, max) {
		return Math.random() * (max - min) + min;
	}

	const uniqueID = `select-${getRandomArbitraryNumber(1, 100)}`;

	return <select 
		value={value ? value : "default"} 
		id={uniqueID} 
		name={name} 
		className="form--select" 
		onChange={selectChange}
		>
		<option key={uniqueID} value={null}>{defaultOptionLabel}</option>
		{options}
	</select>
	;
};

export default FormSelectControl;
