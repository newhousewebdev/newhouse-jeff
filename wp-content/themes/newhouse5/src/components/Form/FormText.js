import React from 'react';

const FormText = ({children, className}) => {
	return <p className={`form--note ${className ? className : ''}`}>{children}</p>
}
export default FormText;
