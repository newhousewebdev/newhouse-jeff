import React from 'react';

const FormTextarea = ({id=null, value, placeholder=null, name=null, inputRef, className}) => {
	return <textarea 
	className={`form--textarea ${className}`} 
	id={id ? id : ''}
	defaultValue={value}
	name={name ? name : ''} 
	rows="3" 
	ref={inputRef}
	placeholder={placeholder ? placeholder: ''}
	/>
}
export default FormTextarea;
