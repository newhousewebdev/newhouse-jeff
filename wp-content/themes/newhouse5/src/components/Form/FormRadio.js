import React, {Fragment} from 'react';

const FormRadio = ({name, items}) => {
	return items.map((item, key) => {
		return <div className="form--check" key={key}>
			<input type="radio" id={item.id} name={name} value={item.id} />
			<label htmlFor={item.id}>{item.label}</label><br />
		</div>
	})

}
export default FormRadio;
