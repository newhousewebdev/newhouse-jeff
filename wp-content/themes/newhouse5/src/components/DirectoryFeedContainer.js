import React, {
	useContext,
	useEffect,
	useState,
	Fragment,
} from 'react';
import CardFeed from './CardFeed';
import { ArchiveCollectionContext } from '../api/ContextAPI';
import { fetchFacultyAndStaff, fetchDirectoryWithOptions, performSpecifiedFetch } from '../api/fetches/DirectoryFetches';
import { buildDirectoryCards } from '../api/directoryUtilities';
import Loader from './Loader';

/* This fetches specific url or generally, includes options as well */
const DirectoryFeedContainer = ({ fetchUrl = undefined, options = undefined, ...props }) => {
	const [archiveCollection, setArchiveCollection] = useContext(ArchiveCollectionContext);
	const [loadingCollection, setLoadingCollection] = useState(true);

	useEffect(() => {
		setLoadingCollection(true);
		// ? refactor into a reusable hook
		// resets the collection every load: ie unnecessary fetches
		const getDirectoryData = async () => {
			// Create a switch here?
			if (options) {
				fetchDirectoryWithOptions(options)
					.then((directoryData) => {
						setArchiveCollection(directoryData);
						setLoadingCollection(false);
					})
					.catch((err) => { console.error(err); });
			} else if (fetchUrl) {
				performSpecifiedFetch(fetchUrl)
					.then((directoryData) => {
						setArchiveCollection(directoryData);
						setLoadingCollection(false);
					})
					.catch((err) => { console.error(err); });
			} else {
				fetchFacultyAndStaff()
					.then((directoryData) => {
						setArchiveCollection(directoryData);
						setLoadingCollection(false);
					})
					.catch((err) => { console.error(err); });
			}
		};

		getDirectoryData();
	}, []);

	return <Fragment>
		{loadingCollection ? <Loader />
			: <CardFeed cards={buildDirectoryCards(archiveCollection)} {...props}/>}
	</Fragment>;
};

export default DirectoryFeedContainer;
