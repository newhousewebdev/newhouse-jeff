import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

/*
	? Not sure how to categorize this behavior, seems to just be an effect and not a component per se.
*/
// scroll restoration
export default function ScrollToTop() {
	const { pathname } = useLocation();

	useEffect(() => {
		window.scrollTo(0, 0);
	}, [pathname]);

	return null;
}
