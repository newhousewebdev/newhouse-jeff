import React from 'react';
import NewsFeed from '../views/Page/NewsFeed';

const NewsFeedContainer = (attribs) => {
	const feedData = { per_page: 6 };
	// Abstract into a simplified function
	const dataParams = attribs['data-posts-feed'];
	const dataParamsArray = dataParams.split('&');
	const newsData = dataParamsArray.map((param) => {
		const valuePairs = param.split('=');
		/* per_page split here */
		if (valuePairs[0] === 'per_page') {
			// eslint-disable-next-line prefer-destructuring
			feedData.per_page = valuePairs[1];
			return false;
		}
		return ({
			key: valuePairs[0].replace('-', '').toUpperCase(),
			termIDs: valuePairs[1].split(','),
		});
	}).filter((param) => param);

	/* Only using Title right now */
	const displayDataArray = attribs['data-posts-feed-display'].split('&');
	const displayData = displayDataArray.reduce((object, param) => {
		const valuePairs = param.split('=');
		const paramObject = {
			[valuePairs[0].replace('-', '')]: valuePairs[1].replace('+', ' '),
		};
		/* per_page split here */
		return ({ ...object, ...paramObject });
	}, {});

	const { title } = displayData;

	return <NewsFeed feedData={feedData} newsData={newsData} title={title}/>;
};

export default NewsFeedContainer;
