import React from 'react';
import Loader from './Loader';

/**
 * A higher Order Component that returns a Loader Component or the InnerComponent: isLoading = true
 *  @param {Component} InnerComponent that is wrapped by WithLoading
 *  @param {Boolean} isLoading - condition to be met to return the inner component
 */
const WithLoading = (InnerComponent) => function withLoadingComponent({
	isLoading, ...props
}) {
	return !isLoading ? <InnerComponent {...props} /> : <Loader />;
};

export default WithLoading;
