import React, { useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';

// contexts
import {
	MobileContext,
	TilesContext,
} from '../api/ContextAPI';

const ToggleBar = ({
	path, label, degrees, activeDegree, handleOnChange
}) => {
	const history = useHistory();
	console.log({path});
	const [isMobile, setIsMobile] = useContext(MobileContext);
	const [isTilesOpen, setIsTilesOpen] = useContext(TilesContext);

	return (
		<div className="toggle-bar">
			<div className="container">
				{isMobile ? (
					<div className="form--row">
						<div className="form--col">
							<form action="" className="form">
								<label className="form--label" htmlFor="degrees">{label}</label>
								<select className="form--select" name="degrees" id="degreesSelectList" onChange={(e) => {
									e.preventDefault();
									handleOnChange(e.target.value);
									history.push(`${path}/${e.target.value}`);
								}}>
									{degrees.map((degree, key) => {
										const { name, slug } = degree.node;
										return (
											<option key={key} value={slug}>{name}</option>
										);
									})}
								</select>
							</form>
						</div>
					</div>
				) : (
					<div className = "toggle-bar--row">
						<span className = "toggle-bar--label">{ label }</span>
						<ul className="toggle-bar--list">
							{degrees.map((degree, key) => {
								const { name, slug } = degree.node;
								return (
									<li key={key}
										className={`toggle-bar--list--item ${activeDegree === slug ? 'active' : ''}`}
									>
										<Link
											to={`${path}/${slug}`}
											onClick={(e) => {
												e.preventDefault();
												handleOnChange(slug);
												history.push(`${path}/${slug}`);
											}}
										>{name}</Link>
									</li>
								);
							})}
						</ul>
					</div>
				)
				}

			</div>
		</div>
	);
};
export default ToggleBar;
