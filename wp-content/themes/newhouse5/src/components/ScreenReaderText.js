import React from 'react';

const ScreenReaderText = ({ text }) => <span className="screen-reader-text">{text}</span>;

export default ScreenReaderText;
