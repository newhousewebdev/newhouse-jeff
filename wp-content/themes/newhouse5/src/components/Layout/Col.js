import React from 'react';
import classNames from 'classnames';

const Col = ({
	xs, sm, md, lg, xl, children, className = null,
}) => {
	const columnClasses = classNames('col', // default first
		{
			[`col--xs--${xs}`]: xs, // responsive classes
			[`col--sm--${sm}`]: sm,
			[`col--md--${md}`]: md,
			[`col--lg--${lg}`]: lg,
			[`col--xl--${xl}`]: xl,
			[className]: className, // any trailing classes
		});
	return <div className={columnClasses}>{children}</div>;
};
export default Col;
