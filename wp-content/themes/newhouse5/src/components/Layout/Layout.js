import React, { Fragment, useContext, useEffect } from 'react';

// hooks
// context
import {
	MobileContext,
} from '../../api/ContextAPI';

// components
import Header from '../../regions/Header';
import NavMobile from '../../regions/Nav/NavMobile';
import Footer from '../../regions/Footer';

const Layout = ({ children }) => {
	// const initTracking = usePageTracking();
	const [isMobile, setIsMobile] = useContext(MobileContext);
	const handleWindowSizeChange = () => {
		// ! needs to match the media query!
		const isWindowMobile = window.innerWidth < 992;
		setIsMobile(isWindowMobile);
	};

	useEffect(() => {
		window.addEventListener('resize', handleWindowSizeChange);
		window.addEventListener('load', handleWindowSizeChange);
	}, []);

	return <Fragment>
		<NavMobile />
		<Header />
		<div className="layout">
			{children}
		</div>
		<Footer />
	</Fragment>
};

export default Layout;
