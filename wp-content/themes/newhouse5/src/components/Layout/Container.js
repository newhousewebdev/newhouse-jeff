import React from 'react';

const Container = ({ children }) => <div className="container">
	{children}
</div>;

const Fluid = ({ children }) => <div className="container--fluid">
	{children}
</div>;
Container.Fluid = Fluid;

const Content = ({ children }) => <div className="container--content">
	{children}
</div>;
Container.Content = Content;

export default Container;
