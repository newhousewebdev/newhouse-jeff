import React from 'react';

const Row = ({children, className=null}) => {
	return <div className={`row ${className ? className: ''}`}>{children}</div>
}
export default Row;
