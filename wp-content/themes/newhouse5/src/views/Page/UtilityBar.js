import React, { useState, useContext } from 'react';
import { Link } from 'react-router-dom';
import Icon from '../../components/Icon';

// contexts
import { MobileContext } from '../../api/ContextAPI';

const UtilityBar = ({ sticky, title, subpages }) => {
	const [isUtilityBarOpen, setUtilityBarOpen] = useState(false);
	const [isMobile, setIsMobile] = useContext(MobileContext);

	console.log({subpages});

	return <div className={`page--utility-bar ${sticky ? "sticky" : ""}`}>
		<div className="container--fluid">
			<div className="row justify-content-between">
				<div className="col">
					<div className="row justify-content-between">
						<div className="col">
						<span className={`title ${isUtilityBarOpen ? 'active' : ''}`} onClick={() => {
							console.log("clicked on title");
							if (isMobile && isUtilityBarOpen) {
								setUtilityBarOpen(false);
							} else {
								setUtilityBarOpen(true);
							}
						}}>
							<strong>{title}</strong>
							<Icon icon="angle-down" />
						</span>
						</div>
					</div>
				</div>
				<div className="col">
					<nav className={`utility-bar-nav ${isUtilityBarOpen ? 'active' : ''}`}>
					<ul>
						{subpages.map((subpage, key) => {
							const {
								label, slug, uri,
							} = subpage.node;
							return <li key={key}><Link to={`/${uri}`}>{label}</Link></li>
						})}
					</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
}
export default UtilityBar;
