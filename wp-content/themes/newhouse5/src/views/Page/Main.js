import React from 'react';

const Main = ({ children }) => (
	<div className="page--main">
		{children}
	</div>
);
export default Main;
