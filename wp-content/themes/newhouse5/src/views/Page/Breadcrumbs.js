import React from 'react';
import { Link } from 'react-router-dom';

const Breadcrumbs = ({ sectionParent, currentPage, parentItems }) => {
	const degreeSlugArr = ['bachelors', 'masters', 'online', 'doctoral'];

	const parentArr = [];

	if (currentPage) {
		parentArr.push(currentPage);
	}
	if (parentItems) {
		if (!degreeSlugArr.includes(parentItems.slug)) {
			parentArr.push({
				title: parentItems.title,
				uri: parentItems.uri,
			});
		}
		if (parentItems.hasOwnProperty('parent') && parentItems.parent !== null) {
			// if not null
			if (parentItems.parent) {
				parentArr.push({
					title: parentItems.parent.title,
					uri: parentItems.parent.uri,
				});
			}
			if (parentItems.parent.hasOwnProperty('parent') && parentItems.parent !== null) {
				// if not null
				if (parentItems.parent.parent) {
					parentArr.push({
						title: parentItems.parent.parent.title,
						uri: parentItems.parent.parent.uri,
					});
				}
			}
		} else {
			// console.log('parentItems does not hasOwnProperty(parent)');
		}
	} // end if parentItems

	if (sectionParent) {
		parentArr.push(sectionParent);
	}
	const reversedParentsArr = parentArr.reverse();
	// console.log({ reversedParentsArr});
	return (
		<div className="breadcrumbs">
			<div className="breadcrumbs--list--scroll-container">
				<ul className="breadcrumbs--list">
					{reversedParentsArr.map((item, key) => <li key={key} className="breadcrumbs--list--item">
						<Link to={`/${item.uri ? item.uri : ''}`}>
							<span dangerouslySetInnerHTML={{ __html: item.title }}></span>
						</Link>
					</li>)}
				</ul>
			</div>
		</div>
	);
};
export default Breadcrumbs;
