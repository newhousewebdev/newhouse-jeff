import React from 'react';
// import Card from '../../components/Card/Card';
import { buildDirectoryCards } from '../../api/directoryUtilities';

const RelatedContent = ({ collection = [] }) => <div className="page--related-content text-align-center">
	<h3 className="section--label">Related Faculty</h3>
	<div className="page--related-content--container">
		<div className="page--related-content--row">
			{buildDirectoryCards(collection)}
		</div>
	</div>
</div>;

export default RelatedContent;
