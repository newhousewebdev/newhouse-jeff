import React from 'react';

import Layout from '../../components/Layout/Layout';
import Breadcrumbs from './Breadcrumbs';
import Content from './Content';
import FeaturedImage from './FeaturedImage';
import Filters from './Filters';
import Hero from './Hero';
import Intro from './Intro';
import Main from './Main';
import Section from './Section';
import Sidebar from './Sidebar';
import RelatedContent from './RelatedContent';
import Subpage from './Subpage';
import ToggleBar from '../../components/ToggleBar';
import UtilityBar from './UtilityBar';

// Refactor SEO into here?
const Page = ({ children, className }) => <Layout><main className={className}>{children}</main></Layout>;

// sub components
Page.Breadcrumbs = Breadcrumbs;
Page.Content = Content;
Page.FeaturedImage = FeaturedImage;
Page.Filters = Filters;
Page.Hero = Hero;
Page.Intro = Intro;
Page.Main = Main;
Page.RelatedContent = RelatedContent;
Page.Section = Section;
Page.Sidebar = Sidebar;
Page.Subpage = Subpage;
Page.ToggleBar = ToggleBar;
Page.UtilityBar = UtilityBar;

export default Page;
