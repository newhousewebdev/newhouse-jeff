import React from 'react';

import Section from '../../../regions/Section/Section';
import Slider from '../../../components/Slider';
import Card from '../../../components/Card/Card';

const nodes = [
	{
		"name": "Alexia Tsairis Chair for Documentary Photography",
		"description": "The Alexia Chair supports photographers whose work contributes to the improvement of the human condition. It was established by Peter and Aphrodite Tsairis, parents of Pan Am 103 victim Alexia Tsairis, who was a Newhouse photojournalism student. <a title=\"Mike Davis bio\" href=\"\/people/davis-mike\" target=\"_self\">Mike Davis <\/a> holds the position.",
		"person" : {
			"name" : "Mike Davis",
			"slug": "davis-mike",
			"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/davis_mike_-_sized1.jpg",
		},
	},
	{
		"name": "David J. Levidow Endowed Professor",
		"description": "The David J. Levidow Endowed Professor was created by Newhouse alumnus David Levidow '48. <a title=\"T. Makana Chock bio\" href=\"\/people/chock-makana\" target=\"_self\">T. Makana Chock<\/a> holds the position. Chock has conducted internationally recognized research in the area of media psychology, and studies the ways in which people process and respond to persuasive messages in mass media, social media and extended reality contexts.",
		"person": {
			"name": "T. Makana Chock",
			"slug": "chock-makana",
			"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/chock-m1.jpg",
		},
	},
	{
		"name": "Horvitz Endowed Chair in Journalism Innovation",
		"description": "Funded by Peter Horvitz \u201976, chairman, president and CEO of Horvitz Newspapers LLC, the Endowed Chair in Journalism Innovation was established to develop new courses that allow students to explore the intersection of journalism and technology. Professor <a title=\"Dan Pacheco bio\" href=\"\/people/pacheco-dan\" target=\"_self\">Dan Pacheco<\/a> holds the position. ",
		"person": {
			"name": "Dan Pacheco",
			"slug": "pacheco-dan",
			"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/pacheco-dan-20201.jpg",
		},
	},
	{
		"name": "John Ben Snow Endowed Research Chair",
		"description": "The John Ben Snow Endowed Research Chair was established by the John Ben Snow Foundation, whose mission is to make grants to enhance the quality of life in Central and Northern New York state. Professor <a title=\"Lars Willnat bio\" href=\"\/people/willnat-lars\" target=\"_self\">Lars Willnat<\/a> holds the position.",
		"person": {
			"name": "Lars Willnat",
			"slug": "willnat-lars",
			"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/willnat_lars1.jpg",
		},
	},
	{
		"name": "Knight Chair in Data and Explanatory Journalism",
		"description": "The Knight Chair in Data and Explanatory Journalism is supported by the John S. and James L. Knight Foundation. The position was established to encourage students to tell deeper stories and produce quality journalism using the tools of the 21st century. <a title=\"Jodi Upton bio\" href=\"\/people/upton-jodi\" target=\"_self\">Jodi Upton<\/a>, a data journalist with 20 years of experience, holds the position.",
		"person": {
			"name": "Jodi Upton",
			"slug": "upton-jodi",
			"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/upton_resized1.jpg",
		},
	},
	{
		"name": "Newhouse Endowed Chair of Public Communications",
		"description": "<a title=\"Carol Liebler bio\" href=\"\/people/liebler-carol\" target=\"_self\">Carol Liebler<\/a> is the Newhouse Endowed Chair of Public Communications. The position is awarded to a distinguished faculty member whose accomplishments significantly advance the school\u2019s reputation in research and creative activity.",
		"person": {
			"name": "Carol Liebler",
			"slug": "liebler-carol",
			"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/liebler-c1.jpg",
		},
	},
	{
		"name": "Trustee Professor of Television & Popular Culture",
		"description": "<a title=\"Robert Thompson bio\" href=\"\/people/thompson-robert\" target=\"_self\">Robert Thompson<\/a> is one of the most quoted experts on popular culture in the world. At Newhouse, he serves as director of the <a href=\"http:\/\/tvcenter.syr.edu\" target=\"_blank\" title=\"Bleier Center for Television and Popular Culture\">Bleier Center for Television and Popular Culture<\/a>, which is supported by a gift from longtime media executive and Newhouse alumnus Edward Bleier '51.",
		"person": {
			"name": "Robert Thompson",
			"slug": "thompson-robert",
			"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/thompson_robert1.jpg",
		},
	}
];

const NamedChairs = () => {
	return <Section className="research--named-chairs" container="fluid">
		<Section.Header title="Named Chairs and Professorships" alignment="center" />
		<div className="container">
			<Slider id="namedChairsSlider">
				{nodes.map((node, key) => {
					const {name, description, person} = node;
					return <Card key={key} className="information">
						<Card.TopCap 
							title={name}
						/>
						<Card.BottomCap
							person={{
								name: person.name,
								image: {
									src: person.image,
									alt: person.name
								},
							}}
							description={description}
						/>
					</Card>
				})}
			</Slider>
		</div>
	</Section>
}
export default NamedChairs;
