import React from 'react';

import Section from '../Section';

const ResearchClusters = () => {
	return <Section className="research--clusters" container="content">
		<h2 className="section--title">Research Clusters</h2>
		<div className="gutenberg--content">
			<div className="lede">
				<p>Syracuse University supports several interdisciplinary and multidisciplinary themes or “clusters” that serve to strengthen research activity, enhance faculty diversity and develop new opportunities for student research and learning.</p>
			</div>
		</div>
		<article className="row cluster">
			<div className="cluster--left">
				<h3>Citizenship and Democratic Institutions</h3>
			</div>
			<div className="cluster--right">
				<p>The Citizenship and Democratic Institutions cluster draws together researchers who address critical issues related to the multiple and contested meanings of local, national and global citizenship, and how diverse institutions, such as journalism and mass media, law, government, politics, the military and the market, influence civic engagement and social and economic well-being.</p>
			</div>
		</article>
		<article className="row cluster">
			<div className="cluster--left">
				<h3>Social Differences / Social Justice</h3>
			</div>
			<div className="cluster--right">
				<p>The interdisciplinary Social Differences, Social Justice cluster places Syracuse University scholars at the center of national and global discussions of social differences. By attending to racial, ethnic, indigenous, LGBT and gender identities; understandings of culture; ability; and disability, the cluster is centered around the pursuit of just futures. In recent years, racial, sexual and economic inequalities have dominated the headlines.</p>
			</div>
		</article>
	</Section>

}
export default ResearchClusters;
