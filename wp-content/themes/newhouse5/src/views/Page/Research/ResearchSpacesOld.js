import React, {useState} from 'react';
import { motion } from 'framer-motion';

import Section from '../Section';
import Button from '../../../components/Button';

const spacesArr = [
	{
		"name": "Emerging Insights Lab",
		"slug": "emerging-insights-lab",
		"description": "<p>The W2O Emerging Insights Lab, funded through the generosity of Jim Weiss \u201987, CEO and founder of the W2O group, and his wife Audra Weiss \u201989, is a multi-disciplinary social media research command center in the heart of Newhouse. As part of an active classroom, a bank of several large interactive monitors track the convergence of paid, earned, shared and owned media in one place, enabling students and faculty to assess in real time the impact of media on specific brands or campaigns.<\/p>"
	},
	{
		"name" : "M.I.N.D. Labs",
		"slug" : "mind-lab",
		"description": "<p>The Media Interface and Networking Design (M.I.N.D.) Labs are a network of labs dedicated to groundbreaking research in communication and human-computer interaction. Centered at the Newhouse School, the research projects pursued at the M.I.N.D. Labs examine ways in which media interfaces affect human cognition and performance, and how the mind can be augmented or enhanced by media interfaces, applications or messages.<\/p>"
	},
	{
		"name": "Experimental Media Lab",
		"slug": "experimental-media-lab",
		"description": "<p>Since 1998, the Experimental Media Lab has been an active new media research and development center within the Newhouse School. Beginning as the joint Newhouse\/iSchool Convergence Center, the lab has allowed Newhouse graduate students to experiment with digital content management systems and digital communications technologies. In 2008, the Lab was endowed by Tom and Lisa Mandel, and since that time graduates of the associated new media programs have gone on to lead development in voice-powered AI, advanced digital communications, and even start companies with global reach.<\/p>"
	},
];

const ResearchLabs = () => {
	const [currentSpace, setCurrentSpace] = useState(0);
	const totalSpaces = 3;

	const spaceVariants = {
		open: {
			x: 0,
			opacity: 1,
			transition: {
				duration: .5,
				when: "beforeChildren",
				staggerChildren: .05,
				//x: { stiffness: 1000, velocity: -100 },
			},
		},
		closed: {
			x: 100,
			opacity: 0,
			transition: {
				duration: .5,
				when: "afterChildren",
				//x: { stiffness: 1000 },
			},
		},
	};

	const handleNextClick = (e) => {
		e.preventDefault();
		currentSpace < (totalSpaces-1) ? setCurrentSpace(currentSpace + 1) : setCurrentSpace(0);
	}
	const handlePreviousClick = (e) => {
		e.preventDefault();
		currentSpace <= 0 ? setCurrentSpace(totalSpaces - 1) : setCurrentSpace(currentSpace - 1);
	}
	const getPosition = (index) => {
		const currentRightDeck = currentSpace === totalSpaces - 1 ? 0 : currentSpace + 1;
		const currentLeftDeck = currentSpace === 0 ? totalSpaces - 1 : currentSpace - 1;
		if (index === currentSpace) {
			return 'active';
		} else if (index === currentRightDeck) {
			return 'rightDeck';
		} else if (index === currentLeftDeck) {
			return 'leftDeck';
		}
	}

	return <Section className="research--spaces">
		<h2>Research Spaces</h2>
		<div className="spaces--container">
			<div className="spaces">
				<div className={`space`}>
					<div className={`space--left`}>
						<div className="spaces--controls top">
							<Button.UI
								icon="angle-left"
								id="spaceNext"
								colorMode="orange"
								onClickHandler={handlePreviousClick}
								className="mobile-only"
							/>
							<p className="mobile-only">{currentSpace + 1} of {totalSpaces}</p>
							<Button.UI
								icon="angle-right"
								id="spaceNext"
								colorMode="orange"
								onClickHandler={handleNextClick}
							/>
						</div>
						<ul>
							{spacesArr.map((space, key) => {
								return <li key={key} className={`space--image ${space.slug} ${getPosition(key)}`}></li>
							})}
						</ul>
						<div className="spaces--controls bottom">
							<p className="desktop-only">{currentSpace + 1} of {totalSpaces}</p>
							<Button.UI
								icon="angle-left"
								id="spacePrevious"
								colorMode="orange"
								onClickHandler={handlePreviousClick}
								className="desktop-only"
							/>
						</div>
					</div>
					<div className="space--right">
							<motion.ul>
							{spacesArr.map((space, key) => {
								return <motion.li 
								key={key} 
								initial="closed"
								animate={currentSpace === key ? 'open' : 'closed'}
								variants={spaceVariants}
								className={currentSpace === key ? 'active' : ''}>
									<motion.h3 
									initial="closed"
										variants={spaceVariants}
									className="space--content--name" dangerouslySetInnerHTML={{ __html: space.name }}></motion.h3>
									<motion.div 
									initial="closed"
										variants={spaceVariants}
									className="space--content--description" dangerouslySetInnerHTML={{ __html: space.description }}></motion.div>
								</motion.li>
							})}
							</motion.ul>
							
						</div>
					</div>
			</div>
			
		</div>
		
	</Section>

}
export default ResearchLabs;
