import React from 'react';
import { useQuery } from 'react-apollo';

// query
import GetResearchNewsCategory from '../../../queries/research/researchNewsCategory.gql';

// components
import Loader from '../../../components/Loader';
import Section from '../../../regions/Section/Section';
import Slider from '../../../components/Slider';
import Card from '../../../components/Card/Card';
import Button from '../../../components/Button';

const facultyProfilesArr = [
	{
		"name": "Hua Jiang",
		"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/jiang_hua_01.jpg",
		"job" : [
			{
				"title": "Interim Chair",
				"dept" : "Public Relations"
			},
			{
				"title": "Associate Professor",
				"dept": "Public Relations"
			},
		]
	},
	{
		"name": "Stephen Masiclat",
		"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/masiclat-s1.jpg",
		"job": [
			{
				"title": "Professor",
				//"dept": "Public Relations"
			},
			{
				"title": "Director",
				"dept": "New Media Management"
			},
		]
	},
	{
		"name": "Anne Osborne",
		"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/osborne_anne1.jpg",
		"job": [
			{
				"title": "Professor",
				"dept": "Communications"
			},
		]
	},
	{
		"name": "Harriet Brown",
		"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/harriet-brown-saved-for-web1.jpg",
		"job": [
			{
				"title": "Professor",
				"dept": "Magazine, News and Digital Journalism"
			},
		]
	},
	{
		"name": "Joon Soo Lim",
		"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/lim_joon_soo_01.jpg",
		"job": [
			{
				"title": "Associate Professor",
				"dept": "Public Relations"
			},
		]
	},
	{
		"name": "Nina Iacono Brown",
		"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/brownresized1.jpg",
		"job": [
			{
				"title": "Assistant Professor",
				"dept": "Communications"
			},
		]
	},
	{
		"name": "Rebecca Ortiz",
		"image": "https://resources.newhouse.syr.edu/directory/wp-content/uploads/sites/6/2020/07/ortiz_resized1.jpg",
		"job": [
			{
				"title": "Assistant Professor",
				"dept": "Advertising"
			},
		]
	}

]

const CreativeProfiles = () => {
	const { loading, error, data } = useQuery(GetResearchNewsCategory, {
		variables: {
			slug: "creative-profiles",
		},
	});
	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const {nodes} = data.newsCategories.posts;

	const getProfileImageSrc = (name) => {
		const match = facultyProfilesArr.find(element => element.name === name);
		return match.image;
	}
	const getJob = (name) => {
		const match = facultyProfilesArr.find(element => element.name === name);
		const jobs = match.job.map((job, key) => {
			return <li key={key}>
				<h4 className="card--content--position">{job.title}</h4>
				<h5 className="card--content--company">{job.dept}</h5>
			</li>
		});
		return <ul>{jobs}</ul>
	}

	return <Section className="section section--research--creative-profiles" container="fluid">
			<Section.Header title="Featured Researchers" alignment="center" />
			<div className="container">
			<Slider id="featuredResearchersSlider">
					{nodes.map((post, key) => {
						const { title, slug, author, excerpt, featuredImage, uri } = post;

						// secretKey is our temporary way of connecting the data array above
						const secretKey = featuredImage.altText;
						return <Card key={key} className="people">
							<div className="card--top">
								{featuredImage
									&& <Card.DrupalProfileImage featuredImage={{
										src: getProfileImageSrc(secretKey),
										alt: title
									}} />
								}
								<div className="card--content text-align-center">
									<h3 className="card--content--title" dangerouslySetInnerHTML={{ __html: secretKey }} />
									{getJob(secretKey)}
									<h4 className="card--content--articleTitle" dangerouslySetInnerHTML={{ __html: `"${title}"` }} />
								</div>
							</div>
							{uri
								&& <Button.Group justifyContent="center">
								<Button.CTA path={uri} label="Read profile" type="text" colorMode="orange"  />
								</Button.Group>
							}
						</Card>
					})}
			</Slider>
			</div>
	</Section>
}
export default CreativeProfiles;
