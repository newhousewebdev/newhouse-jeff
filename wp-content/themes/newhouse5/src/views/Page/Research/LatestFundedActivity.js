import React from 'react';
import { useQuery } from 'react-apollo';

// query
import postsByNewsCategoryQuery from '../../../queries/postsByNewsCategory.gql';

// components
import Loader from '../../../components/Loader';
import Slider from '../../../components/Slider';
import Cards from '../../../components/Card/Cards';

const ResearchNews = () => {
	const { loading, error, data } = useQuery(postsByNewsCategoryQuery, {
		variables: {
			slug: "latest-funded-activity",
		},
	});
	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const { nodes } = data.newsCategories.posts;
	//console.log({ nodes });

	return <section className="section section--research--latest-funded-activity">

		<h2 className="section--label text-align-center">Latest Funded Activity</h2>
		<div className="container">
			<Slider id="latestFundedActivitySlider">
				{nodes.map((post, key) => {
					return <Cards.News key={key} post={post} />
				})}
			</Slider>
		</div>
	</section>
}
export default ResearchNews;
