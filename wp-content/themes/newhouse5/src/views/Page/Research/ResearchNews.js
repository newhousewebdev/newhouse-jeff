import React from 'react';
import { useQuery } from 'react-apollo';

// query
import postsByNewsCategoryQuery from '../../../queries/postsByNewsCategory.gql';

// components
import Loader from '../../../components/Loader';
import Slider from '../../../components/Slider';
import Section from '../../../regions/Section/Section';
import Cards from '../../../components/Card/Cards';

const ResearchNews = () => {
	const { loading, error, data } = useQuery(postsByNewsCategoryQuery, {
		variables: {
			slug: "research-and-scholarship",
		},
	});
	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const { nodes } = data.newsCategories.posts;
	//console.log({nodes});

	return <Section className="research--news" container="fluid">
		<h2 className="section--label text-align-center">Research News</h2>
		<div className="container">
			<Slider id="researchNewsSlider">
				{nodes.map((post, key) => {
					return <Cards.News key={key} post={post} />
				})}
			</Slider>
		</div>
	</Section>	
}
export default ResearchNews;
