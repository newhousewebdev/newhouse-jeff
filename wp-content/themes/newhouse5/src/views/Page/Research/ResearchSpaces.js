import React, {useState, Fragment} from 'react';
import { motion } from 'framer-motion';

import Section from '../Section';
import Button from '../../../components/Button';

const spacesArr = [
	{
		"name": "Emerging Insights Lab",
		"slug": "emerging-insights-lab",
		"description": "<p>The W20 Emerging Insights Lab, funded by Jim Weiss \u201987 and Audra Weiss \u201989, is a state-of-the-art social media command center that serves as a central hub for the interfacing of digital media monitored and researched by faculty and students throughout the S.I. Newhouse School of Public Communications. The lab serves as a multidisciplinary research center focusing on analytics, artificial intelligence, digital and emerging media as well as an active classroom, and shared lab environment for projects undertaken by faculty and students.<\/p>"
	},
	{
		"name" : "M.I.N.D. Labs",
		"slug" : "mind-lab",
		"description": "<p>The Media Interface and Networking Design (M.I.N.D.) Labs are a network of labs dedicated to groundbreaking research in communication and human-computer interaction. Centered at the Newhouse School, the research projects pursued at the M.I.N.D. Labs examine ways in which media interfaces affect human cognition and performance, and how the mind can be augmented or enhanced by media interfaces, applications or messages.<\/p>"
	},
	{
		"name": "Experimental Media Lab",
		"slug": "experimental-media-lab",
		"description": "<p>The W20 Emerging Insights Lab, funded by Jim Weiss \u201987 and Audra Weiss \u201989, is a state-of-the-art social media command center that serves as a central hub for the interfacing of digital media monitored and researched by faculty and students throughout the S.I. Newhouse School of Public Communications. The lab serves as a multidisciplinary research center focusing on analytics, artificial intelligence, digital and emerging media as well as an active classroom, and shared lab environment for projects undertaken by faculty and students.<\/p>"
	},
];

const ResearchLabs = () => {
	const [currentSpace, setCurrentSpace] = useState(0);
	const [spacesLocked, setSpacesLocked] = useState(false);
	const totalSpaces = 3;

	const spaceVariants = {
		open: {
			x: 0,
			opacity: 1,
			transition: {
				duration: .5,
				when: "beforeChildren",
				staggerChildren: .05,
				//x: { stiffness: 1000, velocity: -100 },
			},
		},
		closed: {
			x: 100,
			opacity: 0,
			transition: {
				duration: .5,
				when: "afterChildren",
				//x: { stiffness: 1000 },
			},
		},
	};

	const handleNextClick = (e) => {
		e.preventDefault();
		currentSpace < (totalSpaces-1) ? setCurrentSpace(currentSpace + 1) : setCurrentSpace(0);
	}
	const handlePreviousClick = (e) => {
		e.preventDefault();
		currentSpace <= 0 ? setCurrentSpace(totalSpaces - 1) : setCurrentSpace(currentSpace - 1);
	}
	const getPosition = (index) => {
		const currentRightDeck = currentSpace === totalSpaces - 1 ? 0 : currentSpace + 1;
		const currentLeftDeck = currentSpace === 0 ? totalSpaces - 1 : currentSpace - 1;
		if (index === currentSpace) {
			return 'active';
		} else if (index === currentRightDeck) {
			return 'rightDeck';
		} else if (index === currentLeftDeck) {
			return 'leftDeck';
		}
	}

	const handlePanelClick = (index) => {
		setSpacesLocked(true);
		setCurrentSpace(index);
	}
	const addSpaceClasses = (index) => {
		if (spacesLocked) {
			return currentSpace === index ? 'locked' : 'collapsed';
		} else {
			return '';
		}
	}

	return <Section title="Research Spaces" className="research--spaces">
		<div className="research--spaces--panels--relative">
			{spacesLocked &&
				<>
				<Button.UI id="spacesCloseBtn" colorMode="orange" icon="times" onClickHandler={() => { setSpacesLocked(false) }} />
				<Button.UI id="spacesPrevBtn" colorMode="orange" icon="angle-left" onClickHandler={() => { handlePreviousClick(event) }} />
				<Button.UI id="spacesNextBtn" colorMode="orange" icon="angle-right" onClickHandler={() => { handleNextClick(event) }} />
				</>
			}
			
			<div className="research--spaces--panels">
				{spacesArr.map((space, key) => {
					const { name, slug } = space;
					return <div
						key={key}
						className={`research--spaces--panel ${addSpaceClasses(key)}`}
						onClick={() => { handlePanelClick(key) }}
					>
						<div className="panel--bg" data-space={slug}></div>
						<div className="panel--text">
							<h3 dangerouslySetInnerHTML={{ __html: name }}></h3>
						</div>
					</div>
				})}
			</div>
		</div>
		<div className="research--spaces--content--container">
		{spacesLocked &&
			spacesArr.map((space, key) => {
				const { description } = space;
				return <div
					key={key}
					className={`research--spaces--content ${currentSpace === key ? 'active' : ''}`}
				>
					<div 
					className="container--content" 
					dangerouslySetInnerHTML={{ __html: description }} />	
				</div>
			})
		}
		</div>
	</Section>

}
export default ResearchLabs;
