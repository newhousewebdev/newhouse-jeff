import React from 'react';

const Filters = ({ title, children }) => <section className="section section--container section-blue">
	<div className="container">
		<h3 className="line-behind"><span dangerouslySetInnerHTML={{ __html: title }}></span></h3>
		{children}
	</div>
</section>;
export default Filters;
