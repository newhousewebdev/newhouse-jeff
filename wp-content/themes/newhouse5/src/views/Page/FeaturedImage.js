import React from 'react';

const FeaturedImage = ({ featuredImage }) => {
	const { sourceUrl, altText } = featuredImage;
	return (
		<div className="page--featured-image">
			<img src={sourceUrl} alt={altText} />
		</div>
	);
};
export default FeaturedImage;
