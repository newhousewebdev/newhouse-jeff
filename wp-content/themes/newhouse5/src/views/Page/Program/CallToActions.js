import React from 'react';
import { motion } from 'framer-motion';

import Tile from '../../../components/Tiles/Tile';

const CallToActions = ({degreeSlug=null}) => {
	//console.log({degreeSlug});
	const tilesVariants = {
		open: {
			opacity: 1,
			transition: {
				when: "beforeChildren",
				staggerChildren: 0.15,
			},
		},
		closed: {
			opacity: 0,
			transition: {
				when: "afterChildren",
			},
		},
	};
	const actionsArr = [
		[
			// bachelors
			{
				title: "Explore academic programs",
				link: {
					uri: "academics/programs/bachelors",
					type: "internal"
				},
				label: "Explore now"
			},
			{
				title: "Request more information",
				link: {
					uri: "https://www.syracuse.edu/admissions/undergraduate/contact/request-information/",
					type: "external"
				},
				label: "Learn more"
			},
			{
				title: "Schedule a visit",
				link: {
					uri: "admissions/undergraduate/visit/",
					type: "internal"
				},
				label: "Learn more"
			},
			{
				title: "Apply now",
				link: {
					uri: "https://www.syracuse.edu/admissions/undergraduate/apply/",
					type: "external"
				},
				label: "Learn more"
			}
		],
		[
			// masters
			{
				title: "Explore academic programs",
				link: {
					uri: "academics/programs/masters",
					type: "internal"
				},
				label: "Explore now"
			},
			{
				title: "Request more information",
				link: {
					uri: "admissions/graduate",
					type: "internal"
				},
				label: "Request now"
			},
			{
				title: "Schedule a visit",
				link: {
					uri: "admissions/graduate/meet-us",
					type: "internal"
				},
				label: "Learn more"
			},
			{
				title: "Apply now",
				link: {
					uri: "https://www.applyweb.com/cgi-bin/app?s=syr",
					type: "external"
				},
				label: "Apply now"
			}
		],
		[
			// doctoral
			{
				title: "Explore academic programs",
				link: {
					uri: "academics/programs/doctoral",
					type: "internal"
				},
				label: "Explore now"
			},
			{
				title: "Request more information",
				link: {
					uri: "academics/mass-communications/",
					type: "internal"
				},
				label: "Request now"
			},
			{
				title: "Schedule a visit",
				link: {
					uri: "academics/mass-communications/",
					type: "internal"
				},
				label: "Learn more"
			},
			{
				title: "Apply now",
				link: {
					uri: "https://apply.communications.syr.edu/",
					type: "external"
				},
				label: "Learn more"
			}
		],
		[
			// online
			{
				title: "Explore academic programs",
				link: {
					uri: "academics/programs/online",
					type: "internal"
				},
				label: "Explore now"
			},
			{
				title: "Request more information",
				link: {
					uri: "academics/programs/online",
					type: "internal"
				},
				label: "Request now"
			},
			{
				title: "Apply now",
				link: {
					uri: "academics/programs/online",
					type: "internal"
				},
				label: "Learn more"
			}
		],
		[
			// certificates-programs
			{
				title: "Apply now",
				link: {
					uri: "https://www.applyweb.com/cgi-bin/app?s=syr",
					type: "external"
				},
				label: "Apply now"
			}
		],
		[
			// minors
			{
				title: "Request more information",
				link: {
					uri: "admissions/undergraduate/",
					type: "internal"
				},
				label: "Request now"
			},
		],
	];
	const degreesArr = [
		{
			"name": "Bachelor's",
			"slug": "bachelors"
		},
		{
			"name": "Master's",
			"slug": "masters"
		},
		{
			"name": "Doctoral",
			"slug": "doctoral"
		},
		{
			"name": "Online",
			"slug": "online"
		},
		{
			"name": "Certificates",
			"slug": "certificates"
		},
		{
			"name": "Minors",
			"slug": "minors"
		}
	];
	const getDegreeArrIndex = (degreeSlug) => {
		const indexNum = degreesArr.findIndex(degree => degree.slug === degreeSlug);
		return indexNum;
	}
	return <div className="container">
			<motion.ul
				className="tiles"
				initial="closed"
				animate="open"
				variants={tilesVariants}
				key={Math.floor(Math.random() * 1000)}
			>
				{degreeSlug &&
					actionsArr[getDegreeArrIndex(degreeSlug)].map((action, key) => {
						const { title, link, label } = action;
						let randomNumber = Math.floor(Math.random() * 1000);
						return <Tile key={key * randomNumber}>
							<Tile.Hover uri={link.uri} label={label} type={link.type} />
							<Tile.Title title={title} />
						</Tile>
					})
				}
			</motion.ul>
		</div>
};
export default CallToActions;
