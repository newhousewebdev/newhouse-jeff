import React from 'react';

const Section = ({
	className = 'bachelors',
	children,
	container = null,
	title = null,
	description = null,
}) => {
	const containerClass = container ? `container--${container}` : 'container';
	return (
		<section className={`section ${className ? `section--${className}` : ''}`}>
			{(title || description)
				&& <div className="container--content">
					{title
					&& <h2 className="section--title" dangerouslySetInnerHTML={{ __html: title }} />
					}
					{description
						&& <p className="section--description" dangerouslySetInnerHTML={{ __html: description }} />
					}
				</div>
			}
			<div className={containerClass}>
				{children}
			</div>
		</section>
	);
};

export default Section;
