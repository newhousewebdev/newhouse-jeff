import React from 'react';

const Hero = ({featuredImage, title, label}) => {
	const { sourceUrl, altText } = featuredImage;
	const convertDegreeSlugToDegreeFullName = (degreeLabel) => {
		if (degreeLabel === 'bachelors') {
			return 'Bachelor\'s';
		} if (degreeLabel === 'masters') {
			return 'Master\'s';
		} if (degreeLabel === 'online' && title === 'Communications Management') {
			return 'Online Master\'s';
		} if (degreeLabel === 'online') {
			return 'Online Master\'s';
		} if (degreeLabel === 'doctoral') {
			return 'PhD';
		}
		return degreeLabel;
	};
	return <div className="page--hero" style={{ backgroundImage: `url(${sourceUrl})`}}>
		<div className="page--hero--text">
			<div className="container">
				{label &&
					<h3 className="label" dangerouslySetInnerHTML={{ __html: convertDegreeSlugToDegreeFullName(label) }} />
				}
				<h1>{title}</h1>
			</div>
		</div>
	</div>
}
export default Hero;
