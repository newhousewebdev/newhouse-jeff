import React from 'react';

const Sidebar = ({children}) => {
	return (
		<div className="page--sidebar">
			{children}
		</div>
	)
}
export default Sidebar;
