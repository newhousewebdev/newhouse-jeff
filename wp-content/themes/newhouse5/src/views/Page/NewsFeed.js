import React, { Fragment, useState, useEffect } from 'react';
import { useQuery } from 'react-apollo';
import Section from './Section';
import Grid from '../../components/Layout/Grid';
import Loader from '../../components/Loader';

import PostsByMultipleTaxonomies from '../../queries/postByMultipleTaxonomies.gql';
import Cards from '../../components/Card/Cards';
import Button from '../../components/Button';

const matchTaxKeyToSlug = (key) => {
	if (key.toLowerCase() === 'departments') {
		return 'DEPARTMENT';
	}
	return key;
};

const NewsFeed = ({
	newsData,
	feedData,
	title,
	...props
}) => {
	const [newsArticles, setNewsArticles] = useState();
	const [hasMorePosts, setHasMorePosts] = useState();
	const [isLoadingMorePosts, setIsLoadingMorePosts] = useState(false);

	const taxonomyQuery = newsData.map((taxonomy) => ({
		field: 'ID',
		terms: taxonomy.termIDs,
		operator: 'IN',
		taxonomy: matchTaxKeyToSlug(taxonomy.key),
	}));

	const {
		loading,
		error,
		data,
		fetchMore,
	} = useQuery(PostsByMultipleTaxonomies, {
		variables: {
			per_page: feedData.per_page,
			after: null,
			taxQuery: {
				relation: 'AND',
				taxArray: taxonomyQuery,
			},
		},
	});

	useEffect(() => {
		if (data && !loading) {
			const feedArticles = data.posts.nodes;
			setNewsArticles(feedArticles);
			setHasMorePosts(data.posts.pageInfo.hasNextPage);
		}
	}, [data]);

	const loadMoreArticles = async () => {
		setIsLoadingMorePosts(true);
		return fetchMore({
			variables: {
				per_page: feedData.per_page,
				after: data.posts.pageInfo.endCursor,
			},
			updateQuery: (previousResult, { fetchMoreResult }) => {
				const newPosts = fetchMoreResult.posts.nodes;
				const { pageInfo } = fetchMoreResult.posts;

				return newPosts.length ? {
					posts: {
						nodes: [...previousResult.posts.nodes, ...newPosts],
						pageInfo,
						// eslint-disable-next-line no-underscore-dangle
						__typename: previousResult.posts.__typename,
					},
					terms: { ...previousResult.terms },
				}
					: previousResult;
			},
		})
			.then(() => {
				setIsLoadingMorePosts(false);
			});
	};

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const buildNewsCards = () => newsArticles
		.map((post) => <Cards.News key={post.databaseId} post={post} />);

	return <Fragment>
		{loading ? <Loader/> : <Section className='align-wide' {...props}>
			{title && <h2 className="section--header--title">{title}</h2>}
			<Grid> {/* container */}
				{newsArticles && buildNewsCards()}
				{isLoadingMorePosts && <Loader/> }
			</Grid>
			<Button.Group justifyContent='center'>
				{hasMorePosts && <Button.UI
					id='fetchMore'
					colorMode='orange'
					icon='angle-right'
					onClickHandler={loadMoreArticles}
					label='Show More '
					screenText='Load More Articles'
				/>}
			</Button.Group>
		</Section>}
	</Fragment>;
};

export default NewsFeed;
