import React from 'react';
import { Link } from 'react-router-dom';
import Moment from 'react-moment';

import {
	filterPublishedDate,
	getDegreeNameBySlug,
} from '../../api/utilityFunctions';

const Intro = ({
	title, label, categories, author, content, paragraph, profileImage, date, ledeParagraph, className
}) => (
		<header className={`page--intro ${className || ''}`}>
		{profileImage
					&& <div className="page--row">
						<img src={profileImage.sourceUrl} alt={profileImage.altText} className="page--profileImage" />
					</div>
		}
		{categories
					&& <ul className="page--intro--categories">
						{categories.map((category, key) => <li key={key} className="page--intro--categories--item">
							<Link to={`/news-category/${category.slug}`}>{category.name}</Link>
						</li>)}
					</ul>
		}
		{label
					&& <h3 className="label" dangerouslySetInnerHTML={{ __html: getDegreeNameBySlug(label) }} />
		}
		{title
					&& <h1 dangerouslySetInnerHTML={{ __html: title }} />
		}
		{author
					&& <h3 className="author">By <span dangerouslySetInnerHTML={{ __html: author }}></span><br />
					Published on <Moment format="MMM D, YYYY" filter={filterPublishedDate} date={date} />
					</h3>
		}
		{ledeParagraph
					&& <div dangerouslySetInnerHTML={{ __html: ledeParagraph }}></div>
		}
		{paragraph
					&& <p>{paragraph}</p>
		}
		{content
					&& <div
						className="page--intro--content"
						dangerouslySetInnerHTML={{ __html: content }}>
					</div>
		}
	</header>
);
export default Intro;
