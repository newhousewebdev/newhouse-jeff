import React, { Fragment } from 'react';
import parse from 'html-react-parser';

// gutenberg components
import Faq from '../../components/Gutenberg/Faq';
import DirectoryFeedContainer from '../../components/DirectoryFeedContainer';
import NewsFeedContainer from '../../components/NewsFeedContainer';

import ContactForm7Parser from '../../components/Form/ContactForm7Parser';

// ! if no content the app crashes
const Content = ({
	element, content = '', featuredImage = null, ledeParagraph = null, children = null,
}) => {
	const buildFeaturedImage = (image) => {
		const { altText, sourceUrl } = image;
		return <div className="container--content--break">
			<div className="page--featured-image">
				<img src={sourceUrl} alt={altText} className="responsive-img" />
			</div>
		</div>;
	};

	const options = {
		replace: (domNode) => {
			const { attribs, children } = domNode;
			if (!attribs) return;

			const classes = attribs.class ? attribs.class.split(' ') : [];
			if (classes.includes('lede')) {
				// eslint-disable-next-line consistent-return
				// append featuredImage
				// eslint-disable-next-line consistent-return
				return <Fragment></Fragment>;
			}
			// wp-block-embed-youtube
			/* if (classes.includes('wp-block-embed-youtube')) {
				// eslint-disable-next-line consistent-return
				// append featuredImage
				// eslint-disable-next-line consistent-return
				const youtubeUrl = new URL(youtubeBlock.url);
				const searchParams = new URLSearchParams(youtubeUrl.search);
				const youtubeID = searchParams.get("v");
				//console.log({youtubeID});

				return <div className="wp-block-embed-youtube alignfull">
					<div className="wp-block-embed__wrapper">
						<iframe width="560" height="315" src={`https://www.youtube.com/embed/${youtubeID}?rel=0&modestbranding=1`} frameBorder="0" allowFullScreen></iframe>
					</div>
				</div>
			} */

			if (classes.includes('faq')) {
				// eslint-disable-next-line consistent-return
				return <Faq>{children}</Faq>;
			}

			if (classes.includes('wpcf7')) {
				// eslint-disable-next-line consistent-return
				return <ContactForm7Parser>{children}</ContactForm7Parser>;
			}

			if (attribs['data-directory-feed']) {
				const url = attribs['data-directory-feed'];
				// eslint-disable-next-line consistent-return
				return <DirectoryFeedContainer fetchUrl={url} className='align-wide'/>;
			}

			if (attribs['data-posts-feed']) {
				// eslint-disable-next-line consistent-return
				return <NewsFeedContainer attribs={attribs} />;
			}
		},
	};
	const filteredContent = parse(content, options);

	return <div className="gutenberg--content">
		{ledeParagraph
			&& <div dangerouslySetInnerHTML={{ __html: ledeParagraph }}></div>
		}
		{featuredImage
			&& buildFeaturedImage(featuredImage)
		}
		{filteredContent}
		{children || ''}
	</div>;
};
export default Content;
