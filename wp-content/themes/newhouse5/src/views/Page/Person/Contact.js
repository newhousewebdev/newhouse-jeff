import React from 'react';

const Contact = ({ contact }) => {
	const { email, phone, office } = contact;
	return (
		<div className="container--content">
			<div className="page--person--contact">
				<ul>
					<li><span className="label">E-mail</span><br />
						<a href="mailto:{email}">{email}</a></li>
					<li><span className="label">Phone</span><br />
						{phone}</li>
					<li><span className="label">Office location</span><br />
						{office}</li>
				</ul>
			</div>
		</div>
	);
};
export default Contact;
