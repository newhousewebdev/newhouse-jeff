import React, { Fragment } from 'react';

const News = () => {
	return (
		<div className="container--content">
			<div className="page--person--news">
				<h3>Related news</h3>
				<ul>
					<li><a href="#">Headline goes here</a></li>
					<li><a href="#">Headline goes here</a></li>
					<li><a href="#">Headline goes here</a></li>
					<li><a href="#">Headline goes here</a></li>
					<li><a href="#">Headline goes here</a></li>
				</ul>
			</div>
		</div>
	)
}
export default News;
