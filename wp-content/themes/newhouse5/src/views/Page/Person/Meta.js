import React from 'react';

const Meta = ({
	email, phone = null, officeNumber = null, officeBuilding = null, officeLetter = null,
}) => (
	<div className="container--content">
		<div className="page--person--meta">
			<ul className="page--person--meta--list">
				<li className="page--person--meta--list--item">
					<p className="page--person--meta--list--item--label">Email</p>
					<p className="page--person--meta--list--item--value"><a href={`mailto:${email}`}>{email}</a></p>
				</li>
				{(phone && phone.length === 14)
				&& <li className="page--person--meta--list--item">
					<p className="page--person--meta--list--item--label">Phone</p>
					<p className="page--person--meta--list--item--value" dangerouslySetInnerHTML={{
						__html: phone,
					}}></p>
				</li>
				}
				{officeBuilding
				&& <li className="page--person--meta--list--item">
					<p className="page--person--meta--list--item--label">Office</p>
					<p className="page--person--meta--list--item--value">{officeNumber || ''}{`${officeLetter || ''}`} {`${officeBuilding}`}</p>
				</li>
				}
			</ul>
		</div>
	</div>
);
export default Meta;
