import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

import Page from '../Page';
import Container from '../../../components/Layout/Container';
import Loader from '../../../components/Loader';
import Intro from './Intro';
import Meta from './Meta';
import News from './News';
import SEO from '../../../components/SEO';
import TopCap from '../../../sass/img/top-cap.svg';

import { fetchFacultyByDepartment, fetchPersonBySlug } from '../../../api/fetches/DirectoryFetches';
import { buildJobTitleList, reverseSlug } from '../../../api/directoryUtilities';

// contexts
import { ActiveDegreeContext, ArchiveCollectionContext } from '../../../api/ContextAPI';

const Person = () => {
	const { personSlug } = useParams();
	// ! slugs from directory are in reverse order: {last}-{first}
	const directorySlug = reverseSlug(personSlug);
	Person.Intro = Intro;
	Person.Meta = Meta;
	Person.News = News;

	// Note: renamed to a more semantic variable
	const [collectionIsLoading, setCollectionIsLoading] = useState(true);
	const [person, setPerson] = useState(true);
	const [activeDegree] = useContext(ActiveDegreeContext);
	const [archiveCollection, setArchiveCollection] = useContext(ArchiveCollectionContext);

	// ?refactor into a more reusable method
	const buildPersonData = (personData) => {
		const info = personData.meta.person_meta;
		const {
			name: { display_name: displayName },
			images: { profile_image: profileImage },
			contact: { email, phone },
			contact: { office: { building, room, room_letter: roomLetter } },
			bio: { full: biography },
		} = info;
		const path = `people/${personData.slug}`;
		// const jobTitle = buildJobTitleList(info.job);

		return {
			displayName,
			profileImage,
			email,
			phone,
			building,
			room,
			roomLetter,
			biography,
			path,
			job: info.job,
			id: personData.id,
			slug: personData.slug,
		};
	};

	useEffect(() => {
		// Note: placeholder to make this more dynamic: ie administrative departments later
		const taxonomySlug = 'academic_departments';
		setCollectionIsLoading(true);
		const getPersonFromDirectory = async (pathSlug) => fetchPersonBySlug(pathSlug, taxonomySlug);

		if (archiveCollection.length > 0) {
			const match = archiveCollection.find((entry) => entry.slug === directorySlug);
			if (match) {
				setPerson(buildPersonData(match));
				setCollectionIsLoading(false);
				// verify that the collection matches the person's department?
			} else {
				getPersonFromDirectory(directorySlug)
					.then((fetchedPerson) => {
						setPerson(buildPersonData(fetchedPerson[0]));
						setCollectionIsLoading(false);
					})
					.catch((err) => { console.error(err); });
			}
		} else {
			// find the person
			getPersonFromDirectory(directorySlug)
				.then((fetchedPerson) => {
					setPerson(buildPersonData(fetchedPerson[0]));
					return fetchFacultyByDepartment(fetchedPerson[0][taxonomySlug][0]);
				})
				.then((faculty) => {
					setArchiveCollection(faculty);
					setCollectionIsLoading(false);
				})
				.catch((err) => { console.error(err); });
		}
	}, [archiveCollection, personSlug]);

	// TODO : add additional filters ie, program, academic_department, admin_department
	const filteredCollection = (collection) => collection.filter((entry) => entry.id !== person.id);

	return <Page>
		{
			collectionIsLoading ? <Loader />
				: <Page.Main>
					<SEO
						title={person.displayName}
						uri={person.path}
					/>
					{/* getBreadcrumbs()  */}
					<Page.Section className={activeDegree || 'bachelors'}>
						<div className="container">
							<div className="page--person">
								<div className="page--person--header">
									<TopCap />
									<Person.Intro
										profileImage={person.profileImage}
									/>
								</div>
								<div className="page--person--body">
									<div className="page--person--body--intro">
										<h1>{person.displayName}</h1>
										{person.job
											&& <ul>{buildJobTitleList(person.job)}
											</ul>
										}
									</div>

									<Person.Meta
										email={person.email}
										phone={person.phone}
										officeNumber={person.room}
										officeBuilding={person.building}
										officeLetter={person.roomLetter}
									/>
									{person.biography
								&& <Container.Content>
										<Page.Content content={person.biography} />
									</Container.Content>
									}
								</div>
							</div>
						</div>
					</Page.Section>
					{archiveCollection.length > 0 && <Page.RelatedContent
						collection={filteredCollection(archiveCollection)}
					/>}
				</Page.Main>
		}
	</Page>;
};
export default Person;
