import React from 'react';

const Intro = ({ profileImage }) => (
	<div className="page--person--header--intro text-align-center">
		{profileImage
				&& <div className="page--person--header--intro--row">
					<img src={profileImage.src} alt={profileImage.alt} className="page--profileImage" /><br />
				</div>
		}
	</div>
);
export default Intro;
