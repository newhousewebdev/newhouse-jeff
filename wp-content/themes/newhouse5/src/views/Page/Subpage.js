import React, { useEffect, useState, Fragment } from 'react';

// components
import Button from '../../components/Button';

// Api
import { matchProgramSlugToID } from '../../api/directoryUtilities';
import { fetchFacultyByProgram } from '../../api/fetches/DirectoryFetches';

const blockSizes = ['quarter', 'third', 'half', 'full'];
//const blockColors = ['orange', 'orange--medium', 'orange--light', 'orange--10', 'blue', 'blue--medium', 'blue--light', 'blue--10'];
const blockColors = ['orange--10', 'blue--10']

const shuffleArray = (array) => {

	let currentIndex = array.length;
	let temporaryValue, randomIndex;

	// While there remain elements to shuffle...
	while (0 !== currentIndex) {
		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex -= 1;

		// And swap it with the current element.
		temporaryValue = array[currentIndex];
		array[currentIndex] = array[randomIndex];
		array[randomIndex] = temporaryValue;
	}

	return array;

};

const randomBlockSize = () => {
	const randomBlockNum = Math.floor(Math.random() * blockSizes.length);
	return blockSizes[randomBlockNum];
}
const randomBlockColor = () => {
	const randomBlockNum = Math.floor(Math.random() * blockColors.length);
	return blockColors[randomBlockNum];
}
const blockClassNames = () => {
	const classNames = `${randomBlockSize()} ${randomBlockColor()}`;
	//console.log({classNames});
	return classNames;
}

const BlockThumbnail = ({person}) => {
	const { slug, meta } = person;
	const { profile_image } = meta.person_meta.images;
	
	return <Fragment>
		{/*<li className={`${blockSizes[0]} ${blockColors[7]}`}></li>*/}
		<li data-slug={person.slug} className={randomBlockColor()}>
			<img src={profile_image.src} alt={profile_image.alt} width="308" height="199" />
		</li>
		{/*<li className={`${blockSizes[0]} ${blockColors[7]}`}></li>*/}
	</Fragment>
}

const BlockAnimatedFaculty = ({ node, programSlug }) => {
	//console.log({ programSlug});
	const [blockFacultyData, setBlockFacultyData] = useState(false);
	
	const {
		label, excerpt, slug, uri,
	} = node;

	useEffect(() => {
		//console.log("useEffect for getFacultyBy...");
		const getFacultyByDepartmentOrProgram = async (programName) => {
			if (programName) {
				//console.log({programName});
				matchProgramSlugToID(programName)
					.then((programID) => fetchFacultyByProgram(programID))
					.then((facultyData) => {
						//console.log({facultyData});
						setBlockFacultyData(facultyData);
					})
					.catch((err) => { console.error(err); });
			}
		};
		getFacultyByDepartmentOrProgram(programSlug);
	}, []);

	const calculateAnimationDuration = (arr) => {
		const defaultBlockHeight = 108;
		const quarterHeight = 25;
		const halfHeight = 50;
		const marginBottom = 16;
		const packageHeight = (defaultBlockHeight + marginBottom) + (quarterHeight + marginBottom) + (halfHeight + marginBottom);

		const columnHeight = arr.length * packageHeight;
		//console.log({columnHeight})
		//const columnTravelDistance = (30 * columnHeight) / 100;
		const columnTravelDistance = 200;
		//console.log({ columnTravelDistance});
		const columnTravelSpeed = 4500; 

		const columnTravelTime = columnTravelSpeed / columnTravelDistance;
		//console.log({columnTravelTime});
		return { animationDuration : `${columnTravelTime}s` }
	}

	const buildFacultyColumn = (arr) => {
		return arr.length > 0 ? (
			<ul style={calculateAnimationDuration(arr)}>
			
				{arr.map((person, key) => <BlockThumbnail key={key} person={person} />)}
				{
					// extra output to fill the column, if needed
					arr.length < 4 &&
					<Fragment>
					{arr.map((person, key) => <BlockThumbnail key={key} person={person} />)}
					{arr.map((person, key) => <BlockThumbnail key={key} person={person} />)}
					{arr.map((person, key) => <BlockThumbnail key={key} person={person} />)}
					</Fragment>
				}
			</ul>
		) : '';
	}

	const initFacultyColumns = (data) => {
		//console.log({data});
		//shuffleArray(data);

		let a,b,c,d;
		let firstCol, secondCol, thirdCol, fourthCol, fifthCol;
		// empty arrays
		a = Math.ceil(data.length / 5);
		b = Math.ceil(2 * data.length / 5);
		c = Math.ceil(3 * data.length / 5);
		d = Math.ceil(4 * data.length / 5);

		firstCol = data.slice(0, a);
		secondCol = data.slice(a, b);
		thirdCol = data.slice(b, c);
		fourthCol = data.slice(c, d);
		fifthCol = data.slice(d, data.length);

		//console.log({ firstCol, secondCol, thirdCol });
		return <div className={`container--${data.length < 6 ? 'content' : 'fluid'}`}>
			<div className="page--subpage--wrap">
			{data.length >= 6 &&
				<div className="page--subpage--left--faculty">
					<div className="page--subpage--left--faculty--relative">
						{buildFacultyColumn(shuffleArray(data))}
						{buildFacultyColumn(shuffleArray(data))}
						{buildFacultyColumn(shuffleArray(data))}
						{buildFacultyColumn(shuffleArray(data))}
						{buildFacultyColumn(shuffleArray(data))}
					</div>
				</div>
			}
			
			<div className={`page--subpage--right--faculty page--subpage--content ${data.length < 6 ? 'break' : ''}`}>
				<div className="page--subpage--content--inside">
					<h2>{label}</h2>
					<p dangerouslySetInnerHTML={{ __html: excerpt }}></p>
					<Button.Group>
						<Button.CTA type="primary" label="Explore now" path={uri} colorMode="orange" />
					</Button.Group>
				</div>
			</div>
			</div>
		</div>
		
	}

	return <article className="page--subpage">
		{blockFacultyData && 
			initFacultyColumns(blockFacultyData)
		}
	</article>
}

const BlockWithImage = ({node}) => {
	const {
		label, featuredImage, excerpt, slug, uri,
	} = node;
	return <article className="page--subpage" id={`block--${slug}`}>
		<div className="container--fluid">
			<div className="page--subpage--wrap">
				<div className="page--subpage--image">
					<img src={featuredImage.sourceUrl} alt={featuredImage.altText} />
				</div>
				<div className="page--subpage--content">
					<div className="page--subpage--content--inside">
						<h2>{label}</h2>
						<p dangerouslySetInnerHTML={{ __html: excerpt }}></p>
						<Button.Group>
							<Button.CTA type="primary" label="Explore now" path={uri} colorMode="orange" />
						</Button.Group>
					</div>
				</div>
			</div>
		</div>
	</article>
}
const BlockPlain = ({node}) => {
	const {
		label, excerpt, slug, uri,
	} = node;
	return <article className="page--subpage" id={`block--${slug}`}>
		<div className="container--content">
		<div className="page--subpage--wrap">
			<div className="page--subpage--content break">
				<div className="page--subpage--content--inside">
					<h2>{label}</h2>
					<p dangerouslySetInnerHTML={{ __html: excerpt }}></p>
					<Button.Group>
						<Button.CTA type="primary" label="Explore now" path={uri} colorMode="orange" />
					</Button.Group> 
				</div>
			</div>
		</div>
	</div>
	</article>
}

const Subpage = ({ subpage, programSlug }) => {
	//console.log({programSlug});
	const {
		label, featuredImage
	} = subpage.node;

	if (label === "Faculty") {
		return <BlockAnimatedFaculty node={subpage.node} programSlug={programSlug} />
	} else if (featuredImage) {
		return <BlockWithImage node={subpage.node} />
	} else {
		return <BlockPlain node={subpage.node} />
	}
};

export default Subpage;
