import React, { useContext } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useQuery } from 'react-apollo';
import gql from 'graphql-tag';

// components
import Layout from '../components/Layout/Layout';
import Container from '../components/Layout/Container';
import Loader from '../components/Loader';
import Button from '../components/Button';
import Card from '../components/Card/Card';
import Page from './Page/Page';

// query
import SEARCH_QUERY from '../queries/search.gql';

// A custom hook that builds on useLocation to parse
// the query string for you.
function useLocationQuery() {
	return new URLSearchParams(useLocation().search);
}

const SearchResults = () => {
	let locationQuery = useLocationQuery();
	const searchTerm = locationQuery.get("q");
	//console.log({ searchTerm });

	const { loading, error, data } = useQuery(SEARCH_QUERY, {
		variables: {
			term: searchTerm,
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	//console.log(data);
	const { nodes } = data.contentNodes;

	// ToDo refactor to load the different Card types

	return (
		<Layout>
			<Container.Content>
				<Page.Intro title={`Search results for ${searchTerm}`} />
			 	<section className="gutenberg--content">
					{nodes.map((node, key) => {
						const {	title, excerpt, uri, featuredImage } = node;
						return (
							<article key={key} className="search--result">
								{featuredImage &&
								<div className="search--result--left">
									<Link to={`/${uri}`}>
										<Card.FeaturedImage featuredImage={featuredImage} />
									</Link>
								</div>
								}
								<div className="search--result--right">
									<h3 className="card--content--title">
										<Link to={`/${uri}`} dangerouslySetInnerHTML={{ __html: title }} />
									</h3>
									{excerpt &&
										<p className="card--content--excerpt" dangerouslySetInnerHTML={{ __html: excerpt }} />
									}
									<Button.Group>
										<Button.CTA type="text" label="Read more" path={uri} colorMode="orange" />
									</Button.Group>
								</div>
							</article>
						);
					})}
				 </section>
			</Container.Content>
		</Layout>
	);
};
export default SearchResults;
