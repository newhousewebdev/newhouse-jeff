import React, { useEffect, useState } from 'react';
import { useQuery } from 'react-apollo';

// components
import Section from '../../regions/Section/Section';
import Slider from '../../components/Slider';
import Cards from '../../components/Card/Cards';
import Loader from '../../components/Loader';
import Button from '../../components/Button';

// query
import postsByNewsCategoryQuery from '../../queries/postsByNewsCategory.gql';

const News = () => {
	const [featuredPosts, setFeaturedPosts] = useState();
	const { loading, error, data } = useQuery(postsByNewsCategoryQuery, {
		variables: {
			slug: 'featured-news',
			per_page: 6,
		},
	});

	useEffect(() => {
		// console.log({ data });
		if (data && !loading) {
			const posts = data.newsCategories && data.newsCategories.posts.nodes;
			if (posts) {
				setFeaturedPosts(posts);
			}
		}
	}, [data]);

	// if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	return <Section className="news" container="fluid">
		<Section.Header alignment="center" title="News" icon="newspaper" />
		<div className="container">
			<Slider id="newsSlider">
				{featuredPosts
					? featuredPosts.map((post, key) => <Cards.News key={key} post={post} />)
					: <Loader />}
			</Slider>
		</div>
		{featuredPosts && <Button.Group justifyContent="center">
			<Button.CTA path="about/news" label={'View Latest News'} />
		</Button.Group>}
	</Section>;
};
export default News;
