import React from 'react';
// regions
import Layout from '../../components/Layout/Layout';
import DynamicLead from '../../regions/DynamicLead';
import FindYourProgram from '../../regions/FindYourProgram';
import Events from './HomeEvents';
import News from './News';

// components
import SEO from '../../components/SEO';

/* Refactor this to a home page setting */
const eventsFilters = [{
	condition: 'exclude',
	key: 'organizers',
	subKey: 'id',
	type: 'array',
	value: [19833, 19836],
}];

const Home = () => (
	<Layout>
		<SEO />
		<DynamicLead />
		<FindYourProgram />
		<News />
		<Events eventsFilters={eventsFilters} linkToArchive/>
	</Layout>
);

export default Home;
