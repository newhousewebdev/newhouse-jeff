import React, {
	useContext, useEffect, useState,
} from 'react';
import Section from '../../regions/Section/Section';
import Slider from '../../components/Slider';
import Cards from '../../components/Card/Cards';
import WithLoading from '../../components/LoadingContainer';

// contexts
import {
	EventsDataContext,
} from '../../api/ContextAPI';
import { getEventsViaAjax, getMoreEventsViaAjax } from '../../api/fetches/EventsFetches';
import { parameterizeObject } from '../../api/fetches/DirectoryFetches';

import Button from '../../components/Button';
/*
	sample eventsFilters =[
		{
			condition: exclude, // ['include', 'exclude']
			key: organizer,
			['organizer'[{},{}], 'types'[{}, {}], 'categories'[{},{}], visibility, region, audiences ]
			type: array, ['array', 'string', 'number']
			value: 19833
		}
	]
*/

const EventsSlider = ({ cards }, { noResultsMessage }) => <div className="container">
	{cards.length > 0 ? <Slider id="eventsSlider">
		{cards}
	</Slider> : noResultsMessage}
</div>;

const EventsSliderWithLoading = WithLoading(EventsSlider);

const Events = ({
	eventsFilters = undefined,
	feedCount = 6,
	linkToArchive = false,
}) => {
	const [eventsArr, setEventsArr] = useContext(EventsDataContext);
	const [eventsIsLoading, setEventsIsLoading] = useState(true);
	const [eventsNextFetch, setEventsNextFetch] = useState();
	const [moreEvents, setMoreEvents] = useState();

	useEffect(() => {
		setEventsIsLoading(true);
		const fetchData = async () => {
			const params = {
				group: 3438,
				per_page: 10,
			};
			const searchParams = parameterizeObject(params);
			getEventsViaAjax(searchParams)
				.then((res) => {
					setEventsArr(res.data);
					setEventsNextFetch(res.links.next);
					setMoreEvents(!!res.links.next); // Bang bang you are a boolean
					setEventsIsLoading(false);
				});
		};
		if (eventsArr.length === 0) {
			fetchData();
		} else {
			setEventsIsLoading(false);
		}
	}, []);

	const loadMoreEvents = async () => {
		setEventsIsLoading(true);
		const url = eventsNextFetch;
		if (url && moreEvents) {
			await getMoreEventsViaAjax(url)
				.then((res) => {
					setEventsArr([...eventsArr, ...res.data]);
					setEventsNextFetch(res.links.next);
					setMoreEvents(!!res.links.next); // Bang bang you are a boolean

					setEventsIsLoading(false);
				});
		}
	};

	const filterEvents = (events, filters) => (!filters ? events : filters
		.reduce((filteredEvents, filter) => filteredEvents.filter((event) => {
			// console.log({ event }, { filteredEvents }, { filter });
			const key = [filter.key];
			const target = event[key];
			// console.log({ target });
			// the checked value is an array
			if (Array.isArray(target)) {
				const checkValues = target.map((entry) => entry[filter.subKey]);
				// console.log({ checkValues });
				if (filter.type === 'array') {
					const isIncluded = filter.value.some((filterValue) => checkValues.includes(filterValue));
					return filter.condition === 'exclude' ? !isIncluded : isIncluded;
				}
				const isIncluded = checkValues.includes(filter.value);
				return filter.condition === 'exclude' ? !isIncluded : isIncluded;
			}

			if (filter.type === 'array') {
				// if array then need to check each objects values
				const isIncluded = target.includes(filter.value);
				return filter.condition === 'exclude' ? !isIncluded : isIncluded;
			}
			const isEqual = target === filter.value;
			return filter.condition === 'exclude' ? !isEqual : isEqual;
		}), events));

	const buildEventCards = (events) => events
		.map((event) => <Cards.Event key={event.eventId} event={event} />);

	const limitEvents = (events) => (feedCount ? events.slice(0, feedCount) : events);

	const displayCards = (events, filters) => {
		const limitedEvents = limitEvents(filterEvents(events, filters));
		if (limitedEvents.length < feedCount) {
			if (!eventsIsLoading && moreEvents) {
				loadMoreEvents();
			}
			if (!moreEvents) {
				return eventsArr.length !== 0
					? buildEventCards(limitEvents(eventsArr)) : <h4>No Events</h4>;
			}
		}
		return buildEventCards(limitedEvents);
	};

	return <Section className="news" container="fluid">
		<Section.Header alignment="center" title="Events" icon="calendar-alt" />
		<EventsSliderWithLoading
			isLoading={eventsIsLoading}
			cards={eventsArr && displayCards(eventsArr, eventsFilters)}
			noResultsMessage='No upcoming events, check back soon.'
		/>
		{eventsArr && <Button.Group justifyContent="center">
			{linkToArchive && <Button.CTA path={'events'} label={'View Latest Events'} />}
		</Button.Group>}
	</Section>;
};

export default Events;
