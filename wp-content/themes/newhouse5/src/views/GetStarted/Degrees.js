import React, { useContext } from 'react';
import * as Scroll from 'react-scroll';

import Button from '../../components/Button';

const { scroller } = Scroll;

const Degrees = ({ onClickDegree }) => (
	<section className="gs--section white" data-gs-state="degrees">
		<div className="container--content">
			<h2 className="gs--section--title">What degree would you like to pursue?</h2>
			<Button.Group justifyContent="center">
				<Button.Toggle
					type="primary"
					label="Bachelor's"
					colorMode="orange"
					onClickHandler={(e) => {
						onClickDegree('bachelors');
						console.log('clicked!!!!');
						scroller.scrollTo('degree--intro', {
							duration: 1500,
							delay: 100,
							smooth: true,
							// containerId: 'ContainerElementID',
							// offset: 50, // Scrolls to element + 50 pixels down the page
						});
						e.target.classList.toggle('toggled');
					}}

				/>
				<Button.Toggle
					type="primary"
					label="Master's"
					colorMode="orange"
					onClickHandler={() => {
						onClickDegree('masters');
						// scroller.scrollTo('degree--intro', {
						// 	duration: 500,
						// 	delay: 100,
						// 	smooth: true,
						// });
					}}
				/>
				<Button.Toggle
					type="primary"
					label="Doctoral"
					colorMode="orange"
					onClickHandler={() => {
						onClickDegree('doctoral');
						// scroller.scrollTo('degree--intro', {
						// 	duration: 500,
						// 	delay: 100,
						// 	smooth: true,
						// });
					}}
				/>
				<Button.Toggle
					type="primary"
					label="Other"
					colorMode="orange"
					onClickHandler={() => {
						onClickDegree('other');
						// scroller.scrollTo('degree--intro', {
						// 	duration: 500,
						// 	delay: 100,
						// 	smooth: true,
						// });
					}}
				/>
			</Button.Group>
		</div>
	</section>
);
export default Degrees;
