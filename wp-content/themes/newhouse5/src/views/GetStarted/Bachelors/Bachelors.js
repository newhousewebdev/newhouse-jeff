import React, {Fragment} from 'react';

import Button from '../../../components/Button';
import List from '../../../components/List/List';

const Bachelors = () => {
	return (
		<Fragment>
			<section className="gs--section orange" data-gs-state="degree-intro" data-degree="bachelors">
				<div className="container--content">
					<h4>Bachelor's</h4>
					<h2 className="gs--section--title">It all starts here.</h2>
					<p>Whether you’re interested in journalism, interactive communications, advertising, public relations, multimedia storytelling or television and film, you will find a wealth of choices at the Newhouse School.</p>
				</div>
			</section>
			<section className="gs--section white" data-gs-state="degree-intro" data-degree="bachelors">
				<div className="container--content">
					<h4>Bachelor's</h4>
					<h2 className="gs--section--title">It all starts here.</h2>
					<div className="row">
						<div className="col col--6 text-align-right">
							<h3>Virtual tour</h3>
							<p>At the Newhouse School, we’re training the next generation of communications leaders—professionals who are prepared not only to enter a rapidly-changing media industry, but also to shape its future.</p>
							<Button.Group justifyContent="flex-end">
								<Button.Toggle
									type="primary"
									label="Explore now"
									colorMode="orange"
									onClickHandler={() => {
										scroller.scrollTo('virtual-tour', {
											duration: 500,
											delay: 100,
											smooth: true,
										});
									}}
								/>
							</Button.Group>
						</div>
						<div className="col col--6 text-align-left">
							<h4>Quick links</h4>
							<List type="toc">
								<List.Item label="Explore academic programs" path="/" />
								<List.Item label="Schedule a visit" path="/" />
								<List.Item label="Request information" path="/" />
								<List.Item label="Apply Now" path="/" />
							</List>
						</div>
					</div>
				</div>
			</section>
			<section className="gs--section blue" name="virtual-tour">
				<div className="gs--elevator">
					<ul>
						<li><a href="#" onClick={(e) => {
							e.preventDefault();
							scroller.scrollTo('history', {
								activeClass: "active",
								containerId: "gs--slides",
								spy: true,
								duration: 500,
								delay: 100,
								smooth: true,
							});
						}} >History</a></li>
						<li><a href="#" onClick={(e) => {
							e.preventDefault();
							scroller.scrollTo('facilities', {
								activeClass: "active",
								containerId: "gs--slides",
								spy: true,
								duration: 500,
								delay: 100,
								smooth: true,
							});
						}}>Facilities</a></li>
						<li><a href="#" onClick={(e) => {
							e.preventDefault();
							scroller.scrollTo('programs', {
								activeClass: "active",
								containerId: "gs--slides",
								spy: true,
								duration: 500,
								delay: 100,
								smooth: true,
							});
						}}>Programs</a></li>
						<li><a href="#" onClick={(e) => {
							e.preventDefault();
							scroller.scrollTo('diversity', {
								activeClass: "active",
								containerId: "gs--slides",
								spy: true,
								duration: 500,
								delay: 100,
								smooth: true,
							});
						}}>Diversity</a></li>
						<li><a href="#" onClick={(e) => {
							e.preventDefault();
							scroller.scrollTo('alumni', {
								activeClass: "active",
								containerId: "gs--slides",
								spy: true,
								duration: 500,
								delay: 100,
								smooth: true,
							});
						}}>Alumni</a></li>
					</ul>
				</div>
				<div id="gs--slides">
					<div className="gs--slide" name="history">
						<div className="container--content">
							<h2>History</h2>
						</div>
					</div>
					<div className="gs--slide" name="facilities">
						<div className="container--content">
							<h2>Facilities</h2>
						</div>
					</div>
					<div className="gs--slide" name="programs">
						<div className="container--content">
							<h2>Programs</h2>
						</div>
					</div>
					<div className="gs--slide" name="diversity">
						<div className="container--content">
							<h2>Diversity</h2>
						</div>
					</div>
					<div className="gs--slide" name="alumni">
						<div className="container--content">
							<h2>Alumni</h2>
						</div>
					</div>
				</div>
			</section>
			<section className="gs--section white" data-gs-state="closing" data-degree="bachelors">
				<div className="container--content">
					<h4>Bachelor's</h4>
					<h2 className="gs--section--title">It all starts here.</h2>
					<div className="row">
						<div className="col">Closing Options</div>
					</div>
				</div>
			</section>
		</Fragment>
			// end of bachelors
	)
}
export default Bachelors;
