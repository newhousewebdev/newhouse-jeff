import React, {useState, useEffect, Fragment} from 'react';
import { motion } from 'framer-motion';
import * as Scroll from 'react-scroll';
import { useHistory } from 'react-router-dom';
const scroller = Scroll.scroller;

import Tile from '../../components/Tiles/Tile';
import CallToActions from '../Page/Program/CallToActions';

const Actions = ({ 
	activePanelDegree, 
	setActivePanelDegree, 
	activePanelName,
	isTogglebarActive,
	setTogglebarActive}) => {
	
	let history = useHistory();
	
	const panelsArr = [
		"education",
		"experience",
		"connections",
		"future"
	];

	const getPanelIndex = (panelName) => {
		return panelsArr.indexOf(panelName);
	}
	const activePanelIndex = getPanelIndex(activePanelName);

	const toggleItems = [
		{
			"name" : "Bachelor's",
			"slug" : "bachelors"
		},
		{
			"name": "Master's",
			"slug": "masters"
		},
		{
			"name": "Doctoral",
			"slug": "doctoral"
		},
		{
			"name": "Online",
			"slug": "online"
		},
	];
	
	const handleDegreeClick = (item) => {
		setActivePanelDegree(item.slug);
		setTogglebarActive(true);
		scroller.scrollTo('tiles', {
			duration: 800,
			delay: 0,
			smooth: true,
		});
		history.push(`/get-started/?panel=${activePanelName}&degree=${item.slug}`);
	}
	
	return <section className="gs--actions">
		<div className="section--header">
			<div className="container--content">
				<h2 className="section--title">Ready to begin?</h2>
				<p className="section--description">Come see us and let us show you around a little. View the beautiful, iconic campus in any season, tour our state-of-the-art facilities, and get a feel for the student experience at Newhouse.</p>
			</div>
		</div>
		<div className="container">
			<h3 className="form--title">1. Choose a degree:</h3>
			<div className={`form--toggle--row ${isTogglebarActive ? 'active' : ''}`}>
				{
					toggleItems.map((item, key) => {
						return <div 
						key={key} 
						className={`form--toggle--item ${activePanelDegree === item.slug ? 'active' : ''}`} 
						data-option={item.slug}
						tabIndex="0"
						onClick={() => { 
							handleDegreeClick(item);
						}}
						onKeyPress={() =>{
							if (event.charCode === 13) {
								handleDegreeClick(item);
							}
						}}>
							{item.name}
						</div>
					})
				}
			</div>
			
		</div>
		<div className="container" name="tiles">
		{activePanelDegree &&
		<Fragment>
			<motion.div
				initial="closed"
				animate="open"
			>
				<h3 className="form--title">2. Click on an action:</h3>
			</motion.div>
			<CallToActions degreeSlug={activePanelDegree} />
		</Fragment>
		}
		</div>
	</section>
}
export default Actions;
