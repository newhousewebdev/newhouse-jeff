import React, { useState, useEffect, Fragment } from 'react';
import { motion } from 'framer-motion';
import * as Scroll from 'react-scroll';
import { useHistory } from 'react-router-dom';

import Icon from '../../components/Icon';
import Button from '../../components/Button';

const scroller = Scroll.scroller;

const Panels = ({ 
		arePanelsVisible, 
		activePanelName, 
		setActivePanelName, 
		arePanelsLocked, 
		setPanelsLocked, 
		arePanelsEngaged, 
		setPanelsEngaged, 
		activePanelDegree
	}) => {
	//console.log({ arePanelsVisible });
	//console.log({ arePanelsLocked });
	//console.log({ activePanelName });

	let history = useHistory();

	const [activePanelIndex, setActivePanelIndex] = useState(null);
	//console.log({ activePanelIndex});

	const panelsArr = [
		"education",
		"experience",
		"connections",
		"future"
	];

	const getPanelIndex = (panelName) => {
		return panelsArr.indexOf(panelName);
	}
	const isThisPanelActive = (panelNum) => {
		return panelNum === activePanelIndex;
	}

	useEffect(() => {
		activePanelName ? setActivePanelIndex(getPanelIndex(activePanelName)) : '';
	}, []);

	useEffect(() => {
		if (arePanelsLocked) {
			if (activePanelDegree && activePanelName) {
				history.push(`/get-started/?panel=${activePanelName}&degree=${activePanelDegree}`);
			} else if (activePanelName) {
				history.push(`/get-started/?panel=${activePanelName}`);
			} else {
				history.push(`/get-started/`);
			}
		}
	}, [activePanelName]);
	
	const totalPanels = 4;

	const parentPanelVariants = {
		open: {
			opacity: 1,
			transition: {
				when: "beforeChildren",
				staggerChildren: 0.2,
			},
		},
		closed: {
			opacity: 0,
			transition: {
				when: "afterChildren",
			},
		},
	};
	const panelLabelVariants = {
		open: {
			x: 0,
			opacity: 1,
			transition: {
				duration: .5,
				//x: { stiffness: 1000, velocity: -100 },
			},
		},
		closed: {
			x: -100,
			opacity: 0,
			transition: {
				duration: .5,
				//x: { stiffness: 1000 },
			},
		},
	};
	const panelColumnVariants = {
		open: {
			x: 0,
			opacity: 1,
			transition: {
				duration: .5,
			},
		},
		closed: {
			x: -100,
			opacity: 0,
			transition: {
				duration: .5,
			},
		},
	};

	const getPanelClassNames = (panelIndex) => {
		//console.log({panelIndex});
		let displayClass;
		if (arePanelsLocked && isThisPanelActive(panelIndex)) {
			displayClass = 'locked';
		} else if (arePanelsLocked && !isThisPanelActive(panelIndex)) {
			displayClass = 'collapsed';
		} else if (isThisPanelActive(panelIndex)) {
			displayClass = 'active';
		} else {
			displayClass = '';
		}
		//console.log({displayClass});
		return displayClass;
	}

	const handlePreviousClick = () => {
		const newIndex = activePanelIndex === 0 ? totalPanels - 1 : activePanelIndex - 1;
		setActivePanelIndex(newIndex);
		setActivePanelName(panelsArr[newIndex]);
	}
	const handleNextClick = () => {
		const newIndex = activePanelIndex === (totalPanels - 1) ? 0 : activePanelIndex + 1;
		setActivePanelIndex(newIndex);
		setActivePanelName(panelsArr[newIndex]);
	}

	return (
	<Fragment>
		<div className="container">
			<div className="panels-relative-frame">
				<motion.div 
					className={`panels-container ${arePanelsVisible ? 'active' : ''}`}
					animate={arePanelsVisible ? "open" : "closed"}
					variants={parentPanelVariants}
					onMouseEnter={() => {
						!arePanelsLocked ? setPanelsEngaged(true) : '';
					}}
					onMouseLeave={() => {
						!arePanelsLocked ? setPanelsEngaged(false) : '';
						!arePanelsLocked ? setActivePanelIndex(null) : '';
					}}
				>
				{
					panelsArr.map((panelSlug, key) => {
						const handleClickAndTabEnter = () => {
							setPanelsEngaged(true);
							setPanelsLocked(true);
							setActivePanelName(panelSlug);
							setActivePanelIndex(key);
						}
						return <motion.div 
						key={key} 
						initial="closed"
						variants={panelColumnVariants}
						className={`panel ${getPanelClassNames(key)}`} 
						tabIndex="0"
						onKeyPress={() => {
							if (event.charCode === 13) {
								handleClickAndTabEnter();
							}
						}}
						onClick={() => {
							handleClickAndTabEnter();
						}}
						onMouseOver={() => {
							!arePanelsLocked ? setActivePanelIndex(key) : '';
						}} 
						>
							<div className={`panel-bg panel-${panelSlug}`}></div>
							{!arePanelsLocked && 
								<button className="panel-btn trigger" onClick={() => {
									handleClickAndTabEnter();
								}}><Icon icon="angle-down" /></button>
							}
						</motion.div> 
					})
				}
				</motion.div>
				{(arePanelsVisible && arePanelsLocked) &&
				<Fragment>
					<Button.UI
						icon="times"
						colorMode="orange"
						id="panelCloseBtn"
						onClickHandler={() => {
							//console.log("clicked on close");
							setActivePanelIndex(null);
							setActivePanelName(null);
							setPanelsLocked(false);
							setPanelsEngaged(false);
							scroller.scrollTo('panels-container', {
								duration: 800,
								delay: 0,
								smooth: true,
							});
							history.push(`/get-started/`);
						}}
					/>
					{activePanelIndex > 0 &&
						<Button.UI
							icon="angle-left"
							id="panelPreviousBtn"
							colorMode="orange"
							onClickHandler={() => {
								handlePreviousClick();
								scroller.scrollTo('panels-container', {
									duration: 800,
									delay: 0,
									smooth: true,
								});
							}}
						/>
					}
					{activePanelIndex < (totalPanels - 1) &&
						<Button.UI
							icon="angle-right"
							id="panelNextBtn"
							colorMode="orange"
							onClickHandler={() => {
								handleNextClick();
								scroller.scrollTo('panels-container', {
									duration: 800,
									delay: 0,
									smooth: true,
								});
							}}
						/>
					}
				</Fragment>
			}
			
			<div className="panel-text-container">
				{arePanelsVisible &&
					<h2>Why Newhouse?</h2>
				} 
				<motion.ul
					className="tenets"
					animate={arePanelsVisible ? "open" : "closed"}
				>
					{panelsArr.map((panelSlug, key) => {
						if (key === activePanelIndex) {
							return <motion.li
								key={key}
								initial="closed"
								variants={panelLabelVariants}
							>Your <span>{panelSlug}</span></motion.li>
						}
					})}
				</motion.ul>
			</div>
			
			</div>
		</div>
	</Fragment>
	)
}
export default Panels;
