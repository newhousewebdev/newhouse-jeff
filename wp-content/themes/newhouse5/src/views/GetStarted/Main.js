import React from 'react';
import { motion } from 'framer-motion';

const panelContent = [
	{
		name: "Education",
		slug: "education",
		subsections: [
			{
				heading: "Academic programs",
				image: "academic-programs-1200.jpg",
				content: "Our top-rated academic programs cover all aspects of the industry, making Newhouse the most comprehensive communications school in the country. Whatever your academic and career focus, you’ll find what you need here."
			},
			{
				heading: "Scholarly endeavors",
				image: "scholarly-endeavors-1200.jpg",
				content: "At Newhouse, we’re doing more than teaching communications principles and skills—we’re putting them to work. The school houses centers and labs dedicated to social media, free speech, social commerce, sports media and more, as well as research labs and specialized programs in areas like political reporting. "
			},
			{
				heading: "Innovative work",
				image: "innovative-work-1200.jpg",
				content: "The communications industry is constantly changing and evolving. With a degree from Newhouse, you’ll be ready to face that uncertainty—and to embrace it. Entrepreneurship is built into our curricula, and supported by the Newhouse Center for Digital Media Entrepreneurship, as well as places like the Experimental Media Lab and the Alan Gerry Center for Media Innovation."
			},
		]
	},
	{
		name: "Experience",
		slug: "experience",
		subsections: [
			{
				heading: "On campus...",
				image: "on-campus-1200.jpg",
				content: "The Newhouse School’s three-building complex is a leading-edge learning environment where you’ll be exposed to the tools and technologies used by professionals in the field. Dick Clark Studios is a high-tech entertainment production facility that rivals many Hollywood studios. Our digital news center features a contemporary news set, a green screen, state-of-the-art lighting systems and cameras. The Collaborative Media Room is home to our student-produced multimedia news site, The NewsHouse. These are just a few examples of what you’ll find inside Newhouse."
			},
			{
				heading: "...and beyond",
				image: "and-beyond-2-1200.jpg",
				content: "Syracuse is Newhouse’s home, but the school has strong ties to New York, Los Angeles and other large cities across the country. Our students complete at least one internship—usually more—during their Newhouse education, most of in major metropolitan areas. And our satellite campus programs in New York and Los Angeles provide students with the opportunity to live, learn and work in the news and entertainment capitals of the world."
			},
			{
				heading: "Endless opportunities",
				image: "endless-opportunities-1200.jpg",
				content: "You can join our student-run advertising or public relations firms and work with real clients. You can produce live pre-game and half-game shows for ACC Network Extra. You can travel to New Hampshire to cover presidential primary events. You can enter national competitions and be an award-winner in your field even before your graduate. At every turn, you’ll find ways to supplement and enhance your education at Newhouse."
			},
		]
	},
	{
		name: "Connections",
		slug: "connections",
		subsections: [
			{
				heading: "Students",
				image: "students-1200.jpg",
				content: "At Newhouse, you are part of an elite group of students who come from different backgrounds and locations, but who share a passion for communications and a vision for success. Your fellow students are your collaborators and friends, your competitors and champions. They push you to be your best and celebrate your successes alongside their own."
			},
			{
				heading: "Faculty",
				image: "faculty-1200.jpg",
				content: "Newhouse faculty bring a wealth of experience and professional connections to the classroom; many spent years in the field before joining Newhouse, and some continue professional work on the side. Faculty serve not just as your teachers, but also as your mentors, guiding you through your education and towards your career. And they will continue to be a source of encouragement, advice and support long after you’ve finished your time at Newhouse."
			},
			{
				heading: "Alumni",
				image: "alumni-1200.jpg",
				content: "At the heart of the Newhouse School’s reputation for excellence are our alumni. The famed “Newhouse Network” is one of the most accomplished groups of communications professionals in the world. They make themselves available to students in multiple ways—they host visiting students on professional tours in New York in LA, they visit campus to speak to classes and they’re willing to go the extra mile to assist students with job, internship and networking opportunities. Newhouse students have access to our alumni database of more than 6,000 alumni who have agreed to provide students with guidance, advice and—of course—job leads!"
			},
		]
	},
	{
		name: "Future",
		slug: "future",
		subsections: [
			{
				heading: "Career guidance",
				image: "career-guidance-1200.jpg",
				content: "From the moment you come to Newhouse until long after you graduate, the Newhouse Career Development Center (CDC) will be your secret weapon. As a student, you’ll benefit from seminars on resume writing, interviewing, networking and the job search, and have regular access to job and internship opportunities. You’ll also attend career fairs tailored specifically for communications professionals, and have opportunities to visit and shadow alumni in the field. As a Newhouse graduate, you continue to have access to CDC resources, including the Newhouse Network, our alumni database, for career support."
			},
			{
				heading: "Dream job",
				image: "dream-job-1200.jpg",
				content: "From classroom learning to on-the-job training, Newhouse gives you everything you need to find career success. Some 97 percent of recent Newhouse graduates reported having found a position in their desired field within six months of graduation. Our alumni span the industry—from journalism and photography to advertising and public relations, from music business and film to television and radio—you will find Newhouse graduates everywhere."
			},
			{
				heading: "Success stories",
				image: "success-stories-2-1200.jpg",
				content: "Just take a look at our alumni and you’ll start to understand where a Newhouse degree can take you. Among our successful alumni are CBS News White House Correspondent Weijia Jiang G’06; Pulitzer Prize-winning Washington Post reporter Eli Saslow ’04; Google director of technology Lori Sobel ’98; and award-winning screenwriter Michael H. Weber ’00."
			},
		]
	}
];

const mainColumnVariants = {
	visible: {
		y: 0,
		opacity: 1,
		transition: {
			duration: .5,
		},
	},
	hidden: {
		y: 200,
		opacity: 0,
		transition: {
			duration: .5,
		},
	},
};

const Main = ({ activePanelName }) => {
	const panelsArr = [
		"education",
		"experience",
		"connections",
		"future"
	];

	const getPanelIndex = (panelName) => {
		return panelsArr.indexOf(panelName);
	}
	const activePanelIndex = getPanelIndex(activePanelName);

	const { subsections } = panelContent[activePanelIndex];

	return <motion.div 
		className="gs--main" 
		name={activePanelName}
		initial="hidden"
		animate="visible"
		>
			<motion.div 
			className="container"
			variants={mainColumnVariants}
			initial="hidden"
			animate="visible"
			>
				{subsections.map((subsection, key) => {
					const { heading, content, image } = subsection;
					const imgSrc = require(`../../sass/img/${image}`);
					return <div key={key} className="gs--subsection">
						<div className="gs--subsection--image">
							<img src={imgSrc.default} alt="#" />
						</div>
						<div className="gs--subsection--content">
							<h3>{heading}</h3>
							<p dangerouslySetInnerHTML={{ __html: content }}></p>
						</div>
					</div>
				})}
			</motion.div>
	</motion.div>
}
export default Main;
