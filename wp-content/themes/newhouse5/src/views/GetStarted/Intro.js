import React from 'react';
import { motion } from 'framer-motion';
import * as Scroll from 'react-scroll';

import Button from '../../components/Button';

const scroller = Scroll.scroller;

const sectionVariants = {
	open: {
		width: "100vw",
		opacity: 1,
		transition: {
			duration: .5,
			delay: .5,
			when: "beforeChildren",
			staggerChildren: 1,
		},
	},
	closed: {
		width: 0,
		opacity: 0,
		transition: {
			duration: .5,
			//x: { stiffness: 1000 },
		},
	},
};

const h2Variants = {
	open: { 
		opacity: 1, 
		x: 0,
		transition: {
			duration: .5,
			delay: 0.9
		},
	},
	closed: { 
		opacity: 0, 
		x: -100 
	},
}
const pVariants = {
	open: {
		opacity: 1,
		x: 0,
		transition: {
			duration: .5,
			delay: 1
		},
	},
	closed: {
		opacity: 0,
		x: -100
	},
}
const btnVariants = {
	open: {
		opacity: 1,
		x: 0,
		transition: {
			duration: .5,
			delay: 1.2
		},
	},
	closed: {
		opacity: 0,
		x: -100
	},
}

const Intro = ({ setPanelsVisible }) => {
	return (
		<section 
		className="gs--section gs--intro blue" 
		>
			
			<motion.div 
			className="container--content"
			initial="closed"
			animate="open"
			>
				<motion.h2
				initial="closed"
				variants={h2Variants}
				className="intro--title"
				>It all starts here.</motion.h2>
				<motion.p 
				className="intro--description"
				initial="closed"
				variants={pVariants}
				>At the Newhouse School, today’s communications students are tomorrow’s communications leaders. From advertising to journalism to sports broadcasting to film—and everything in between—our programs span the industry, and provide students with the education, training and connections they need to succeed. If you’re dreaming of a career in communications, the Newhouse School is the place to get started.</motion.p>
			</motion.div>
			<motion.div 
				initial="closed"
				variants={btnVariants}
				animate="open">
			<Button.UI 
			colorMode="orange" 
			icon="angle-down"
			id="gsDown"
			onClickHandler={() => {
				setPanelsVisible(true);
				scroller.scrollTo('panels-container', {
					duration: 800,
					delay: 50,
					smooth: true,
					isDynamic: true
				});
			}}
			/>
			</motion.div>
		</section>
	)
}
export default Intro;
