import React, { useState, useEffect, useContext, useRef } from 'react';

// scroll utility
import * as Scroll from 'react-scroll';
const scroller = Scroll.scroller;

import queryString from 'query-string';

// hooks
import { useLocation, useHistory } from 'react-router-dom';
//import usePageTracking from '../../hooks/usePageTracking';

// regions
import Intro from './Intro';
import Panels from './Panels';
import Main from './Main';
import Actions from './Actions';

// components
import Button from '../../components/Button';
import SEO from '../../components/SEO';

// context
import {
	GetStartedPreviousPageContext
} from '../../api/ContextAPI';

const GetStarted = () => {
	//const initTracking = usePageTracking();
	
	// local state
	const [arePanelsVisible, setPanelsVisible] = useState(false);
	const [arePanelsEngaged, setPanelsEngaged] = useState(false);
	const [activePanelName, setActivePanelName] = useState(null);
	const [arePanelsLocked, setPanelsLocked] = useState(false);
	const [activePanelDegree, setActivePanelDegree] = useState(null);
	const [isTogglebarActive, setTogglebarActive] = useState(false);
	
	// context
	const [previousPage, setPreviousPage] = useContext(GetStartedPreviousPageContext);
	
	useEffect(() => {
		// in case you hit reload on Get Started, it will allow you to exit back to the homepage
		previousPage === null ? setPreviousPage('/') : '';
	}, [previousPage]);

	const location = useLocation();
	//console.log({location});

	const { ref } = useRef();

	let parsedLocation = queryString.parse(location.search);
	//console.log(parsedLocation);

	let history = useHistory();

	useEffect(() => {
		//console.log("useEffect for arePanelsVisible");
		//console.log({ arePanelsVisible});
		if (parsedLocation.degree !== undefined) {
			//console.log("panel degree is defined");
			//console.log({ arePanelsVisible });
			setActivePanelName(parsedLocation.panel);
			setActivePanelDegree(parsedLocation.degree)
			setPanelsVisible(true);
			setPanelsEngaged(true);
			setPanelsLocked(true);
			setTogglebarActive(true);
			scroller.scrollTo('tiles', {
				containerId: ref,
				duration: 800,
				delay: 50,
				smooth: true,
				isDynamic: true
			});
		} else if (parsedLocation.panel !== undefined) {
			//console.log("panel is defined but not degree");
			setActivePanelName(parsedLocation.panel);
			setPanelsVisible(true);
			setPanelsEngaged(true);
			setPanelsLocked(true);
			scroller.scrollTo('panels-container', {
				containerId: ref,
				duration: 800,
				delay: 50,
				smooth: true,
				isDynamic: true
			});
		} else if (arePanelsVisible) {
			//console.log("nothing is defined, but panels were just made visible");
			scroller.scrollTo('panels-container', {
				duration: 800,
				delay: 50,
				smooth: true,
				isDynamic: true
			});
		}
	}, [arePanelsVisible]);

	return (
		<div className="gs" id="gsOverlay" name="gs-top" ref={ref}>
			<SEO 
				title="Get Started | Newhouse School"
				uri ='/get-started'
				description="At the Newhouse School, today’s communications students are tomorrow’s communications leaders. From advertising to journalism to sports broadcasting to film—and everything in between—our programs span the industry, and provide students with the education, training and connections they need to succeed. If you’re dreaming of a career in communications, the Newhouse School is the place to get started."
			/>
			<Button.UI icon="times" id="gsClose" colorMode="orange" onClickHandler={() => {
				history.push(previousPage);
			}} />
			
			<Intro setPanelsVisible={setPanelsVisible} />
			<div name="panels-container">
			{arePanelsVisible &&
				<section 
				className={`gs--section gs--panels blue ${arePanelsEngaged ? 'engaged' : ''} ${arePanelsLocked ? 'locked' : ''}`}
				>
				<Panels
					arePanelsVisible={arePanelsVisible}
					activePanelName={activePanelName}
					setActivePanelName={setActivePanelName}
					arePanelsLocked={arePanelsLocked}
					setPanelsLocked={setPanelsLocked}
					arePanelsEngaged={arePanelsEngaged}
					setPanelsEngaged={setPanelsEngaged}
					activePanelDegree={activePanelDegree}
				/>
				{arePanelsLocked &&
					<Button.UI icon="angle-up" id="gsTop" colorMode="orange" onClickHandler={() => {
						scroller.scrollTo('panels-container', {
							duration: 800,
							delay: 0,
							smooth: true,
						});
					}} />
				}
				{/* 
				The <div name="#"></div> stay on the outside for react-scroll purposes...
				The div name has to be in the DOM tree in order for the react-scroll "slide" to work. It won't wait for it to be loaded after state changes, triggering a "target not found" error message.
				*/}
				<div name="main">
				{(arePanelsLocked && activePanelName) &&
					<Main
						activePanelName={activePanelName}
					/>
				}
				</div>
				<div name="actions">
					{arePanelsLocked &&
						<Actions
							activePanelName={activePanelName}
							activePanelDegree={activePanelDegree}
							setActivePanelDegree={setActivePanelDegree}
							isTogglebarActive={isTogglebarActive}
							setTogglebarActive={setTogglebarActive}
						/>
					}
				</div>
			</section>
			}
			</div>
		
		</div>
	)
}
export default GetStarted;
