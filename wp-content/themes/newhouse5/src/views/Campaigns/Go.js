import React, { Fragment, useState, useEffect } from 'react';
import { renderToStaticMarkup } from 'react-dom/server';
import { Link } from 'react-router-dom';
import { useQuery } from 'react-apollo';

import importScript from '../../hooks/importScript';
//import wFormsCallbacks from './scripts/wFormsCallbacks';
import { 
	gCaptchaReadyCallback, 
	wformsReadyCallback,
	enableSubmitButton,
	disableSubmitButton,
	onloadCallback
} from './scripts/wFormsCallbacks';
import javascriptWarning from './scripts/javascriptWarning';
import timeStart from './scripts/timeStart';
import parse from 'html-react-parser';

// utilities
import { getLedeParagraph } from '../../api/programFunctions';

// components
import Loader from '../../components/Loader';
import Logo from '../../components/Logo';
import Page from '../../views/Page/Page';
import Container from '../../components/Layout/Container';
import Row from '../../components/Layout/Row';
import Col from '../../components/Layout/Col';
import Footer from '../../regions/Footer';

// query
import PageQuery from '../../queries/page.gql';

// iframe html
import NewhouseWIF from './Newhouse.WIF.Form89.html';

// spot styling
import './Go.scss';

const GoHeader = () => {
	return <header>
		<Container>
			<Row>
				<Col>
					<Link to="/"><Logo.Primary className="lg" /></Link> 
				</Col>
			</Row>
		</Container>
	</header>
}

const GoScripts = () => {
	//console.log("GoScripts!");
	importScript("https://www.google.com/recaptcha/api.js?render=explicit&hl=en_US", gCaptchaReadyCallback);
	importScript(javascriptWarning); 
	importScript(timeStart);
	importScript("https://syr.tfaforms.net/wForms/3.11/js/wforms.js", wformsReadyCallback);
	importScript("https://syr.tfaforms.net/js/iframe_message_helper_internal.js?v=2");
	return <Fragment />
}
const GoFeaturedImage = ({featuredImage, title}) => {
	const { sourceUrl, altText } = featuredImage;
	return <div className="page--featured-image fixed-height" style={{ backgroundImage: `url(${sourceUrl})`}}>
		<div className="page--featured-image--anchor">
			<Container>
				<div className="anchor--underline">
					<h1 dangerouslySetInnerHTML={{ __html: title }} />
				</div>
			</Container>
		</div>
	</div >
}
const GoFooter = () => {
	return <footer className="site-footer">
		<Container>
			<Row>
				<Col><p style={{fontSize: "1rem"}}>Copyright 2020, Newhouse School at Syracuse University.</p></Col>
				<Col>
					<nav className="nav nav--footer_nav">
						<ul className="menu menu--row menu--dark menu--footer_nav">
						<li className="menu--item">
							<a href="https://www.syracuse.edu/life/accessibility-diversity/accessible-syracuse/">Accessibility</a>
						</li>
						<li className="menu--item">
							<a href="https://www.syracuse.edu/about/contact/emergencies/">Emergencies</a>
						</li>
						<li className="menu--item">
							<a href="https://www.syracuse.edu/about/site/privacy-policy/">Privacy</a>
						</li>
					</ul>
					</nav>
				</Col>
			</Row>
		</Container>
	</footer>
}

const Go = () => {
	const [goForm, setGoForm] = useState(null);

	const options = {
		replace: (domNode) => {
			const { attribs, children } = domNode;
			if (!attribs) return;

			if (domNode.name === "script") {
				//console.log(domNode);
				//scripts.push(domNode);
				return <Fragment />;
			}

		}
	};

	useEffect(() => {
		//console.log("filteredForm useEffect");
		const filteredForm = parse(NewhouseWIF, options);
		setGoForm(filteredForm);
	}, [data]);

	const { loading, error, data } = useQuery(PageQuery, {
		variables: {
			slug: 'go',
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const {
		title, content, featuredImage, blocks,
	} = data.pageBy;

	return <Fragment>
		<GoHeader />
		<GoFeaturedImage 
		featuredImage={featuredImage} 
		title="Your Communications Career<br /> Starts Here" />
		<Container>
			<Row>
				<Col sm={8}>
				<Page.Main>
						<div dangerouslySetInnerHTML={{ __html: getLedeParagraph(blocks) }}></div>
					<Page.Content
						//ledeParagraph={null}
						content={content}
					>	
					</Page.Content>
				</Page.Main>
				</Col>
				<Col sm={4}>
					<h3 style={{ color: "#000E54" }} className="mb--1 pt--1">For more information:</h3>
					<p style={{ color: "#000E54", fontSize: "1rem" }} className="mb--2">Fill out the short form below and one of our recruitment specialists will be in touch via email.</p>
					{goForm &&
						<div dangerouslySetInnerHTML={{ __html: renderToStaticMarkup(goForm) }}></div>
					}
				</Col>
			</Row>	
		</Container>
		<GoFooter />
		<GoScripts />
	</Fragment>
}

export default Go;
