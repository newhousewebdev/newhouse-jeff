import React, { Fragment, useState, useEffect } from 'react';
import { useForm } from "react-hook-form";
import { Link } from 'react-router-dom';
import { useQuery } from 'react-apollo';
import axios from "axios";

// utilities
import { getLedeParagraph } from '../../api/programFunctions';

// components
import Loader from '../../components/Loader';
import Logo from '../../components/Logo';
import Page from '../../views/Page/Page';
import Form from '../../components/Form/Form';
import Container from '../../components/Layout/Container';
import Row from '../../components/Layout/Row';
import Col from '../../components/Layout/Col';
import Footer from '../../regions/Footer';
import Button from '../../components/Button';

// query
import PageQuery from '../../queries/page.gql';

// iframe html
//import NewhouseWIF from './Newhouse.WIF.Form89.html';
const NewhouseWIF = require('./Newhouse.WIF.Form89.js');

const GoForm = () => {

	const { register, handleSubmit, watch, errors } = useForm();


	const FORM_TIME_START = Math.floor((new Date).getTime() / 1000);
	let formElement = document.getElementById("89");
	console.log({ formElement });

	useEffect(() => {
		formElement = document.getElementById("89");
		console.log({ formElement });
	}, []);
	/*
	useEffect(() => {
		console.log("use effect for form");
		const FORM_TIME_START = Math.floor((new Date).getTime() / 1000);
		let formElement = document.getElementById("517");
		console.log({formElement});
		let appendJsTimerElement = function () {
			console.log("appendJsTimerElement");
			let formTimeDiff = Math.floor((new Date).getTime() / 1000) - FORM_TIME_START;
			let cumulatedTimeElement = document.getElementById("tfa_dbCumulatedTime");
			if (null !== cumulatedTimeElement) {
				let cumulatedTime = parseInt(cumulatedTimeElement.value);
				if (null !== cumulatedTime && cumulatedTime > 0) {
					formTimeDiff += cumulatedTime;
				}
			}
			let jsTimeInput = document.createElement("input");
			jsTimeInput.setAttribute("type", "hidden");
			jsTimeInput.setAttribute("value", formTimeDiff.toString());
			jsTimeInput.setAttribute("name", "tfa_dbElapsedJsTime");
			jsTimeInput.setAttribute("id", "tfa_dbElapsedJsTime");
			jsTimeInput.setAttribute("autocomplete", "off");
			if (null !== formElement) {
				formElement.appendChild(jsTimeInput);
			}
		};
		if (null !== formElement) {
			if (formElement.addEventListener) {
				formElement.addEventListener('submit', appendJsTimerElement, false);
			} else if (formElement.attachEvent) {
				formElement.attachEvent('onsubmit', appendJsTimerElement);
			}
		}
	}, []);
	*/	

	const customSubmit = () => {
		console.log("appendJsTimerElement");
		let formTimeDiff = Math.floor((new Date).getTime() / 1000) - FORM_TIME_START;
		let cumulatedTimeElement = document.getElementById("tfa_dbCumulatedTime");
		if (null !== cumulatedTimeElement) {
			let cumulatedTime = parseInt(cumulatedTimeElement.value);
			if (null !== cumulatedTime && cumulatedTime > 0) {
				formTimeDiff += cumulatedTime;
			}
		}
		let jsTimeInput = document.createElement("input");
		jsTimeInput.setAttribute("type", "hidden");
		jsTimeInput.setAttribute("value", formTimeDiff.toString());
		jsTimeInput.setAttribute("name", "tfa_dbElapsedJsTime");
		jsTimeInput.setAttribute("id", "tfa_dbElapsedJsTime");
		jsTimeInput.setAttribute("autocomplete", "off");
		if (null !== formElement) {
			formElement.appendChild(jsTimeInput);
		}
		//handleSubmit(onSubmit, onError);
	}
	
	var cors_api_url = 'https://cors-anywhere.herokuapp.com/';
	function doCORSRequest(options, printResult) {
		var x = new XMLHttpRequest();
		x.open(options.method, cors_api_url + options.url);
		x.onload = x.onerror = function () {
			printResult(
				options.method + ' ' + options.url + '\n' +
				x.status + ' ' + x.statusText + '\n\n' +
				(x.responseText || '')
			);
		};
		if (/^POST/i.test(options.method)) {
			x.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		}
		x.send(options.data);
	}

	const onSubmit = (data, e) => {
		
		console.log(data);
		data = JSON.stringify(data);
		console.log(data);
		
		doCORSRequest({
			method: 'POST',
			url: 'https://syr.tfaforms.net/responses/processor',
			data: data
		}, function printResult(result) {
			console.log({result});
		});
		
		/*return fetch('https://syr.tfaforms.net/responses/processor', {
			method: 'POST', // *GET, POST, PUT, DELETE, etc.
			//mode: 'no-cors', // no-cors, *cors, same-origin
			//cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
			credentials: 'include', // include, *same-origin, omit
			//headers: {
				//'Access-Control-Allow-Origin': '*',
				//'Content-Type': 'application/json'
				//'Content-Type': 'application/x-www-form-urlencoded',
		//	},
			//redirect: 'follow', // manual, *follow, error
			//referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
			body: data,
			//body: JSON.stringify(data) // body data type must match "Content-Type" header
		})
			.then((res) => res.json())
			.then((res) => JSON.parse(res))
			.catch((err) => { console.error(err); });
		*/
		return axios
			.post(
				'https://cors-anywhere.herokuapp.com/https://syr.tfaforms.net/rest/responses/processor',
				data,
				{
					//withCredentials: false,
					//credentials: 'include',
					//mode: 'no-cors',
					//headers: {
						//'Content-type': 'application/json',
						//'Access-Control-Allow-Credentials': 'true',
						//'Access-Control-Allow-Origin': '*',
						//'Access-Control-Allow-Headers': 'Content-Type,Authorization',
						//'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,OPTIONS',

						//'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
					//}
					//headers: { 'Access-Control-Allow-Credentials': true },
					//crossorigin: true
					//crossDomain: true
				}
			)
			.then(res => {
				console.log({ res });
			})
		
	}
	const onError = (errors, e) => console.log(errors, e);
	//console.log(watch("tfa_1877"));

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [emailAddress, setEmailAddress] = useState("");
	const [verifyEmailAddress, setVerifyEmailAddress] = useState("");
	const [programOfInterest, setProgramOfInterest] = useState("");
	const [additionalProgramOfInterest, setAdditionalProgramOfInterest] = useState("");
	const [anticipatedStartYear, setAnticipatedStartYear] = useState("");
	const [anticipatedStartTerm, setAnticipatedStartTerm] = useState("");
	const [comments, setComments] = useState("");

	
	const handleFirstNameChange = (event) => {
		event.preventDefault();
		const { value } = event.target;
		setFirstName(value);
	}
	const handleLastNameChange = (event) => {
		event.preventDefault();
		const { value } = event.target;
		setLastName(value);
	}
	const handleEmailAddressChange = (event) => {
		event.preventDefault();
		const { value } = event.target;
		setEmailAddress(value);
	}
	const handleVerifyEmailAddressChange = (event) => {
		event.preventDefault();
		const { value } = event.target;
		setVerifyEmailAddress(value);
	}
	const handleProgramOfInterestChange = (event) => {
		//event.preventDefault();
		const { value } = event.target;
		setProgramOfInterest(value);
		
	}
	const handleAdditionalProgramOfInterestChange = (event) => {
		//event.preventDefault();
		const { value } = event.target;
		setAdditionalProgramOfInterest(value);
	}
	const handleAnticipatedStartYearChange = (event) => {
		//event.preventDefault();
		const { value } = event.target;
		setAnticipatedStartYear(value);
	}
	const handleAnticipatedStartTermChange = (event) => {
		event.preventDefault();
		const { value } = event.target;
		setAnticipatedStartTerm(value);
	}
	const handleCommentsChange = (event) => {
		event.preventDefault();
		const { value } = event.target;
		setComments(value);
	}

	const programsArr = [
		{
			"name" : "Advertising",
			"slug" : "advertising",
			"id": "tfa_2301",
			"id2": "tfa_2312"
		},
		{
			"name": "Audio Arts",
			"slug": "audio-arts",
			"id": "tfa_2606",
			"id2": "tfa_2608"
		},
		{
			"name": "Broadcast and Digital Journalism",
			"slug": "broadcast-and-digital-journalism",
			"id": "tfa_2303",
			"id2": "tfa_2314"
		},
		{
			"name": "Goldring Arts Journalism and Communications",
			"slug": "arts-journalism",
			"id": "tfa_2600",
			"id2": "tfa_2601"
		},
		{
			"name": "Magazine, News and Digital Journalism",
			"slug": "magazine-news-and-digital-journalism",
			"id": "tfa_2602",
			"id2": "tfa_2604"
		},
		{
			"name": "Media and Education",
			"slug": "media-and-education",
			"id": "tfa_2582",
			"id2": "tfa_2585"
		},
		{
			"name": "Multimedia, Photography and Design",
			"slug": "multimedia-photography-and-design",
			"id": "tfa_2595",
			"id2": "tfa_2596"
		},
		{
			"name": "New Media Management",
			"slug": "new-media-management",
			"id": "tfa_2306",
			"id2": "tfa_2317"
		},
		{
			"name": "Public Diplomacy and Global Communications",
			"slug": "public-diplomacy-and-global-communications",
			"id": "tfa_2603",
			"id2": "tfa_2605"
		},
		{
			"name": "Public Relations",
			"slug": "public-relations",
			"id": "tfa_2309",
			"id2": "tfa_2320"
		},
		{
			"name": "Television, Radio and Film",
			"slug": "television-radio-and-film",
			"id": "tfa_2310",
			"id2": "tfa_2321"
		},
	];
	const programsOptionsArr = programsArr.map((option, key) => <option
		key={key}
		id={option.id}
		value={option.id}
	>
		{option.name}
	</option>);

	const additonalProgramsOptionsArr = programsArr.map((option, key) => <option
		key={key}
		id={option.id2}
		value={option.id2}
	>
		{option.name}
	</option>);
	
	const yearsArr = [
		{
			"year" : 2021,
			"id": "tfa_1083"
		},
		{
			"year" : 2022,
			"id": "tfa_2597"
		},
		{
			"year" : 2023,
			"id": "tfa_2598"
		},
		{
			"year" : 2024,
			"id": "tfa_2599"
		},
		{
			"year": 2025,
			"id": "tfa_2612"
		}
	];
	const yearsOptionsArr = yearsArr.map((option, key) => <option
		key={key}
		value={option.id}
		id={option.id}
	>
		{option.year}
	</option>);

	const termsArr = [
		{
			"term" : "Fall",
			"id" : "tfa_286"
		},
		{
			"term": "Spring",
			"id": "tfa_290"
		},
		{
			"term": "Summer",
			"id": "tfa_292"
		},
	];
	const termsOptionsArr = termsArr.map((option, key) => <option
		key={key}
		value={option.id}
		id={option.id}
	>
		{option.term}
	</option>);
	

	return <Form 
		method="post" 
		onSubmitHandler={handleSubmit(onSubmit, onError)}
		//action="https://syr.tfaforms.net/rest/responses/processor"
		id="89"
		>
		<Row>
			<Col>
				<h3 className="mb--4">For more information:</h3>
				<Form.Control
					type="hidden"
					id="tfa_dbElapsedJsTime"
					defaultValue="500"
					inputRef={register}
				/>
				<Form.Control
					type="hidden"
					id="tfa_383"
					name="tfa_383"
					defaultValue="Newhouse WIF"
					inputRef={register}
				/>
				<Form.Control
					type="hidden"
					id="tfa_2298"
					name="tfa_2298"
					defaultValue="Inquiry"
					inputRef={register}
				/>
				<Form.Control
					type="hidden"
					id="tfa_891"
					name="tfa_891"
					defaultValue="Graduate"
					inputRef={register}
				/>
				<Form.Control
					type="hidden"
					id="tfa_2300"
					name="tfa_2300"
					defaultValue="S.I. Newhouse School of Public Communications"
					inputRef={register}
				/>
				<Form.Control
					type="hidden"
					id="tfa_2586"
					name="tfa_2586"
					defaultValue="Graduate"
					inputRef={register}
				/>
				<Form.Control
					type="hidden"
					id="tfa_dbFormId"
					name="tfa_dbFormId"
					defaultValue="89"
					inputRef={register}
				/>
				<Form.Control
					type="hidden"
					id="tfa_dbResponseId"
					name="tfa_dbResponseId"
					defaultValue=""
					inputRef={register}
				/>
				<Form.Control
					type="hidden"
					id="tfa_dbControl"
					name="tfa_dbControl"
					defaultValue="38b3e2caa3e56ef6e5c3570aca3a4176"
					inputRef={register}
				/>
				<Form.Control
					type="hidden"
					id="tfa_dbVersionId"
					name="tfa_dbVersionId"
					defaultValue="28"
					inputRef={register}
				/>
				<Form.Control
					type="hidden"
					id="tfa_switchedoff"
					name="tfa_switchedoff"
					defaultValue=""
					inputRef={register}
				/>
				<Form.Group>
					<Form.Label htmlFor="tfa_11">First name</Form.Label>
					<Form.Control 
					type="text" 
					id="tfa_11" 
					defaultValue={firstName}
					className={errors.tfa_11 ? 'invalid' : ''}
					placeholder="Enter first name..." 
					inputRef={register({ required: true })}
					onChange={handleFirstNameChange}
					/>
					{errors.tfa_11 && <Form.Text className="alert">
						This field is required.
    				</Form.Text>}
				</Form.Group>
				<Form.Group>
					<Form.Label htmlFor="tfa_12">Last name</Form.Label>
					<Form.Control 
					type="text" 
					id="tfa_12" 
					defaultValue={lastName}
					className={errors.tfa_12 ? 'invalid' : ''}
					placeholder="Enter last name..."
					inputRef={register({ required: true })} 
					onChange={handleLastNameChange}
					/>
					{errors.tfa_12 && <Form.Text className="alert">
						This field is required.
    				</Form.Text>}
				</Form.Group>
				<Form.Group>
					<Form.Label htmlFor="tfa_20">Email address</Form.Label>
					<Form.Control 
					type="email" 
					id="tfa_20" 
					defaultValue={emailAddress}
					className={errors.tfa_20 ? 'invalid' : ''}
					placeholder="Enter email address..." 
					inputRef={register({
						required: "Required",
						pattern: {
							value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
							message: "Invalid email address"
						}
					})}
					onChange={handleEmailAddressChange}
					/>
					{errors.tfa_20 && <Form.Text className="alert">
						{errors.tfa_20.message}
    				</Form.Text>}
				</Form.Group>
				<Form.Group>
					<Form.Label htmlFor="tfa_21">Verify email</Form.Label>
					<Form.Control 
					type="text" 
					id="tfa_21" 
					defaultValue={verifyEmailAddress}
					className={errors.tfa_21 ? 'invalid' : ''}
					placeholder="Enter address again..."
					inputRef={register({
						required: "Required",
						validate: value => value === emailAddress
					})}
					onChange={handleVerifyEmailAddressChange} 
					/>
					{errors.tfa_21 && <Form.Text className="alert">
						Email addresses don't match.
    				</Form.Text>}
				</Form.Group>
				<Form.Group>
					<Form.Label htmlFor="tfa_1877">Select program of interest</Form.Label>
					<Form.Select
						options={programsOptionsArr} 
						className={errors.tfa_1877 ? 'invalid' : ''}
						name="tfa_1877"
						defaultValue={programOfInterest}
						id="tfa_1877"
						selectRef={register({ 
							required: true,
							validate: value => value !== 'true'
						})}
						defaultOptionLabel="Please select..."
						onChange={handleProgramOfInterestChange}
					/>
					{errors.tfa_1877 && <Form.Text className="alert">
						This field is required.
					</Form.Text>}
				</Form.Group>
				<Form.Group>
					<Form.Label htmlFor="tfa_1897">Additional program of interest (optional)</Form.Label>
					<Form.Select
						options={additonalProgramsOptionsArr}
						className={errors.tfa_1897 ? 'invalid' : ''}
						name="tfa_1897"
						defaultValue={additionalProgramOfInterest}
						id="tfa_1897"
						selectRef={register({
							required: true,
							validate: value => value !== 'true'
						})}
						defaultOptionLabel="Please select..."
						onChange={handleAdditionalProgramOfInterestChange}
					/>
					{errors.tfa_1897 && <Form.Text className="alert">
						This field is required.
					</Form.Text>}
				</Form.Group>
				<Form.Group>
					<Form.Label htmlFor="tfa_294">Anticipated start year</Form.Label>
					<Form.Select
						options={yearsOptionsArr}
						className={errors.tfa_294 ? 'invalid' : ''}
						id="tfa_294"
						selectRef={register({
							required: true,
							validate: value => value !== 'true'
						})}
						name="tfa_294"
						defaultValue={anticipatedStartYear}
						defaultOptionLabel="Choose year..."
						onChange={handleAnticipatedStartYearChange}
					/>
					{errors.tfa_294 && <Form.Text className="alert">
						This field is required.
					</Form.Text>}
				</Form.Group>
				<Form.Group>
					<Form.Label htmlFor="tfa_285">Anticipated start term</Form.Label>
					<Form.Select
						options={termsOptionsArr}
						className={errors.tfa_285 ? 'invalid' : ''}
						id="tfa_285"
						selectRef={register({
							required: true,
							validate: value => value !== 'true'
						})}
						name="tfa_285"
						defaultValue={anticipatedStartTerm}
						defaultOptionLabel="Choose year..."
						onChange={handleAnticipatedStartTermChange}
					/>
					{errors.tfa_285 && <Form.Text className="alert">
						This field is required.
					</Form.Text>}
				</Form.Group>
				<Form.Group>
					<Form.Label htmlFor="tfa_381">Comments or Questions</Form.Label>
					<Form.Textarea
						id="tfa_381"
						inputRef={register({
							required: true
						})}
						name="tfa_381"
						className={errors.tfa_381 ? 'invalid' : ''}
						title="Comments or Questions"
						placeholder="Type here..."
						onChange={handleCommentsChange}
						value={comments}
					/>
					{errors.tfa_381 && <Form.Text className="alert">
						This field is required.
					</Form.Text>}
				</Form.Group>
				<Form.Group>
					<div className="btn--group">
						<Button.UI 
						type="submit" 
						label="Submit"
						colorMode="orange" 
						/>
					</div>
				</Form.Group>
			</Col>
		</Row>		
	</Form>
}

const GoHeader = () => {
	return <header>
		<Container.Content>
			<Row>
				<Col>
					<Link to="/"><Logo.Primary /></Link>
				</Col>
			</Row>
		</Container.Content>
	</header>
}

const Go = () => {
	console.log("Go init");
	
	
	const { loading, error, data } = useQuery(PageQuery, {
		variables: {
			slug: 'go',
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const {
		title, content, uri, excerpt, childPages, featuredImage, blocks,
	} = data.pageBy;

	return <Fragment>
		<GoHeader />
		<Container.Content>
			<Row>
				<Col>
					<Page.Intro 
					title={title} 
					ledeParagraph={getLedeParagraph(blocks)}
					/>
					
				</Col>
			</Row>
		</Container.Content>
		<Page.FeaturedImage featuredImage={featuredImage} />
		<Container>
			<Row>
				<Col sm={8}>
				<Page.Main>
					<Page.Content
						//featuredImage={featuredImage}
						ledeParagraph={null}
						content={content}
					>
						
					</Page.Content>
				</Page.Main>
				</Col>
				<Col sm={4}>
					<h3 className="mb--4" style={{ color: "#000E54"}}>For more information:</h3>
					<div dangerouslySetInnerHTML={{ __html: NewhouseWIF }}></div>
				</Col>
			</Row>	
		</Container>
		<Footer />
	</Fragment>
}

export default Go;
