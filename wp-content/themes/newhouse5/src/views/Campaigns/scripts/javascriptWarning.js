document.addEventListener("DOMContentLoaded", function () {
	//console.log("javascript warning js");
	var warning = document.getElementById("javascript-warning");
	if (warning != null) {
		warning.parentNode.removeChild(warning);
	}
	var oldRecaptchaCheck = parseInt('1');
	if (oldRecaptchaCheck !== -1) {
		var explanation = document.getElementById('disabled-explanation');
		var submitButton = document.getElementById('submit_button');
		if (submitButton != null) {
			submitButton.disabled = true;
			if (explanation != null) {
				explanation.style.display = 'block';
			}
		}
	}
});
