//import localization from './localization';
//import validation from './validation';

//console.log("NewhouseWIF");
// initialize our variables
var captchaReady = 0;
var wFORMSReady = 0;

// when wForms is loaded call this
export function wformsReadyCallback() {
	//console.log("wformsReadyCallback!!! ");
	
	// using this var to denote if wForms is loaded
	wFORMSReady = 1;
	// call our recaptcha function which is dependent on both
	// wForms and an async call to google
	// note the meat of this function wont fire until both
	// wFORMSReady = 1 and captchaReady = 1
	onloadCallback();
	//localization();
	//validation();
}

export function gCaptchaReadyCallback() {
	//console.log("gCaptchaReadyCallback!!!");
//console.log({ grecaptcha});
	// using this var to denote if captcha is loaded
	captchaReady = 1;
	// call our recaptcha function which is dependent on both
	// wForms and an async call to google
	// note the meat of this function wont fire until both
	// wFORMSReady = 1 and captchaReady = 1
	onloadCallback();
};

// add event listener to fire when wForms is fully loaded
document.addEventListener("wFORMSLoaded", wformsReadyCallback);

export function enableSubmitButton() {
	var submitButton = document.getElementById('submit_button');
	var explanation = document.getElementById('disabled-explanation');
	if (submitButton != null) {
		submitButton.removeAttribute('disabled');
		if (explanation != null) {
			explanation.style.display = 'none';
		}
	}
};
export function disableSubmitButton() {
	var submitButton = document.getElementById('submit_button');
	var explanation = document.getElementById('disabled-explanation');
	if (submitButton != null) {
		submitButton.disabled = true;
		if (explanation != null) {
			explanation.style.display = 'block';
		}
	}
};

// call this on both captcha async complete and wforms fully
// initialized since we can't be sure which will complete first
// and we need both done for this to function just check that they are
// done to fire the functionality
export function onloadCallback() {
	//console.log("onloadCallback");
	// if our captcha is ready (async call completed)
	// and wFORMS is completely loaded then we are ready to add
	// the captcha to the page
	if (captchaReady && wFORMSReady) {
		//console.log("captchaReady and wFORMSReady!!!");
		wFORMS.behaviors.prefill.skip = false;
		//console.log({wFORMS});
		//console.log(grecaptcha);
	
		window.grecaptcha.ready(() => {
			//console.log("grecaptcha ready!!"); 
			//console.log(document.getElementById('tfa_captcha_text'));
			grecaptcha.render('tfa_captcha_text', {
				'sitekey': '6LeISQ8UAAAAAL-Qe-lDcy4OIElnii__H_cEGV0C',
				'theme': 'light',
				'size': 'normal',
				'callback': 'enableSubmitButton',
				'expired-callback': 'disableSubmitButton'
			});
		});
	
		var oldRecaptchaCheck = parseInt('1');
		if (oldRecaptchaCheck === -1) {
			var standardCaptcha = document.getElementById("tfa_captcha_text");
			standardCaptcha = standardCaptcha.parentNode.parentNode.parentNode;
			standardCaptcha.parentNode.removeChild(standardCaptcha);
		}

		if (!wFORMS.instances['paging']) {
			//console.log("paging!");
			document.getElementById("g-recaptcha-render-div").parentNode.parentNode.parentNode.style.display = "block";
			//document.getElementById("g-recaptcha-render-div").parentNode.parentNode.parentNode.removeAttribute("hidden");
		}
		document.getElementById("g-recaptcha-render-div").getAttributeNode('id').value = 'tfa_captcha_text';

		var captchaError = '';
		if (captchaError == '1') {
			var errMsgText = 'The CAPTCHA was not completed successfully.';
			var errMsgDiv = document.createElement('div');
			errMsgDiv.id = "tfa_captcha_text-E";
			errMsgDiv.className = "err errMsg";
			errMsgDiv.innerText = errMsgText;
			var loc = document.querySelector('.g-captcha-error');
			loc.insertBefore(errMsgDiv, loc.childNodes[0]);

			/* See wFORMS.behaviors.paging.applyTo for origin of this code */
			if (wFORMS.instances['paging']) {
				var b = wFORMS.instances['paging'][0];
				var pp = base2.DOM.Element.querySelector(document, wFORMS.behaviors.paging.CAPTCHA_ERROR);
				if (pp) {
					var lastPage = 1;
					for (var i = 1; i < 100; i++) {
						if (b.behavior.isLastPageIndex(i)) {
							lastPage = i;
							break;
						}
					}
					b.jumpTo(lastPage);
				}
			}
		}
	}
};
