//console.log("wFORMS behavior validation messages");

var field1 = "tfa_20";
var field2 = "tfa_21";

wFORMS.behaviors.validation.messages['areSame'] = "The given values do not match";

wFORMS.behaviors.validation.rules['areSame'] = {
	selector: "#" + field2,
	check: function (element) {
		var c = document.getElementById(field1);
		if (element.value != c.value) {
			return false;
		}
		return true;
	}
};
