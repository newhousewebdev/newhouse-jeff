import React from 'react';
import Go from './Go';

const Campaigns = ({ children }) => ({ children });

Campaigns.Go = Go;

export default Campaigns;
