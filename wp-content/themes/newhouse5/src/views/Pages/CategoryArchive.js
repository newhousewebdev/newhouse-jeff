import React from 'react';
import { useQuery } from 'react-apollo';
import { useParams } from 'react-router-dom';

// components
import Loader from '../../components/Loader';
import Page from '../Page/Page';
import Grid from '../../components/Layout/Grid';
import Container from '../../components/Layout/Container';

import postsByNewsCategoryQuery from '../../queries/postsByNewsCategory.gql';
import Cards from '../../components/Card/Cards';


const CategoryArchive = () => {
	const { newsCategory } = useParams();
	const { loading, error, data } = useQuery(postsByNewsCategoryQuery, {
		variables: {
			slug: `${newsCategory}`,
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	/* Refactor in useEffect and use State */
	const { name: title, description: content } = data.newsCategories;
	const posts = data.newsCategories.posts.nodes
		.map((post) => <Cards.News key={post.postID} post={post}/>);
	return <Page>
		<Page.Main className="archive">
			{/* <Page.Breadcrumbs
				sectionParent={{
					title: 'Academic Programs',
					uri: `/news/programs/${activeDegree}`,
				}}
				currentPage={currentPage}
				parentItems={parent}
			/> */}
			<Container.Content>
				<Page.Intro
					// label={parent.title}
					title={title}
					// Get the content from the taxonomy description
					content={content}
				/>
			</Container.Content>
			<Page.Section>
				<Grid>
					{posts}
				</Grid>
			</Page.Section>
		</Page.Main>
	</Page>;
};
export default CategoryArchive;
