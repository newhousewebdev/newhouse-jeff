/* eslint-disable camelcase */
import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from 'react-apollo';

import Page from '../Page/Page';

// ! this need to be fetch the entire json file is being bundled
import alumniData from '../Page/featuredAlumni.json';
import Button from '../../components/Button';
import Card from '../../components/Card/Card';
import Loader from '../../components/Loader';
import Grid from '../../components/Layout/Grid';
import Container from '../../components/Layout/Container';

// query
import AcademicPageQuery from '../../queries/academicPage.gql';

// contexts
import {
	ActiveDegreeContext,
} from '../../api/ContextAPI';
import SEO from '../../components/SEO';


const FeaturedAlumni = () => {
	const { programSlug } = useParams();
	const [activeDegree] = useContext(ActiveDegreeContext);

	const { loading, error, data } = useQuery(AcademicPageQuery, {
		variables: {
			slug: `${programSlug}/featured-alumni`,
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	// compared Program Title (e.g. Advertising) to the academic program in the json
	// if it matches, return and place the person into the filtered list
	console.log({ data });
	const {
		title, content, uri, parent,
	} = data.academicProgramBy;
	console.log({ alumniData });
	const filteredAlumni = alumniData.nodes
		.filter((person) => person.node.field_fa_academic_program === parent.title);
	console.log({ filteredAlumni });

	const currentPage = {
		title,
		uri,
	};

	return <Page>
		<Page.Main className="archive archive--featured-alumni">
			<Page.Breadcrumbs
				sectionParent={{
					title: 'Academic Programs',
					uri: `academics/programs/${activeDegree || ''}`,
				}}
				currentPage={currentPage}
				parentItems={parent}
			/>
			<SEO
				title={currentPage.title}
				description=''
				uri={currentPage.uri}
			/>
			<Container.Content>
				<Page.Intro
					label={parent.title}
					title={title}
				/>
				{content
					&& <Page.Content content={content} />
				}
			</Container.Content>
			<Page.Section className={activeDegree}>
				<Grid>
					{filteredAlumni.map((edge, key) => {
						const {
							// eslint-disable-next-line no-shadow
							title,
							field_fa_company_name,
							field_fa_grad_grad_year,
							field_fa_job_position,
							field_fa_portfolio_cta,
							field_fa_portfolio_url,
							field_fa_under_grad_year,
							field_fa_profile_image,
						} = edge.node;

						if (field_fa_profile_image.alt === '') {
							field_fa_profile_image.alt = title;
						}
						return <Card key={key} className="grid--item card--people">
							{field_fa_profile_image
								&& <Card.DrupalProfileImage featuredImage={field_fa_profile_image} />
							}

							<Card.Content alignment="center">
								<Card.Title
									title={title}
									undergraduateYear={field_fa_under_grad_year}
									graduateYear={field_fa_grad_grad_year}
								/>
								<Card.Position position={field_fa_job_position} />
								{field_fa_company_name
									&& <Card.Company company={field_fa_company_name} />
								}
								{field_fa_portfolio_url
									&& <Button.Group justifyContent="center">
										<Button.External path={field_fa_portfolio_url} label={field_fa_portfolio_cta} />
									</Button.Group>
								}
							</Card.Content>
						</Card>;
					})}
				</Grid>
			</Page.Section>
		</Page.Main>
	</Page>;
};
export default FeaturedAlumni;
