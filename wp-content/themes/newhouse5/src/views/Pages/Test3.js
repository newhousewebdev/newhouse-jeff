import React from 'react';
import { useQuery } from 'react-apollo';


import { getLedeParagraph } from '../../api/programFunctions';

import Loader from '../../components/Loader';
import Page from '../Page/Page';
import Container from '../../components/Layout/Container';
import ContactForm7Parser from '../../components/Form/ContactForm7Parser';

// query
import PageQuery from '../../queries/page.gql';

const Test3 = () => {
	const { loading, error, data } = useQuery(PageQuery, {
		variables: {
			slug: "test",
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const {
		title, content, uri, featuredImage, blocks,
	} = data.pageBy;

	const currentPage = {
		title,
		uri,
	};

	return <Page className="page page--test">
		<Container.Content>
			<Page.Main>
				<Page.Intro title={title} />
				<Page.Content
					featuredImage={featuredImage}
					ledeParagraph={getLedeParagraph(blocks)}
					content={content}
				/>
			</Page.Main>
		</Container.Content>
	</Page>
}
export default Test3;
