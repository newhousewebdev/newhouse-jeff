import React, { useState, useEffect } from 'react';
import { useQuery } from 'react-apollo';
import Masonry from 'react-masonry-component';

// components
import Loader from '../../components/Loader';
import Page from '../Page/Page';
import postsWithNewsCategories from '../../queries/postsWithNewsCategories.gql';
import Cards from '../../components/Card/Cards';
import { deepCopy } from '../../api/utilityFunctions';
import Button from '../../components/Button';
import Container from '../../components/Layout/Container';

const SelectControl = ({ options, onChange, value }) => {
	const selectChange = (event) => {
		const { target } = event;
		onChange(parseInt(target.value, 10));
	};
	return <select className="form--select" id="bachelors" onChange={selectChange} value={value}>
		<option value={null}>{value ? 'Any topic' : 'Select a topic'}</option>
		{options}
	</select>;
};

const NewsFilters = ({ data, onChange, newsFilter }) => {
	const optionsArray = data.map((option) => <option
		key={option.databaseId}
		value={option.databaseId}
	>
		{option.name}
	</option>);

	return <Page.Section className="container section-blue">
		<h3 className='line-behind'><span>Filter News by topic</span></h3>
		<form action="" className="form">
			<div className="form--row">
				<div className="form--col">
					<label className="form--label hide">Select news topic to filter by</label>
					<SelectControl options={optionsArray} onChange={onChange} value={newsFilter}/>
				</div>
			</div>
		</form>
	</Page.Section>;
};

const normalizePosts = (rawPosts) => rawPosts.map((post) => {
	const postCopy = deepCopy(post);
	const newsCategoryIDs = post.newsCategories.nodes.map((category) => category.databaseId);
	postCopy.newsCategoryIDs = newsCategoryIDs;
	return postCopy;
});

const NewsArchive = () => {
	const [newsFilter, setNewsFilter] = useState();
	const [newsCategories, setNewsCategories] = useState();
	const [newsArticles, setNewsArticles] = useState();
	const [hasMorePosts, setHasMorePosts] = useState();
	const [isLoadingMorePosts, setIsLoadingMorePosts] = useState(false);
	const perPage = 9;

	const {
		loading,
		error,
		data,
		fetchMore,
	} = useQuery(postsWithNewsCategories, {
		variables: {
			per_page: perPage,
			after: null,
		},
	});

	useEffect(() => {
		if (data && !loading) {
			const postData = data.posts;
			setNewsArticles(normalizePosts(postData.nodes));
			setHasMorePosts(postData.pageInfo.hasNextPage);
			setNewsCategories(data.terms.nodes);
		}
	}, [data]);

	const loadMoreArticles = async () => {
		setIsLoadingMorePosts(true);
		return fetchMore({
			variables: {
				per_page: perPage,
				after: data.posts.pageInfo.endCursor,
			},
			updateQuery: (previousResult, { fetchMoreResult }) => {
				const newPosts = fetchMoreResult.posts.nodes;
				const { pageInfo } = fetchMoreResult.posts;

				return newPosts.length ? {
					posts: {
						nodes: [...previousResult.posts.nodes, ...newPosts],
						pageInfo,
						// eslint-disable-next-line no-underscore-dangle
						__typename: previousResult.posts.__typename,
					},
					terms: { ...previousResult.terms },
				}
					: previousResult;
			},
		})
			.then(() => {
				setIsLoadingMorePosts(false);
			});
	};

	const updateNewsFilter = (value) => {
		setNewsFilter(value);
	};

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const renderCards = () => {
		const filteredPosts = newsArticles
			.filter((post) => (newsFilter ? post.newsCategoryIDs.includes(newsFilter) : true));

		if (filteredPosts.length === 0) {
			if (hasMorePosts && !isLoadingMorePosts) {
				loadMoreArticles();
			}
			if (!hasMorePosts) {
				return <h4>No news stories match your selected topic</h4>;
			}
		}

		return filteredPosts.map((post) => <Cards.News key={post.databaseId} post={post}/>);
	};

	return <Page className="page page--news">
		<Page.Main className="archive">
			<Page.Breadcrumbs
				sectionParent={{
					title: 'Home',
					uri: null,
				}}
				parentItems={{
					title: 'About',
					uri: `about`,
				}}
				currentPage={{
					title: 'Newsroom',
					uri: 'about/news'
				}}
			/>
			<Container.Content>
				<Page.Intro
					title={'Newsroom'}
				/>
			</Container.Content>
			{newsCategories
				&& <NewsFilters
					data={ newsCategories }
					newsFilter={ newsFilter }
					onChange={updateNewsFilter}/>}
			<Page.Section>
				<Masonry
					className="masonry--grid">
					{newsArticles && renderCards()}
					{isLoadingMorePosts && <Loader />}
				</Masonry>
				<Button.Group justifyContent='center'>
					{hasMorePosts && <Button.UI
						id='fetchMore'
						colorMode='orange'
						icon='angle-right'
						onClickHandler={loadMoreArticles}
						label='Show More '
						screenText='Load More Articles'
					/>}
				</Button.Group>
			</Page.Section>
		</Page.Main>
	</Page>;
};

export default NewsArchive;
