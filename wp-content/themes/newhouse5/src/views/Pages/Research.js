import React from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from 'react-apollo';

// utilities
import {
	getLedeParagraph
} from '../../api/programFunctions';

// components
import Page from '../Page/Page';
import LatestFundedActivity from '../Page/Research/LatestFundedActivity';
import ResearchNews from '../Page/Research/ResearchNews';
import ResearchSpaces from '../Page/Research/ResearchSpaces';
import NamedChairs from '../Page/Research/NamedChairs';
import CreativeProfiles from '../Page/Research/CreativeProfiles';
import ResearchClusters from '../Page/Research/ResearchClusters';
import Loader from '../../components/Loader';
import SEO from '../../components/SEO';
import Container from '../../components/Layout/Container';

// query
import PageQuery from '../../queries/page.gql';


// refactor out the graphql portion
const Research = () => {
	const { loading, error, data } = useQuery(PageQuery, {
		variables: {
			slug: "research",
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const {
		title, content, uri, excerpt, childPages, blocks
	} = data.pageBy;

	// console.log({ data }, { ...data.pageBy });
	const currentPage = {
		title,
		uri,
	};

	return (
		<Page className="page page--program">
			<Page.Main>
				<Page.Breadcrumbs
					sectionParent={{ title: 'Home', uri: null }}
					currentPage={currentPage}
				/>
				<SEO
					title={title}
					description={excerpt}
					uri={currentPage.uri}
				/>
				<Container.Content>
					<Page.Intro title={title} />
					<Page.Content
						ledeParagraph={getLedeParagraph(blocks)}
						content={content}
					/>
				</Container.Content>
				<LatestFundedActivity />
				<ResearchNews />
				<ResearchSpaces />
				<NamedChairs />
				<CreativeProfiles />
				<ResearchClusters />
				{childPages && childPages.edges ? (
					<Page.Section>
						{childPages.edges.length > 0 ? (
							childPages.edges.map((subpage, key) => <Page.Subpage key={key} subpage={subpage} />)
						) : ''}
					</Page.Section>
				) : ''}
			</Page.Main>
		</Page>
	);
};
export default Research;
