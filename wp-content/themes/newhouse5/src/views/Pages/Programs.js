import React, { useContext, useEffect } from 'react';
import { useParams } from 'react-router-dom';

import { useQuery } from 'react-apollo';
import gql from 'graphql-tag';

import Page from '../Page/Page';
import Tiles from '../../components/Tiles/Tiles';
import Tile from '../../components/Tiles/Tile';
import Loader from '../../components/Loader';
import SEO from '../../components/SEO';
import Container from '../../components/Layout/Container';

import {
	getDegreeNameBySlug
} from '../../api/utilityFunctions';

import { ActiveDegreeContext } from '../../api/ContextAPI';


const Programs = () => {
	// console.log("PAGE.PROGRAMS loaded");
	const { degreeSlug } = useParams(); 
	const [activeDegree, setActiveDegree] = useContext(ActiveDegreeContext);

	useEffect(() => {
		if (degreeSlug !== undefined) {
			setActiveDegree(degreeSlug);
		} else if (activeDegree === null) {
			setActiveDegree('bachelors');
		} else {
			setActiveDegree(activeDegree);
		}
	}, [activeDegree]);

	const GET_ALL_ACADEMIC_PROGRAMS = gql`
query MyQuery {
   academicPrograms(first: 50) {
    edges {
      node {
		uri
		databaseId
        title
        slug
        excerpt
        degrees {
          edges {
            node {
              slug
            }
          }
        }
      }
    }
  }
  degrees {
    edges {
      node {
        name
		slug
		description
      }
    }
  }
}
	`;
	// fetch from wpgraphql
	const { loading, error, data } = useQuery(GET_ALL_ACADEMIC_PROGRAMS);

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	// do something with academic programs
	const programs = data.academicPrograms.edges;
	// console.log(programs);
	const filteredPrograms = programs.filter((program) => {
		const { degrees } = program.node;
		const slugs = degrees.edges ? degrees.edges.map((degree) => degree.node.slug) : [];
		return slugs.includes(activeDegree);
	});

	// alphabetize
	filteredPrograms.sort((a, b) => {
		const nameA = a.node.title.toUpperCase(); // ignore upper and lowercase
		const nameB = b.node.title.toUpperCase(); // ignore upper and lowercase
		if (nameA < nameB) {
			return -1;
		}
		if (nameA > nameB) {
			return 1;
		}

		// names must be equal
		return 0;
	});

	const degrees = data.degrees.edges;

	const matchingDegree = degrees.find((degree) => degree.node.slug === activeDegree);

	const { name, description } = matchingDegree.node;

	const currentPage = {
		title: 'Academic Programs',
		uri: `/academics/programs/${degreeSlug || ''}`,
	};
	// advertising/bachelors, advertising/masters, and advertising/online are ok
	// mass-communications/doctoral should just be mass-communications
	// media-and-education-cas/certificates should just be media-and-education-cas
	// photography-video-and-design-minor/minors should just be photography-video-and-design-minor
	const allowedDegreeUrlPaths = ['bachelors', 'masters', 'online' ];
	return <Page className={`page page--${activeDegree}`}>
		<SEO
			title={`${activeDegree ? getDegreeNameBySlug(activeDegree) : ''} | ${currentPage.title}`}
			description="Whether you're interested in journalism, interactive communications, advertising, public relations, multimedia storytelling or television and film, you will find a wealth of choices at the Newhouse School."
			uri={`/academics/programs/${degreeSlug || ''}`}
		/>
		<Page.Breadcrumbs
			sectionParent={{
				title: 'Home',
				uri: null,
			}}
			currentPage={currentPage}
		/>
		<Container.Content>
			<Page.Intro
				title="Academic Programs"
			/>
			<Page.Content
				content="<p>Whether you're interested in journalism, interactive communications, advertising, public relations, multimedia storytelling or television and film, you will find a wealth of choices at the Newhouse School.</p>"
			/>
		</Container.Content>
		<Page.ToggleBar
			path="/academics/programs"
			label="Filter by"
			degrees={degrees}
			activeDegree={activeDegree}
			handleOnChange={setActiveDegree}
		/>
		<Page.Section
			className={activeDegree}
		>
			<Tiles type="grid" activeDegree={activeDegree}>
				{filteredPrograms.map((item) => {
					const {
						uri, title, excerpt, databaseId,
					} = item.node;
					return <Tile key={databaseId + Math.random()} activeDegree={activeDegree}>
						<Tile.Hover uri={`${uri}${allowedDegreeUrlPaths.includes(activeDegree) ? `${activeDegree}/` : ''}`} />
						<Tile.Title title={title} />
						<Tile.Excerpt excerpt={excerpt} />
					</Tile>;
				})}
			</Tiles>
		</Page.Section>
	</Page>;
};
export default Programs;
