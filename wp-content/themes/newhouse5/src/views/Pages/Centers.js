import React, { useContext } from 'react';
import { useQuery } from 'react-apollo';

// components
import Page from '../Page/Page';
import Loader from '../../components/Loader';
import Tiles from '../../components/Tiles/Tiles';
import Tile from '../../components/Tiles/Tile';
import SEO from '../../components/SEO';

import { ActiveDegreeContext } from '../../api/ContextAPI';

import GetCentersPageWithListOfCenters from '../../queries/centers/centers.gql';
import Container from '../../components/Layout/Container';

const Centers = () => {
	const [activeDegree] = useContext(ActiveDegreeContext);
	const { loading, error, data } = useQuery(GetCentersPageWithListOfCenters, {
		variables: {
			slug: 'about/centers',
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const { pageBy, centers } = data;

	const {
		title, content, uri, excerpt,
	} = pageBy;

	const currentPage = {
		title,
		uri,
	};

	return (
		<Page className={'page page--centers'}>
			<SEO
				title={title}
				description={excerpt}
				uri={uri}
			/>
			<Page.Breadcrumbs
				sectionParent={{
					title: 'About',
					uri: 'about',
				}}
				currentPage={currentPage}
			/>
			<Container.Content>
				<Page.Intro
					title={title}
				/>
				<Page.Content content={content} />
			</Container.Content>
			<Page.Section>
				<Tiles type="grid">
					{centers.edges.map((item, key) => {
						// eslint-disable-next-line no-shadow
						const { uri, title, excerpt } = item.node;
						return <Tile key={key} activeDegree={activeDegree}>
							<Tile.Hover uri={uri} />
							<Tile.Title title={title} />
							<Tile.Excerpt excerpt={excerpt} />
						</Tile>;
					})}
				</Tiles>
			</Page.Section>
		</Page>
	);
};
export default Centers;
