import React from 'react';


// components
import Page from '../Page/Page';
import Container from '../../components/Layout/Container';

const NotFound = () => (
	<Page className="page page--not-found">
		<Page.Main>
			<Container.Content>
				<Page.Intro title="Page not found" />
				<Page.Content content="Sorry, your page was not found." />
			</Container.Content>
		</Page.Main>
	</Page>
);
export default NotFound;
