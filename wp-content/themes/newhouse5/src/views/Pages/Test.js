import React, {useState, Fragment } from 'react';
import { useQuery } from 'react-apollo';
import axios from "axios";
import parse, { domToReact } from 'html-react-parser';


// utilities
import { getLedeParagraph } from '../../api/programFunctions';
import { getForm } from '../../api/pageFunctions';

// components
import Page from '../Page/Page';
import Loader from '../../components/Loader';
import SEO from '../../components/SEO';
import Button from '../../components/Button';
import Container from '../../components/Layout/Container';

// query
import PageQuery from '../../queries/page.gql';

const ContactForm2 = ({form}) => {
	console.log({form});
	return <h2>Contact form v2 goes here</h2>
}

const ContactForm = () =>  {
	const [name, setName] = useState("");
	const [email, setEmail] = useState("");
	const [subject, setSubject] = useState("");
	const [message, setMessage] = useState("");
	const [messageSent, setMessageSent] = useState(false);
	const [errorMessage, setErrorMessage] = useState(null);

	const onChangeHandler = e => {
		console.log(e.target.value)

		if (e.target.name === "name") {
			setName(e.target.value);
		} else if (e.target.name === "email") {
			setEmail(e.target.value);
		} else if (e.target.name === "subject") {
			setSubject(e.target.value);
		} else if (e.target.name === "message") {
			setMessage(e.target.value);
		} 
	}

	const onSubmit = e => {
		e.preventDefault();
		console.log("onSubmit");

		let formData = new FormData();

		formData.append("your-name", name);
		formData.append("your-email", email);
		formData.append("your-subject", subject);
		formData.append("your-message", message);

		console.log({formData});

		// Debug: Display the key/value pairs
		//for (var pair of formData.entries()) {
		//	console.log(pair[0] + ', ' + pair[1]);
		//}

		axios
			.post(
				`${process.env.SITE_URL}/wp-json/contact-form-7/v1/contact-forms/1193/feedback`,
				formData,
				{
					headers: {
						"content-type": "multipart/form-data",
					},
				}
			)
			.then(res => {
				console.log({res});
				// reset form after sent
				res.data.status === "mail_sent" ? ( 
					<Fragment>
						{setMessageSent(res.data.message)}
						{setName("")}
						{setEmail("")}
						{setSubject("")}
						{setMessage("")}
					</Fragment>
				) : setErrorMessage(res.data.message)
			})
	}

	return <Fragment>
		<form onSubmit={onSubmit}>
			<div className="form">
				<input
					type="text"
					name="name"
					value={name}
					placeholder="Your name..."
					onChange={onChangeHandler}
				/><br />
				<input
					type="email"
					name="email"
					value={email}
					placeholder="Your email..."
					onChange={onChangeHandler}
				/><br />
				<input
					name="subject"
					value={subject}
					type="text"
					placeholder="Your subject..."
					onChange={onChangeHandler}
				/><br />
				<input
					type="textarea"
					name="message"
					value={message}
					placeholder="Your message..."
					onChange={onChangeHandler}
				/>
			</div>
			{messageSent && (
				<p>
					{messageSent}
				</p>
			)}
			{errorMessage && (
				<p>
					{errorMessage}
				</p>
			)}
			<div className="btn--group">
				<Button.UI type="submit" id="formSubmit" colorMode="orange" label="Submit" screenText="Submit form" />
			</div>
		</form>			
	</Fragment>
}


const Test = () => {
	const { loading, error, data } = useQuery(PageQuery, {
		variables: {
			slug: "test",
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const {
		title, content, uri, featuredImage, blocks,
	} = data.pageBy;
	//console.log({blocks});
	//console.log({ data }, { ...data.pageBy });
	const currentPage = {
		title,
		uri,
	};

	

	return <Page className="page page--test">
		<Container.Content>
		<Page.Main>
			<Page.Intro title={title} />
			<Page.Content
				featuredImage={featuredImage}
				ledeParagraph={getLedeParagraph(blocks)}
				content={content}
			/>
		</Page.Main>
		</Container.Content>
	</Page>
};
export default Test;
