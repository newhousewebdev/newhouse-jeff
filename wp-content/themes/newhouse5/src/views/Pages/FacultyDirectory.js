import React, { useContext, useState, useEffect } from 'react';
import { useQuery } from 'react-apollo';
import PageQuery from '../../queries/page.gql';
// components
import Page from '../Page/Page';
import Container from '../../components/Layout/Container';
import Loader from '../../components/Loader';
import SEO from '../../components/SEO';
import CardFeed from '../../components/CardFeed';
import { buildDirectoryCards } from '../../api/directoryUtilities';
import { ArchiveCollectionContext } from '../../api/ContextAPI';
import { fetchDirectoryWithOptions, fetchAcademicPrograms } from '../../api/fetches/DirectoryFetches';
import WithLoading from '../../components/LoadingContainer';
import {
	deepCopy,
	removeUnderscore,
	capitalizeWords,
	removeTrailingS,
} from '../../api/utilityFunctions';
import SelectControl from '../../components/Form/FormSelectControl';


const CardFeedWithLoading = WithLoading(CardFeed);

const DirectorySelectFilter = ({
	taxonomy, onChange, taxonomyKey, value, taxonomyDefaultLabel,
}) => {
	const humanReadableKey = capitalizeWords(removeUnderscore(removeTrailingS(taxonomyDefaultLabel)));
	const optionsArray = taxonomy.map((option) => <option
		key={option.id}
		value={option.id}
	>
		{option.name}
	</option>);

	const updateFilters = (selected) => {
		onChange({
			key: taxonomyKey,
			value: selected,
		});
	};

	return <div className="form--col">
		<SelectControl
			options={optionsArray}
			onChange={updateFilters}
			value={value}
			label={`Select ${humanReadableKey} to filter by`}
			defaultOptionLabel={humanReadableKey}/>
	</div>;
};

const SearchInput = ({ onChange, directoryFilters, metaKey }) => {
	const assignedValue = directoryFilters && directoryFilters[metaKey] ? directoryFilters[metaKey] : '';
	const textInputChange = (event) => {
		const { value } = event.target;
		onChange({ key: metaKey, value });
	};

	return <div style={{
		display: 'flex',
		flexDirection: 'column',
	}}
	className="form--col"
	>
		<label
			htmlFor={metaKey}
			style={{
				lineHeight: '1.2',
				padding: '.25rem',
			}}
			className='text--white'
		>Search by {capitalizeWords(removeUnderscore(metaKey))}</label>
		<input
			type="text"
			name={metaKey}
			id={metaKey}
			onChange={textInputChange}
			value={assignedValue}
			style={{
				lineHeight: '1rem',
				padding: '.25rem',
			}}
		/>
	</div>;
};

const DirectoryFilters = ({
	onChangeFilters, directoryFilters, directoryTaxonomies,
}) => {
	const taxonomyKeys = Object.keys(directoryTaxonomies);

	const taxonomyFilters = taxonomyKeys.map((taxonomyKey) => {
		const taxonomy = directoryTaxonomies[taxonomyKey];
		const taxonomyDefaultLabel = taxonomyKey;

		const value = (directoryFilters && directoryFilters[taxonomyKey])
			? directoryFilters[taxonomyKey] : '';

		return <DirectorySelectFilter
			key={taxonomyKey}
			taxonomy={taxonomy}
			taxonomyKey={taxonomyKey}
			onChange={onChangeFilters}
			value={value}
			taxonomyDefaultLabel={taxonomyDefaultLabel}
		/>;
	});

	return <Page.Section className="container section-blue">
		<h3 className='line-behind'><span>Filter Directory</span></h3>
		<form action="" className="form">
			<div className="form--row" style={{ alignItems: 'flex-end' }}>
				<SearchInput onChange={onChangeFilters} metaKey='last_name' directoryFilters={directoryFilters}/>
				{taxonomyFilters}
			</div>
		</form>
	</Page.Section>;
};

const DirectoryFiltersWithLoading = WithLoading(DirectoryFilters);

const Directory = () => {
	const [currentPage, setCurrentPage] = useState();
	const [archiveCollection, setArchiveCollection] = useContext(ArchiveCollectionContext);
	const [directoryData, setDirectoryData] = useState();
	const [loadingCollection, setLoadingCollection] = useState(true);
	const [loadingTaxonomies, setLoadingTaxonomies] = useState(true);
	const [directoryFilters, setDirectoryFilters] = useState();
	const [directoryTaxonomies, setDirectoryTaxonomies] = useState();
	const metaKeys = ['last_name'];
	const metaKeysMap = {
		last_name: 'name',
	};
	const { loading, error, data } = useQuery(PageQuery, {
		variables: {
			slug: 'about/faculty',
		},
	});

	// initial fetches
	useEffect(() => {
		const getDirectoryData = async () => {
			// ! fix this to be words not ints
			fetchDirectoryWithOptions({
				person_type: 198,
				faculty_type_exclude: '227, 4',
				faculty_type: 2,
			},
			[
				'academic_programs',
			])
				.then((directoryResults) => {
					setDirectoryData(directoryResults);
					setArchiveCollection(directoryResults);
					setLoadingCollection(false);
				})
				.catch((err) => { console.error(err); });

			fetchAcademicPrograms()
				.then((programs) => {
					setDirectoryTaxonomies({ academic_programs: programs });
					setLoadingTaxonomies(false);
				})
				.catch((err) => { console.error(err); });
		};

		getDirectoryData();
	}, []);

	useEffect(() => {
		if (!loading && data) {
			setCurrentPage(data.pageBy);
		}
	}, [data]);

	useEffect(() => {
		if (directoryData && directoryFilters) {
			const filteredCollection = directoryData.filter((person) => {
				const directoryFilterKeys = Object.keys(directoryFilters);
				const conditionsMet = [];

				directoryFilterKeys.forEach((filterKey) => {
					// last name
					if (metaKeys.includes(filterKey)) {
						const metaGroupKey = metaKeysMap[filterKey];
						const personMeta = person.meta.person_meta;
						conditionsMet.push(personMeta[metaGroupKey][filterKey].toLowerCase()
							.includes(directoryFilters[filterKey].toLowerCase()));
					// taxonomy
					} else {
						conditionsMet.push(person[filterKey].includes(directoryFilters[filterKey]));
					}
				});

				return !conditionsMet.includes(false);
			});

			setArchiveCollection(filteredCollection);
		}
	}, [directoryFilters, directoryData]);

	const updateFilters = (newSettings) => {
		const filtersCopy = directoryFilters ? deepCopy(directoryFilters) : {};
		// how do we remove?
		if (!newSettings.value || newSettings.value === '') {
			delete filtersCopy[newSettings.key];
		} else {
			filtersCopy[newSettings.key] = newSettings.value;
		}

		setDirectoryFilters(filtersCopy);
	};

	if (!currentPage) return <Loader />;
	// Refactor into an Error BOundary Component with a 404 route handler
	if (error) return `Error! ${error.message}`;

	return <Page className="page archive--directory">
		<Page.Main className="archive">
			{currentPage.parent ? (
				<Page.Breadcrumbs
					currentPage={currentPage}
					parentItems={currentPage.parent}
				/>
			) : (
				<Page.Breadcrumbs
					sectionParent={{ title: 'Home', uri: '/' }}
					currentPage={currentPage}
				/>
			)}
			<SEO
				title={currentPage.title}
				description={currentPage.excerpt}
				uri={currentPage.uri}
			/>
			<Container.Content>
				<Page.Intro title={currentPage.title} />
				<Page.Content content={currentPage.content} />
			</Container.Content>
			<DirectoryFiltersWithLoading
				isLoading={loadingTaxonomies}
				directoryTaxonomies={directoryTaxonomies}
				onChangeFilters={updateFilters}
				directoryFilters={directoryFilters}
			/>
			{/* filterMessage && <h4>{`Results match:${filterMessage}`}</h4> */}
			<CardFeedWithLoading
				isLoading={loadingCollection}
				cards={buildDirectoryCards(archiveCollection)}
				noResultsMessage='No people were found that matched you selection'
			/>
		</Page.Main>
	</Page>;
};

export default Directory;
