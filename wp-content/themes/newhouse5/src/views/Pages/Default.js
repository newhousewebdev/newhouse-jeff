import React from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from 'react-apollo';

// utilities
import { getLedeParagraph } from '../../api/programFunctions';

// components
import Page from '../Page/Page';
import Loader from '../../components/Loader';
import SEO from '../../components/SEO';
import Container from '../../components/Layout/Container';

// query
import PageQuery from '../../queries/page.gql';


// refactor out the graphql portion
const Default = () => {
	const { pageParentSlug, pageChildSlug, pageGrandchildSlug, pageGreatGrandchildSlug } = useParams();
	// console.log('Default Page in Use', { pageParentSlug }, { pageChildSlug }, { pageGrandchildSlug });

	const getSlug = () => {
		let slug;
		if (pageGreatGrandchildSlug) {
			slug = `${pageParentSlug}/${pageChildSlug}/${pageGrandchildSlug}/${pageGreatGrandchildSlug}`;
		} else if (pageGrandchildSlug) {
			slug = `${pageParentSlug}/${pageChildSlug}/${pageGrandchildSlug}`;
		} else if (pageChildSlug) {
			slug = `${pageParentSlug}/${pageChildSlug}`;
		} else {
			slug = pageParentSlug;
		}
		return slug;
	};

	const { loading, error, data } = useQuery(PageQuery, {
		variables: {
			slug: getSlug(),
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	const {
		title, content, uri, excerpt, childPages, featuredImage, blocks,
	} = data.pageBy;

	//console.log({ data }, { ...data.pageBy });
	const currentPage = {
		title,
		uri,
	};

	return (
		<Page className="page page--program">
			<Page.Main>
				{data.pageBy.parent ? (
					<Page.Breadcrumbs
						currentPage={currentPage}
						parentItems={data.pageBy.parent}
					/>
				) : (
					<Page.Breadcrumbs
						sectionParent={{ title: 'Home', uri: null }}
						currentPage={currentPage}
					/>
				)}
				<SEO
					title={title}
					description={excerpt}
					uri={currentPage.uri}
				/>
				<Container.Content>
					<Page.Intro title={title} />
					<Page.Content
						featuredImage={featuredImage}
						ledeParagraph={getLedeParagraph(blocks)}
						content={content}
					/>
				</Container.Content>
				{childPages && childPages.edges ? (
					<Page.Section>
						{childPages.edges.length > 0 ? (
							childPages.edges.map((subpage, key) => <Page.Subpage key={key} subpage={subpage} />)
						) : ''}
					</Page.Section>
				) : ''}
			</Page.Main>
		</Page>
	);
};
export default Default;
