import React from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from 'react-apollo';

// utilities
import {
	buildPageSlug,
	getActiveDegreeSlug,
	getPageContent,
	getPageSections,
	getLabel,
	checkDegreeSlug,
	getParentItems,
	getLedeParagraph,
	getCoreEmbedYoutubeBlock
} from '../../api/programFunctions';

import { filterNewsCategories } from '../../api/newsFunctions';

// query
import PostQuery from '../../queries/post.gql';

// components
import Page from '../Page/Page';
import Loader from '../../components/Loader';
import SEO from '../../components/SEO';
import Container from '../../components/Layout/Container';

const SingleNews = () => {
	const { postSlug } = useParams();

	const { loading, error, data } = useQuery(PostQuery, {
		variables: {
			slug: postSlug,
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;
	console.log(data.postBy);
	const { 
		title, 
		slug, 
		uri, 
		date,
		content, 
		featuredImage,
		displayFeaturedImage,
		author, 
		customAuthorName, 
		displayAuthorToggle,
		excerpt, 
		newsCategories,
		blocks
		} = data.postBy;

	const postAuthor = customAuthorName ? customAuthorName : author.name;
	
	const filteredNewsCategories = filterNewsCategories(newsCategories.nodes);
	
	//console.log(getCoreEmbedYoutubeBlock(blocks));

	return (
		<Page className="page page--default">
			<Page.Main>
				<SEO
					title={title}
					description={excerpt}
					//uri={`/news/${slug}`}
					uri={uri}
				/>
				<Page.Breadcrumbs
					sectionParent={{
						title: 'Home',
						uri: null,
					}}
					parentItems={{ title: "News", uri: "about/news" }}
					currentPage={{ title, uri }}
				/>
				<Container.Content>
					<Page.Intro 
						title={title} 
						author={displayAuthorToggle ? postAuthor : null} 
						categories={filteredNewsCategories}
						date={date}
					/>
					<Page.Content 
						ledeParagraph={getLedeParagraph(blocks)}
						featuredImage={displayFeaturedImage ? featuredImage : null}
						content={content} 
					/>
				</Container.Content>
			</Page.Main>
		</Page>
	);
};
export default SingleNews;
