import React, { useContext, useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { EventsDataContext } from '../../api/ContextAPI';

import Page from '../Page/Page';
import SEO from '../../components/SEO';
import Icon from '../../components/Icon';
import Container from '../../components/Layout/Container';
import { getDateFormatted } from '../../api/eventsFunctions';
import Button from '../../components/Button';
import { getSingleEventViaAjax } from '../../api/fetches/EventsFetches';

const cleanRegistrationLabel = (label) => (!label || label === 'None' ? '' : label);

const buildAccessibilityList = (accessibility) => <ul>
	{accessibility.map((item) => <li key={item}>{item}</li>)}
</ul>;

const Event = () => {
	const [eventsArr] = useContext(EventsDataContext);
	const [eventIsLoading, setEventIsLoading] = useState(true);
	const [singleEvent, setSingleEvent] = useState(false);
	const { dateSlug, eventSlug } = useParams();
	// this where the event is limited
	const slug = `${dateSlug}/${eventSlug}`;

	// Initial Data
	useEffect(() => {
		const fetchEvent = async () => {
			getSingleEventViaAjax(slug)
				.then((res) => {
					console.log({ res });
					setSingleEvent(res);
					setEventIsLoading(false);
				});
		};

		const eventInCollection = eventsArr.filter((event) => event.slug === slug);

		if (!eventInCollection[0]) {
			fetchEvent();
		} else {
			setSingleEvent(eventInCollection[0]);
			setEventIsLoading(false);
		}
	}, []);

	const {
		title,
		excerpt,
		description,
		startDateTimeLocal,
		imageUrl,
		imageDescription,
		region,
		registrationUrl,
		registrationLabel,
		accessibility,
		contactName,
		contactEmail,
		contactPhone,
	} = singleEvent;
	const formattedDate = getDateFormatted(startDateTimeLocal);

	return <Page className="page page--event">
		<Page.Main>
			{!eventIsLoading && <Container.Content>
				<SEO
					title={title}
					uri={`event/${slug}`}
					description={excerpt}
				/>
				<Page.Intro
					label="Event"
					title={singleEvent.title}
				/>
				<div className="page--event--meta">

					{formattedDate
							&& <p className="page--event--startTime"><Icon icon="calendar-alt" /> {formattedDate}</p>
					}
					<p className="page--event--region"><Icon icon="map-marker-alt" /> {region}</p>
				</div>
				{imageUrl
						&& <img
							src={imageUrl}
							alt={imageDescription}
							className="page--event--featuredImage"
						/>
				}
				<Page.Content content={description} >
					{registrationUrl && <Button.Group justifyContent="left">
						<Button.External path={registrationUrl} label={cleanRegistrationLabel(registrationLabel)} colorMode="orange" />
					</Button.Group>}
				</Page.Content>
				<section className='event-details'>
					<h2 className='label'>Event Details</h2>
					{/* <h3>Open to</h3> */}
					<h3>Contact</h3>
					<span className='contact-name'>{contactName}</span>
					<address>
						{contactEmail && <a href={`mailto:${contactEmail}`}>{contactEmail}</a>}
						{contactPhone && <a href={`tel:${contactPhone}`}>{contactPhone}</a>}
					</address>
					<h3>Accessibility</h3>
					{accessibility[0] !== '' ? buildAccessibilityList(accessibility) : <p>{`Contact ${contactName} to request accommodations.`}</p>}
				</section>
			</Container.Content>}
		</Page.Main>
	</Page>;
};

export default Event;
