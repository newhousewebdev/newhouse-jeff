/* A TOC of the types of Pages available */
// import Archive from '../../trash/Archive';
import CategoryArchive from './CategoryArchive';
import Center from './Center';
import Centers from './Centers';
import Default from './Default';
import Directory from './Directory';
import FacultyDirectory from './FacultyDirectory';
import Event from './Event';
import Faculty from './Faculty';
import FeaturedAlumni from './FeaturedAlumni';
import SingleNews from './SingleNews';
import News from './News';
import NotFound from './NotFound';
import Person from '../Page/Person/Person';
import Program from './Program';
import Programs from './Programs';
import Research from './Research';
import EventsArchive from './Events';
import Test from './Test';
import Test2 from './Test2';
import Test3 from './Test3';

const Pages = ({ children }) => ({ children });

// Pages.Archive = Archive;
Pages.CategoryArchive = CategoryArchive;
Pages.Center = Center;
Pages.Centers = Centers;
Pages.Default = Default;
Pages.Directory = Directory;
Pages.FacultyDirectory = FacultyDirectory;
Pages.Event = Event;
Pages.Events = EventsArchive;
Pages.Faculty = Faculty;
Pages.FeaturedAlumni = FeaturedAlumni;
Pages.News = News;
Pages.NotFound = NotFound;
Pages.Person = Person;
Pages.Program = Program;
Pages.Programs = Programs;
Pages.Research = Research;
Pages.SingleNews = SingleNews;
Pages.Test = Test;
Pages.Test2 = Test2;
Pages.Test3 = Test3;

export default Pages;
