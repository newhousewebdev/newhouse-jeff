import React, {
	useContext, useEffect, useState, Fragment,
} from 'react';

// packages
import { useQuery } from 'react-apollo';
import PageQuery from '../../queries/page.gql';

// context
import {
	EventsDataContext,
} from '../../api/ContextAPI';
import { getEventsViaAjax, getMoreEventsViaAjax } from '../../api/fetches/EventsFetches';
import { parameterizeObject } from '../../api/fetches/DirectoryFetches';

// components
import Page from '../Page/Page';
import SEO from '../../components/SEO';
import Loader from '../../components/Loader';
import Button from '../../components/Button';
import Cards from '../../components/Card/Cards';
import Grid from '../../components/Layout/Grid';
import Form from '../../components/Form/Form';
import Container from '../../components/Layout/Container';
import Row from '../../components/Layout/Row';
import Col from '../../components/Layout/Col';

// utilities
import { eventCategories, eventTypes } from '../../api/eventsFunctions';
import { deepCopy } from '../../api/utilityFunctions';

const sampleEvents = [
	{
		eventId: 90211,
		title: 'Bananas',
		types: [
			{
				description: '',
				id: 149,
				name: 'Juggling',
				slug: 'juggling',
			},
		],
		categories: [
			{
				description: 'Life → Career Development',
				id: 119,
				name: 'Recreation',
				slug: 'recreation',
			},
		],
		startDateTimeLocal: '2020-09-18 15:00:00',
		region: 'Online',
		excerpt: '<p>The 2nd session in the Newhouse CDC&#8217;s Job Hunt series, Mastering Networking, teaches the networking best practices to establish and maintain professional relationships in the communications industry.</p>',
		slug: '2020-sep-18/communications-industry-job-hunt-session-2-mastering-networking-21147',
		organizers: [
			{
				description: '<p>For the Newhouse Career Development Center’s seminars for Newhouse students only.</p>',
				email: 'ldrich@syr.edu',
				id: 19833,
				name: 'Newhouse Career Development Center: Seminars',
				phone: '315.443.4700',
				slug: 'newhouse-career-development-center-seminars',
				website: '',
			},
		],
	},
	{
		eventId: 90210,
		title: 'Apples',
		types: [
			{
				description: '',
				id: 149,
				name: 'Witchcraft',
				slug: 'witchcraft',
			},
		],
		categories: [
			{
				description: 'Life → Career Development',
				id: 119,
				name: 'Recreation',
				slug: 'recreation',
			},
		],
		startDateTimeLocal: '2020-09-18 15:00:00',
		region: 'Online',
		excerpt: '<p>The 2nd session in the Newhouse CDC&#8217;s Job Hunt series, Mastering Networking, teaches the networking best practices to establish and maintain professional relationships in the communications industry.</p>',
		slug: '2020-sep-18/communications-industry-job-hunt-session-2-mastering-networking-21147',
		organizers: [
			{
				description: '<p>For the Newhouse Career Development Center’s seminars for Newhouse students only.</p>',
				email: 'ldrich@syr.edu',
				id: 19833,
				name: 'Newhouse Career Development Center: Seminars',
				phone: '315.443.4700',
				slug: 'newhouse-career-development-center-seminars',
				website: '',
			},
		],
	},
	{
		eventId: 90214,
		title: 'Crepes',
		types: [
			{
				description: '',
				id: 149,
				name: 'Pastries',
				slug: 'pastries',
			},
		],
		categories: [
			{
				description: 'Life → Career Development',
				id: 119,
				name: 'Recreation',
				slug: 'recreation',
			},
		],
		startDateTimeLocal: '2020-09-18 15:00:00',
		region: 'Online',
		excerpt: '<p>The 2nd session in the Newhouse CDC&#8217;s Job Hunt series, Mastering Networking, teaches the networking best practices to establish and maintain professional relationships in the communications industry.</p>',
		slug: '2020-sep-18/communications-industry-job-hunt-session-2-mastering-networking-21147',
		organizers: [
			{
				description: '<p>For the Newhouse Career Development Center’s seminars for Newhouse students only.</p>',
				email: 'ldrich@syr.edu',
				id: 19833,
				name: 'Newhouse Career Development Center: Seminars',
				phone: '315.443.4700',
				slug: 'newhouse-career-development-center-seminars',
				website: '',
			},
		],
	},
	{
		eventId: 90215,
		title: 'Tim Cake',
		types: [
			{
				description: '',
				id: 149,
				name: 'Pastries',
				slug: 'pastries',
			},
		],
		categories: [
			{
				description: 'Life → Career Development',
				id: 119,
				name: 'Timber framing',
				slug: 'timber-framing',
			},
		],
		startDateTimeLocal: '2020-09-18 15:00:00',
		region: 'Online',
		excerpt: '<p>The 2nd session in the Newhouse CDC&#8217;s Job Hunt series, Mastering Networking, teaches the networking best practices to establish and maintain professional relationships in the communications industry.</p>',
		slug: '2020-sep-18/communications-industry-job-hunt-session-2-mastering-networking-21147',
		organizers: [
			{
				description: '<p>For the Newhouse Career Development Center’s seminars for Newhouse students only.</p>',
				email: 'ldrich@syr.edu',
				id: 19833,
				name: 'Newhouse Career Development Center: Seminars',
				phone: '315.443.4700',
				slug: 'newhouse-career-development-center-seminars',
				website: '',
			},
		],
	},
];

const EventsArchive = () => {
	const [currentPage, setCurrentPage] = useState();
	const [eventsArr, setEventsArr] = useContext(EventsDataContext);
	const [eventsIsLoading, setEventsIsLoading] = useState(true);
	const [eventsNextFetch, setEventsNextFetch] = useState();
	const [moreEvents, setMoreEvents] = useState();
	const [activeEventFilters, setActiveEventFilters] = useState([]);
	const [filteredEventsCollection, setFilteredEventsCollection] = useState([]);

	const { loading, error, data } = useQuery(PageQuery, {
		variables: {
			slug: 'events',
		},
	});

	const initEventFilters = (events, filters) => <Form>
		<Row>
			{filters.map((filter, key) => {
				const reducedItems = events
					.map((event) => event[filter])
					.filter((event, index, self) => index === self.findIndex((x) => x[0].slug === event[0].slug));

				const selectOptions = reducedItems.map((item, key) => {
					// console.log({item});
					const { name, id } = item[0];
					return <option key={key} value={id}>
						{name}
					</option>;
				});

				const onSelectChange = (id) => {
					// console.log({id});
					if (id) {
						const newFilter = {
							[filter]: id,
						};
						setActiveEventFilters([newFilter]);
					} else {
						setActiveEventFilters([]);
					}
				};
				return <Col key={key}>
					<Form.SelectControl
						options={selectOptions}
						onChange={onSelectChange}
						value={activeEventFilters.length > 0 ? activeEventFilters[0][filter] : 'default'}
						name={filter}
						label={filter}
						defaultOptionLabel={`event ${filter}`}
					/>
				</Col>;
			})}
		</Row>
	</Form>;

	const filterEvents = (events) => {
		const filterKeys = activeEventFilters.length > 0 ? Object.keys(activeEventFilters[0]) : null;
		// console.log({ activeEventFilters});
		// console.log({ filterKeys });

		const filteredEvents = activeEventFilters.length > 0 ? (
			events.filter((event) => {
				if (event[filterKeys] && event[filterKeys].length > 0) {
					const ids = event[filterKeys].map((entry) => entry.id);
					return !!ids.includes(activeEventFilters[0][filterKeys]);
				}
				return false;
			})
		) : events;
		setFilteredEventsCollection(filteredEvents);
	};

	const loadMoreEvents = async () => {
		setEventsIsLoading(true);
		const url = eventsNextFetch;
		if (url && moreEvents) {
			await getMoreEventsViaAjax(url)
				.then((res) => {
					setEventsArr([...eventsArr, ...res.data]);
					setEventsNextFetch(res.links.next);
					setMoreEvents(!!res.links.next); // Bang bang you are a boolean
					setEventsIsLoading(false);
				});
		}
	};

	// callbacks
	// 1. once GQL completes, put queried page data into state
	useEffect(() => {
		if (!loading && data) {
			setCurrentPage(data.pageBy);
		}
	}, [data]);

	// 2. run immediately, go fetch events
	useEffect(() => {
		setEventsIsLoading(true);
		const fetchData = async () => {
			const params = {
				group: 3438,
				per_page: 9,
			};
			const searchParams = parameterizeObject(params);
			getEventsViaAjax(searchParams)
				.then((res) => {
					// setEventsArr(sampleEvents);
					setEventsArr(res.data);
					setEventsNextFetch(res.links.next);
					setMoreEvents(!!res.links.next); // Bang bang shrimp is delicious
					setEventsIsLoading(false);
				});
		};
		if (eventsArr.length === 0) {
			fetchData();
		} else {
			setEventsIsLoading(false);
		}
	}, []);

	useEffect(() => {
		setEventsIsLoading(true);
		filterEvents(eventsArr);
		setEventsIsLoading(false);
	}, [eventsArr, activeEventFilters]);

	// the wall
	if (!currentPage) return <Loader />;
	if (error) return `Error! ${error.message}`;

	return <Page className="page archive--events">
		<Page.Main className="archive">
			<Page.Breadcrumbs
				sectionParent={{ title: 'Home', uri: null }}
				currentPage={currentPage}
			/>
			<SEO
				title={currentPage.title}
				description={currentPage.excerpt}
				uri={currentPage.uri}
			/>
			<Container.Content>
				<Page.Intro title={currentPage.title} />
				<Page.Content content={currentPage.content} />
			</Container.Content>
			{!eventsIsLoading
				&& <Page.Filters title="Filter events">
					{initEventFilters(eventsArr, ['types', 'categories'])}
				</Page.Filters>
			}
			<Page.Section>
				{eventsIsLoading && <Loader />}
				{filteredEventsCollection
				&& <Fragment>
					<Grid>
						{filteredEventsCollection.map((event, key) => <Cards.Event key={key} event={event} />)}
					</Grid>
					{(!eventsIsLoading && moreEvents)
						&& <Button.UI
							id='fetchMore'
							colorMode='orange'
							icon='angle-right'
							onClickHandler={loadMoreEvents}
							label='Show More Events'
							screenText='Load More Events'
						/>
					}
				</Fragment>
				}
			</Page.Section>
		</Page.Main>
	</Page>;
};

export default EventsArchive;
