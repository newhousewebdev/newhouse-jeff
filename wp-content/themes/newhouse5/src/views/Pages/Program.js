import React, { useContext, Fragment } from 'react';
import { useQuery } from 'react-apollo';
import { useParams } from 'react-router-dom';

// utilities
import {
	buildPageSlug,
	getActiveDegreeSlug,
	getPageContent,
	getPageSections,
	getLabel,
	checkDegreeSlug,
	getParentItems,
	getLedeParagraph
} from '../../api/programFunctions';

// query
import GET_ACADEMIC_PROGRAM_AND_DEGREES_BY_SLUG from '../../queries/programs/academicProgramsAndDegrees.gql';

// utilities
import { extractSubUrl } from '../../api/utilityFunctions';

// base view
import Page from '../Page/Page';

// components
import Loader from '../../components/Loader';
import CallToActions from '../Page/Program/CallToActions';
import SEO from '../../components/SEO';
import Container from '../../components/Layout/Container';

// contexts
import { ActiveDegreeContext } from '../../api/ContextAPI';


const Program = () => {
	// console.log('Program');
	const { programSlug, degreeSlug, subpageSlug } = useParams();
	const [activeDegree, setActiveDegree] = useContext(ActiveDegreeContext);

	const slugToQuery = buildPageSlug(programSlug, degreeSlug, subpageSlug);
	const fullPageSlug = `academics/${slugToQuery}`;
	console.log({ fullPageSlug });
	const { loading, error, data } = useQuery(GET_ACADEMIC_PROGRAM_AND_DEGREES_BY_SLUG, {
		variables: {
			slug: slugToQuery,
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	// console.log({ data });

	const {
		title, 
		slug, 
		content, 
		featuredImage, 
		degrees, 
		programs,
		uri, 
		childAcademicPrograms, 
		parent, 
		excerpt, 
		blocks
	} = data.academicProgramBy;

	// advertising/bachelors is good
	// advertising/curriculum is not
	const activeDegreeSlug = getActiveDegreeSlug(degreeSlug, activeDegree, degrees);

	// if page is...
	if (degreeSlug && checkDegreeSlug(degreeSlug)) {
		// e.g. advertising/masters
		// set activeDegree to masters
		// prevent careers in advertising/careers from being set
		setActiveDegree(degreeSlug);
	} else if ((!activeDegree) && degrees.edges && degrees.edges.length > 0) {
		// e.g. advertising
		// if there's no active degree already set in context
		// set to first degree associated with program
		// advertising would be set activeDegree to bachelors
		// arts-journalism would be set activeDegree to masters
		setActiveDegree(degrees.edges[0].node.slug);
	}

	// build an array of page sections to output in the main column
	const pageSections = getPageSections(childAcademicPrograms, activeDegreeSlug);
	// console.log({pageSections});

	// const currentPage = {
	// 	title,
	// 	uri,
	// };

	const getTitleForUtilityBar = () => {
		if (parent && parent.parent) {
			return parent.parent.title;
		} if (parent) {
			return parent.title;
		}
		return title;
	};
	const getSubPagesForUtilityBar = () => {
		//console.log({ parent });
		const degreeSlugArr = ['bachelors', 'masters', 'online', 'doctoral'];

		let subPagesArr = [];

		if (parent && parent.parent && parent.parent.childAcademicPrograms) {
			subPagesArr = parent.parent.childAcademicPrograms.edges;
		} else if (parent && !degreeSlugArr.includes(parent.slug) && parent.childAcademicPrograms) {
			subPagesArr = parent.childAcademicPrograms.edges;
		} else {
			subPagesArr = pageSections;
		}
		//console.log({ subPagesArr });
		return subPagesArr.filter((subpage) => !degreeSlugArr.includes(subpage.node.slug));
	};

	return <Page className="page page--program">
		<Page.Main>
			<SEO
				title={title}
				description={excerpt}
				uri={`academics/${programSlug}/${activeDegreeSlug}`}
			/>
			<Page.Breadcrumbs
				sectionParent={{
					title: 'Academic Programs',
					uri: `academics/programs/${activeDegree}`,
				}}
				currentPage={{title, uri}}
				parentItems={getParentItems(parent)}
			/>
			<Container.Content>
				<Page.Intro title={title} label={getLabel(parent, activeDegreeSlug)} />
				<Page.Content
					ledeParagraph={getLedeParagraph(blocks)}
					featuredImage={featuredImage}
					content={getPageContent(fullPageSlug, childAcademicPrograms, content)} />
			</Container.Content>
		
			{// show if program has multiple degrees
				// and prevent sub-pages from displaying the toggle bar
				// e.g. advertising/careers shouldn't show toggle bar
				// e.g. advertising/bachelors should show toggle bar
				// e.g. bandier-program (single degree) shouldn't show toggle bar
			}
			{/* !Refactor */}
			{/*degrees && degrees.edges && childAcademicPrograms.edges ? (
				degrees.edges.length > 1 ? (
					<Page.ToggleBar
						label="Choose degree"
						path={extractSubUrl(uri)}
						degrees={degrees.edges}
						activeDegree={activeDegree}
						handleOnChange={setActiveDegree}
					/>
				) : ''
				) : ''*/}
			<Page.Section
				className={activeDegree}
				container="fluid">
				{pageSections && pageSections.length > 0 ? (
					pageSections.map((subpage, key) => {
					return <Page.Subpage 
						key={key} 
						subpage={subpage} 
						programSlug={programs.nodes[0].slug} />
					})
				) : ''}
			</Page.Section>
			<Page.Section
				className="cta"
				title="Ready to begin?"
				description="Come see us and let us show you around a little. View the beautiful, iconic campus in any season, tour our state-of-the-art facilities, and get a feel for the student experience at Newhouse."
			>
				<CallToActions degreeSlug={activeDegree ? activeDegree: null} />
			</Page.Section>
		</Page.Main>
	</Page>;
};
export default Program;
