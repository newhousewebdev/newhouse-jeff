import React, { useContext, useState, useEffect } from 'react';
import { useQuery } from 'react-apollo';
import PageQuery from '../../queries/page.gql';
// components
import Page from '../Page/Page';
import Loader from '../../components/Loader';
import SEO from '../../components/SEO';
import CardFeed from '../../components/CardFeed';
import { buildDirectoryCards } from '../../api/directoryUtilities';
import { ArchiveCollectionContext } from '../../api/ContextAPI';
import { fetchFacultyAndStaff, fetchDirectoryTaxonmies } from '../../api/fetches/DirectoryFetches';
import WithLoading from '../../components/LoadingContainer';
import {
	deepCopy,
	removeUnderscore,
	capitalizeWords,
	removeTrailingS,
} from '../../api/utilityFunctions';

import Container from '../../components/Layout/Container';
import Row from '../../components/Layout/Row';
import Col from '../../components/Layout/Col';

import Form from '../../components/Form/Form';


const CardFeedWithLoading = WithLoading(CardFeed);

const DirectorySelectFilter = ({
	taxonomy, onChange, taxonomyKey, value, taxonomyDefaultLabel,
}) => {
	const humanReadableKey = capitalizeWords(removeUnderscore(removeTrailingS(taxonomyDefaultLabel)));
	const optionsArray = taxonomy.map((option) => <option
		key={option.id}
		value={option.id}
	>
		{option.name}
	</option>);

	const updateFilters = (selected) => {
		onChange({
			key: taxonomyKey,
			value: selected,
		});
	};

	return <Col>
		<Form.SelectControl
			options={optionsArray}
			onChange={updateFilters}
			value={value}
			label={`Select ${humanReadableKey} to filter by`}
			defaultOptionLabel={humanReadableKey}/>
	</Col>;
};

const SearchInput = ({ onChange, directoryFilters, metaKey }) => {
	const assignedValue = directoryFilters && directoryFilters[metaKey] ? directoryFilters[metaKey] : '';
	const textInputChange = (event) => {
		const { value } = event.target;
		onChange({ key: metaKey, value });
	};

	return <Form.Group>
		<Form.Label htmlFor={metaKey} className="text--white">
			Search by {capitalizeWords(removeUnderscore(metaKey))}
		</Form.Label>	
		<Form.Control 
			type="text"
			name={metaKey}
			id={metaKey}
			onChange={textInputChange}
			value={assignedValue}
		/>
	</Form.Group>
};

const DirectoryFilters = ({
	onChangeFilters, directoryFilters, directoryTaxonomies,
}) => {
	const taxonomyKeys = Object.keys(directoryTaxonomies);

	const taxonomyFilters = taxonomyKeys.map((taxonomyKey) => {
		let taxonomy = directoryTaxonomies[taxonomyKey];
		const taxonomyDefaultLabel = taxonomyKey === 'person_type' ? 'Position Type' : taxonomyKey;
		if (taxonomyKey === 'person_type') {
			const includedTypes = ['staff', 'faculty'];
			taxonomy = taxonomy.filter((type) => includedTypes.includes(type.slug));
		}

		const value = (directoryFilters && directoryFilters[taxonomyKey])
			? directoryFilters[taxonomyKey] : '';

		return <DirectorySelectFilter
			key={taxonomyKey}
			taxonomy={taxonomy}
			taxonomyKey={taxonomyKey}
			onChange={onChangeFilters}
			value={value}
			taxonomyDefaultLabel={taxonomyDefaultLabel}
		/>;
	});

	return <Page.Section className="container section-blue">
		<h3 className='line-behind'><span>Filter Directory</span></h3>
		<Form>
			<Row>
				<Col>
					<SearchInput onChange={onChangeFilters} metaKey='first_name' directoryFilters={directoryFilters} />
				</Col>
				<Col>
					<SearchInput onChange={onChangeFilters} metaKey='last_name' directoryFilters={directoryFilters} />
				</Col>
			</Row>
			<Row>
				{taxonomyFilters}
			</Row>
		</Form>
	</Page.Section>;
};

const DirectoryFiltersWithLoading = WithLoading(DirectoryFilters);

const Directory = () => {
	const [currentPage, setCurrentPage] = useState();
	const [archiveCollection, setArchiveCollection] = useContext(ArchiveCollectionContext);
	const [directoryData, setDirectoryData] = useState();
	const [loadingCollection, setLoadingCollection] = useState(true);
	const [loadingTaxonomies, setLoadingTaxonomies] = useState(true);
	const [directoryFilters, setDirectoryFilters] = useState();
	// const [filterMessage, setfilterMessage] = useState();
	const [directoryTaxonomies, setDirectoryTaxonomies] = useState();
	const metaKeys = ['first_name', 'last_name'];
	const metaKeysMap = {
		first_name: 'name',
		last_name: 'name',
	};
	const { loading, error, data } = useQuery(PageQuery, {
		variables: {
			slug: 'about/directory',
		},
	});

	// initial fetches
	useEffect(() => {
		const getDirectoryData = async () => {
			fetchFacultyAndStaff([
				'person_type',
				'academic_programs',
				// 'academic_departments',
				'administrative_departments',
			])
				.then((directoryResults) => {
					setDirectoryData(directoryResults);
					setArchiveCollection(directoryResults);
					setLoadingCollection(false);
				})
				.catch((err) => { console.error(err); });

			fetchDirectoryTaxonmies()
				.then((taxonomies) => {
					setDirectoryTaxonomies(taxonomies);
					setLoadingTaxonomies(false);
				})
				.catch((err) => { console.error(err); });
		};

		getDirectoryData();
	}, []);

	useEffect(() => {
		if (!loading && data) {
			setCurrentPage(data.pageBy);
		}
	}, [data]);

	useEffect(() => {
		if (directoryData && directoryFilters) {
			const filteredCollection = directoryData.filter((person) => {
				const directoryFilterKeys = Object.keys(directoryFilters);
				console.log({ directoryFilterKeys });
				const conditionsMet = [];

				directoryFilterKeys.forEach((filterKey) => {
					if (metaKeys.includes(filterKey)) {
						const metaGroupKey = metaKeysMap[filterKey];
						const personMeta = person.meta.person_meta;
						conditionsMet.push(personMeta[metaGroupKey][filterKey].toLowerCase()
							.includes(directoryFilters[filterKey].toLowerCase()));
					} else {
						conditionsMet.push(person[filterKey].includes(directoryFilters[filterKey]));
					}
				});

				return !conditionsMet.includes(false);
			});

			setArchiveCollection(filteredCollection);
		}
	}, [directoryFilters, directoryData]);

	const updateFilters = (newSettings) => {
		const filtersCopy = directoryFilters ? deepCopy(directoryFilters) : {};
		// how do we remove?
		if (!newSettings.value || newSettings.value === '') {
			delete filtersCopy[newSettings.key];
		} else {
			filtersCopy[newSettings.key] = newSettings.value;
		}

		setDirectoryFilters(filtersCopy);
	};

	if (!currentPage) return <Loader />;
	// Refactor into an Error BOundary Component with a 404 route handler
	if (error) return `Error! ${error.message}`;

	return <Page className="page archive--directory">
		<Page.Main className="archive">
			{currentPage.parent ? (
				<Page.Breadcrumbs
					currentPage={currentPage}
					parentItems={currentPage.parent}
				/>
			) : (
				<Page.Breadcrumbs
					sectionParent={{ title: 'Home', uri: '/' }}
					currentPage={currentPage}
				/>
			)}
			<SEO
				title={currentPage.title}
				description={currentPage.excerpt}
				uri={currentPage.uri}
			/>
			<Container.Content>
				<Page.Intro title={currentPage.title} />
				<Page.Content content={currentPage.content} />
			</Container.Content>
			<DirectoryFiltersWithLoading
				isLoading={loadingTaxonomies}
				directoryTaxonomies={directoryTaxonomies}
				onChangeFilters={updateFilters}
				directoryFilters={directoryFilters}
			/>
			{/* filterMessage && <h4>{`Results match:${filterMessage}`}</h4> */}
			<CardFeedWithLoading
				isLoading={loadingCollection}
				cards={buildDirectoryCards(archiveCollection)}
				noResultsMessage='No people were found that matched you selection'
			/>
		</Page.Main>
	</Page>;
};

export default Directory;
