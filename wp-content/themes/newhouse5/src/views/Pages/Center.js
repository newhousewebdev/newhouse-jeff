import React from 'react';
import { useQuery } from 'react-apollo';
import { useParams } from 'react-router-dom';

// utilities
import {
	getLedeParagraph
} from '../../api/programFunctions';

import Page from '../Page/Page';
import Loader from '../../components/Loader';
import SEO from '../../components/SEO';
import Container from '../../components/Layout/Container';

// query
import GET_CENTER_BY_SLUG from '../../queries/centers/center.gql';


const Center = () => {
	const { centerSlug } = useParams();

	const { loading, error, data } = useQuery(GET_CENTER_BY_SLUG, {
		variables: {
			slug: centerSlug,
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	console.log({ data });

	const {
		title, content, uri, parent, excerpt, blocks
	} = data.centerBy;
	const currentPage = {
		title,
		uri,
	};
	return (
		<Page className="page page--center">
			<Page.Main>
				<SEO
					title={title}
					description={excerpt}
					uri={`about/centers/${centerSlug}`}
				/>
				<Page.Breadcrumbs
					sectionParent={{
						title: 'Centers',
						uri: 'about/centers',
					}}
					currentPage={currentPage}
					parentItems={parent}
				/>
				<Container.Content>
					<Page.Intro
						label="Centers"
						title={title}
					/>
					{content && 
						<Page.Content 
						ledeParagraph={getLedeParagraph(blocks)}
						content={content} />
					}
				</Container.Content>
			</Page.Main>
		</Page>
	);
};
export default Center;
