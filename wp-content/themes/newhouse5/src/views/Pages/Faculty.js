import React, { useContext, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useQuery } from 'react-apollo';

// queries
import GET_ACADEMIC_PROGRAM_BY_SLUG from '../../queries/programs/academicPrograms.gql';

// components
import Page from '../Page/Page';
import Loader from '../../components/Loader';
import SEO from '../../components/SEO';
import CardFeed from '../../components/CardFeed';
import WithLoading from '../../components/LoadingContainer';
import Container from '../../components/Layout/Container';

// contexts
import {
	ActiveDegreeContext, ArchiveCollectionContext,
} from '../../api/ContextAPI';
// Api
import { buildDirectoryCards, matchProgramSlugToID, matchDepartmentSlugToID } from '../../api/directoryUtilities';
import { fetchFacultyByProgram, fetchFacultyByDepartment } from '../../api/fetches/DirectoryFetches';


const CardFeedWithLoading = WithLoading(CardFeed);

const Faculty = () => {
	const { programSlug } = useParams();
	const [activeDegree] = useContext(ActiveDegreeContext);
	const [archiveCollection, setArchiveCollection] = useContext(ArchiveCollectionContext);

	const { loading, error, data } = useQuery(GET_ACADEMIC_PROGRAM_BY_SLUG, {
		variables: {
			slug: `${programSlug}/faculty`,
		},
	});
	const [currentPage, setCurrentPage] = useState(null);
	const [currentDepartment, setCurrentDepartment] = useState();
	const [currentProgram, setCurrentProgram] = useState();
	const [loadingCollection, setLoadingCollection] = useState(true);

	useEffect(() => {
		if (loading === false && data) {
			const newPage = data.academicProgramBy;
			setCurrentPage(newPage);
			const newDepartment = newPage.departments.nodes.length
				? newPage.departments.nodes[0].slug : undefined;
			const newProgram = newPage.programs.nodes.length
				? newPage.programs.nodes[0].slug : undefined;
			setCurrentDepartment(newDepartment);
			setCurrentProgram(newProgram);
		}
	}, [loading, data]);

	useEffect(() => {
		setLoadingCollection(true);
		// ? refactor into a reusable hook
		const getFacultyByDepartmentOrProgram = async (departmentName, programName) => {
			if (programName) {
				matchProgramSlugToID(programName)
					.then((programID) => fetchFacultyByProgram(programID))
					.then((facultyData) => {
						setArchiveCollection(facultyData);
						setLoadingCollection(false);
					})
					.catch((err) => { console.error(err); });
			} else if (departmentName) {
				matchDepartmentSlugToID(departmentName)
					.then((departmentID) => fetchFacultyByDepartment(departmentID))
					.then((facultyData) => {
						setArchiveCollection(facultyData);
						setLoadingCollection(false);
					})
					.catch((err) => { console.error(err); });
			}
		};

		getFacultyByDepartmentOrProgram(currentDepartment, currentProgram);
	}, [currentDepartment, currentProgram]);

	// Refactor into One Component
	if (loading) return <Loader />;
	// Refactor into an Error BOundary Component with a 404 route handler
	if (error) return `Error! ${error.message}`;

	if (!currentPage) {
		return <p>Loading page information...</p>;
	}

	return <Page>
		<Page.Main className="archive archive--faculty">
			<Page.Breadcrumbs
				sectionParent={{
					title: 'Academic Programs',
					uri: `academics/programs/${activeDegree || ''}`,
				}}
				currentPage={{
					title: currentPage.title,
					uri: currentPage.uri,
				}}
				parentItems={currentPage.parent}
			/>
			<SEO
				title={currentPage.title}
				description={currentPage.excerpt}
				uri={currentPage.uri}
			/>
			<Container.Content>
				<Page.Intro
					label={currentPage.parent.title}
					title={currentPage.title}
				/>
				{currentPage.content
					&& <Page.Content content={currentPage.content} />
				}
			</Container.Content>
			<CardFeedWithLoading
				isLoading={loadingCollection}
				cards={buildDirectoryCards(archiveCollection)}
				noResultsMessage='No people were found that matched you selection'
			/>
		</Page.Main>
	</Page>;
};
export default Faculty;
