import React, {useContext} from 'react';

// context 
import { ModalShowContext } from '../App';

import Button from '../components/Button';

const Modal = ({ children }) => {
	const [modalShow, setModalShow] = useContext(ModalShowContext);
	
	return (
		<div className="modal">
			<Button.UI
				icon="times"
				id="closeModal"
				colorMode="orange"
				onClickHandler={() => {
					setModalShow(false);
				}}
				screenText='Close Modal'
			/>
			<div className="container">
				{children}
			</div>
		</div>
	)
}
const Page = ({ node, activeProgram }) => {
	const { label, content } = node;
	return (
		<Modal>
			<div className="modal--intro">
				<h3 className="modal--intro--label">{activeProgram}</h3>
				<h1 className="modal--intro--title">{label}</h1>
			</div>
			<div className="container--content">
				<div className="gutenberg--content" dangerouslySetInnerHTML={{ __html: content }}></div>
			</div>
		</Modal>
	)
}
Modal.Page = Page;
export default Modal;
