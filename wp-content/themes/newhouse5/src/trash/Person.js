/* eslint-disable camelcase */
import React from 'react';

import Card from '../components/Card/Card';
import Button from '../components/Button';

const Person = ({ person }) => {
	const {
		title, field_position, path, field_image_single,
	} = person;
	const pathSplit = path.split('/');
	const newPath = `people/${pathSplit[2]}`;
	return (
		<Card className="grid--item">
			{field_image_single
				&& <Card.DrupalProfileImage featuredImage={field_image_single} />
			}
			<div className="card--content text-align-center">
				<h3 className="card--content--title" dangerouslySetInnerHTML={{ __html: title }} />
				{field_position
					&& <p className="card--content--position" dangerouslySetInnerHTML={{ __html: field_position }} />
				}
				{path
					&& <Button.Group justifyContent="center">
						<Button.CTA path={newPath} label="Read bio" type="text" colorMode="orange"  />
					</Button.Group>
				}
			</div>
		</Card>
	);
};
export default Person;
