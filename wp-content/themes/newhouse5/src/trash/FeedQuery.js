import React, { useContext, useEffect, useState } from 'react';

import Section from '../views/Page/Section';
import Grid from '../components/Layout/Grid';
import Card from '../components/Card/Card';
import Loader from '../components/Loader';

import { fetchFacultyByDepartment, fetchFacultyByProgram } from '../api/fetches/DirectoryFetches';
import { buildJobTitle, matchDepartmentSlugToID, matchProgramSlugToID, buildJobTitleList } from '../api/directoryUtilities';
import { ArchiveCollectionContext } from '../api/ContextAPI';

// Todo: Give a more semantic name
// ? degree is used as a class only?
// ! department is coupled to parent-page slug. Refactor to use Site Taxonomy
const FeedQuery = ({ department, program, degree }) => {
	console.log('FeedQuery', { department }, { program }, { degree });
	const [archiveCollection, setArchiveCollection] = useContext(ArchiveCollectionContext);
	const [loadingCollection, setLoadingCollection] = useState(true);

	useEffect(() => {
		console.log('FeedQuery UseEffect', { department });
		setLoadingCollection(true);
		// ? refactor into a reusable hook
		const getFacultyByDepartmentOrProgram = async (departmentName, programName) => {
			if (programName) {
				matchProgramSlugToID(programName)
					.then((programID) => fetchFacultyByProgram(programID))
					.then((facultyData) => {
						setArchiveCollection(facultyData);
						setLoadingCollection(false);
					})
					.catch((err) => { console.error(err); });
			} else if (departmentName) {
				matchDepartmentSlugToID(departmentName)
					.then((departmentID) => fetchFacultyByDepartment(departmentID))
					.then((facultyData) => {
						setArchiveCollection(facultyData);
						setLoadingCollection(false);
					})
					.catch((err) => { console.error(err); });
			}
		};

		getFacultyByDepartmentOrProgram(department, program);
	}, [department, program]);

	const buildFacultyCards = () => archiveCollection.map((person) => {
		const info = person.meta.person_meta;
		const { images: { profile_image: profileImage }, name: { display_name: displayName } } = info;
		const path = `people/${person.slug}`;
		// const jobTitle = buildJobTitle(info.job);

		return <Card key={person.id} className="grid--item card--people">
			{profileImage
				&& <Card.DrupalProfileImage featuredImage={profileImage} />
			}

			<Card.Content alignment="center">
				<Card.Title title={displayName} />
				{buildJobTitleList(info.job)}
				{path
					&& <Card.CTA path={path} label="Read more" />
				}
			</Card.Content>
		</Card>;
	});

	return <Section className={degree}>
		<Grid>
			{loadingCollection ? <Loader/> : buildFacultyCards()}
		</Grid>
	</Section>;
};

export default FeedQuery;
