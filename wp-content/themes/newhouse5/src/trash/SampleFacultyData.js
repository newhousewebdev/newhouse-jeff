const SampleFacultyData = () => {
	const facultyData = {
		"edges": [
			{
				"node": {
					"display_name": "Bruce Strong",
					"position": "Chair, Visual Communications, Associate Professor",
					"uri": "/people/faculty/bruce-strong",
					"slug": "bruce-strong",
					"featured_image": {
						"altText": "Bruce Strong",
						"sourceUrl": "https://newhouse.syr.edu/sites/default/files/styles/faculty-big-square/public/strong_bruce_-_sized.jpg"
					},
					"company": "NBC Universal",
					"contact" : {
						"email" : "brstrong@syr.edu",
						"phone" : "315-443-4868",
						"office" : "318-F Newhouse 3"
					},
					"bio": {
						"short" : "<p>Short excerpt bio goes here for search results and meta content.</p>",
						"long": "<h3>What he teaches...</h3><p>Through video, still photography, audio and multimedia storytelling, Bruce Strong teaches undergraduate, graduate and military students how to combine visual strategies and storytelling principles with a variety of technical tools to create compelling stories that powerfully engage the viewer’s heart and mind.In the process of teaching them how to tell a good story, Bruce also challenges students to evaluate their own goals and dreams, pushing them to live a story worth telling.</p><h3>Bio</h3><p>An award- winning photojournalist and professor and the chair of the Visual Communications department, Bruce Strong has traveled to nearly 70 countries and has worked as both a staff and an independent photojournalist/ videographer around the world.His most recent adventures have taken him to Iceland, Croatia, Oman, Mauritius, Mongolia, Armenia and South Sudan.</p><p>Strong worked at one of the largest newspapers in California for 11 years and also has freelanced for a variety of international publications and non- profit organizations.His images have been published in magazines such as TIME and National Geographic and have earned numerous awards.Strong also served as a Knight - Wallace Journalism Fellow at the University of Michigan, as a Knight Fellow at Ohio University, and as the first professional in residence at MediaStorm(then in New York).While there, he helped produce “A Darkness Visible: Afghanistan, ” which was nominated for an Emmy, was a finalist for Documentary of the Year at POYi, earned second place in Long Form Multimedia Story at POYi, and won the Media For Liberty Award.</p><p>When he’s not in the field producing his own work, Strong spends most of his time helping others learn to tell stories that matter and considers teaching a rewarding and enlightening journey.Many of his students have won top honors from NPPA, POY, World Press and BOP even while still in school.He also mentored back- to - back College Photographer of the Year winners Matt Eich and Travis Dove.In 2010, Strong was honored with a Meredith Teaching Recognition Award from Syracuse University and the National Press Photographers Association’s Robin F.Garland Educator Award.</p><p>Strong also lectures and leads workshops for a variety of groups, including the National Press Photographers Association’s Multimedia Immersion Workshop, the Maine Media Workshops and the U.S.Navy.He also has been sent by the U.S.State Department to teach visual storytelling, photography and video in Oman and Mauritius.Last summer, Bruce spent five weeks as a student and photographer climbing the Austrian Alps through an outdoor leadership / mountaineering program.This summer, he took students to Valencia, Spain, for a four - week workshop in visual communication and cultural immersion.Bruce is most proud, however, of his two sons, Jack and Cole, and loves adventuring through life with his designer / writer / professor wife, Claudia.</p>"
					}
				}
			},
			{
				"node": {
					"display_name": "Claudia Strong",
					"position": "Adjunct Professor",
					"uri": "/people/faculty/claudia-strong",
					"slug": "claudia-strong",
					"featured_image": {
						altText: "Claudia Strong",
						sourceUrl: "https://newhouse.syr.edu/sites/default/files/styles/faculty-big-square/public/strong_claudia_2019-3.jpg"
					},
					"contact": {
						"email": "brstrong@syr.edu",
						"phone": "315-443-4868",
						"office": "318-F Newhouse 3"
					},
					"bio": {
						"short": "<p>Short excerpt bio goes here for search results and meta content.</p>",
						"long": "<h3>What he teaches...</h3><p>Through video, still photography, audio and multimedia storytelling, Bruce Strong teaches undergraduate, graduate and military students how to combine visual strategies and storytelling principles with a variety of technical tools to create compelling stories that powerfully engage the viewer’s heart and mind.In the process of teaching them how to tell a good story, Bruce also challenges students to evaluate their own goals and dreams, pushing them to live a story worth telling.</p><h3>Bio</h3><p>An award- winning photojournalist and professor and the chair of the Visual Communications department, Bruce Strong has traveled to nearly 70 countries and has worked as both a staff and an independent photojournalist/ videographer around the world.His most recent adventures have taken him to Iceland, Croatia, Oman, Mauritius, Mongolia, Armenia and South Sudan.</p><p>Strong worked at one of the largest newspapers in California for 11 years and also has freelanced for a variety of international publications and non- profit organizations.His images have been published in magazines such as TIME and National Geographic and have earned numerous awards.Strong also served as a Knight - Wallace Journalism Fellow at the University of Michigan, as a Knight Fellow at Ohio University, and as the first professional in residence at MediaStorm(then in New York).While there, he helped produce “A Darkness Visible: Afghanistan, ” which was nominated for an Emmy, was a finalist for Documentary of the Year at POYi, earned second place in Long Form Multimedia Story at POYi, and won the Media For Liberty Award.</p><p>When he’s not in the field producing his own work, Strong spends most of his time helping others learn to tell stories that matter and considers teaching a rewarding and enlightening journey.Many of his students have won top honors from NPPA, POY, World Press and BOP even while still in school.He also mentored back- to - back College Photographer of the Year winners Matt Eich and Travis Dove.In 2010, Strong was honored with a Meredith Teaching Recognition Award from Syracuse University and the National Press Photographers Association’s Robin F.Garland Educator Award.</p><p>Strong also lectures and leads workshops for a variety of groups, including the National Press Photographers Association’s Multimedia Immersion Workshop, the Maine Media Workshops and the U.S.Navy.He also has been sent by the U.S.State Department to teach visual storytelling, photography and video in Oman and Mauritius.Last summer, Bruce spent five weeks as a student and photographer climbing the Austrian Alps through an outdoor leadership / mountaineering program.This summer, he took students to Valencia, Spain, for a four - week workshop in visual communication and cultural immersion.Bruce is most proud, however, of his two sons, Jack and Cole, and loves adventuring through life with his designer / writer / professor wife, Claudia.</p>"
					}
				}
			},
			{
				"node": {
					"display_name": "Renée Stevens",
					"position": "Assistant Professor, Associate Chair",
					"uri": "/people/faculty/renee-stevens",
					"slug": "renee-stevens",
					"featured_image": {
						altText: "Renée Stevens",
						sourceUrl: "https://newhouse.syr.edu/sites/default/files/styles/faculty-big-square/public/stevens_renee_-_sized.jpg"
					},
					"contact": {
						"email": "brstrong@syr.edu",
						"phone": "315-443-4868",
						"office": "318-F Newhouse 3"
					},
					"bio": {
						"short": "<p>Short excerpt bio goes here for search results and meta content.</p>",
						"long": "<h3>What he teaches...</h3><p>Through video, still photography, audio and multimedia storytelling, Bruce Strong teaches undergraduate, graduate and military students how to combine visual strategies and storytelling principles with a variety of technical tools to create compelling stories that powerfully engage the viewer’s heart and mind.In the process of teaching them how to tell a good story, Bruce also challenges students to evaluate their own goals and dreams, pushing them to live a story worth telling.</p><h3>Bio</h3><p>An award- winning photojournalist and professor and the chair of the Visual Communications department, Bruce Strong has traveled to nearly 70 countries and has worked as both a staff and an independent photojournalist/ videographer around the world.His most recent adventures have taken him to Iceland, Croatia, Oman, Mauritius, Mongolia, Armenia and South Sudan.</p><p>Strong worked at one of the largest newspapers in California for 11 years and also has freelanced for a variety of international publications and non- profit organizations.His images have been published in magazines such as TIME and National Geographic and have earned numerous awards.Strong also served as a Knight - Wallace Journalism Fellow at the University of Michigan, as a Knight Fellow at Ohio University, and as the first professional in residence at MediaStorm(then in New York).While there, he helped produce “A Darkness Visible: Afghanistan, ” which was nominated for an Emmy, was a finalist for Documentary of the Year at POYi, earned second place in Long Form Multimedia Story at POYi, and won the Media For Liberty Award.</p><p>When he’s not in the field producing his own work, Strong spends most of his time helping others learn to tell stories that matter and considers teaching a rewarding and enlightening journey.Many of his students have won top honors from NPPA, POY, World Press and BOP even while still in school.He also mentored back- to - back College Photographer of the Year winners Matt Eich and Travis Dove.In 2010, Strong was honored with a Meredith Teaching Recognition Award from Syracuse University and the National Press Photographers Association’s Robin F.Garland Educator Award.</p><p>Strong also lectures and leads workshops for a variety of groups, including the National Press Photographers Association’s Multimedia Immersion Workshop, the Maine Media Workshops and the U.S.Navy.He also has been sent by the U.S.State Department to teach visual storytelling, photography and video in Oman and Mauritius.Last summer, Bruce spent five weeks as a student and photographer climbing the Austrian Alps through an outdoor leadership / mountaineering program.This summer, he took students to Valencia, Spain, for a four - week workshop in visual communication and cultural immersion.Bruce is most proud, however, of his two sons, Jack and Cole, and loves adventuring through life with his designer / writer / professor wife, Claudia.</p>"
					}
				}
			},
			{
				"node": {
					"display_name": "SooYeon Hong",
					"position": "Chair, Visual Communications, Associate Professor",
					"uri": "/people/faculty/sooyeon-hong",
					"slug": "sooyeon-hong",
					"featured_image": {
						altText: "Bruce Strong",
						sourceUrl: "https://newhouse.syr.edu/sites/default/files/styles/faculty-big-square/public/strong_bruce_-_sized.jpg"
					},
					"contact": {
						"email": "brstrong@syr.edu",
						"phone": "315-443-4868",
						"office": "318-F Newhouse 3"
					},
					"bio": {
						"short": "<p>Short excerpt bio goes here for search results and meta content.</p>",
						"long": "<h3>What he teaches...</h3><p>Through video, still photography, audio and multimedia storytelling, Bruce Strong teaches undergraduate, graduate and military students how to combine visual strategies and storytelling principles with a variety of technical tools to create compelling stories that powerfully engage the viewer’s heart and mind.In the process of teaching them how to tell a good story, Bruce also challenges students to evaluate their own goals and dreams, pushing them to live a story worth telling.</p><h3>Bio</h3><p>An award- winning photojournalist and professor and the chair of the Visual Communications department, Bruce Strong has traveled to nearly 70 countries and has worked as both a staff and an independent photojournalist/ videographer around the world.His most recent adventures have taken him to Iceland, Croatia, Oman, Mauritius, Mongolia, Armenia and South Sudan.</p><p>Strong worked at one of the largest newspapers in California for 11 years and also has freelanced for a variety of international publications and non- profit organizations.His images have been published in magazines such as TIME and National Geographic and have earned numerous awards.Strong also served as a Knight - Wallace Journalism Fellow at the University of Michigan, as a Knight Fellow at Ohio University, and as the first professional in residence at MediaStorm(then in New York).While there, he helped produce “A Darkness Visible: Afghanistan, ” which was nominated for an Emmy, was a finalist for Documentary of the Year at POYi, earned second place in Long Form Multimedia Story at POYi, and won the Media For Liberty Award.</p><p>When he’s not in the field producing his own work, Strong spends most of his time helping others learn to tell stories that matter and considers teaching a rewarding and enlightening journey.Many of his students have won top honors from NPPA, POY, World Press and BOP even while still in school.He also mentored back- to - back College Photographer of the Year winners Matt Eich and Travis Dove.In 2010, Strong was honored with a Meredith Teaching Recognition Award from Syracuse University and the National Press Photographers Association’s Robin F.Garland Educator Award.</p><p>Strong also lectures and leads workshops for a variety of groups, including the National Press Photographers Association’s Multimedia Immersion Workshop, the Maine Media Workshops and the U.S.Navy.He also has been sent by the U.S.State Department to teach visual storytelling, photography and video in Oman and Mauritius.Last summer, Bruce spent five weeks as a student and photographer climbing the Austrian Alps through an outdoor leadership / mountaineering program.This summer, he took students to Valencia, Spain, for a four - week workshop in visual communication and cultural immersion.Bruce is most proud, however, of his two sons, Jack and Cole, and loves adventuring through life with his designer / writer / professor wife, Claudia.</p>"
					}
				}
			},
			{
				"node": {
					"display_name": "Sherri Taylor",
					"position": "Chair, Visual Communications, Associate Professor",
					"uri": "/people/faculty/sherri-taylor",
					"slug": "sherri-taylor",
					"featured_image": {
						altText: "Bruce Strong",
						sourceUrl: "https://newhouse.syr.edu/sites/default/files/styles/faculty-big-square/public/strong_bruce_-_sized.jpg"
					},
					"contact": {
						"email": "brstrong@syr.edu",
						"phone": "315-443-4868",
						"office": "318-F Newhouse 3"
					},
					"bio": {
						"short": "<p>Short excerpt bio goes here for search results and meta content.</p>",
						"long": "<h3>What he teaches...</h3><p>Through video, still photography, audio and multimedia storytelling, Bruce Strong teaches undergraduate, graduate and military students how to combine visual strategies and storytelling principles with a variety of technical tools to create compelling stories that powerfully engage the viewer’s heart and mind.In the process of teaching them how to tell a good story, Bruce also challenges students to evaluate their own goals and dreams, pushing them to live a story worth telling.</p><h3>Bio</h3><p>An award- winning photojournalist and professor and the chair of the Visual Communications department, Bruce Strong has traveled to nearly 70 countries and has worked as both a staff and an independent photojournalist/ videographer around the world.His most recent adventures have taken him to Iceland, Croatia, Oman, Mauritius, Mongolia, Armenia and South Sudan.</p><p>Strong worked at one of the largest newspapers in California for 11 years and also has freelanced for a variety of international publications and non- profit organizations.His images have been published in magazines such as TIME and National Geographic and have earned numerous awards.Strong also served as a Knight - Wallace Journalism Fellow at the University of Michigan, as a Knight Fellow at Ohio University, and as the first professional in residence at MediaStorm(then in New York).While there, he helped produce “A Darkness Visible: Afghanistan, ” which was nominated for an Emmy, was a finalist for Documentary of the Year at POYi, earned second place in Long Form Multimedia Story at POYi, and won the Media For Liberty Award.</p><p>When he’s not in the field producing his own work, Strong spends most of his time helping others learn to tell stories that matter and considers teaching a rewarding and enlightening journey.Many of his students have won top honors from NPPA, POY, World Press and BOP even while still in school.He also mentored back- to - back College Photographer of the Year winners Matt Eich and Travis Dove.In 2010, Strong was honored with a Meredith Teaching Recognition Award from Syracuse University and the National Press Photographers Association’s Robin F.Garland Educator Award.</p><p>Strong also lectures and leads workshops for a variety of groups, including the National Press Photographers Association’s Multimedia Immersion Workshop, the Maine Media Workshops and the U.S.Navy.He also has been sent by the U.S.State Department to teach visual storytelling, photography and video in Oman and Mauritius.Last summer, Bruce spent five weeks as a student and photographer climbing the Austrian Alps through an outdoor leadership / mountaineering program.This summer, he took students to Valencia, Spain, for a four - week workshop in visual communication and cultural immersion.Bruce is most proud, however, of his two sons, Jack and Cole, and loves adventuring through life with his designer / writer / professor wife, Claudia.</p>"
					}
				}
			},
			{
				"node": {
					"display_name": "Seth Gitner",
					"position": "Chair, Visual Communications, Associate Professor",
					"uri": "/people/faculty/seth-gitner",
					"slug": "seth-gitner",
					"featured_image": {
						altText: "Bruce Strong",
						sourceUrl: "https://newhouse.syr.edu/sites/default/files/styles/faculty-big-square/public/strong_bruce_-_sized.jpg"
					},
					"contact": {
						"email": "brstrong@syr.edu",
						"phone": "315-443-4868",
						"office": "318-F Newhouse 3"
					},
					"bio": {
						"short": "<p>Short excerpt bio goes here for search results and meta content.</p>",
						"long": "<h3>What he teaches...</h3><p>Through video, still photography, audio and multimedia storytelling, Bruce Strong teaches undergraduate, graduate and military students how to combine visual strategies and storytelling principles with a variety of technical tools to create compelling stories that powerfully engage the viewer’s heart and mind.In the process of teaching them how to tell a good story, Bruce also challenges students to evaluate their own goals and dreams, pushing them to live a story worth telling.</p><h3>Bio</h3><p>An award- winning photojournalist and professor and the chair of the Visual Communications department, Bruce Strong has traveled to nearly 70 countries and has worked as both a staff and an independent photojournalist/ videographer around the world.His most recent adventures have taken him to Iceland, Croatia, Oman, Mauritius, Mongolia, Armenia and South Sudan.</p><p>Strong worked at one of the largest newspapers in California for 11 years and also has freelanced for a variety of international publications and non- profit organizations.His images have been published in magazines such as TIME and National Geographic and have earned numerous awards.Strong also served as a Knight - Wallace Journalism Fellow at the University of Michigan, as a Knight Fellow at Ohio University, and as the first professional in residence at MediaStorm(then in New York).While there, he helped produce “A Darkness Visible: Afghanistan, ” which was nominated for an Emmy, was a finalist for Documentary of the Year at POYi, earned second place in Long Form Multimedia Story at POYi, and won the Media For Liberty Award.</p><p>When he’s not in the field producing his own work, Strong spends most of his time helping others learn to tell stories that matter and considers teaching a rewarding and enlightening journey.Many of his students have won top honors from NPPA, POY, World Press and BOP even while still in school.He also mentored back- to - back College Photographer of the Year winners Matt Eich and Travis Dove.In 2010, Strong was honored with a Meredith Teaching Recognition Award from Syracuse University and the National Press Photographers Association’s Robin F.Garland Educator Award.</p><p>Strong also lectures and leads workshops for a variety of groups, including the National Press Photographers Association’s Multimedia Immersion Workshop, the Maine Media Workshops and the U.S.Navy.He also has been sent by the U.S.State Department to teach visual storytelling, photography and video in Oman and Mauritius.Last summer, Bruce spent five weeks as a student and photographer climbing the Austrian Alps through an outdoor leadership / mountaineering program.This summer, he took students to Valencia, Spain, for a four - week workshop in visual communication and cultural immersion.Bruce is most proud, however, of his two sons, Jack and Cole, and loves adventuring through life with his designer / writer / professor wife, Claudia.</p>"
					}
				}
			},
			{
				"node": {
					"display_name": "Jon Glass",
					"position": "Chair, Visual Communications, Associate Professor",
					"uri": "/people/faculty/jon-glass",
					"slug": "jon-glass",
					"featured_image": {
						altText: "Bruce Strong",
						sourceUrl: "https://newhouse.syr.edu/sites/default/files/styles/faculty-big-square/public/strong_bruce_-_sized.jpg"
					},
					"contact": {
						"email": "brstrong@syr.edu",
						"phone": "315-443-4868",
						"office": "318-F Newhouse 3"
					},
					"bio": {
						"short": "<p>Short excerpt bio goes here for search results and meta content.</p>",
						"long": "<h3>What he teaches...</h3><p>Through video, still photography, audio and multimedia storytelling, Bruce Strong teaches undergraduate, graduate and military students how to combine visual strategies and storytelling principles with a variety of technical tools to create compelling stories that powerfully engage the viewer’s heart and mind.In the process of teaching them how to tell a good story, Bruce also challenges students to evaluate their own goals and dreams, pushing them to live a story worth telling.</p><h3>Bio</h3><p>An award- winning photojournalist and professor and the chair of the Visual Communications department, Bruce Strong has traveled to nearly 70 countries and has worked as both a staff and an independent photojournalist/ videographer around the world.His most recent adventures have taken him to Iceland, Croatia, Oman, Mauritius, Mongolia, Armenia and South Sudan.</p><p>Strong worked at one of the largest newspapers in California for 11 years and also has freelanced for a variety of international publications and non- profit organizations.His images have been published in magazines such as TIME and National Geographic and have earned numerous awards.Strong also served as a Knight - Wallace Journalism Fellow at the University of Michigan, as a Knight Fellow at Ohio University, and as the first professional in residence at MediaStorm(then in New York).While there, he helped produce “A Darkness Visible: Afghanistan, ” which was nominated for an Emmy, was a finalist for Documentary of the Year at POYi, earned second place in Long Form Multimedia Story at POYi, and won the Media For Liberty Award.</p><p>When he’s not in the field producing his own work, Strong spends most of his time helping others learn to tell stories that matter and considers teaching a rewarding and enlightening journey.Many of his students have won top honors from NPPA, POY, World Press and BOP even while still in school.He also mentored back- to - back College Photographer of the Year winners Matt Eich and Travis Dove.In 2010, Strong was honored with a Meredith Teaching Recognition Award from Syracuse University and the National Press Photographers Association’s Robin F.Garland Educator Award.</p><p>Strong also lectures and leads workshops for a variety of groups, including the National Press Photographers Association’s Multimedia Immersion Workshop, the Maine Media Workshops and the U.S.Navy.He also has been sent by the U.S.State Department to teach visual storytelling, photography and video in Oman and Mauritius.Last summer, Bruce spent five weeks as a student and photographer climbing the Austrian Alps through an outdoor leadership / mountaineering program.This summer, he took students to Valencia, Spain, for a four - week workshop in visual communication and cultural immersion.Bruce is most proud, however, of his two sons, Jack and Cole, and loves adventuring through life with his designer / writer / professor wife, Claudia.</p>"
					}
				}
			},
			{
				"node": {
					"display_name": "Ken Harper",
					"position": "Chair, Visual Communications, Associate Professor",
					"uri": "/people/faculty/ken-harper",
					"slug": "ken-harper",
					"featured_image": {
						altText: "Bruce Strong",
						sourceUrl: "https://newhouse.syr.edu/sites/default/files/styles/faculty-big-square/public/strong_bruce_-_sized.jpg"
					},
					"contact": {
						"email": "brstrong@syr.edu",
						"phone": "315-443-4868",
						"office": "318-F Newhouse 3"
					},
					"bio": {
						"short": "<p>Short excerpt bio goes here for search results and meta content.</p>",
						"long": "<h3>What he teaches...</h3><p>Through video, still photography, audio and multimedia storytelling, Bruce Strong teaches undergraduate, graduate and military students how to combine visual strategies and storytelling principles with a variety of technical tools to create compelling stories that powerfully engage the viewer’s heart and mind.In the process of teaching them how to tell a good story, Bruce also challenges students to evaluate their own goals and dreams, pushing them to live a story worth telling.</p><h3>Bio</h3><p>An award- winning photojournalist and professor and the chair of the Visual Communications department, Bruce Strong has traveled to nearly 70 countries and has worked as both a staff and an independent photojournalist/ videographer around the world.His most recent adventures have taken him to Iceland, Croatia, Oman, Mauritius, Mongolia, Armenia and South Sudan.</p><p>Strong worked at one of the largest newspapers in California for 11 years and also has freelanced for a variety of international publications and non- profit organizations.His images have been published in magazines such as TIME and National Geographic and have earned numerous awards.Strong also served as a Knight - Wallace Journalism Fellow at the University of Michigan, as a Knight Fellow at Ohio University, and as the first professional in residence at MediaStorm(then in New York).While there, he helped produce “A Darkness Visible: Afghanistan, ” which was nominated for an Emmy, was a finalist for Documentary of the Year at POYi, earned second place in Long Form Multimedia Story at POYi, and won the Media For Liberty Award.</p><p>When he’s not in the field producing his own work, Strong spends most of his time helping others learn to tell stories that matter and considers teaching a rewarding and enlightening journey.Many of his students have won top honors from NPPA, POY, World Press and BOP even while still in school.He also mentored back- to - back College Photographer of the Year winners Matt Eich and Travis Dove.In 2010, Strong was honored with a Meredith Teaching Recognition Award from Syracuse University and the National Press Photographers Association’s Robin F.Garland Educator Award.</p><p>Strong also lectures and leads workshops for a variety of groups, including the National Press Photographers Association’s Multimedia Immersion Workshop, the Maine Media Workshops and the U.S.Navy.He also has been sent by the U.S.State Department to teach visual storytelling, photography and video in Oman and Mauritius.Last summer, Bruce spent five weeks as a student and photographer climbing the Austrian Alps through an outdoor leadership / mountaineering program.This summer, he took students to Valencia, Spain, for a four - week workshop in visual communication and cultural immersion.Bruce is most proud, however, of his two sons, Jack and Cole, and loves adventuring through life with his designer / writer / professor wife, Claudia.</p>"
					}
				}
			},
			{
				"node": {
					"display_name": "Jeff Passetti",
					"position": "Chair, Visual Communications, Associate Professor",
					"uri": "/people/faculty/jeff-passetti",
					"slug": "jeff-passetti",
					"featured_image": {
						altText: "Bruce Strong",
						sourceUrl: "https://newhouse.syr.edu/sites/default/files/styles/faculty-big-square/public/strong_bruce_-_sized.jpg"
					},
					"contact": {
						"email": "brstrong@syr.edu",
						"phone": "315-443-4868",
						"office": "318-F Newhouse 3"
					},
					"bio": {
						"short": "<p>Short excerpt bio goes here for search results and meta content.</p>",
						"long": "<h3>What he teaches...</h3><p>Through video, still photography, audio and multimedia storytelling, Bruce Strong teaches undergraduate, graduate and military students how to combine visual strategies and storytelling principles with a variety of technical tools to create compelling stories that powerfully engage the viewer’s heart and mind.In the process of teaching them how to tell a good story, Bruce also challenges students to evaluate their own goals and dreams, pushing them to live a story worth telling.</p><h3>Bio</h3><p>An award- winning photojournalist and professor and the chair of the Visual Communications department, Bruce Strong has traveled to nearly 70 countries and has worked as both a staff and an independent photojournalist/ videographer around the world.His most recent adventures have taken him to Iceland, Croatia, Oman, Mauritius, Mongolia, Armenia and South Sudan.</p><p>Strong worked at one of the largest newspapers in California for 11 years and also has freelanced for a variety of international publications and non- profit organizations.His images have been published in magazines such as TIME and National Geographic and have earned numerous awards.Strong also served as a Knight - Wallace Journalism Fellow at the University of Michigan, as a Knight Fellow at Ohio University, and as the first professional in residence at MediaStorm(then in New York).While there, he helped produce “A Darkness Visible: Afghanistan, ” which was nominated for an Emmy, was a finalist for Documentary of the Year at POYi, earned second place in Long Form Multimedia Story at POYi, and won the Media For Liberty Award.</p><p>When he’s not in the field producing his own work, Strong spends most of his time helping others learn to tell stories that matter and considers teaching a rewarding and enlightening journey.Many of his students have won top honors from NPPA, POY, World Press and BOP even while still in school.He also mentored back- to - back College Photographer of the Year winners Matt Eich and Travis Dove.In 2010, Strong was honored with a Meredith Teaching Recognition Award from Syracuse University and the National Press Photographers Association’s Robin F.Garland Educator Award.</p><p>Strong also lectures and leads workshops for a variety of groups, including the National Press Photographers Association’s Multimedia Immersion Workshop, the Maine Media Workshops and the U.S.Navy.He also has been sent by the U.S.State Department to teach visual storytelling, photography and video in Oman and Mauritius.Last summer, Bruce spent five weeks as a student and photographer climbing the Austrian Alps through an outdoor leadership / mountaineering program.This summer, he took students to Valencia, Spain, for a four - week workshop in visual communication and cultural immersion.Bruce is most proud, however, of his two sons, Jack and Cole, and loves adventuring through life with his designer / writer / professor wife, Claudia.</p>"
					}
				}
			},
		] // edges
	}// end of facultyData
	return facultyData;
} // end of function
export default SampleFacultyData;
