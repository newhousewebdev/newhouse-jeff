import React from 'react';
import { extractSubUrl } from '../api/utilityFunctions';
import Button from '../components/Button';

const Icons = ({ menuItems, settings }) => <ul 
	className={`menu menu--${settings.parentLinks.format} menu--${settings.colorMode} menu--${settings.suffix}`}>
	{menuItems.map((menuItem, key) => {
		const { label, url } = menuItem.node;
		return (
			<Button.CTA
				key={key}
				path={extractSubUrl(url)}
				icon={label.toLowerCase()}
				label={label}
				colorMode="inverted"
			/>
		);
	})}
</ul>;

export default Icons;
