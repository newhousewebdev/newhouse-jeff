import React, { useContext } from 'react';
import { useQuery } from 'react-apollo';
import gql from 'graphql-tag';
import { useParams } from 'react-router-dom';

// components
import Loader from '../components/Loader';
import Page from '../views/Page/Page';
import Card from '../components/Card/Card';
import Grid from '../components/Layout/Grid';

// contexts
import {
	ActiveDegreeContext,
	ArchiveCollectionContext,
} from '../api/ContextAPI';

// sample data
// import facultyData from '../../trash/faculty-staff.json';

const Archive = ({ children }) => {
	const { programSlug, degreeSlug } = useParams();

	const [activeDegree, setActiveDegree] = useContext(ActiveDegreeContext);
	const [archiveCollection, setArchiveCollection] = useContext(ArchiveCollectionContext);

	setArchiveCollection(facultyData);

	const PAGE_QUERY = gql`
	query GET_ACADEMIC_PROGRAM_BY_SLUG($slug: String) {
		academicProgramBy(uri: $slug) {
			title
			link
			slug
			uri
			content
			featuredImage {
				altText
				sourceUrl(size: LARGE)
			}
			degrees {
				edges {
					node {
						name
						slug
					}
				}
			}
			childAcademicPrograms {
				edges {
					node {
						label: title
						content
						excerpt
						slug
						uri
						url: link
						featuredImage {
							altText
							sourceUrl(size: LARGE)
						}
						childItems: childAcademicPrograms {
							edges {
								node {
									label: title
									content
									slug
									uri
									url: link
									excerpt
									featuredImage {
										altText
										sourceUrl(size: LARGE)
									}
									childItems: childAcademicPrograms {
										edges {
											node {
												label: title
												slug
												uri
												url: link
											}
										}
									}
								}
							}
						}
					}
				}
			}
			parent {
				... on AcademicProgram {
					id
					title
					uri
					slug
					parent {
						... on AcademicProgram {
							id
							title
							uri
							slug
						}
					}
				}
			}
		}
	}
	`;

	const { loading, error, data } = useQuery(PAGE_QUERY, {
		variables: {
			slug: `${programSlug}/faculty`,
		},
	});

	if (loading) return <Loader />;
	if (error) return `Error! ${error.message}`;

	// console.log({ data });
	const {
		title, content, degrees, uri, childAcademicPrograms, parent,
	} = data.academicProgramBy;

	const currentPage = {
		title,
		uri,
	};

	// console.log(peopleData.nodes);
	// console.log(parent.title);
	const filteredFaculty = facultyData.nodes.filter((person) => person.node.field_department === parent.title);
	console.log({ filteredFaculty });
	return (
		<Page.Main className="archive">

			<Page.Breadcrumbs
				sectionParent={{
					title: 'Academic Programs',
					uri: `/academics/programs/${activeDegree}`,
				}}
				currentPage={currentPage}
				parentItems={parent}
			/>
			<Page.Intro
				label={parent.title}
				title={title}
				content={content}
			/>
			{children}
			<Page.Section className={activeDegree}>
				<Grid>
					{filteredFaculty.map((edge, key) => {
						const {
							title, field_position, path, field_image_single,
						} = edge;
						return <Card key={key}>
							<Card.Title title={title} />
						</Card>;
					})}
				</Grid>
			</Page.Section>
		</Page.Main>
	);
};
export default Archive;
