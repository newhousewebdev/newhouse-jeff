import { useEffect } from 'react';

const importScript = (resourceUrl, callback) => {
	useEffect(() => {
		const script = document.createElement('script');
		script.src = resourceUrl;
		script.async = true;
		script.onload = callback ? callback : null;
		//console.log({script});
		document.body.appendChild(script);
		return () => {
			document.body.removeChild(script);
		}
	}, [resourceUrl]);
};
export default importScript;
