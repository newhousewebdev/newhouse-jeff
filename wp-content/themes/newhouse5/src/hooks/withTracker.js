
import React, { useEffect } from 'react';
import ReactGA, { FieldsObject } from 'react-ga';
import { RouteComponentProps } from 'react-router-dom';

// Initialize the react-ga plugin using your issued GA tracker code
ReactGA.initialize('UA-12748771-1', {
	debug: true
});

// React.FC component used as a wrapper for route components - e.g. withTracker(RouteComponent)
export const WithTracker = (WrappedComponent, options) => {
	const trackPage = (page) => {
		ReactGA.set({ page, ...options });
		ReactGA.pageview(page);
	};

	return (props) => {
		const { pathname } = props.location;

		useEffect(() => {
			trackPage(pathname);
		}, [pathname]);

		return <WrappedComponent {...props} />;
	};
};
