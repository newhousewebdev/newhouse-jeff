import { useEffect } from "react";
import { useLocation } from "react-router-dom";
import ReactGA from "react-ga";

const usePageTracking = () => {
	const location = useLocation();

	useEffect(() => {
		ReactGA.set({
			page: location.pathname,
			location: `${process.env.SITE_URL}${location.pathname}`
		});
		ReactGA.pageview(location.pathname + location.search);
	}, [location]);
};

export default usePageTracking;
