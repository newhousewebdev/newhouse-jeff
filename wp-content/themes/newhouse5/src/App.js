import React, { useState } from 'react';
import ReactGA from 'react-ga';

// This is a React Router v5.2 app
import { BrowserRouter, Switch, Route } from 'react-router-dom';

// gql
import { ApolloProvider } from 'react-apollo';
import { ApolloClient } from 'apollo-client';
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';
import { onError } from 'apollo-link-error';
import { ApolloLink } from 'apollo-link';
import introspectionQueryResultData from './fragmentTypes.json';

// components
import ScrollToTop from './components/ScrollToTop';

// views
import Home from './views/Home/Home';
import Campaigns from './views/Campaigns/Campaigns';
import GetStarted from './views/GetStarted/GetStarted';
import Pages from './views/Pages/Pages';
import SearchResults from './views/SearchResults';

// contexts
import {
	MobileMenuContext,
	MobileSearchContext,
	ActiveDegreeContext,
	MobileContext,
	TilesContext,
	ArchiveCollectionContext,
	// ?Are events a Context, or specific to one component
	EventsDataContext,
	//EventsLoadingContext,
	GetStartedPreviousPageContext,
} from './api/ContextAPI';

// css
// Refactor for production in separate file
import './sass/style.scss';

ReactGA.initialize(`${process.env.GA_ID}`, {
	// debug: true
});

const fragmentMatcher = new IntrospectionFragmentMatcher({
	introspectionQueryResultData,
});

const cache = new InMemoryCache({ fragmentMatcher });

const client = new ApolloClient({
	link: ApolloLink.from([
		onError(({ graphQLErrors, networkError }) => {
			if (graphQLErrors) {
				graphQLErrors.forEach(({ message, locations, path }) => console.error(
					`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
				));
			}
			if (networkError) console.error(`[Network error]: ${networkError}`);
		}),
		new HttpLink({
			uri: `${process.env.API_URL}graphql`,
			credentials: 'same-origin',
		}),
	]),
	cache,
});

const App = () => {
	// ? combine contexts
	const [mobileMenuVisibility, setMobileMenuVisibility] = useState(false);
	const [mobileSearchVisibility, setMobileSearchVisibility] = useState(false);
	// ? end combine contexts

	const [activeDegree, setActiveDegree] = useState(null);
	const [isMobile, setIsMobile] = useState(false);
	// ? this seems very local does it need to be context
	const [isTilesOpen, setIsTilesOpen] = useState(true);

	// ? can we just switch archives and not wipe them with each page/department change
	const [archiveCollection, setArchiveCollection] = useState([]);

	const [eventsArr, setEventsArr] = useState([]);

	// needed for Get Started
	const [previousPage, setPreviousPage] = useState(null);

	return (
		<ApolloProvider client={client}>
			<BrowserRouter>
				<ScrollToTop />
				<ActiveDegreeContext.Provider value={[activeDegree, setActiveDegree]}>
				<MobileMenuContext.Provider value={[mobileMenuVisibility, setMobileMenuVisibility]}>
				<MobileSearchContext.Provider
					value={[mobileSearchVisibility, setMobileSearchVisibility]}>
				<MobileContext.Provider value={[isMobile, setIsMobile]}>
				<TilesContext.Provider value={[isTilesOpen, setIsTilesOpen]}>
				<ArchiveCollectionContext.Provider
					value={[archiveCollection, setArchiveCollection]}>
				<EventsDataContext.Provider value={[eventsArr, setEventsArr]}>
				<GetStartedPreviousPageContext.Provider
					value={[previousPage, setPreviousPage]}>
					{/* ? Refactor into a routes Component */}
					<Switch>
						{/* note: React Router 5.2 matching system needs specific routes listed before generic routes.  React Router 6 did not. */}
						<Route path="/go" component={Campaigns.Go} />
						<Route path="/test" component={Pages.Test} />
						<Route path="/test2" component={Pages.Test2}/>
						<Route path="/test3" component={Pages.Test3} />
						<Route path="/get-started" component={GetStarted} />
						<Route path="/search/" component={SearchResults} />
						<Route path="/research" component={Pages.Research} />
						
						{ /* Events */ }
						<Route path="/events/" component={Pages.Events} />
						<Route path="/event/:dateSlug/:eventSlug" component={Pages.Event} />
						
						{ /* Directory */ }
						<Route path="/people/:personSlug" component={Pages.Person} />

						{ /* about */ }
						<Route path="/about/news" component={Pages.News} />
						<Route path="/about/centers" component={Pages.Centers} />
						<Route path="/about/directory" component={Pages.Directory} />
						<Route path="/about/faculty" component={Pages.FacultyDirectory} />
						
						{ /* centers */}
						<Route path="/centers/:centerSlug" component={Pages.Center} />

						{ /* academics: specific to generic */}
						<Route path="/academics/:programSlug/featured-alumni/:degreeSlug" component={Pages.FeaturedAlumni} />
						<Route path="/academics/:programSlug/:degreeSlug/:subpageSlug" component={Pages.Program} />
						<Route path="/academics/:programSlug/featured-alumni" component={Pages.FeaturedAlumni} />
						<Route path="/academics/:programSlug/faculty" component={Pages.Faculty} />
						
						<Route path="/academics/programs/:degreeSlug" component={Pages.Programs} />
						<Route path="/academics/programs" component={Pages.Programs} />
						
						<Route path="/academics/:programSlug/:degreeSlug" component={Pages.Program} />
						<Route path="/academics/:programSlug" component={Pages.Program} />
						
						{ /* news */}
						<Route path="/news/:postSlug" component={Pages.SingleNews} />
						<Route path="/news-category/:newsCategory" component={Pages.CategoryArchive} />
					
						{ /* page wildcards: specific to generic */}
						<Route path="/:pageParentSlug/:pageChildSlug/:pageGrandchildSlug/:pageGreatGrandchildSlug" component={Pages.Default} />
						<Route path="/:pageParentSlug/:pageChildSlug/:pageGrandchildSlug" component={Pages.Default} />
						<Route path="/:pageParentSlug/:pageChildSlug" component={Pages.Default} />
						<Route path="/:pageParentSlug" component={Pages.Default} />
						
						{ /* home */}
						<Route path="/" component={Home} />
					</Switch>
				</GetStartedPreviousPageContext.Provider>
				</EventsDataContext.Provider>
				</ArchiveCollectionContext.Provider>
				</TilesContext.Provider>
				</MobileContext.Provider>
				</MobileSearchContext.Provider>
				</MobileMenuContext.Provider>
				</ActiveDegreeContext.Provider>
			</BrowserRouter>
		</ApolloProvider>
	);
};
export default App;
