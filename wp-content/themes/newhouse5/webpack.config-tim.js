const webpackMerge = require('webpack-merge');

/* Configurations */
const files = require('./webpack-build-utilities/files');
const common = require('./webpack-build-utilities/webpack.common');

const loadPresets = require('./webpack-build-utilities/loadPresets');

// eslint-disable-next-line global-require, import/no-dynamic-require
const modeConfig = (env) => require(`./webpack-build-utilities/webpack.${env}`)(env);

module.exports = ({ mode, presets } = { mode: 'development', presets: [] }) => {
	console.log(mode);
	return webpackMerge(
		{
			mode,
		},
		modeConfig(mode),
		loadPresets({ mode, presets }),
		common(),
		files(),
	);
};
