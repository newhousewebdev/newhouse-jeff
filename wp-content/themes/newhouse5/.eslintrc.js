module.exports = {
	env: {
		browser: true,
		es6: true,
	},
	extends: ['airbnb-base', 'plugin:react/recommended'],
	parser: 'babel-eslint',
	rules: {
		// Need Console for dev
		'no-console': 0,

		// indentation
		indent: [2, 'tab'],
		'no-tabs': 0,

		// functions are hoisted so allow their definition after declared use
		'no-use-before-define': ['error', { functions: false, classes: true }],
		'linebreak-style': [
			'error',
			'unix',
		],
		quotes: [
			'error',
			'single',
		],
		semi: [
			'error',
			'always',
		],
		'arrow-body-style': ['error', 'as-needed'],
		'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
		'react/prop-types': 0,
	},
	globals: {
		window: true,
		document: true,
		// wp press instance in js
		wp: true,
	},
};
