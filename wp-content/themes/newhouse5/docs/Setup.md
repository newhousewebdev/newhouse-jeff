# Newhouse Main Site Setup

## Setup A Local Work Space

### Get copy of the existing Site (_in progress_)

#### Option(1) From a backup

Download a backup copy from desired site [WP Engine Site list](https://my.wpengine.com/sites)

#### Option(2) Setup Local by Flywheel

Download and install according to [Wp-engine instructions](https://wpengine.com/support/local/)

### Setup Git

In order to make sure version control and tracking to the existing project history is up to date, you will need to initialize and pull down from the BitBucket **_blessed repo_**,   and then push to your developer repo. Integrators will also push to Wp Engine

#### Get a copy of "_Blessed Repo_"

``` cmd
git init

git remote add blessed https://bitbucket.org/newhousewebdev/newhouse-blessed/

git pull blessed

# accept incoming changes
```

##### Create a "_Blessed Branch_"

``` cmd
# Create the new branch
git checkout -b blessed


# Set branch to track "_Blessed Repo_"
git push --set-upstream blessed blessed

```

#### Create an individual repo

Login into bitbucket [https://bitbucket.org/newhousewebdev/](https://bitbucket.org/newhousewebdev/) and create a new Repo for tracking your individual work.

After creating your individual repo, push up a copy of the "_Blessed Repo_" to your individual account and set it as your upstream repo

```cmd
git remote add <name-for-repo> <bitbucket url>

git push <name-for-repo> master -u

```

#### Add wp-engine repos

``` cmd
# git remote add <repo-name> <url>
git remote add deploy-staging git@git.wpengine.com:staging/newhouse2stage.git
git remote add deploy-production git@git.wpengine.com:staging/newhouse2stage.git
```

---

The project is now set up with connections to necessary repos to begin contributing

##### References for this section

References with an * are authoritative, Others are for informational/background purposes.

* [WP-Engine GIT Instructions*](https://wpengine.com/support/git/)
* [General Git Explanations](https://launchschool.com/books/git/read/github)
* [Setting up and using multiple repos](https://mmikowski.github.io/git-cross-origin/)
* [Everyday Git](https://git-scm.com/docs/giteveryday)

---

##### GIT cmds related to repos

```cmd
# general syntax
#
 To add a git remote
git remote add REMOTE-ID REMOTE-URL

# To Remove a remote
git remote remove REMOTE-ID

# To check remotes existence
git remote -v

# To check a remotes information
git remote show REMOTE-ID

# To rename a remote
git remote rename <old>REMOTE-ID <new>REMOTE-ID

# To track branch history of an remote repository
git --set-upstream-to=REMOTE-ID/BRANCH-ID

# git push
git push REMOTE-ID BRANCH-ID

```

[Git working with Remotes](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes)

### Setting up Webpack

#### Download packages

Install npm packages in Theme and Newhouse Plugin folders

```cmd
npm install

```

#### Run audit (_if prompted_)

```cmd
npm audit fix

```

#### Create env.development file in theme folder

In theme folder create a new file env.development and add the graphql end point for your local machine

```txt
<!-- Jeff's -->
API_URL=http://newhouse4-graphql.local/
<!-- Tim's -->
API_URL=https://newhouse.local/
```

#### Run Webpack

To test your local build run the following.

##### Test Local config

Build scripts have options that can be mixed and matched

environment:

* local : use your local machine as source for data fetches
* live : use production data sources

mode:

* development :  use un-minified code, includes warnings/error messages, webpack watches for file changes
* production  :  use minified code, no error message, file changes are not tracked

```cmd
# Webpack Build options
# dev - environment=local mode=development
# dev:live - environment=production mode=development
# prod:local - environment=local mode=production
# prod - environment=production mode=production

npm run dev
```

Check the local instance of the site and be sure it is working before moving on to Testing on WP-Engine.

### Test Configuration on WP-Engine (_in progress_)

#### Create Restore Point on WP-Engine

Create backup points on Staging and Production. Code will be pushed to Staging as one last integration test and then to Production. Backups set just before pushing are a fail safe, if a push breaks the site, rollback to last blessed commit and push to revert to.

#### Push latest to staging

##### Test Staging config

Visit site and check

---

```cmd
// need to add staging as a variable in package.json
npm run-script staging
```

``` cmd
# git push -verbose <remote-name> <branch-name>
git push -v staging master
```
