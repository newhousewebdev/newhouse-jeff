# Contributing and Code Maintenance

If your environment is not currently setup follow the [Setup Guide](./Setup.md)

## Repo Organization

### Integrated manager

![](https://git-scm.com/book/en/v2/images/integration-manager.png)

[Integration-Manager Workflow](https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows)

The integration manager is currently a simulated "seat" in that any of the devs can act as the integration manager.

To act as the integrator it is important to create and checkout to a _"blessed branch"_

``` cmd
git checkout blessed
```

The blessed branch is where code merging between developers occurs. 

#### Merging

1) Pull down blessed repo to make sure your local branch is up to date with the __latest working version__.

1) Pull down each contributing developers 'blessed' branch and resolve merge conflicts. Each 'blessed' branch should already be up to date and the integrator is only making sure the different repos are synced. ie don't add features or work on new code that is not related to resolving the merge conflicts.

	(Hotfixes occur at this level)

1) Test locally

1) Commit the merged code

#### Deploying

1) Deploy to Staging and test

``` cmd
git push ????? blessed(branchID)
```

1) Deploy to Production and test

``` cmd
git push ????? blessed(branchID)
```

#### Syncing

If the code works on each

---

#### Useful Git commands for integration Manager

```cmd
# To stop tracking a folder
git rm -r --cached <path/foldername>

then add folder to .gitignore

# To add changes to your last commit
git commit --amend
```


#### Branching Methodology

![](https://www.canddi.com/images/blog/2013-10-24/gitflow-simplified.png)

## Repos


## Contributing Standards

### Only _Push_ Working code

All code pushed to remote repositories (sacred branch and individual repos) should be working and clean.

All linting errors must be fixed or rule overridden explicitly. Rule overrides must have initials as minimum i.e. we know who to ask why. Better rule overrides include an explanation

``` js
// bad
// eslint-disable-next-line import/no-unresolved
import { Component, Fragment } from '@wordpress/element';

// better
// (TS)
// eslint-disable-next-line import/no-unresolved
import { Component, Fragment } from '@wordpress/element';

// best
//(TS) @wordpress/element is in global scope
// eslint-disable-next-line import/no-unresolved
import { Component, Fragment } from '@wordpress/element';

```

### Never commit to release broken code

If you need to commit to preserve work, ie code that does nto work, checkout to a feature branch so that the master branch remains clean/working

### Commit messages standard

Standardized commit messages will help streamline merges and 

``` cmd
<!-- Template -->
Short Excerpt of what Commit Does

Longer description of commit. Can be paragraph(s) 
or bulleted List.

One paragraph per problem separated by spaces

* On list item per line
* Second


<!-- Example -->
Refactored Card components to use custom hook

Consolidated api look ups into custom hook.

* Added FetchCardInfo() hook to utility.js
* Cards edited
** People Card
** Blog Card


```

#### Commit message further reading

[Proper git commit messages](https://medium.com/@steveamaza/how-to-write-a-proper-git-commit-message-e028865e5791)

## References

[WP-Engine recommended flow](https://wpengine.com/support/development-workflow-best-practices/)

Options:

* Push to WP-Engine
* Continuous Integration - Push to Bitbucket and use pipeline to Push up to WP-Engine
[WP-Engine CI with Bitbucket](https://wpengine.com/support/deploying-code-with-bitbucket-pipelines-wp-engine/)

## Recovery

First step of recovery should be to restore a back up