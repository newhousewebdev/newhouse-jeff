# Recovery Steps

## Preferred Option

### Restore Code last working version

## Fallback Option

### Restore a backup on the server

## Failsafe Option

### Contact WP Engine
