<?php
/**
 * Theme Functions
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
} //Deny direct file access.


/*
	New in 5.5 environment func
	https://make.wordpress.org/core/2020/07/24/new-wp_get_environment_type-function-in-wordpress-5-5/

*/
/* Filters and Actions */

// Setup Theme.
add_action( 'after_setup_theme', 'newhouse_parent_setup' );
// Load styles and scripts.
add_action( 'wp_enqueue_scripts', 'wp_newhouse_scripts' );


/* Functions */

if ( ! function_exists( 'newhouse_parent_setup' ) ) :
	/**
	 * Setup newhouse parent theme features.
	 */
	function newhouse_parent_setup() {
		// ! SUPER IMPORTANT: stop WordPress from redirecting to nearest matching URL.
		// ! allows for react router to take over.
		remove_filter( 'template_redirect', 'redirect_canonical' );

		// omit the args array( post, page )
		// the args array locks down post thumbnails for specific post types
		// allows custom posts to enable 'supports' => array('thumbnail') upon registration.
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'wp-block-styles' );
		add_theme_support( 'editor-styles' );
		add_theme_support( 'align-wide' );

		add_editor_style( 'src/sass/editor-styles.css' );

		add_post_type_support( 'page', 'excerpt' );

		// prevent Wordpress from getting main content when excerpt is blank
		remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );
		
		require get_template_directory() . '/inc/register-menus.php';
	}
endif;

/**
 * Enqueue front-end styles and scripts
 */
function wp_newhouse_scripts() {
	/* Styles */
	$style_sheet_uri  = get_stylesheet_uri();
	$style_sheet_path = get_stylesheet_directory();
	wp_enqueue_style( 'style', $style_sheet_uri, array(), filemtime( $style_sheet_path ) );

	$script_uri  = get_stylesheet_directory_uri() . '/dist/main.js';
	$script_path = get_stylesheet_directory() . '/dist/main.js';
	wp_enqueue_script( 'newhouse-react', $script_uri, array(), filemtime( $script_path ), true );

	$params = array(
		'ajax'        => site_url( 'wp-admin/admin-ajax.php' ),
		'eventsNonce' => wp_create_nonce( 'events_nonce' ),
	);

	wp_localize_script( 'newhouse-react', 'eventsParams', $params );
}

/**
 * EventsFunctions
 *
 * Functions related to Events Calendar.
 * Removes comments
 */
require get_template_directory() . '/inc/events-functions.php';

/**
* Theme optimizations
*
* Remove unused scripts and assets to improve performance
*/
require get_template_directory() . '/inc/optimize.php';

/**
 * Admin Functions
 *
 * Functions that alter the backend interface
 * Removes comments
 */
require get_template_directory() . '/inc/admin-functions.php';

/**
 * Developer Functions
 *
 * Development related Functions
 * Includes Server debugger.
 * Todo Comment out on Live Site
 * ! Make sure no my_server_debugger(s) are in the code on live
 */
require get_template_directory() . '/inc/developer-functions.php';
