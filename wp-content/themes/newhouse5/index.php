<?php
/**
 * Main Template
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}//Deny direct file access.
?>

<?php
	/*
		Note: we can probably preload some information: ie server side rendering
	*/
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<?php //get_template_part( 'partials/social', 'meta' ); ?>
	<?php //get_template_part( 'partials/title' ); ?>
	<?php wp_head(); ?>
	<!-- SU Google Tag Manager -->
	<!-- https://www.syracuse.edu/about/brand/analytics/ -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5FC97GL');</script>
<!-- End Google Tag Manager -->
</head>
<body>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FC97GL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div id="root">
		<style type="text/css">
		.temporary {
			color: #fff;
		}
		.temporary a {
			color: #fff;
		}
		.screen-reader-text{
			clip: rect(1px, 1px, 1px, 1px);
			position: absolute !important;
			height: 1px;
			width: 1px;
			overflow: hidden;
			word-wrap: normal !important; /* Many screen reader and browser combinations announce broken words as they would appear visually. */
		}
		</style>
		<div class="temporary screen-reader-text">
		<?php
		if ( have_posts() ) :
			while ( have_posts() ) :
				the_post();
				?>
				<h1><?php the_title(); ?></h1>
				<?php the_content(); ?>

		<?php endwhile; else : ?>
			<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
		<?php endif; ?>
		</div>
	</div>

	<?php wp_footer(); ?>
</body>
</html>
