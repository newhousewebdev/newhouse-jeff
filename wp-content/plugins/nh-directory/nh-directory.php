<?php
/**
 * Plugin Name: Newhouse Directory Blocks
 * Plugin URI:
 * Description: Collection Blocks related to the Newhouse Directory
 * Version: 1.0.0
 * Author: Tim Starmer
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit();
}//Deny direct file access.

/* Actions and Hooks */

// Plugin Activation.
register_activation_hook( __FILE__, 'activate_newhouse_custom_post_types' );
// Plugin deactivation.
register_deactivation_hook( __FILE__, 'deactivate_newhouse_custom_post_types' );
// Register new directory blocks.
add_action( 'enqueue_block_editor_assets', 'register_newhouse_directory_blocks' );
// Creates out gutenberg block templates.
add_filter( 'block_categories', 'newhouse_directory_block_categories', 10, 2 );

/**
 * Activate Newhouse custom_post_types Plugin
 */
function activate_newhouse_custom_post_types() {
	flush_rewrite_rules();
}

/**
 * Deactivate Newhouse custom post types Plugin
 */
function deactivate_newhouse_custom_post_types() {
	flush_rewrite_rules();
}


/**
 * Enqueue scripts for directory blocks
 */
function register_newhouse_directory_blocks() {
	// automatically load dependencies and version. ref: https://developer.wordpress.org/block-editor/tutorials/javascript/js-build-setup/ .
	$asset_file = include( plugin_dir_path( __FILE__ ) . 'build/index.asset.php' ); // phpcs:ignore
	wp_register_script(
		'newhouse-directory-blocks',
		plugins_url( 'build/index.js', __FILE__ ),
		$asset_file['dependencies'],
		$asset_file['version'],
		false
	);

	register_block_type(
		'newhouse/directory',
		array(
			'editor_script' => 'newhouse-directory-blocks',
		)
	);
}

/**
 * Creating a newhouse directory block category.
 *
 * @param   array  $categories     List of block categories.
 * @param   object $post being used.
 * @return  array
 */
function newhouse_directory_block_categories( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug'  => 'newhouse-directory',
				'title' => 'Newhouse Directory',
			),
		)
	);
}
