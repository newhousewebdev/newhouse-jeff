/*
	Collection of utility functions
*/

/* React specific method */
export const displayContent = (content) => ({ __html: content });

/* Object methods */
export const deepCopy = (object) => ({ ...JSON.parse(JSON.stringify(object)) });

export const isEmpty = (object) => (
	Object.entries(object).length === 0 && object.constructor === Object
);

export const isObject = (object) => (
	typeof object === 'object' && object !== null
);

/* String methods */
export const capitalizeFirstLetter = (string) => string.charAt(0).toUpperCase() + string.slice(1);

export const capitalizeWords = (string) => string.split(' ').map((word) => capitalizeFirstLetter(word)).join(' ');

export const wordToInitial = (string) => `${string.charAt(0).toUpperCase()}.`;

export const wordsToInitials = (string) => string.split(' ').map((word) => wordToInitial(word)).join(' ');

/* array methods */

export const addUniqueItemToArray = (item, array) => {
	if (array.includes(item)) {
		return array;
	}
	const newArray = [...array];
	newArray.push(item);
	return newArray;
};

export const removeUniqueItemFromArray = (item, array) => array.filter((entry) => entry !== item);

export const removeDuplicateItemsFromArray = (item, array) => {
	const firstInstance = array.indexOf(item);
	const firstChunk = array.slice(0, firstInstance);
	const theRest = removeUniqueItemFromArray(item, array.slice(firstInstance + 1));

	return [...firstChunk, theRest];
};

/* Directory Specific Functions */

export const findFieldByKey = (key, fields) => {
	const matches = Object.values(fields).filter((field) => field.key === key);

	return matches.length !== 0 ? matches[0] : false;
};
