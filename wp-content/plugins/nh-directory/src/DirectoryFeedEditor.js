/* eslint-disable react/react-in-jsx-scope */
// TS @wordpress imports are handled by wp-scripts
// eslint-disable-next-line import/no-unresolved
import { InspectorControls } from '@wordpress/block-editor';
// eslint-disable-next-line import/no-unresolved
import { SelectControl, Button } from '@wordpress/components';
// eslint-disable-next-line import/no-extraneous-dependencies
import { useEffect, useState, Fragment } from '@wordpress/element';
// eslint-disable-next-line import/no-unresolved
import { registerBlockType } from '@wordpress/blocks';
import {
	fetchAcademicPrograms,
	fetchPeopleTypes,
	fetchAcademicDepartments,
	fetchAdministrativeDepartments,
	performFetch,
} from './DirectoryFetches';
import Loader from './Loader';

/* ! These are theme dependent */
import Card from '../../../themes/newhouse5/src/components/Card/Card';
import { buildJobTitle } from '../../../themes/newhouse5/src/api/directoryUtilities';

console.log('Directory Feed Loaded');

const directoryBaseUrl = 'https://resources.newhouse.syr.edu/directory/wp-json/wp/v2/directory?per_page=100&_fields=id,title,slug,meta&orderby=title&order=asc';

const ResetButton = ({ onChange, taxonomyKey }) => <Button
	style={{
		color: 'red', marginTop: '-24px', marginBottom: '18px', display: 'block',
	}}
	className="btn-tertiary"
	onClick={() => onChange({ [taxonomyKey]: [] })}
>
		Reset
</Button>;

/* Inputs */
const AcademicDepartmentSelector = ({ searchParams, onChange }) => {
	const taxonomyKey = 'academic_departments';
	const [departmentOptions, setDepartmentOptions] = useState();
	const setValue = searchParams.get(taxonomyKey);
	const assignedValue = setValue ? setValue.split(',') : [];

	useEffect(() => {
		const getDepartments = async () => {
			fetchAcademicDepartments()
				.then((departments) => {
					const options = departments
						.map((department) => ({ label: department.name, value: department.id }));
					setDepartmentOptions(options);
				})
				.catch((err) => { console.error(err); });
		};

		getDepartments();
	}, []);

	return <Fragment>
		{!departmentOptions
			? <Loader message="Data Loading......" />
			: <SelectControl
				multiple
				label='Academic Departments'
				value={assignedValue}
				options={departmentOptions}
				onChange={(value) => onChange({ [taxonomyKey]: value })}
			/>
		}
		<ResetButton onChange={onChange} taxonomyKey={taxonomyKey}/>
	</Fragment>;
};

const AdministrativeDepartmentSelector = ({ searchParams, onChange }) => {
	const taxonomyKey = 'administrative_departments';
	const [departmentOptions, setDepartmentOptions] = useState();
	const setValue = searchParams.get(taxonomyKey);
	const assignedValue = setValue ? setValue.split(',') : [];

	useEffect(() => {
		const getDepartments = async () => {
			fetchAdministrativeDepartments()
				.then((departments) => {
					const options = departments
						.map((department) => ({ label: department.name, value: department.id }));
					setDepartmentOptions(options);
				})
				.catch((err) => { console.error(err); });
		};

		getDepartments();
	}, []);

	return <Fragment>
		{!departmentOptions
			? <Loader message="Data Loading......" />
			: <SelectControl
				multiple
				label='Academic Departments'
				value={assignedValue}
				options={departmentOptions}
				onChange={(value) => onChange({ [taxonomyKey]: value })}
			/>
		}
		<ResetButton onChange={onChange} taxonomyKey={taxonomyKey}/>
	</Fragment>;
};

const ProgramSelector = ({ searchParams, onChange }) => {
	const taxonomyKey = 'academic_programs';
	const [programOptions, setProgramOptions] = useState();
	const setValue = searchParams.get(taxonomyKey);
	const assignedValue = setValue ? setValue.split(',') : [];
	console.log({ assignedValue });

	useEffect(() => {
		console.log('ProgramSelect Use effect, run only once');
		const getPrograms = async () => {
			fetchAcademicPrograms()
				.then((programs) => {
					const options = programs.map((program) => ({ label: program.name, value: program.id }));
					setProgramOptions(options);
				})
				.catch((err) => { console.error(err); });
		};

		getPrograms();
	}, []);

	return <Fragment>
		{!programOptions
			? <Loader message="Data Loading......" />
			: <SelectControl
				multiple
				label='Academic Programs'
				value={assignedValue}
				options={programOptions}
				onChange={(value) => onChange({ [taxonomyKey]: value })}
			/>}
		<ResetButton onChange={onChange} taxonomyKey={taxonomyKey}/>
	</Fragment>;
};

const PersonTypeSelector = ({ searchParams, onChange }) => {
	const taxonomyKey = 'person_type';
	const [personTypeOptions, setPersonTypeOptions] = useState();
	const setValue = searchParams.get(taxonomyKey);
	const assignedValue = setValue ? setValue.split(',') : [];

	useEffect(() => {
		const getPersonTypes = async () => {
			fetchPeopleTypes()
				.then((peopleTypes) => {
					const options = peopleTypes.map((type) => ({ label: type.name, value: type.id }));
					setPersonTypeOptions(options);
				})
				.catch((err) => { console.error(err); });
		};

		getPersonTypes();
	}, []);

	return <Fragment>
		{!personTypeOptions
			? <Loader message="Data Loading......" />
			: <SelectControl
				multiple
				label='Person Type'
				value={assignedValue}
				options={personTypeOptions}
				onChange={(value) => onChange({ [taxonomyKey]: value })}
			/>
		}
		<ResetButton onChange={onChange} taxonomyKey={taxonomyKey}/>
	</Fragment>;
};

const FeedControls = ({ onChange, searchParams }) => <InspectorControls>
	<PersonTypeSelector searchParams={searchParams} onChange={onChange}/>
	<AcademicDepartmentSelector searchParams={searchParams} onChange={onChange}/>
	<ProgramSelector searchParams={searchParams} onChange={onChange}/>
	<AdministrativeDepartmentSelector searchParams={searchParams} onChange={onChange}/>
</InspectorControls>;

/* Display */

const DirectoryFeed = ({ data }) => {
	const directoryCards = data.map((person) => {
		const info = person.meta.person_meta;
		const { images: { profile_image: profileImage }, name: { display_name: displayName } } = info;
		const jobTitle = buildJobTitle(info.job);

		return <Card key={person.id} className="grid--item card--people">
			{profileImage
				&& <Card.DrupalProfileImage featuredImage={profileImage} />
			}

			<Card.Content alignment="center">
				<Card.Title title={displayName} />
				<Card.Position position={jobTitle} />
			</Card.Content>
		</Card>;
	});

	return <div className='container'>
		<div className="grid">
			{directoryCards}
		</div>
	</div>;
};

const DirectoryFeedEditor = ({ onChange, ...props }) => {
	const { isSelected, feedUrl } = props;
	const [feedContents, setFeedContents] = useState();

	const baseUrl = new URL(feedUrl);
	const feedParams = baseUrl.search;
	const searchParams = new URLSearchParams(feedParams);

	const [feedURl, setFeedURl] = useState();

	useEffect(() => {
		setFeedURl(baseUrl.toString());
	}, []);

	useEffect(() => {
		console.log('DirectoryFeedEditor', searchParams.toString());
		const getResults = async () => {
			performFetch(baseUrl.toString(), searchParams.toString())
				.then((results) => {
					console.log({ results });
					setFeedContents(results);
				});
		};

		getResults();
	}, [feedURl]);

	const onChangeSettings = (newSettings) => {
		const newSettingsKeys = Object.keys(newSettings);

		newSettingsKeys.forEach((settingKey) => {
			if (newSettings[settingKey].length > 0) {
				searchParams.set(settingKey, newSettings[settingKey].join(','));
			} else {
				searchParams.delete(settingKey);
			}
		});

		baseUrl.search = searchParams.toString();
		onChange(baseUrl.toString());
		setFeedURl(baseUrl.toString());
	};

	return <Fragment>
		{isSelected
			&& <FeedControls onChange={onChangeSettings} searchParams={searchParams} {...props}/>}
		{feedContents && <DirectoryFeed data={feedContents}/>}
	</Fragment>;
};

registerBlockType('newhouse/directory',
	{
		title: 'Directory Feed',
		icon: 'id-alt',
		category: 'newhouse-directory',
		/* Attributes come from the div with data-selector attributes */
		attributes: {
			feedUrl: {
				type: 'string',
				source: 'attribute',
				selector: 'div',
				attribute: 'data-directory-feed',
				default: directoryBaseUrl,
			},
		},

		edit(props) {
			const onChangeFeedUrl = (newFeedUrl) => {
				props.setAttributes({ feedUrl: newFeedUrl });
			};
			/* force focus into the inspector area */
			return <DirectoryFeedEditor
				onChange={onChangeFeedUrl}
				feedUrl={props.attributes.feedUrl}
				{...props}
			/>;
		},

		save(props) {
			console.log('save', props);
			const { feedUrl } = props.attributes;
			console.log({ feedUrl });
			// add component here so it displays on the page preview
			return <div data-directory-feed={feedUrl}>
				Loading Directory Results
				{{/* Alt/fallback a list of names with links to their individual pages */}}
			</div>;
		},
	});
