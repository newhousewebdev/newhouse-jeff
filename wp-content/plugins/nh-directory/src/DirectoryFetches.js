const basePeopleUrl = 'https://resources.newhouse.syr.edu/directory/wp-json/wp/v2/directory';
const baseUrl = 'https://resources.newhouse.syr.edu/directory/wp-json/wp/v2/';

/*
	Note: A copy of this is used in the Directory Plugin
	* Could this be js file retrieved from directory, similar to wp.api
	* it could be registered as a dependency in wordpress similar to jquery from a cdn
*/
// base params are to get all, ordered by ascending title, ie this is alphabetical by last name
const defaultPeopleFetchParams = {
	per_page: 100,
	orderby: 'title',
	order: 'asc',
	_fields: 'id,title,slug,meta',
};

const defaultTaxonomyFetchParams = {
	per_page: 100,
	orderby: 'name',
	order: 'asc',
	_fields: 'id,name,slug',
};

/* Utility Functions */
// assumes object is single level object: ie no nesting
const parameterizeObject = (object) => Object.entries(object).map(([key, value]) => `${key}=${value}`).join('&');

// params are objects here
// refactor second arg to be unlimited
const mergeParams = (defaultParams, additionalParams) => {
	const baseParams = { ...defaultParams };

	if (additionalParams) {
		const paramKeys = Object.keys(additionalParams);
		paramKeys.forEach((paramKey) => {
			baseParams[paramKey] = additionalParams[paramKey];
		});
	}
	return baseParams;
};

const addToDefaultFields = (defaultFields, additionalFields) => {
	if (Array.isArray(additionalFields)) {
		const newFields = additionalFields.join(',');
		return `${defaultFields},${newFields}`;
	}
	return `${defaultFields},${additionalFields}`;
};

/* Fetches */

/**
 * The core fetch
 * @param {string} url - The base url
 * @param {string} searchParams - Additional info sent as a query string
*/
export const performFetch = async (url, searchParams) => {
	const fullUrl = `${url}?${searchParams}`;
	return fetch(fullUrl)
		.then((res) => res.json())
		.then((res) => res)
		.catch((err) => { console.error(err); });
};

export const fetchPersonByName = ({ firstName, lastName }) => {
	const nameParams = {
		title: `${lastName} ${firstName}`,
	};

	return performFetch(basePeopleUrl,
		parameterizeObject(mergeParams(defaultPeopleFetchParams, nameParams)));
};

/**
 * Perform a basic fetch with the ability to add optional variables and _fields
 *
 * @param {string} slug - Person slug to look up.
 * @param {array|string} additionalFields - Additional _fields to add to fetch.
 *
 * @example <caption>Example of Additional Fields</caption>
 * additionalFields = ['id','name','slug','author']
 * @returns {array|error} array of matching people from the directory
*/
export const fetchPersonBySlug = (slug, additionalFields = undefined) => {
	let slugParams = {
		slug,
	};

	if (additionalFields) {
		const newFields = {
			// eslint-disable-next-line no-underscore-dangle
			_fields: addToDefaultFields(defaultPeopleFetchParams._fields, additionalFields),
		};
		slugParams = mergeParams(slugParams, newFields);
	}

	return performFetch(basePeopleUrl,
		parameterizeObject(mergeParams(defaultPeopleFetchParams, slugParams)));
};

export const fetchPeopleByProgram = (programID, additionalFields = undefined) => {
	let programParams = {
		academic_programs: programID,
	};

	if (additionalFields) {
		const newFields = {
			// eslint-disable-next-line no-underscore-dangle
			_fields: addToDefaultFields(defaultPeopleFetchParams._fields, additionalFields),
		};
		programParams = mergeParams(programParams, newFields);
	}

	return performFetch(basePeopleUrl,
		parameterizeObject(mergeParams(defaultPeopleFetchParams, programParams)));
};

export const fetchFacultyByDepartment = (departmentID, additionalFields = undefined) => {
	let departmentParams = {
		academic_departments: departmentID,
	};

	if (additionalFields) {
		const newFields = {
			// eslint-disable-next-line no-underscore-dangle
			_fields: addToDefaultFields(defaultPeopleFetchParams._fields, additionalFields),
		};
		departmentParams = mergeParams(departmentParams, newFields);
	}

	return performFetch(basePeopleUrl,
		parameterizeObject(mergeParams(defaultPeopleFetchParams, departmentParams)));
};

export const fetchStaffByDepartment = (departmentID, additionalFields = undefined) => {
	let departmentParams = {
		administrative_departments: departmentID,
	};

	if (additionalFields) {
		const newFields = {
			// eslint-disable-next-line no-underscore-dangle
			_fields: addToDefaultFields(defaultPeopleFetchParams._fields, additionalFields),
		};
		departmentParams = mergeParams(departmentParams, newFields);
	}

	return performFetch(basePeopleUrl,
		parameterizeObject(mergeParams(defaultPeopleFetchParams, departmentParams)));
};

export const fetchFacultyAndStaff = (additionalFields = undefined) => {
	let facultyStaffParams = {
		person_type_exclude: '202,203',
	};

	if (additionalFields) {
		const newFields = {
			// eslint-disable-next-line no-underscore-dangle
			_fields: addToDefaultFields(defaultPeopleFetchParams._fields, additionalFields),
		};
		facultyStaffParams = mergeParams(facultyStaffParams, newFields);
	}

	return performFetch(basePeopleUrl,
		parameterizeObject(mergeParams(defaultPeopleFetchParams, facultyStaffParams)));
};

/**
 * Perform a basic fetch with the ability to add optional variables and _fields
 *
 * @param {object} options - Additional arguments to include in fetch.
 * @param {array|string} additionalFields - Additional _fields to add to fetch.
 *
 * @example <caption>Example options object</caption>
 * options = {
 * 	person_type: '{int}',
 *  person_type_exclude: '{int}',
 *  administrative_departments: '{int},{int}',
 * }
 * @example <caption>Example of Additional Fields</caption>
 * additionalFields = ['id','name','slug','author']
 * @returns {array|error} array of matching people from the directory
*/
export const fetchDirectoryWithOptions = (options = undefined, additionalFields = undefined) => {
	/* Options will be an object */
	let basicParams = options || {};

	if (additionalFields) {
		const newFields = {
			// eslint-disable-next-line no-underscore-dangle
			_fields: addToDefaultFields(defaultPeopleFetchParams._fields, additionalFields),
		};
		basicParams = mergeParams(basicParams, newFields);
	}

	return performFetch(basePeopleUrl,
		parameterizeObject(mergeParams(defaultPeopleFetchParams, basicParams)));
};

/* Taxonomy Related Fetches */
export const fetchAcademicPrograms = () => {
	const programsURL = `${baseUrl}academic_programs`;

	return performFetch(programsURL, parameterizeObject(defaultTaxonomyFetchParams));
};

export const fetchAcademicDepartments = () => {
	const departmentsURL = `${baseUrl}academic_departments`;

	return performFetch(departmentsURL, parameterizeObject(defaultTaxonomyFetchParams));
};

export const fetchAdministrativeDepartments = () => {
	const departmentsURL = `${baseUrl}administrative_departments`;

	return performFetch(departmentsURL, parameterizeObject(defaultTaxonomyFetchParams));
};

export const fetchPeopleTypes = () => {
	const peopleTypeURL = `${baseUrl}person_type`;

	return performFetch(peopleTypeURL, parameterizeObject(defaultTaxonomyFetchParams));
};
