import './blocks/lede-paragraph';
import './blocks/aside';
import './blocks/faq';
import './related-stories';

// custom author
import './editor/customDocumentSettingPanel.js';

// custom featured image panel
import './editor/customFeaturedImagePanel.js';
