import { __ } from '@wordpress/i18n';
import { registerPlugin } from '@wordpress/plugins';
import { PluginDocumentSettingPanel } from '@wordpress/edit-post';
import { TextControl, ToggleControl } from '@wordpress/components';
import { withSelect, withDispatch, dispatch, select } from '@wordpress/data';
import { compose } from '@wordpress/compose';

// custom author toggle true/false
let CustomAuthorToggle = (props) => {
	return <ToggleControl
		label="Display author on posts"
		help={props.displayAuthor ? 'Yes, display the author.' : 'No, don\'t display the author.'}
		checked={props.displayAuthor}
		onChange={(value) => {
			props.onMetaFieldChange(value);
		}}
	/>
};

CustomAuthorToggle = compose([
	withSelect( (select) => {
		return {
			displayAuthor: select('core/editor').getEditedPostAttribute('meta')['display_author_toggle']
		}
	} ),
	withDispatch( (dispatch) => {
		return {
			onMetaFieldChange: (value) => {
				dispatch('core/editor').editPost({ meta: { display_author_toggle: value } })
			}
		}
	} ),
] )(CustomAuthorToggle);


// custom author name
let CustomAuthorNameMeta = (props) => (
	<TextControl
		value={props.customAuthorName}
		label={__("Custom author name", "textdomain")}
		onChange={(value) => props.onMetaFieldChange(value)}
	/>
);

CustomAuthorNameMeta = compose([
	withSelect( (select) => {
		return {
			customAuthorName: select('core/editor').getEditedPostAttribute('meta')['custom_author_name']
		}
	} ) ,
	withDispatch( (dispatch) => {
		return {
			onMetaFieldChange: (value) => {
				dispatch('core/editor').editPost({ meta: { custom_author_name: value } })
			}
		}
	} ),
] )(CustomAuthorNameMeta);


const CustomAuthorDocumentSettingPanel = () => {
	// Check if a value has been set
	// This is for editing a post, because you don't want to override it everytime
	if (!select('core/editor').getEditedPostAttribute('meta')['display_author_toggle']) {
		// Set initial value
		dispatch('core/editor').editPost({ meta: { display_author_toggle: false } });
	}
	if (!select('core/editor').getEditedPostAttribute('meta')['custom_author_name']) {
		// Set initial value
		dispatch('core/editor').editPost({ meta: { custom_author_name: '' } });
	}
	return <PluginDocumentSettingPanel
		name="author-settings"
		title="Author Settings"
		className="author-settings"
	>
		<CustomAuthorToggle />
		<CustomAuthorNameMeta />
	</PluginDocumentSettingPanel>
};

registerPlugin('nhblocks-document-setting-panel', {
	render: CustomAuthorDocumentSettingPanel,
	icon: '',
});
