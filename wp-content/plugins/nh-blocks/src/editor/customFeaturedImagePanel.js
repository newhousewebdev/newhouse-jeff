import React, {Fragment} from 'react';
import { TextControl, ToggleControl } from '@wordpress/components';
import { withSelect, withDispatch, dispatch, select } from '@wordpress/data';
import { compose } from '@wordpress/compose';
import { addFilter } from '@wordpress/hooks';

addFilter(
	'editor.PostFeaturedImage',
	'enchance-featured-image/display-featured-image-toggle',
	wrapPostFeaturedImage
);

// custom toggle true/false for displaying the featured image
let DisplayFeaturedImageToggle = (props) => {
	return <ToggleControl
		label="Display featured image on posts"
		help={props.displayFeaturedImage ? 'Yes, display the featured image.' : 'No, don\'t display the featured image.'}
		checked={props.displayFeaturedImage}
		onChange={(value) => {
			props.onMetaFieldChange(value);
		}}
	/>
};

DisplayFeaturedImageToggle = compose([
	withSelect((select) => {
		return {
			displayFeaturedImage: select('core/editor').getEditedPostAttribute('meta')['display_featured_image']
		}
	}),
	withDispatch((dispatch) => {
		return {
			onMetaFieldChange: (value) => {
				dispatch('core/editor').editPost({ meta: { display_featured_image: value } })
			}
		}
	}),
])(DisplayFeaturedImageToggle);

function wrapPostFeaturedImage(OriginalComponent) {
	return (props) => (
		<Fragment>
			<OriginalComponent {...props} />
			<DisplayFeaturedImageToggle />
		</Fragment>
	)
} 
