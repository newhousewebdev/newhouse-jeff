/* eslint-disable react/react-in-jsx-scope */
// eslint-disable-next-line import/no-unresolved
import { InspectorControls } from '@wordpress/block-editor';
// eslint-disable-next-line import/no-unresolved
import { SelectControl, TextControl, Button, PanelBody } from '@wordpress/components';
// eslint-disable-next-line import/no-extraneous-dependencies
import { useEffect, useState, Fragment } from '@wordpress/element';
// eslint-disable-next-line import/no-unresolved
import { registerBlockType } from '@wordpress/blocks';
import { getTaxonomy, getPosts } from './api';
import Loader from './Loader';

console.log('Related Stories Loaded');
const ResetButton = ({ onChange, taxonomyKey }) => <Button
	style={{
		color: 'red', marginTop: '-24px', marginBottom: '18px', display: 'block',
	}}
	className="btn-tertiary"
	onClick={() => onChange({ [taxonomyKey]: [] })}
>
		Reset
</Button>;

const TaxonomySelect = ({
	feedParams,
	onChange,
	taxonomyKey,
	taxonomyLabel,
}) => {
	const setValue = feedParams.get(taxonomyKey);
	const assignedValue = setValue ? setValue.split(',') : [];
	const taxonomy = getTaxonomy(taxonomyKey);
	const [termOptions, setTermOptions] = useState();

	useEffect(() => {
		if (taxonomy) {
			const options = taxonomy
				.map((term) => ({ value: term.id, label: term.name }));
			options.unshift({ value: null, label: `Select ${taxonomyLabel}` });
			setTermOptions(options);
		}
	}, [taxonomy]);

	return <Fragment>
		{!termOptions
			? <Loader message="Data Loading......" />
			: <SelectControl
				multiple
				label={taxonomyLabel}
				value={assignedValue }
				options={termOptions}
				onChange={(value) => onChange({ [taxonomyKey]: value })}
			/>
		}
		<ResetButton onChange={onChange} taxonomyKey={taxonomyKey}/>
	</Fragment>;
};

const FeedTitleTextControl = ({ onChange, feedDisplay }) => {
	const assignedValue = feedDisplay.get('title');

	return <TextControl
		label='Feed Title (optional)'
		type='text'
		value={assignedValue || ''}
		onChange={(value) => {
			const key = 'title';
			onChange({ [key]: [value] });
		}}
	/>;
};

const PerPageSelectControl = ({ onChange, feedParams }) => {
	const options = [
		{ label: 'Three posts', value: 3 },
		{ label: 'Six posts', value: 6 },
	];
	const setValue = feedParams.get('per_page');
	const assignedValue = setValue || 3;

	return <SelectControl
		label='Number of posts to display'
		type='text'
		value={assignedValue || ''}
		options={options}
		onChange={(value) => {
			onChange({ per_page: [value] });
		}}
	/>;
};

const FeedControls = ({
	onChangeFeedData,
	onChangeFeedDisplay,
	feedParams,
	feedDisplayParams,
}) => {
	console.log({ feedParams });
	return <InspectorControls>
		<PanelBody title='Feed Display Settings'>
			<FeedTitleTextControl onChange={onChangeFeedDisplay} feedDisplay={feedDisplayParams} />
			{/* per_page */}
			<PerPageSelectControl onChange={onChangeFeedData} feedParams={feedParams} />
		</PanelBody>
		<PanelBody title='Feed Filters'>
			<TaxonomySelect feedParams={feedParams} onChange={onChangeFeedData} taxonomyKey='news-categories' taxonomyLabel='News Categories'/>
			<TaxonomySelect feedParams={feedParams} onChange={onChangeFeedData} taxonomyKey='programs' taxonomyLabel='Programs'/>
			<TaxonomySelect feedParams={feedParams} onChange={onChangeFeedData} taxonomyKey='departments' taxonomyLabel='Departments'/>
		</PanelBody>
	</InspectorControls>;
};

/* Card Stuff */
const FeaturedImage = ({ featuredImage }) => (
	<div className="card--featuredImage--container">
		<img
			src={featuredImage.sourceUrl}
			alt={featuredImage.altText}
			className="card--featuredImage"
		/>
	</div>
);

const Card = ({ children, className }) => (
	<article className={`card card--${className}`} tabIndex="1">
		{children}
	</article>
);

/* End Card Stuff */
const Feed = ({ posts, feedTitle }) => {
	const newsCards = posts.map((post) => {
		const {
			title: { rendered: title }, excerpt: { rendered: excerpt },
		} = post;
		// eslint-disable-next-line no-underscore-dangle
		const featuredImageData = post._embedded && post._embedded['wp:featuredmedia'][0];
		const featuredImage = featuredImageData && {
			sourceUrl: featuredImageData.source_url, altText: featuredImageData.alt_text,
		};
		// eslint-disable-next-line no-underscore-dangle
		const author = post._embedded && post._embedded.author[0];
		return <Card key={post.id} className="news">
			{featuredImage
				&& <FeaturedImage featuredImage={featuredImage} />
			}
			<div className="card--content">
				<h3 className="card--content--title" dangerouslySetInnerHTML={{ __html: title }}/>
				<p className="card--content--excerpt" dangerouslySetInnerHTML={{ __html: excerpt }}/>
				{author && <p className="card--content--author">By <span dangerouslySetInnerHTML={{ __html: author.name }}/></p>}
			</div>
		</Card>;
	});

	return <div className='container'>
		{feedTitle && <h2 className="section--header--title">{feedTitle}</h2>}
		<div className="grid">
			{newsCards}
		</div>
	</div>;
};

const PostsFeed = ({ postsQuery, feedTitle }) => {
	const posts = getPosts(postsQuery);

	return posts ? <Feed posts={posts} feedTitle={feedTitle}/> : 'Select a filter to see the feed Results';
};

const RelatedStoriesFeedEditor = ({ onChangeFeedData, onChangeFeedDisplay, ...props }) => {
	const { isSelected, feedData, feedDisplay } = props;
	const [feedParams, setFeedParams] = useState(new URLSearchParams(feedData));
	const [feedDisplayParams, setFeedDisplayParams] = useState(new URLSearchParams(feedDisplay));
	const [postsQuery, setPostsQuery] = useState(feedData);
	console.log('related stories feed editor',
		{ onChangeFeedData },
		{ props },
		{ feedData },
		{ feedDisplay });

	console.log(feedParams.toString(), { postsQuery }, feedDisplayParams.toString());

	const buildPostsQuery = (params) => {
		const paramKeys = Array.from(params.keys());
		const query = paramKeys
			.reduce((object, key) => ({ ...object, [key]: feedParams.get(key) }), {});
		// eslint-disable-next-line no-underscore-dangle
		query._embed = true;
		setPostsQuery(query);
	};

	const onChangeParams = (settings, params) => {
		const newSettingsKeys = Object.keys(settings);

		newSettingsKeys.forEach((settingKey) => {
			if (settings[settingKey].length > 0) {
				params.set(settingKey, settings[settingKey].join(','));
			} else {
				params.delete(settingKey);
			}
		});
	};

	const onChangeSettings = (newSettings) => {
		onChangeParams(newSettings, feedParams);
		onChangeFeedData(feedParams.toString());
		setFeedParams(feedParams);
		buildPostsQuery(feedParams);
	};

	const onChangeFeedDisplaySettings = (newSettings) => {
		onChangeParams(newSettings, feedDisplayParams);
		onChangeFeedDisplay(feedDisplayParams.toString());
		setFeedDisplayParams(feedDisplayParams);
	};

	return <Fragment>
		{isSelected && <FeedControls
			onChangeFeedData={onChangeSettings}
			onChangeFeedDisplay={onChangeFeedDisplaySettings}
			feedParams={feedParams}
			feedDisplayParams={feedDisplayParams}/>
		}
		{postsQuery ? <PostsFeed postsQuery={postsQuery} feedTitle={feedDisplayParams.get('title')}/> : 'Select a Post Filter for the feed'}
	</Fragment>;
};

registerBlockType('nh-blocks/feed',
	{
		title: 'Related Stories',
		icon: 'id-alt',
		category: 'newhouse-blocks',
		/* Attributes come from the div with data-selector attributes */
		attributes: {
			feedData: {
				type: 'string',
				source: 'attribute',
				selector: 'div',
				attribute: 'data-posts-feed',
			},
			feedDisplay: {
				type: 'string',
				source: 'attribute',
				selector: 'div',
				attribute: 'data-posts-feed-display',
			},
		},

		edit(props) {
			const onChangeFeedData = (newFeedData) => {
				props.setAttributes({ feedData: newFeedData });
			};
			const onChangeFeedDisplay = (newFeedDisplaySettings) => {
				props.setAttributes({ feedDisplay: newFeedDisplaySettings });
			};

			return <RelatedStoriesFeedEditor
				onChangeFeedData={onChangeFeedData}
				onChangeFeedDisplay={onChangeFeedDisplay}
				feedData={props.attributes.feedData}
				feedDisplay={props.attributes.feedDisplay}
				{...props}
			/>;
		},

		save(props) {
			const { feedData } = props.attributes;
			const { feedDisplay } = props.attributes;
			console.log('^^^^ saving ^^^^^', { feedData }, { feedDisplay });
			// add component here so it displays on the page preview
			return <div data-posts-feed={feedData} data-posts-feed-display={feedDisplay}>
				Loading News Results
			</div>;
		},
	});
