/* eslint-disable react/react-in-jsx-scope */
import { displayContent } from './utility';

/* Placeholder for customized loader that shows during fetches/lengthy updates are taking place */
const Loader = (props) => (
	<p
		className="loader-message"
		dangerouslySetInnerHTML={displayContent(props.message)}
	/>
);

export default Loader;
