/* React specific method */
export const displayContent = (content) => ({ __html: content });

/* Object methods */
export const deepCopy = (object) => ({ ...JSON.parse(JSON.stringify(object)) });
