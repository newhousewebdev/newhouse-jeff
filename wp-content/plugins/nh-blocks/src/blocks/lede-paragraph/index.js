import { registerBlockType } from '@wordpress/blocks';
import { RichText } from '@wordpress/block-editor';

registerBlockType('nh-blocks/lede-paragraph', {
	title: 'Lede paragraph',
	icon: 'star-filled',
	category: 'common',
	attributes: {
		content: {
			type: 'array',
			source: 'children',
			selector: 'p',
		},
	},
	example: {
		attributes: {
			content: 'Type in your lede paragraph here.',
		},
	},
	edit: (props) => {
		const { attributes: { content }, setAttributes, className } = props;
		const onChangeContent = (newContent) => {
			setAttributes({ content: newContent });
		};
		return (
			<div className={`${className} lede`}>
				<RichText
					tagName="p"
					onChange={onChangeContent}
					value={content}
				/>
			</div>
		);
	},
	save: (props) => {
		const { attributes: { content }, className } = props;
		return <div className={`${className} lede`}>
			<RichText.Content tagName="p" value={content} />
		</div>;
	},
}); // lede paragraph end
