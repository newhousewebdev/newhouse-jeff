import { registerBlockType } from '@wordpress/blocks';

registerBlockType('nh-blocks/toy', {
	title: 'Toy',
	icon: 'star-filled',
	category: 'common',
	attributes: {
		content: {
			type: 'array',
			source: 'children',
			selector: 'p',
		},
	},
	example: {
		attributes: {
			content: 'Type your toy here.',
		},
	},
	edit: () => {
		return (
			<div className="toy">
				Toy test edit
			</div>
		);
	},
	save: () => {
		return (
			<div className="toy">
				Toy test save
			</div>
		);
	},
}); // toy end
