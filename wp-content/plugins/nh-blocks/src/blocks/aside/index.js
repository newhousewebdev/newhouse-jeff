import { registerBlockType } from '@wordpress/blocks';
import { RichText } from '@wordpress/block-editor';

registerBlockType('nh-blocks/aside', {
	title: 'Aside',
	icon: 'warning',
	category: 'common',
	attributes: {
		content: {
			type: 'array',
			source: 'children',
			selector: 'p',
		},
	},
	example: {
		attributes: {
			content: 'Type in your aside note here.',
		},
	},
	edit(props) {
		const { attributes: { content }, setAttributes, className } = props;
		const onChangeContent = (newContent) => {
			setAttributes({ content: newContent });
		};
		return (
			<div className={`${className} aside`}>
				<RichText
					tagName="p"
					onChange={onChangeContent}
					value={content}
				/>
			</div>
		);
	},
	save(props) {
		const { attributes: { content }, className } = props;
		return <div className={`${className} aside`}>
			<RichText.Content tagName="p" value={content} />
		</div>;
	},
}); // lede paragraph end
