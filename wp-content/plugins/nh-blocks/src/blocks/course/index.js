import { registerBlockType } from '@wordpress/blocks';
import { RichText } from '@wordpress/block-editor';

registerBlockType('nh-blocks/course', {
	title: 'Course',
	icon: 'smiley',
	category: 'common',
	attributes: {
		prefix: {
			type: 'string',
			source: 'html',
			selector: '.course--prefix',
		},
		courseNum: {
			type: 'string',
			source: 'html',
			selector: '.course--number',
		},
		title: {
			type: 'string',
			source: 'html',
			selector: '.course--title',
		},
		description: {
			type: 'string',
			source: 'html',
			selector: '.course--description',
		},
	},
	edit({className, attributes, setAttributes}) {
		const {prefix, courseNum, title, description } = attributes;
		const onChangePrefix = (prefix) => {
			setAttributes({ prefix });
		};
		const onChangeCourseNum = (courseNum) => {
			setAttributes({ courseNum });
		};
		const onChangeTitle = (title) => {
			setAttributes({ title });
		};
		const onChangeDescription = (description) => {
			setAttributes({ description });
		};
		return (
			<article className={`${className} course`}>
				<div className="course--left">
					<RichText
						tagName="span"
						className="course--prefix"
						onChange={onChangePrefix}
						value={prefix}
						placeholder="e.g. COM"
					/>
					<RichText
						tagName="span"
						className="course--number"
						onChange={onChangeCourseNum}
						value={courseNum}
						placeholder="e.g. 105"
					/>
				</div>
				<div className="course--right">
					<RichText
						tagName="h3"
						className="course--title"
						onChange={onChangeTitle}
						value={title}
						placeholder="Type the course title here..."
					/>
					<RichText
						tagName="p"
						className="course--description"
						onChange={onChangeDescription}
						value={description}
						placeholder="Type the course description here..."
					/>
				</div>
			</article>
		);
	},
	save({className, attributes}) {
		const { prefix, courseNum, title, description } = attributes;
		return <article className={`${className} course`}>
			<div className="course--left">
				<RichText.Content
					tagName="span"
					className="course--prefix"
					value={prefix}
				/>
				<RichText.Content
					tagName="span"
					className="course--number"
					value={courseNum}
				/>
			</div>
			<div className="course--right">
				<RichText.Content
					tagName="h3"
					className="course--title"
					value={title}
				/>
				<RichText.Content
					tagName="p"
					className="course--description"
					value={description}
				/>
			</div>
		</article>;
	},
});
