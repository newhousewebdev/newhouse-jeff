import { registerBlockType } from '@wordpress/blocks';
import { RichText, BlockControls } from '@wordpress/block-editor';

registerBlockType('nh-blocks/faq', {
	title: 'FAQ',
	icon: 'plus-alt',
	category: 'common',
	attributes: {
		question: {
			type: 'string',
			source: 'html',
			selector: '.faq--question',
		},
		answer: {
			type: 'string',
			source: 'html',
			selector: '.faq--answer--items',
		},
	},
	edit: ({attributes, setAttributes}) => {
		const { question, answer } = attributes;
		const onChangeQuestion = (question) => {
			setAttributes({ question });
		};
		const onChangeAnswer = (answer) => {
			setAttributes({ answer });
		};
		return (
			<article className="faq active">
				<BlockControls />
				<RichText
					tagName="h3"
					className="faq--question"
					onChange={onChangeQuestion}
					value={question}
					placeholder="Type your question here..."
				/>
				<div className="faq--answer">
					<RichText
						tagName="p"
						className="faq--answer--items"
						onChange={onChangeAnswer}
						value={answer}
						placeholder="Type your answer here..."
					/>
				</div>
			</article>
		);
	},
	save({ attributes }) {
		const { question, answer } = attributes;
		
		return <article className="faq">
			<RichText.Content
				tagName="h3"
				className="faq--question"
				value={question}
			/>
			<div className="faq--answer">
				<RichText.Content
					tagName="p"
					className="faq--answer--items"
					value={answer}
				/>
			</div>
		</article>;
	},
});


