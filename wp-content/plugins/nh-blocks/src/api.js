// eslint-disable-next-line import/no-unresolved
import { useSelect } from '@wordpress/data';

/**
 * Get a taxonomy by Slug
 *
 * @param {string} taxonomy - taxonomy slug to get records for, Includes empty
*/
export const getTaxonomy = (taxonomy) => useSelect((taxonomySelect) => taxonomySelect('core').getEntityRecords('taxonomy', taxonomy, { per_page: -1, hide_empty: false }));

export const getCurrentPost = () => useSelect((postSelect) => postSelect('core/editor').getCurrentPost());

export const getPosts = (query) => useSelect((postsSelect) => postsSelect('core').getEntityRecords('postType', 'post', query));

// export const getPostAuthor = (query) => useSelect((postAuthor) => postAuthor('core').getEntityRecords('postType', 'post', query));

// export const getPostsFeaturedImage = (imageID) => useSelect((postFeatureImage) => postFeatureImage('core').getEntityRecords('postType', 'post',));
