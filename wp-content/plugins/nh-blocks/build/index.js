/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/helpers/arrayLikeToArray.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayLikeToArray.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }

  return arr2;
}

module.exports = _arrayLikeToArray;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/arrayWithHoles.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayWithHoles.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

module.exports = _arrayWithHoles;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/defineProperty.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/defineProperty.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

module.exports = _defineProperty;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/extends.js":
/*!********************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/extends.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _extends() {
  module.exports = _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

module.exports = _extends;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArrayLimit(arr, i) {
  if (typeof Symbol === "undefined" || !(Symbol.iterator in Object(arr))) return;
  var _arr = [];
  var _n = true;
  var _d = false;
  var _e = undefined;

  try {
    for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

module.exports = _iterableToArrayLimit;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/nonIterableRest.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/nonIterableRest.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

module.exports = _nonIterableRest;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/objectWithoutProperties.js":
/*!************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/objectWithoutProperties.js ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var objectWithoutPropertiesLoose = __webpack_require__(/*! ./objectWithoutPropertiesLoose */ "./node_modules/@babel/runtime/helpers/objectWithoutPropertiesLoose.js");

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = objectWithoutPropertiesLoose(source, excluded);
  var key, i;

  if (Object.getOwnPropertySymbols) {
    var sourceSymbolKeys = Object.getOwnPropertySymbols(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (excluded.indexOf(key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

module.exports = _objectWithoutProperties;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/objectWithoutPropertiesLoose.js":
/*!*****************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/objectWithoutPropertiesLoose.js ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};
  var sourceKeys = Object.keys(source);
  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (excluded.indexOf(key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

module.exports = _objectWithoutPropertiesLoose;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/slicedToArray.js":
/*!**************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/slicedToArray.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithHoles = __webpack_require__(/*! ./arrayWithHoles */ "./node_modules/@babel/runtime/helpers/arrayWithHoles.js");

var iterableToArrayLimit = __webpack_require__(/*! ./iterableToArrayLimit */ "./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js");

var unsupportedIterableToArray = __webpack_require__(/*! ./unsupportedIterableToArray */ "./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js");

var nonIterableRest = __webpack_require__(/*! ./nonIterableRest */ "./node_modules/@babel/runtime/helpers/nonIterableRest.js");

function _slicedToArray(arr, i) {
  return arrayWithHoles(arr) || iterableToArrayLimit(arr, i) || unsupportedIterableToArray(arr, i) || nonIterableRest();
}

module.exports = _slicedToArray;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__(/*! ./arrayLikeToArray */ "./node_modules/@babel/runtime/helpers/arrayLikeToArray.js");

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return arrayLikeToArray(o, minLen);
}

module.exports = _unsupportedIterableToArray;

/***/ }),

/***/ "./src/Loader.js":
/*!***********************!*\
  !*** ./src/Loader.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _utility__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utility */ "./src/utility.js");


/* eslint-disable react/react-in-jsx-scope */

/* Placeholder for customized loader that shows during fetches/lengthy updates are taking place */

var Loader = function Loader(props) {
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("p", {
    className: "loader-message",
    dangerouslySetInnerHTML: Object(_utility__WEBPACK_IMPORTED_MODULE_1__["displayContent"])(props.message)
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Loader);

/***/ }),

/***/ "./src/api.js":
/*!********************!*\
  !*** ./src/api.js ***!
  \********************/
/*! exports provided: getTaxonomy, getCurrentPost, getPosts */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTaxonomy", function() { return getTaxonomy; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCurrentPost", function() { return getCurrentPost; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getPosts", function() { return getPosts; });
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_0__);
// eslint-disable-next-line import/no-unresolved

/**
 * Get a taxonomy by Slug
 *
 * @param {string} taxonomy - taxonomy slug to get records for, Includes empty
*/

var getTaxonomy = function getTaxonomy(taxonomy) {
  return Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_0__["useSelect"])(function (taxonomySelect) {
    return taxonomySelect('core').getEntityRecords('taxonomy', taxonomy, {
      per_page: -1,
      hide_empty: false
    });
  });
};
var getCurrentPost = function getCurrentPost() {
  return Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_0__["useSelect"])(function (postSelect) {
    return postSelect('core/editor').getCurrentPost();
  });
};
var getPosts = function getPosts(query) {
  return Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_0__["useSelect"])(function (postsSelect) {
    return postsSelect('core').getEntityRecords('postType', 'post', query);
  });
}; // export const getPostAuthor = (query) => useSelect((postAuthor) => postAuthor('core').getEntityRecords('postType', 'post', query));
// export const getPostsFeaturedImage = (imageID) => useSelect((postFeatureImage) => postFeatureImage('core').getEntityRecords('postType', 'post',));

/***/ }),

/***/ "./src/blocks/aside/index.js":
/*!***********************************!*\
  !*** ./src/blocks/aside/index.js ***!
  \***********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/blocks */ "@wordpress/blocks");
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__);



Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__["registerBlockType"])('nh-blocks/aside', {
  title: 'Aside',
  icon: 'warning',
  category: 'common',
  attributes: {
    content: {
      type: 'array',
      source: 'children',
      selector: 'p'
    }
  },
  example: {
    attributes: {
      content: 'Type in your aside note here.'
    }
  },
  edit: function edit(props) {
    var content = props.attributes.content,
        setAttributes = props.setAttributes,
        className = props.className;

    var onChangeContent = function onChangeContent(newContent) {
      setAttributes({
        content: newContent
      });
    };

    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      className: "".concat(className, " aside")
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__["RichText"], {
      tagName: "p",
      onChange: onChangeContent,
      value: content
    }));
  },
  save: function save(props) {
    var content = props.attributes.content,
        className = props.className;
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      className: "".concat(className, " aside")
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__["RichText"].Content, {
      tagName: "p",
      value: content
    }));
  }
}); // lede paragraph end

/***/ }),

/***/ "./src/blocks/faq/index.js":
/*!*********************************!*\
  !*** ./src/blocks/faq/index.js ***!
  \*********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/blocks */ "@wordpress/blocks");
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__);



Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__["registerBlockType"])('nh-blocks/faq', {
  title: 'FAQ',
  icon: 'plus-alt',
  category: 'common',
  attributes: {
    question: {
      type: 'string',
      source: 'html',
      selector: '.faq--question'
    },
    answer: {
      type: 'string',
      source: 'html',
      selector: '.faq--answer--items'
    }
  },
  edit: function edit(_ref) {
    var attributes = _ref.attributes,
        setAttributes = _ref.setAttributes;
    var question = attributes.question,
        answer = attributes.answer;

    var onChangeQuestion = function onChangeQuestion(question) {
      setAttributes({
        question: question
      });
    };

    var onChangeAnswer = function onChangeAnswer(answer) {
      setAttributes({
        answer: answer
      });
    };

    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("article", {
      className: "faq active"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__["BlockControls"], null), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__["RichText"], {
      tagName: "h3",
      className: "faq--question",
      onChange: onChangeQuestion,
      value: question,
      placeholder: "Type your question here..."
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      className: "faq--answer"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__["RichText"], {
      tagName: "p",
      className: "faq--answer--items",
      onChange: onChangeAnswer,
      value: answer,
      placeholder: "Type your answer here..."
    })));
  },
  save: function save(_ref2) {
    var attributes = _ref2.attributes;
    var question = attributes.question,
        answer = attributes.answer;
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("article", {
      className: "faq"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__["RichText"].Content, {
      tagName: "h3",
      className: "faq--question",
      value: question
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      className: "faq--answer"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__["RichText"].Content, {
      tagName: "p",
      className: "faq--answer--items",
      value: answer
    })));
  }
});

/***/ }),

/***/ "./src/blocks/lede-paragraph/index.js":
/*!********************************************!*\
  !*** ./src/blocks/lede-paragraph/index.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/blocks */ "@wordpress/blocks");
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__);



Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_1__["registerBlockType"])('nh-blocks/lede-paragraph', {
  title: 'Lede paragraph',
  icon: 'star-filled',
  category: 'common',
  attributes: {
    content: {
      type: 'array',
      source: 'children',
      selector: 'p'
    }
  },
  example: {
    attributes: {
      content: 'Type in your lede paragraph here.'
    }
  },
  edit: function edit(props) {
    var content = props.attributes.content,
        setAttributes = props.setAttributes,
        className = props.className;

    var onChangeContent = function onChangeContent(newContent) {
      setAttributes({
        content: newContent
      });
    };

    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      className: "".concat(className, " lede")
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__["RichText"], {
      tagName: "p",
      onChange: onChangeContent,
      value: content
    }));
  },
  save: function save(props) {
    var content = props.attributes.content,
        className = props.className;
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])("div", {
      className: "".concat(className, " lede")
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_2__["RichText"].Content, {
      tagName: "p",
      value: content
    }));
  }
}); // lede paragraph end

/***/ }),

/***/ "./src/editor/customDocumentSettingPanel.js":
/*!**************************************************!*\
  !*** ./src/editor/customDocumentSettingPanel.js ***!
  \**************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_plugins__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/plugins */ "@wordpress/plugins");
/* harmony import */ var _wordpress_plugins__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_plugins__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_edit_post__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/edit-post */ "@wordpress/edit-post");
/* harmony import */ var _wordpress_edit_post__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_edit_post__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wordpress/compose */ "@wordpress/compose");
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_wordpress_compose__WEBPACK_IMPORTED_MODULE_6__);






 // custom author toggle true/false

var CustomAuthorToggle = function CustomAuthorToggle(props) {
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__["ToggleControl"], {
    label: "Display author on posts",
    help: props.displayAuthor ? 'Yes, display the author.' : 'No, don\'t display the author.',
    checked: props.displayAuthor,
    onChange: function onChange(value) {
      props.onMetaFieldChange(value);
    }
  });
};

CustomAuthorToggle = Object(_wordpress_compose__WEBPACK_IMPORTED_MODULE_6__["compose"])([Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_5__["withSelect"])(function (select) {
  return {
    displayAuthor: select('core/editor').getEditedPostAttribute('meta')['display_author_toggle']
  };
}), Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_5__["withDispatch"])(function (dispatch) {
  return {
    onMetaFieldChange: function onMetaFieldChange(value) {
      dispatch('core/editor').editPost({
        meta: {
          display_author_toggle: value
        }
      });
    }
  };
})])(CustomAuthorToggle); // custom author name

var CustomAuthorNameMeta = function CustomAuthorNameMeta(props) {
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_components__WEBPACK_IMPORTED_MODULE_4__["TextControl"], {
    value: props.customAuthorName,
    label: Object(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"])("Custom author name", "textdomain"),
    onChange: function onChange(value) {
      return props.onMetaFieldChange(value);
    }
  });
};

CustomAuthorNameMeta = Object(_wordpress_compose__WEBPACK_IMPORTED_MODULE_6__["compose"])([Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_5__["withSelect"])(function (select) {
  return {
    customAuthorName: select('core/editor').getEditedPostAttribute('meta')['custom_author_name']
  };
}), Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_5__["withDispatch"])(function (dispatch) {
  return {
    onMetaFieldChange: function onMetaFieldChange(value) {
      dispatch('core/editor').editPost({
        meta: {
          custom_author_name: value
        }
      });
    }
  };
})])(CustomAuthorNameMeta);

var CustomAuthorDocumentSettingPanel = function CustomAuthorDocumentSettingPanel() {
  // Check if a value has been set
  // This is for editing a post, because you don't want to override it everytime
  if (!Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_5__["select"])('core/editor').getEditedPostAttribute('meta')['display_author_toggle']) {
    // Set initial value
    Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_5__["dispatch"])('core/editor').editPost({
      meta: {
        display_author_toggle: false
      }
    });
  }

  if (!Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_5__["select"])('core/editor').getEditedPostAttribute('meta')['custom_author_name']) {
    // Set initial value
    Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_5__["dispatch"])('core/editor').editPost({
      meta: {
        custom_author_name: ''
      }
    });
  }

  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_edit_post__WEBPACK_IMPORTED_MODULE_3__["PluginDocumentSettingPanel"], {
    name: "author-settings",
    title: "Author Settings",
    className: "author-settings"
  }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(CustomAuthorToggle, null), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(CustomAuthorNameMeta, null));
};

Object(_wordpress_plugins__WEBPACK_IMPORTED_MODULE_2__["registerPlugin"])('nhblocks-document-setting-panel', {
  render: CustomAuthorDocumentSettingPanel,
  icon: ''
});

/***/ }),

/***/ "./src/editor/customFeaturedImagePanel.js":
/*!************************************************!*\
  !*** ./src/editor/customFeaturedImagePanel.js ***!
  \************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/data */ "@wordpress/data");
/* harmony import */ var _wordpress_data__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_wordpress_data__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/compose */ "@wordpress/compose");
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_compose__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/hooks */ "@wordpress/hooks");
/* harmony import */ var _wordpress_hooks__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_5__);






Object(_wordpress_hooks__WEBPACK_IMPORTED_MODULE_5__["addFilter"])('editor.PostFeaturedImage', 'enchance-featured-image/display-featured-image-toggle', wrapPostFeaturedImage); // custom toggle true/false for displaying the featured image

var DisplayFeaturedImageToggle = function DisplayFeaturedImageToggle(props) {
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(_wordpress_components__WEBPACK_IMPORTED_MODULE_2__["ToggleControl"], {
    label: "Display featured image on posts",
    help: props.displayFeaturedImage ? 'Yes, display the featured image.' : 'No, don\'t display the featured image.',
    checked: props.displayFeaturedImage,
    onChange: function onChange(value) {
      props.onMetaFieldChange(value);
    }
  });
};

DisplayFeaturedImageToggle = Object(_wordpress_compose__WEBPACK_IMPORTED_MODULE_4__["compose"])([Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_3__["withSelect"])(function (select) {
  return {
    displayFeaturedImage: select('core/editor').getEditedPostAttribute('meta')['display_featured_image']
  };
}), Object(_wordpress_data__WEBPACK_IMPORTED_MODULE_3__["withDispatch"])(function (dispatch) {
  return {
    onMetaFieldChange: function onMetaFieldChange(value) {
      dispatch('core/editor').editPost({
        meta: {
          display_featured_image: value
        }
      });
    }
  };
})])(DisplayFeaturedImageToggle);

function wrapPostFeaturedImage(OriginalComponent) {
  return function (props) {
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(react__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(OriginalComponent, props), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_0__["createElement"])(DisplayFeaturedImageToggle, null));
  };
}

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _blocks_lede_paragraph__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./blocks/lede-paragraph */ "./src/blocks/lede-paragraph/index.js");
/* harmony import */ var _blocks_aside__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./blocks/aside */ "./src/blocks/aside/index.js");
/* harmony import */ var _blocks_faq__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./blocks/faq */ "./src/blocks/faq/index.js");
/* harmony import */ var _related_stories__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./related-stories */ "./src/related-stories.js");
/* harmony import */ var _editor_customDocumentSettingPanel_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./editor/customDocumentSettingPanel.js */ "./src/editor/customDocumentSettingPanel.js");
/* harmony import */ var _editor_customFeaturedImagePanel_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./editor/customFeaturedImagePanel.js */ "./src/editor/customFeaturedImagePanel.js");



 // custom author

 // custom featured image panel



/***/ }),

/***/ "./src/related-stories.js":
/*!********************************!*\
  !*** ./src/related-stories.js ***!
  \********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/extends */ "./node_modules/@babel/runtime/helpers/extends.js");
/* harmony import */ var _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @babel/runtime/helpers/objectWithoutProperties */ "./node_modules/@babel/runtime/helpers/objectWithoutProperties.js");
/* harmony import */ var _babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @wordpress/block-editor */ "@wordpress/block-editor");
/* harmony import */ var _wordpress_block_editor__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @wordpress/components */ "@wordpress/components");
/* harmony import */ var _wordpress_components__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @wordpress/blocks */ "@wordpress/blocks");
/* harmony import */ var _wordpress_blocks__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./api */ "./src/api.js");
/* harmony import */ var _Loader__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./Loader */ "./src/Loader.js");






function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* eslint-disable react/react-in-jsx-scope */
// eslint-disable-next-line import/no-unresolved
 // eslint-disable-next-line import/no-unresolved

 // eslint-disable-next-line import/no-extraneous-dependencies

 // eslint-disable-next-line import/no-unresolved




console.log('Related Stories Loaded');

var ResetButton = function ResetButton(_ref) {
  var onChange = _ref.onChange,
      taxonomyKey = _ref.taxonomyKey;
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__["Button"], {
    style: {
      color: 'red',
      marginTop: '-24px',
      marginBottom: '18px',
      display: 'block'
    },
    className: "btn-tertiary",
    onClick: function onClick() {
      return onChange(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default()({}, taxonomyKey, []));
    }
  }, "Reset");
};

var TaxonomySelect = function TaxonomySelect(_ref2) {
  var feedParams = _ref2.feedParams,
      _onChange3 = _ref2.onChange,
      taxonomyKey = _ref2.taxonomyKey,
      taxonomyLabel = _ref2.taxonomyLabel;
  var setValue = feedParams.get(taxonomyKey);
  var assignedValue = setValue ? setValue.split(',') : [];
  var taxonomy = Object(_api__WEBPACK_IMPORTED_MODULE_8__["getTaxonomy"])(taxonomyKey);

  var _useState = Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["useState"])(),
      _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState, 2),
      termOptions = _useState2[0],
      setTermOptions = _useState2[1];

  Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["useEffect"])(function () {
    if (taxonomy) {
      var options = taxonomy.map(function (term) {
        return {
          value: term.id,
          label: term.name
        };
      });
      options.unshift({
        value: null,
        label: "Select ".concat(taxonomyLabel)
      });
      setTermOptions(options);
    }
  }, [taxonomy]);
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["Fragment"], null, !termOptions ? Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(_Loader__WEBPACK_IMPORTED_MODULE_9__["default"], {
    message: "Data Loading......"
  }) : Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__["SelectControl"], {
    multiple: true,
    label: taxonomyLabel,
    value: assignedValue,
    options: termOptions,
    onChange: function onChange(value) {
      return _onChange3(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default()({}, taxonomyKey, value));
    }
  }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(ResetButton, {
    onChange: _onChange3,
    taxonomyKey: taxonomyKey
  }));
};

var FeedTitleTextControl = function FeedTitleTextControl(_ref3) {
  var _onChange5 = _ref3.onChange,
      feedDisplay = _ref3.feedDisplay;
  var assignedValue = feedDisplay.get('title');
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__["TextControl"], {
    label: "Feed Title (optional)",
    type: "text",
    value: assignedValue || '',
    onChange: function onChange(value) {
      var key = 'title';

      _onChange5(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default()({}, key, [value]));
    }
  });
};

var PerPageSelectControl = function PerPageSelectControl(_ref4) {
  var _onChange6 = _ref4.onChange,
      feedParams = _ref4.feedParams;
  var options = [{
    label: 'Three posts',
    value: 3
  }, {
    label: 'Six posts',
    value: 6
  }];
  var setValue = feedParams.get('per_page');
  var assignedValue = setValue || 3;
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__["SelectControl"], {
    label: "Number of posts to display",
    type: "text",
    value: assignedValue || '',
    options: options,
    onChange: function onChange(value) {
      _onChange6({
        per_page: [value]
      });
    }
  });
};

var FeedControls = function FeedControls(_ref5) {
  var onChangeFeedData = _ref5.onChangeFeedData,
      onChangeFeedDisplay = _ref5.onChangeFeedDisplay,
      feedParams = _ref5.feedParams,
      feedDisplayParams = _ref5.feedDisplayParams;
  console.log({
    feedParams: feedParams
  });
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(_wordpress_block_editor__WEBPACK_IMPORTED_MODULE_5__["InspectorControls"], null, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__["PanelBody"], {
    title: "Feed Display Settings"
  }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(FeedTitleTextControl, {
    onChange: onChangeFeedDisplay,
    feedDisplay: feedDisplayParams
  }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(PerPageSelectControl, {
    onChange: onChangeFeedData,
    feedParams: feedParams
  })), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(_wordpress_components__WEBPACK_IMPORTED_MODULE_6__["PanelBody"], {
    title: "Feed Filters"
  }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(TaxonomySelect, {
    feedParams: feedParams,
    onChange: onChangeFeedData,
    taxonomyKey: "news-categories",
    taxonomyLabel: "News Categories"
  }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(TaxonomySelect, {
    feedParams: feedParams,
    onChange: onChangeFeedData,
    taxonomyKey: "programs",
    taxonomyLabel: "Programs"
  }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(TaxonomySelect, {
    feedParams: feedParams,
    onChange: onChangeFeedData,
    taxonomyKey: "departments",
    taxonomyLabel: "Departments"
  })));
};
/* Card Stuff */


var FeaturedImage = function FeaturedImage(_ref6) {
  var featuredImage = _ref6.featuredImage;
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("div", {
    className: "card--featuredImage--container"
  }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("img", {
    src: featuredImage.sourceUrl,
    alt: featuredImage.altText,
    className: "card--featuredImage"
  }));
};

var Card = function Card(_ref7) {
  var children = _ref7.children,
      className = _ref7.className;
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("article", {
    className: "card card--".concat(className),
    tabIndex: "1"
  }, children);
};
/* End Card Stuff */


var Feed = function Feed(_ref8) {
  var posts = _ref8.posts,
      feedTitle = _ref8.feedTitle;
  var newsCards = posts.map(function (post) {
    var title = post.title.rendered,
        excerpt = post.excerpt.rendered; // eslint-disable-next-line no-underscore-dangle

    var featuredImageData = post._embedded && post._embedded['wp:featuredmedia'][0];
    var featuredImage = featuredImageData && {
      sourceUrl: featuredImageData.source_url,
      altText: featuredImageData.alt_text
    }; // eslint-disable-next-line no-underscore-dangle

    var author = post._embedded && post._embedded.author[0];
    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(Card, {
      key: post.id,
      className: "news"
    }, featuredImage && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(FeaturedImage, {
      featuredImage: featuredImage
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("div", {
      className: "card--content"
    }, Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("h3", {
      className: "card--content--title",
      dangerouslySetInnerHTML: {
        __html: title
      }
    }), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("p", {
      className: "card--content--excerpt",
      dangerouslySetInnerHTML: {
        __html: excerpt
      }
    }), author && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("p", {
      className: "card--content--author"
    }, "By ", Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("span", {
      dangerouslySetInnerHTML: {
        __html: author.name
      }
    }))));
  });
  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("div", {
    className: "container"
  }, feedTitle && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("h2", {
    className: "section--header--title"
  }, feedTitle), Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("div", {
    className: "grid"
  }, newsCards));
};

var PostsFeed = function PostsFeed(_ref9) {
  var postsQuery = _ref9.postsQuery,
      feedTitle = _ref9.feedTitle;
  var posts = Object(_api__WEBPACK_IMPORTED_MODULE_8__["getPosts"])(postsQuery);
  return posts ? Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(Feed, {
    posts: posts,
    feedTitle: feedTitle
  }) : 'Select a filter to see the feed Results';
};

var RelatedStoriesFeedEditor = function RelatedStoriesFeedEditor(_ref10) {
  var onChangeFeedData = _ref10.onChangeFeedData,
      onChangeFeedDisplay = _ref10.onChangeFeedDisplay,
      props = _babel_runtime_helpers_objectWithoutProperties__WEBPACK_IMPORTED_MODULE_1___default()(_ref10, ["onChangeFeedData", "onChangeFeedDisplay"]);

  var isSelected = props.isSelected,
      feedData = props.feedData,
      feedDisplay = props.feedDisplay;

  var _useState3 = Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["useState"])(new URLSearchParams(feedData)),
      _useState4 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState3, 2),
      feedParams = _useState4[0],
      setFeedParams = _useState4[1];

  var _useState5 = Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["useState"])(new URLSearchParams(feedDisplay)),
      _useState6 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState5, 2),
      feedDisplayParams = _useState6[0],
      setFeedDisplayParams = _useState6[1];

  var _useState7 = Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["useState"])(feedData),
      _useState8 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_2___default()(_useState7, 2),
      postsQuery = _useState8[0],
      setPostsQuery = _useState8[1];

  console.log('related stories feed editor', {
    onChangeFeedData: onChangeFeedData
  }, {
    props: props
  }, {
    feedData: feedData
  }, {
    feedDisplay: feedDisplay
  });
  console.log(feedParams.toString(), {
    postsQuery: postsQuery
  }, feedDisplayParams.toString());

  var buildPostsQuery = function buildPostsQuery(params) {
    var paramKeys = Array.from(params.keys());
    var query = paramKeys.reduce(function (object, key) {
      return _objectSpread(_objectSpread({}, object), {}, _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_3___default()({}, key, feedParams.get(key)));
    }, {}); // eslint-disable-next-line no-underscore-dangle

    query._embed = true;
    setPostsQuery(query);
  };

  var onChangeParams = function onChangeParams(settings, params) {
    var newSettingsKeys = Object.keys(settings);
    newSettingsKeys.forEach(function (settingKey) {
      if (settings[settingKey].length > 0) {
        params.set(settingKey, settings[settingKey].join(','));
      } else {
        params.delete(settingKey);
      }
    });
  };

  var onChangeSettings = function onChangeSettings(newSettings) {
    onChangeParams(newSettings, feedParams);
    onChangeFeedData(feedParams.toString());
    setFeedParams(feedParams);
    buildPostsQuery(feedParams);
  };

  var onChangeFeedDisplaySettings = function onChangeFeedDisplaySettings(newSettings) {
    onChangeParams(newSettings, feedDisplayParams);
    onChangeFeedDisplay(feedDisplayParams.toString());
    setFeedDisplayParams(feedDisplayParams);
  };

  return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["Fragment"], null, isSelected && Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(FeedControls, {
    onChangeFeedData: onChangeSettings,
    onChangeFeedDisplay: onChangeFeedDisplaySettings,
    feedParams: feedParams,
    feedDisplayParams: feedDisplayParams
  }), postsQuery ? Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(PostsFeed, {
    postsQuery: postsQuery,
    feedTitle: feedDisplayParams.get('title')
  }) : 'Select a Post Filter for the feed');
};

Object(_wordpress_blocks__WEBPACK_IMPORTED_MODULE_7__["registerBlockType"])('nh-blocks/feed', {
  title: 'Related Stories',
  icon: 'id-alt',
  category: 'newhouse-blocks',

  /* Attributes come from the div with data-selector attributes */
  attributes: {
    feedData: {
      type: 'string',
      source: 'attribute',
      selector: 'div',
      attribute: 'data-posts-feed'
    },
    feedDisplay: {
      type: 'string',
      source: 'attribute',
      selector: 'div',
      attribute: 'data-posts-feed-display'
    }
  },
  edit: function edit(props) {
    var onChangeFeedData = function onChangeFeedData(newFeedData) {
      props.setAttributes({
        feedData: newFeedData
      });
    };

    var onChangeFeedDisplay = function onChangeFeedDisplay(newFeedDisplaySettings) {
      props.setAttributes({
        feedDisplay: newFeedDisplaySettings
      });
    };

    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])(RelatedStoriesFeedEditor, _babel_runtime_helpers_extends__WEBPACK_IMPORTED_MODULE_0___default()({
      onChangeFeedData: onChangeFeedData,
      onChangeFeedDisplay: onChangeFeedDisplay,
      feedData: props.attributes.feedData,
      feedDisplay: props.attributes.feedDisplay
    }, props));
  },
  save: function save(props) {
    var feedData = props.attributes.feedData;
    var feedDisplay = props.attributes.feedDisplay;
    console.log('^^^^ saving ^^^^^', {
      feedData: feedData
    }, {
      feedDisplay: feedDisplay
    }); // add component here so it displays on the page preview

    return Object(_wordpress_element__WEBPACK_IMPORTED_MODULE_4__["createElement"])("div", {
      "data-posts-feed": feedData,
      "data-posts-feed-display": feedDisplay
    }, "Loading News Results");
  }
});

/***/ }),

/***/ "./src/utility.js":
/*!************************!*\
  !*** ./src/utility.js ***!
  \************************/
/*! exports provided: displayContent, deepCopy */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "displayContent", function() { return displayContent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deepCopy", function() { return deepCopy; });
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/defineProperty */ "./node_modules/@babel/runtime/helpers/defineProperty.js");
/* harmony import */ var _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0__);


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _babel_runtime_helpers_defineProperty__WEBPACK_IMPORTED_MODULE_0___default()(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

/* React specific method */
var displayContent = function displayContent(content) {
  return {
    __html: content
  };
};
/* Object methods */

var deepCopy = function deepCopy(object) {
  return _objectSpread({}, JSON.parse(JSON.stringify(object)));
};

/***/ }),

/***/ "@wordpress/block-editor":
/*!**********************************************!*\
  !*** external {"this":["wp","blockEditor"]} ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["blockEditor"]; }());

/***/ }),

/***/ "@wordpress/blocks":
/*!*****************************************!*\
  !*** external {"this":["wp","blocks"]} ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["blocks"]; }());

/***/ }),

/***/ "@wordpress/components":
/*!*********************************************!*\
  !*** external {"this":["wp","components"]} ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["components"]; }());

/***/ }),

/***/ "@wordpress/compose":
/*!******************************************!*\
  !*** external {"this":["wp","compose"]} ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["compose"]; }());

/***/ }),

/***/ "@wordpress/data":
/*!***************************************!*\
  !*** external {"this":["wp","data"]} ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["data"]; }());

/***/ }),

/***/ "@wordpress/edit-post":
/*!*******************************************!*\
  !*** external {"this":["wp","editPost"]} ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["editPost"]; }());

/***/ }),

/***/ "@wordpress/element":
/*!******************************************!*\
  !*** external {"this":["wp","element"]} ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["element"]; }());

/***/ }),

/***/ "@wordpress/hooks":
/*!****************************************!*\
  !*** external {"this":["wp","hooks"]} ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["hooks"]; }());

/***/ }),

/***/ "@wordpress/i18n":
/*!***************************************!*\
  !*** external {"this":["wp","i18n"]} ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["i18n"]; }());

/***/ }),

/***/ "@wordpress/plugins":
/*!******************************************!*\
  !*** external {"this":["wp","plugins"]} ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["plugins"]; }());

/***/ }),

/***/ "react":
/*!*********************************!*\
  !*** external {"this":"React"} ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["React"]; }());

/***/ })

/******/ });
//# sourceMappingURL=index.js.map