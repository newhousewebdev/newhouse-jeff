<?php
/**
 * Plugin Name: NH Blocks
 * Author: Jeff Passetti
 * Description: Newhouse Blocks
 * Version: 1.0.0
 *
 * @package WordPress
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}//Deny direct file access.

/* Actions and Hooks */\

// Register the Gutenberg Blocks.
add_action( 'init', 'nhblocks_register_post_meta' );
add_action( 'graphql_register_types', 'nhblocks_register_graphql_fields');
add_action( 'init', 'gutenberg_register_blocks' );

// Creates out gutenberg block templates.
add_filter( 'block_categories', 'nh_blocks_block_categories', 10, 2 );

/**
 * Registers the nh_blocks_type
 *
 * @param string $block name being registered.
 * @param array  $options to include.
 */
function nh_blocks_register_block_type( $block, $options = array() ) {
	register_block_type(
		'nh-blocks/' . $block,
		array_merge(
			array(
				'editor_script' => 'nh-blocks-script-editor',
			),
			$options
		)
	);
}

/**
 * Registers gutenberg blocks
 */
function gutenberg_register_blocks() {
	// automatically load dependencies and version. ref: https://developer.wordpress.org/block-editor/tutorials/javascript/js-build-setup/ .
	$asset_file = include( plugin_dir_path( __FILE__ ) . 'build/index.asset.php' ); // phpcs:ignore

	wp_register_script(
		'nh-blocks-script-editor',
		plugins_url( 'build/index.js', __FILE__ ),
		$asset_file['dependencies'],
		$asset_file['version'],
		false,
	);
	nh_blocks_register_block_type( 'lede-paragraph' );
	nh_blocks_register_block_type( 'aside' );
	nh_blocks_register_block_type( 'faq' );
	nh_blocks_register_block_type( 'feed' );
	// phpcs:disable 
	// nh_blocks_register_block_type('course');
	// phpcs:enable
}

/**
 * Creating a newhouse directory block category.
 *
 * @param   array  $categories     List of block categories.
 * @param   object $post being used.
 * @return  array
 */
function nh_blocks_block_categories( $categories, $post ) {
	return array_merge(
		$categories,
		array(
			array(
				'slug'  => 'newhouse-blocks',
				'title' => 'Newhouse Blocks',
			),
		)
	);
}

// register custom meta tag field
function nhblocks_register_post_meta() {
	 register_post_meta( 'post', 'display_author_toggle', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'boolean',
    ) );
    register_post_meta( 'post', 'custom_author_name', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
	) );
	register_post_meta( 'post', 'display_featured_image', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'boolean',
	) );
	register_post_meta( 'attachment', 'image_credit', array(
        'show_in_rest' => true,
        'single' => true,
        'type' => 'string',
    ) );
}

function nhblocks_register_graphql_fields() {
	register_graphql_field( 'Post', 'displayAuthorToggle', [
		'type' => 'Boolean',
		'description' => __( 'Boolean on whether or not to display the author name', 'wp-graphql' ),
		'resolve' => function( $post ) {
		$displayAuthor = get_post_meta( $post->ID, 'display_author_toggle', true );
		return ! empty( $displayAuthor ) ? $displayAuthor : false;
		}
  	] );
	register_graphql_field( 'Post', 'customAuthorName', [
		'type' => 'String',
		'description' => __( 'The custom author name of the post', 'wp-graphql' ),
		'resolve' => function( $post ) {
		$customAuthorName = get_post_meta( $post->ID, 'custom_author_name', true );
		return ! empty( $customAuthorName ) ? $customAuthorName : NULL;
		}
	] );
	register_graphql_field( 'Post', 'displayFeaturedImage', [
		'type' => 'Boolean',
		'description' => __( 'Boolean on whether or not to display the featured image', 'wp-graphql' ),
		'resolve' => function( $post ) {
		$displayFeaturedImage = get_post_meta( $post->ID, 'display_featured_image', true );
		return ! empty( $displayFeaturedImage ) ? $displayFeaturedImage : false;
		}
	  ] );
}

function add_photo_credit_to_image( $form_fields, $post ){
	// https://codex.wordpress.org/Function_Reference/wp_get_attachment_metadata
	$image_credit = get_post_meta( $post->ID, 'image_credit', true );
    
	$form_fields['image_credit'] = array(
		'value' => $image_credit ? $image_credit : '',
		'label' => __( 'Photo credit' )
	); 
	return $form_fields;
}
add_filter( 'attachment_fields_to_edit', 'add_photo_credit_to_image', 10, 2 );

