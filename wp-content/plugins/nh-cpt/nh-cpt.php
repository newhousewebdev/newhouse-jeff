<?php
/**
 * Plugin Name: NH CPT
 * Description: Custom post types for Newhouse
 * Version: 1.0
 * Author: Jeff Passetti
 *
 * @package WordPress
 */

// Initialize the Custom Post Types.
add_action( 'init', 'nh_custom_post_type' );
// hook into the init action and call create_book_taxonomies when it fires.
add_action( 'init', 'create_newhouse_taxonomies', 0 );

// This should be moved to an init function
// remove p element from excerpts.
remove_filter( 'the_excerpt', 'wpautop' );

/**
 * Create Custom Post Types
 */
function nh_custom_post_type() {
	// Academic Programs.
	register_post_type(
		'academic-programs',
		array(
			'labels'              => array(
				'name'          => __( 'Academic Programs' ),
				'singular_name' => __( 'Academic Program' ),
			),
			'public'              => true,
			'has_archive'         => true,
			'hierarchical'        => true,
			'rewrite'             => array(
				'slug'       => '/academics',
				'with_front' => false,
			),
			'supports'            => array( 'title', 'editor', 'excerpt', 'page-attributes', 'thumbnail', 'custom-fields', 'revisions' ),
			'show_in_rest'        => true,
			'show_in_graphql'     => true,
			'graphql_single_name' => 'AcademicProgram',
			'graphql_plural_name' => 'AcademicPrograms',
		)
	);

	// Dynamic Leads.
	register_post_type(
		'dynamic-leads',
		array(
			'labels'              => array(
				'name'          => __( 'Dynamic Leads' ),
				'singular_name' => __( 'Dynamic Lead' ),
			),
			'public'              => true,
			'has_archive'         => true,
			'hierarchical'        => true,
			'rewrite'             => array(
				'slug'       => '/dynamic-lead',
				'with_front' => false,
			),
			'supports'            => array( 'title', 'thumbnail' ),
			'show_in_rest'        => true,
			'show_in_graphql'     => true,
			'graphql_single_name' => 'DynamicLead',
			'graphql_plural_name' => 'DynamicLeads',
		)
	);

	// Centers.
	register_post_type(
		'centers',
		array(
			'labels'              => array(
				'name'          => __( 'Centers' ),
				'singular_name' => __( 'Center' ),
			),
			'public'              => true,
			'has_archive'         => true,
			'hierarchical'        => true,
			'rewrite'             => array(
				'slug'       => '/centers',
				'with_front' => false,
			),
			'supports'            => array( 'title', 'editor', 'excerpt', 'page-attributes', 'thumbnail', 'custom-fields' ),
			'show_in_rest'        => true,
			'show_in_graphql'     => true,
			'graphql_single_name' => 'Center',
			'graphql_plural_name' => 'Centers',
		)
	);
}

/**
 * Create a custom taxonomy for your posts
 */
function create_newhouse_taxonomies() {
	// Add new taxonomy, make it hierarchical like categories
	// first do the translations part for GUI.
	/* Degrees */
	$degree_labels = array(
		'name'              => _x( 'Degrees', 'taxonomy general name' ),
		'singular_name'     => _x( 'Degree', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Degrees' ),
		'all_items'         => __( 'All Degrees' ),
		'parent_item'       => __( 'Parent Degree' ),
		'parent_item_colon' => __( 'Parent Degree:' ),
		'edit_item'         => __( 'Edit Degree' ),
		'update_item'       => __( 'Update Degree' ),
		'add_new_item'      => __( 'Add New Degree' ),
		'new_item_name'     => __( 'New Degree' ),
		'menu_name'         => __( 'Degrees' ),
	);
	// Slug, availability, options.
	register_taxonomy(
		'degrees',
		array( 'academic-programs' ),
		array(
			'hierarchical'        => true,
			'labels'              => $degree_labels,
			'show_ui'             => true,
			'show_in_rest'        => true,
			'show_admin_column'   => true,
			'query_var'           => true,
			'rewrite'             => array( 'slug' => 'degree' ),
			'show_in_graphql'     => true,
			'graphql_single_name' => 'Degree',
			'graphql_plural_name' => 'Degrees',
		)
	);

	/* Departments */
	$dept_labels = array(
		'name'              => _x( 'Departments', 'taxonomy general name' ),
		'singular_name'     => _x( 'Department', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Departments' ),
		'all_items'         => __( 'All Departments' ),
		'parent_item'       => __( 'Parent Department' ),
		'parent_item_colon' => __( 'Parent Department:' ),
		'edit_item'         => __( 'Edit Department' ),
		'update_item'       => __( 'Update Department' ),
		'add_new_item'      => __( 'Add New Department' ),
		'new_item_name'     => __( 'New Department' ),
		'menu_name'         => __( 'Departments' ),
	);

	// slug, availability, options.
	register_taxonomy(
		'departments',
		array( 'post', 'academic-programs' ),
		array(
			'hierarchical'        => true,
			'labels'              => $dept_labels,
			'show_ui'             => true,
			'show_in_rest'        => true,
			'show_admin_column'   => true,
			'query_var'           => true,
			'rewrite'             => array( 'slug' => 'department' ),
			'show_in_graphql'     => true,
			'graphql_single_name' => 'Department',
			'graphql_plural_name' => 'Departments',
		)
	);

	/* Programs */
	$program_labels = array(
		'name'              => _x( 'Programs', 'taxonomy general name' ),
		'singular_name'     => _x( 'Program', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Programs' ),
		'all_items'         => __( 'All Programs' ),
		'parent_item'       => __( 'Parent Program' ),
		'parent_item_colon' => __( 'Parent Program:' ),
		'edit_item'         => __( 'Edit Program' ),
		'update_item'       => __( 'Update Program' ),
		'add_new_item'      => __( 'Add New Program' ),
		'new_item_name'     => __( 'New Program' ),
		'menu_name'         => __( 'Programs' ),
	);

	// slug, availability, options.
	register_taxonomy(
		'programs',
		array( 'post', 'academic-programs' ),
		array(
			'hierarchical'        => true,
			'labels'              => $program_labels,
			'show_ui'             => true,
			'show_in_rest'        => true,
			'show_admin_column'   => true,
			'query_var'           => true,
			'show_in_graphql'     => true,
			'graphql_single_name' => 'programs',
			'graphql_plural_name' => 'programs',
		)
	);

	$default_academic_programs = array(
		'Advertising'                                 => array(
			'description' => '',
			'slug'        => 'advertising',
		),
		'Bandier Program'                             => array(
			'description' => '',
			'slug'        => 'bandier-program',
		),
		'Broadcast and Digital Journalism'            => array(
			'description' => '',
			'slug'        => 'broadcast-and-digital-journalism',
		),
		'Communications'                              => array(
			'description' => '',
			'slug'        => 'communications',
		),
		'Arts Journalism and Communications'          => array(
			'description' => '',
			'slug'        => 'arts-journalism-and-communications',
		),
		'Magazine, News and Digital Journalism'       => array(
			'description' => '',
			'slug'        => 'magazine-news-digital-journalism',
		),
		'Public Relations'                            => array(
			'description' => '',
			'slug'        => 'public-relations',
		),
		'Public Diplomacy and Global Communications'  => array(
			'description' => '',
			'slug'        => 'public-diplomacy-global-communications',
		),
		'Television, Radio and Film'                  => array(
			'description' => '',
			'slug'        => 'television-radio-film',
		),
		'Visual Communications'                       => array(
			'description' => '',
			'slug'        => 'visual-communications',
		),
		'Graphic Design'                              => array(
			'description' => '',
			'slug'        => 'graphic-design',
		),
		'Photography'                                 => array(
			'description' => '',
			'slug'        => 'photography',
		),
		'Multimedia, Photography and Design'          => array(
			'description' => '',
			'slug'        => 'multimedia-photography-design',
		),
		'Audio Arts'                                  => array(
			'description' => '',
			'slug'        => 'audio-arts',
		),
		'Media Studies'                               => array(
			'description' => '',
			'slug'        => 'media-studies',
		),
		'Media Communications'                        => array(
			'description' => '',
			'slug'        => 'media-communications',
		),
		'New Media Management'                        => array(
			'description' => '',
			'slug'        => 'new-media-management',
		),
		'Media and Education'                         => array(
			'description' => '',
			'slug'        => 'media-education',
		),
		'Communications@Syracuse'                     => array(
			'description' => '',
			'slug'        => 'communications-syracuse',
		),
		'Communications Management'                   => array(
			'description' => '',
			'slug'        => 'communications-management',
		),
		'Fashion and Beauty Communications Milestone' => array(
			'description' => '',
			'slug'        => 'fashion-beauty-communications-milestone',
		),
	);

	pre_populate_taxonomies( 'programs', $default_academic_programs );

	$news_category_labels = array(
		'name'              => _x( 'News Categories', 'taxonomy general name' ),
		'singular_name'     => _x( 'News Category', 'taxonomy singular name' ),
		'search_items'      => __( 'Search News Categories' ),
		'all_items'         => __( 'All News Categories' ),
		'parent_item'       => __( 'Parent News Category' ),
		'parent_item_colon' => __( 'Parent News Category:' ),
		'edit_item'         => __( 'Edit News Category' ),
		'update_item'       => __( 'Update News Category' ),
		'add_new_item'      => __( 'Add New News Category' ),
		'new_item_name'     => __( 'New News Category' ),
		'menu_name'         => __( 'News Categories' ),
	);
	/* News Categories */
	register_taxonomy(
		'news-categories',
		array( 'post' ),
		array(
			'hierarchical'        => true,
			'labels'              => $news_category_labels,
			'show_ui'             => true,
			'show_in_rest'        => true,
			'show_admin_column'   => true,
			'rewrite'             => array(
				'slug'       => 'news-category',
				'with_front' => false,
			),
			'query_var'           => true,
			'show_in_graphql'     => true,
			'graphql_single_name' => 'newsCategories',
			'graphql_plural_name' => 'newsCategories',
		)
	);

	$default_news_categories = array(
		'Alumni Stories'           => array(
			'description' => '',
			'slug'        => 'alumni-stories',
		),
		'Announcements'            => array(
			'description' => '',
			'slug'        => 'announcements',
		),
		'Newhouse in the News'     => array(
			'description' => '',
			'slug'        => 'newhouse-in-the-news',
		),
		'Research and Scholarship' => array(
			'description' => '',
			'slug'        => 'research-and-scholarship',
		),
		'Giving Stories'           => array(
			'description' => '',
			'slug'        => 'giving-stories ',
		),
		'Master’s Alumni Profiles' => array(
			'description' => '',
			'slug'        => 'master-alumni-profiles',
		),
		'Featured News'            => array(
			'description' => '',
			'slug'        => 'featured-news',
		),
		'Student News'             => array(
			'description' => '',
			'slug'        => 'student-news',
		),
	);

	pre_populate_taxonomies( 'news-categories', $default_news_categories );
}

/**
 * Insert default Terms
 *
 * @param String $taxonomy to assign term to.
 * @param Array  $terms array to insert.
 */
function pre_populate_taxonomies( $taxonomy, $terms ) {
	foreach ( $terms as $term => $args ) {
		$term_exists = term_exists( $args['slug'], $taxonomy, null );
		if ( ! $term_exists ) {
			wp_insert_term( $term, $taxonomy, $args );
		}
	}
}
